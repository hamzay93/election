﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class saisie_nombrevotant : Form
    {
        Connection conn = new Connection();
        public saisie_nombrevotant()
        {
            InitializeComponent();
        }
        int InscrisBureau=0;
        bool ctrl = false;
        private void saisie_nombrevotant_Load(object sender, EventArgs e)
        {
            this.FillRégion();
            this.FillElection();
        }

        private void FillElection()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT ElectionID, ObjetElection +' - '+ObjetTourElection as NomElection from View_Elect where Etat= 1";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBoxElect.ValueMember = "ElectionID";
            comboBoxElect.DisplayMember = "NomElection";
            comboBoxElect.DataSource = objDs.Tables[0];
        }

        private void Fillheure()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT horaire_id, heure FROM horaire where horaire_id not in(select horaire_id from Particip where BUREAU_ID = " + Int32.Parse(comboBoxBV.SelectedValue.ToString()) + ")";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBox1.ValueMember = "horaire_id";
            comboBox1.DisplayMember = "heure";
            comboBox1.DataSource = objDs.Tables[0];
        }
        private void comboBoxCV_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.FillBureau(Int32.Parse(comboBoxCV.SelectedValue.ToString()));
        }
        //private void FillBureau(int CentreID)
        //{
        //    string sCon = conn.connectdb();
        //    SqlConnection con = new SqlConnection(sCon);
        //    SqlCommand cmd = new SqlCommand();
        //    cmd.Connection = con;
        //    cmd.CommandType = CommandType.Text;
        //    cmd.CommandText = "SELECT BureauID, ( CONVERT(Nvarchar(50), CodeBureau)) + ' - '+NomBureau as NomBureau FROM View_LV WHERE CentreID =@CentreID  ORDER BY CodeCentre,OrdreCentre";
        //    cmd.Parameters.AddWithValue("@CentreID", CentreID);
        //    DataSet objDs = new DataSet();
        //    SqlDataAdapter dAdapter = new SqlDataAdapter();
        //    dAdapter.SelectCommand = cmd;
        //    con.Open();
        //    dAdapter.Fill(objDs);
        //    con.Close();
        //    if (objDs.Tables[0].Rows.Count > 0)
        //    {
        //        comboBoxBV.ValueMember = "BureauID";
        //        comboBoxBV.DisplayMember = "NomBureau";
        //        comboBoxBV.DataSource = objDs.Tables[0];
        //    }
        //}
        
        /*
        private void button1_Click(object sender, EventArgs e)
        {
            if ((comboBoxBV.Text.Length == 0) && (comboBoxBureau_dis.Text.Length == 0))
            {
                MessageBox.Show("Veuillez choisir le bureau de vote ");
                comboBoxBV.Focus();
            }
            else
            {
                if (comboBox1.Text.Length == 0)
                {
                    MessageBox.Show("Veuillez choisir l'heure");
                    comboBox1.Focus();
                }
                else
                {
                    if ((textBoxnbr.Text.Length == 0) || (textBox1.Text.Length == 0))
                    {
                        MessageBox.Show("Veuillez saisir le nombre de personne voté");
                        textBoxnbr.Focus();
                    }
                    else
                    {
                        if (Int32.Parse(textBox1.Text.Trim()) > Int32.Parse(textBoxnbr.Text.Trim()))
                        {
                             MessageBox.Show("Les nombres des votants avec (OR/OM) doivent être inférieur aux nombres des votants totle");
                                 textBoxnbr.Focus();
                        }
                        else{


                            if (comboBoxRégion.SelectedValue.ToString() == "1")
                            {

                                int bureauid = 0;
                                int heureid = 0;
                                SqlDataReader rdbbh = null;
                                string querybbh = "select BureauID,horaire_id from View_participant_bureau where BureauID=" + Int32.Parse(comboBoxBV.SelectedValue.ToString()) + "  and horaire_id=" + Int32.Parse(comboBox1.SelectedValue.ToString());
                                string sContbbh = conn.connectdb();
                                SqlConnection dbConntbbh;
                                dbConntbbh = new SqlConnection(sContbbh);
                                dbConntbbh.Open();
                                SqlCommand cmdbbh = new SqlCommand(querybbh, dbConntbbh);
                                rdbbh = cmdbbh.ExecuteReader();
                                if (rdbbh.Read())
                                {

                                    bureauid = Int32.Parse((rdbbh["BureauID"].ToString()));
                                    heureid = Int32.Parse((rdbbh["horaire_id"].ToString()));

                                }
                                if ((bureauid == Int32.Parse(comboBoxBV.SelectedValue.ToString())) && (heureid == Int32.Parse(comboBox1.SelectedValue.ToString())))
                                {

                                    MessageBox.Show("cette bureau de vote est deja saisie à cette heure");
                                    comboBox1.Focus();
                                }
                                else
                                {


                                    int totalebureauvotes = 0;
                                    int nombrevotants = 0;
                                    int votantords = 0;
                                    SqlDataReader rdbs = null;
                                    string querybs = "select * from View_verification_nombrevotant_par_bureau where BureauID=" + Int32.Parse(comboBoxBV.SelectedValue.ToString());
                                    string sContbs = conn.connectdb();
                                    SqlConnection dbConntbs;
                                    dbConntbs = new SqlConnection(sContbs);
                                    dbConntbs.Open();
                                    SqlCommand cmdbs = new SqlCommand(querybs, dbConntbs);
                                    rdbs = cmdbs.ExecuteReader();
                                    if (rdbs.Read())
                                    {

                                        totalebureauvotes = Int32.Parse((rdbs["totalebureauvote"].ToString()));
                                        nombrevotants = Int32.Parse((rdbs["nombrevotant"].ToString()));
                                        votantords = Int32.Parse((rdbs["votantord"].ToString()));

                                    }


                                    int seuilvotant = totalebureauvotes + votantords + Int32.Parse(textBox1.Text.Trim());
                                    int nombrevotant_saisi = nombrevotants + Int32.Parse(textBoxnbr.Text.Trim());

                                    if (seuilvotant >= nombrevotant_saisi)
                                    {




                                        int totaleArrondis = 0;

                                        SqlDataReader rd = null;
                                        string query = "select * from View_totaleArrondis where ArrondisID=" + Int32.Parse(comboBoxArrondis.SelectedValue.ToString());
                                        string sCona = conn.connectdb();
                                        SqlConnection dbConna;
                                        dbConna = new SqlConnection(sCona);
                                        dbConna.Open();
                                        SqlCommand cmd = new SqlCommand(query, dbConna);
                                        rd = cmd.ExecuteReader();
                                        if (rd.Read())
                                        {

                                            // string btnName = nom;
                                            string testa = (rd["ArrondisTotale"].ToString());

                                            totaleArrondis = Int32.Parse(testa);


                                        }




                                        int totalecommune = 0;

                                        SqlDataReader rdc = null;
                                        string queryc = "select * from View_totalecommune where CommuneID=" + Int32.Parse(comboBoxCommune.SelectedValue.ToString());
                                        string sConc = conn.connectdb();
                                        SqlConnection dbConnc;
                                        dbConnc = new SqlConnection(sConc);
                                        dbConnc.Open();
                                        SqlCommand cmdc = new SqlCommand(queryc, dbConnc);
                                        rdc = cmdc.ExecuteReader();
                                        if (rdc.Read())
                                        {

                                            // string btnName = nom;
                                            string testc = (rdc["CommuneTotale"].ToString());

                                            totalecommune = Int32.Parse(testc);


                                        }




                                        int totalenationale = 0;

                                        SqlDataReader rdt = null;
                                        string queryt = "select * from View_nationaletotale where RégionID=" + Int32.Parse(comboBoxRégion.SelectedValue.ToString());
                                        string sCont = conn.connectdb();
                                        SqlConnection dbConnt;
                                        dbConnt = new SqlConnection(sCont);
                                        dbConnt.Open();
                                        SqlCommand cmdt = new SqlCommand(queryt, dbConnt);
                                        rdt = cmdt.ExecuteReader();
                                        if (rdt.Read())
                                        {

                                            // string btnName = nom;
                                            string testt = (rdt["nationaleregionTotale"].ToString());

                                            totalenationale = Int32.Parse(testt);


                                        }




                                        int totaleArrondis_ord = 0;

                                        SqlDataReader rdao = null;
                                        string queryao = "select * from View_totalearrondis_ordonnance where ArrondisID=" + Int32.Parse(comboBoxArrondis.SelectedValue.ToString());
                                        string sConao = conn.connectdb();
                                        SqlConnection dbConnao;
                                        dbConnao = new SqlConnection(sConao);
                                        dbConnao.Open();
                                        SqlCommand cmdao = new SqlCommand(queryao, dbConnao);
                                        rdao = cmdao.ExecuteReader();
                                        if (rdao.Read())
                                        {
                                            string testao = (rdao["ArrondisTotale_ord"].ToString());

                                            totaleArrondis_ord = Int32.Parse(testao);


                                        }



                                        int totalecommune_ord = 0;

                                        SqlDataReader rdco = null;
                                        string queryco = "select * from View_totalecommune_ordonnance where CommuneID=" + Int32.Parse(comboBoxCommune.SelectedValue.ToString());
                                        string sConco = conn.connectdb();
                                        SqlConnection dbConnco;
                                        dbConnco = new SqlConnection(sConco);
                                        dbConnco.Open();
                                        SqlCommand cmdco = new SqlCommand(queryco, dbConnco);
                                        rdco = cmdco.ExecuteReader();
                                        if (rdco.Read())
                                        {
                                            string testco = (rdco["CommuneTotale_ord"].ToString());

                                            totalecommune_ord = Int32.Parse(testco);


                                        }


                                        int totalenationale_ord = 0;

                                        SqlDataReader rdto = null;
                                        string queryto = "select * from View_centrevilletotale_ordonnance where RégionID=" + Int32.Parse(comboBoxRégion.SelectedValue.ToString());
                                        string sConto = conn.connectdb();
                                        SqlConnection dbConnto;
                                        dbConnto = new SqlConnection(sConto);
                                        dbConnto.Open();
                                        SqlCommand cmdto = new SqlCommand(queryto, dbConnto);
                                        rdto = cmdto.ExecuteReader();
                                        if (rdto.Read())
                                        {
                                            string testto = (rdto["centrevilleTotale_ord"].ToString());

                                            totalenationale_ord = Int32.Parse(testto);
                                        }

                                        int totalebureau = 0;

                                        SqlDataReader rdb = null;
                                        string queryb = "select * from View_totalebureau where BureauID=" + Int32.Parse(comboBoxBV.SelectedValue.ToString());
                                        string sContb = conn.connectdb();
                                        SqlConnection dbConntb;
                                        dbConntb = new SqlConnection(sContb);
                                        dbConntb.Open();
                                        SqlCommand cmdb = new SqlCommand(queryb, dbConntb);
                                        rdb = cmdb.ExecuteReader();
                                        if (rdb.Read())
                                        {
                                            string testb = (rdb["BureauTotale"].ToString());

                                            totalebureau = Int32.Parse(testb);
                                        }


                                        int totalecentre = 0;

                                        SqlDataReader rdcv = null;
                                        string querycv = "select * from View_totaleCentre where CentreID=" + Int32.Parse(comboBoxCV.SelectedValue.ToString());
                                        string sContcv = conn.connectdb();
                                        SqlConnection dbConntcv;
                                        dbConntcv = new SqlConnection(sContcv);
                                        dbConntcv.Open();
                                        SqlCommand cmdcv = new SqlCommand(querycv, dbConntcv);
                                        rdcv = cmdcv.ExecuteReader();
                                        if (rdcv.Read())
                                        {
                                            string testcv = (rdcv["CentreTotale"].ToString());

                                            totalecentre = Int32.Parse(testcv);
                                        }


                                        string sCon = conn.connectdb();
                                        string insertCmd = "INSERT INTO participant (totalecentrevote,totalebureauvote,totalenationale_ord,nombre_votant_ord,totaleArrondis_ord,totalecommune_ord,totalenationale,totalecommune,totaleArrondis,BUREAU_ID,nombre_votant,HORAIRE_ID,datecreation,datemodification,usercreation,usermodification) VALUES (@totalecentrevote,@totalebureauvote,@totalenationale_ord,@nombre_votant_ord,@totaleArrondis_ord,@totalecommune_ord,@totalenationale,@totalecommune,@totaleArrondis,@BUREAU_ID,@nombre_votant,@HORAIRE_ID,@datecreation,@datemodification,@usercreation,@usermodification)";
                                        SqlConnection dbConn;
                                        dbConn = new SqlConnection(sCon);
                                        dbConn.Open();
                                        Login lg = new Login();
                                        //string str;
                                        string dat = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                        DateTime date4 = DateTime.Parse(dat);

                                        GetValue value = new GetValue();
                                        //getAdress add = new getAdress();
                                        SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);

                                        myCommand.Parameters.AddWithValue("@nombre_votant", textBoxnbr.Text);
                                        myCommand.Parameters.AddWithValue("@nombre_votant_ord", textBox1.Text);
                                        myCommand.Parameters.AddWithValue("@HORAIRE_ID", Int32.Parse(comboBox1.SelectedValue.ToString()));


                                        myCommand.Parameters.AddWithValue("@usermodification", value.getAgentModif());//recupere agent modif
                                        myCommand.Parameters.AddWithValue("@usercreation", value.getAgentCréation());//recupere agent de creation


                                        myCommand.Parameters.AddWithValue("@BUREAU_ID", Int32.Parse(comboBoxBV.SelectedValue.ToString()));
                                        myCommand.Parameters.AddWithValue("@DateCreation", date4);
                                        myCommand.Parameters.AddWithValue("@DateModification", date4);

                                        myCommand.Parameters.AddWithValue("@totaleArrondis", totaleArrondis);
                                        myCommand.Parameters.AddWithValue("@totalecommune", totalecommune);
                                        myCommand.Parameters.AddWithValue("@totalenationale", totalenationale);

                                        myCommand.Parameters.AddWithValue("@totaleArrondis_ord", totaleArrondis_ord);
                                        myCommand.Parameters.AddWithValue("@totalecommune_ord", totalecommune_ord);
                                        myCommand.Parameters.AddWithValue("@totalenationale_ord", totalenationale_ord);

                                        myCommand.Parameters.AddWithValue("@totalecentrevote", totalecentre);
                                        myCommand.Parameters.AddWithValue("@totalebureauvote", totalebureau);


                                        myCommand.ExecuteNonQuery();
                                        MessageBox.Show("Enregistrement effectué avec succès");
                                        RecursiveClearTextBoxes(this.Controls);

                                    }
                                    else
                                    {
                                        MessageBox.Show("Veuillez respecter le seuil de nombre de votant sur cette Bureau de vote, le nombre de votant sont déjà " + nombrevotants + " et le totale inscrit sont +" + seuilvotant);
                                        textBoxnbr.Focus();
                                    }
                                }

                            }
                            else
                            {


                                int bureauid = 0;
                                int heureid = 0;
                                SqlDataReader rdbbh = null;
                                string querybbh = "select BureauID,horaire_id from View_participant_bureau where BureauID=" + Int32.Parse(comboBoxBureau_dis.SelectedValue.ToString()) + "  and horaire_id=" + Int32.Parse(comboBox1.SelectedValue.ToString());
                                string sContbbh = conn.connectdb();
                                SqlConnection dbConntbbh;
                                dbConntbbh = new SqlConnection(sContbbh);
                                dbConntbbh.Open();
                                SqlCommand cmdbbh = new SqlCommand(querybbh, dbConntbbh);
                                rdbbh = cmdbbh.ExecuteReader();
                                if (rdbbh.Read())
                                {

                                    bureauid = Int32.Parse((rdbbh["BureauID"].ToString()));
                                    heureid = Int32.Parse((rdbbh["horaire_id"].ToString()));

                                }
                                if ((bureauid == Int32.Parse(comboBoxBureau_dis.SelectedValue.ToString())) && (heureid == Int32.Parse(comboBox1.SelectedValue.ToString())))
                                {

                                    MessageBox.Show("cette bureau de vote est deja saisie à cette heure");
                                    comboBox1.Focus();
                                }
                                else
                                {


                                    int totalebureauvotes = 0;
                                    int nombrevotants = 0;
                                    int votantords = 0;
                                    SqlDataReader rdbs = null;
                                    string querybs = "select * from View_verification_nombrevotant_par_bureau where BureauID=" + Int32.Parse(comboBoxBureau_dis.SelectedValue.ToString());
                                    string sContbs = conn.connectdb();
                                    SqlConnection dbConntbs;
                                    dbConntbs = new SqlConnection(sContbs);
                                    dbConntbs.Open();
                                    SqlCommand cmdbs = new SqlCommand(querybs, dbConntbs);
                                    rdbs = cmdbs.ExecuteReader();
                                    if (rdbs.Read())
                                    {

                                        totalebureauvotes = Int32.Parse((rdbs["totalebureauvote"].ToString()));
                                        nombrevotants = Int32.Parse((rdbs["nombrevotant"].ToString()));
                                        votantords = Int32.Parse((rdbs["votantord"].ToString()));

                                    }


                                    int seuilvotant = totalebureauvotes + votantords + Int32.Parse(textBox1.Text.Trim());
                                    int nombrevotant_saisi = nombrevotants + Int32.Parse(textBoxnbr.Text.Trim());

                                    if (seuilvotant >= nombrevotant_saisi)
                                    {




                                        int totaleregion = 0;

                                        SqlDataReader rd = null;
                                        string query = "select * from View_RegionTotale where RégionID=" + Int32.Parse(comboBoxRégion.SelectedValue.ToString());
                                        string sCona = conn.connectdb();
                                        SqlConnection dbConna;
                                        dbConna = new SqlConnection(sCona);
                                        dbConna.Open();
                                        SqlCommand cmd = new SqlCommand(query, dbConna);
                                        rd = cmd.ExecuteReader();
                                        if (rd.Read())
                                        {

                                            // string btnName = nom;
                                            string testri = (rd["RegionTotale"].ToString());

                                            totaleregion = Int32.Parse(testri);


                                        }

                                        int totaleregion_ord = 0;

                                        SqlDataReader rdor = null;
                                        string queryor = "select * from View_regioninterieur_ordonnance where RégionID=" + Int32.Parse(comboBoxRégion.SelectedValue.ToString());
                                        string sConor = conn.connectdb();
                                        SqlConnection dbConnor;
                                        dbConnor = new SqlConnection(sConor);
                                        dbConnor.Open();
                                        SqlCommand cmdor = new SqlCommand(queryor, dbConnor);
                                        rdor = cmdor.ExecuteReader();
                                        if (rdor.Read())
                                        {

                                            // string btnName = nom;
                                            string testro = (rdor["regioninterieur_ord"].ToString());

                                            totaleregion_ord = Int32.Parse(testro);


                                        }



                                        int totalebureau = 0;

                                        SqlDataReader rdb = null;
                                        string queryb = "select * from View_totalebureau where BureauID=" + Int32.Parse(comboBoxBureau_dis.SelectedValue.ToString());
                                        string sContb = conn.connectdb();
                                        SqlConnection dbConntb;
                                        dbConntb = new SqlConnection(sContb);
                                        dbConntb.Open();
                                        SqlCommand cmdb = new SqlCommand(queryb, dbConntb);
                                        rdb = cmdb.ExecuteReader();
                                        if (rdb.Read())
                                        {
                                            string testbr = (rdb["BureauTotale"].ToString());

                                            totalebureau = Int32.Parse(testbr);
                                        }


                                        int totalecentre = 0;

                                        SqlDataReader rdcv = null;
                                        string querycv = "select * from View_totaleCentre where CentreID=" + Int32.Parse(comboBoxCentre_dis.SelectedValue.ToString());
                                        string sContcv = conn.connectdb();
                                        SqlConnection dbConntcv;
                                        dbConntcv = new SqlConnection(sContcv);
                                        dbConntcv.Open();
                                        SqlCommand cmdcv = new SqlCommand(querycv, dbConntcv);
                                        rdcv = cmdcv.ExecuteReader();
                                        if (rdcv.Read())
                                        {
                                            string testcr = (rdcv["CentreTotale"].ToString());

                                            totalecentre = Int32.Parse(testcr);
                                        }


                                        int totaleLocalite = 0;

                                        SqlDataReader rdl = null;
                                        string queryl = "select * from View_LocaliteTotale where LocalitéID=" + Int32.Parse(comboBoxLoc.SelectedValue.ToString());
                                        string sContl = conn.connectdb();
                                        SqlConnection dbConntl;
                                        dbConntl = new SqlConnection(sContl);
                                        dbConntl.Open();
                                        SqlCommand cmdl = new SqlCommand(queryl, dbConntl);
                                        rdl = cmdl.ExecuteReader();
                                        if (rdl.Read())
                                        {
                                            string testl = (rdl["LocaliteTotale"].ToString());

                                            totaleLocalite = Int32.Parse(testl);
                                        }

                                        string sCon = conn.connectdb();
                                        string insertCmd = "INSERT INTO participant (totalelocalite,nombre_votant_ord,totalecentrevote,totalebureauvote,totalenationale_ord,totalenationale,totaleRegion,BUREAU_ID,nombre_votant,HORAIRE_ID,datecreation,datemodification,usercreation,usermodification) VALUES (@totalelocalite,@nombre_votant_ord,@totalecentrevote,@totalebureauvote,@totalenationale_ord,@totalenationale,@totaleRegion,@BUREAU_ID,@nombre_votant,@HORAIRE_ID,@datecreation,@datemodification,@usercreation,@usermodification)";
                                        SqlConnection dbConn;
                                        dbConn = new SqlConnection(sCon);
                                        dbConn.Open();
                                        Login lg = new Login();
                                        //string str;
                                        string dat = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                        DateTime date4 = DateTime.Parse(dat);

                                        GetValue value = new GetValue();
                                        //getAdress add = new getAdress();
                                        SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);

                                        myCommand.Parameters.AddWithValue("@nombre_votant", textBoxnbr.Text);
                                        myCommand.Parameters.AddWithValue("@nombre_votant_ord", textBox1.Text);
                                        myCommand.Parameters.AddWithValue("@HORAIRE_ID", Int32.Parse(comboBox1.SelectedValue.ToString()));


                                        myCommand.Parameters.AddWithValue("@usermodification", value.getAgentModif());//recupere agent modif
                                        myCommand.Parameters.AddWithValue("@usercreation", value.getAgentCréation());//recupere agent de creation

                                        myCommand.Parameters.AddWithValue("@totaleRegion", totaleregion);
                                        myCommand.Parameters.AddWithValue("@totalenationale", totaleregion);
                                        myCommand.Parameters.AddWithValue("@totalenationale_ord", totaleregion_ord);

                                        myCommand.Parameters.AddWithValue("@BUREAU_ID", Int32.Parse(comboBoxBureau_dis.SelectedValue.ToString()));
                                        myCommand.Parameters.AddWithValue("@DateCreation", date4);
                                        myCommand.Parameters.AddWithValue("@DateModification", date4);

                                        myCommand.Parameters.AddWithValue("@totalecentrevote", totalecentre);
                                        myCommand.Parameters.AddWithValue("@totalebureauvote", totalebureau);
                                        myCommand.Parameters.AddWithValue("@totalelocalite", totaleLocalite);



                                        myCommand.ExecuteNonQuery();
                                        MessageBox.Show("Enregistrement effectué avec succès");
                                        RecursiveClearTextBoxes(this.Controls);

                                    }
                                    else
                                    {
                                        MessageBox.Show("Veuillez respecter le seuil de nombre de votant sur cette Bureau de vote, le nombre de votant sont déjà " + nombrevotants + " et le totale inscrit sont +" + seuilvotant);
                                        textBoxnbr.Focus();
                                    }
                                }
                            }
                         } 
                    }
                }
            }
        }
        private void RecursiveClearTextBoxes(Control.ControlCollection cc)
        {
            foreach (Control ctrl in cc)
            {
                TextBox tb = ctrl as TextBox;
                if (tb != null)
                    tb.Clear();
                else
                    RecursiveClearTextBoxes(ctrl.Controls);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if ((comboBoxBV.Text.Length == 0) && (comboBoxBureau_dis.Text.Length == 0))
            {
                MessageBox.Show("Veuillez choisir le bureau de vote ");
                comboBoxBV.Focus();
            }
            else
            {
                if (comboBox1.Text.Length == 0)
                {
                    MessageBox.Show("Veuillez choisir l'heure");
                    comboBox1.Focus();
                }
                else
                {
                    if ((textBoxnbr.Text.Length == 0) || (textBox1.Text.Length == 0))
                    {
                        MessageBox.Show("Veuillez saisir le nombre de personne voté");
                        textBoxnbr.Focus();
                    }
                    else
                    {
                        if (Int32.Parse(textBox1.Text.Trim()) > Int32.Parse(textBoxnbr.Text.Trim()))
                        {
                            MessageBox.Show("Les nombres des votants avec (OR/OM) doivent être inférieur aux nombres des votants totle");
                            textBoxnbr.Focus();
                        }
                        else
                        {


                            if (comboBoxRégion.SelectedValue.ToString() == "1")
                            {

                                int bureauid = 0;
                                int heureid = 0;
                                SqlDataReader rdbbh = null;
                                string querybbh = "select BureauID,horaire_id from View_participant_bureau where BureauID=" + Int32.Parse(comboBoxBV.SelectedValue.ToString()) + "  and horaire_id=" + Int32.Parse(comboBox1.SelectedValue.ToString());
                                string sContbbh = conn.connectdb();
                                SqlConnection dbConntbbh;
                                dbConntbbh = new SqlConnection(sContbbh);
                                dbConntbbh.Open();
                                SqlCommand cmdbbh = new SqlCommand(querybbh, dbConntbbh);
                                rdbbh = cmdbbh.ExecuteReader();
                                if (rdbbh.Read())
                                {

                                    bureauid = Int32.Parse((rdbbh["BureauID"].ToString()));
                                    heureid = Int32.Parse((rdbbh["horaire_id"].ToString()));

                                }
                                if ((bureauid == Int32.Parse(comboBoxBV.SelectedValue.ToString())) && (heureid == Int32.Parse(comboBox1.SelectedValue.ToString())))
                                {

                                    MessageBox.Show("cette bureau de vote est deja saisie à cette heure");
                                    comboBox1.Focus();
                                }
                                else
                                {


                                    int totalebureauvotes = 0;
                                    int nombrevotants = 0;
                                    int votantords = 0;
                                    SqlDataReader rdbs = null;
                                    string querybs = "select * from View_verification_nombrevotant_par_bureau where BureauID=" + Int32.Parse(comboBoxBV.SelectedValue.ToString());
                                    string sContbs = conn.connectdb();
                                    SqlConnection dbConntbs;
                                    dbConntbs = new SqlConnection(sContbs);
                                    dbConntbs.Open();
                                    SqlCommand cmdbs = new SqlCommand(querybs, dbConntbs);
                                    rdbs = cmdbs.ExecuteReader();
                                    if (rdbs.Read())
                                    {

                                        totalebureauvotes = Int32.Parse((rdbs["totalebureauvote"].ToString()));
                                        nombrevotants = Int32.Parse((rdbs["nombrevotant"].ToString()));
                                        votantords = Int32.Parse((rdbs["votantord"].ToString()));

                                    }


                                    int seuilvotant = totalebureauvotes + votantords + Int32.Parse(textBox1.Text.Trim());
                                    int nombrevotant_saisi = nombrevotants + Int32.Parse(textBoxnbr.Text.Trim());

                                    if (seuilvotant >= nombrevotant_saisi)
                                    {




                                        int totaleArrondis = 0;

                                        SqlDataReader rd = null;
                                        string query = "select * from View_totaleArrondis where ArrondisID=" + Int32.Parse(comboBoxArrondis.SelectedValue.ToString());
                                        string sCona = conn.connectdb();
                                        SqlConnection dbConna;
                                        dbConna = new SqlConnection(sCona);
                                        dbConna.Open();
                                        SqlCommand cmd = new SqlCommand(query, dbConna);
                                        rd = cmd.ExecuteReader();
                                        if (rd.Read())
                                        {

                                            // string btnName = nom;
                                            string testa = (rd["ArrondisTotale"].ToString());

                                            totaleArrondis = Int32.Parse(testa);


                                        }




                                        int totalecommune = 0;

                                        SqlDataReader rdc = null;
                                        string queryc = "select * from View_totalecommune where CommuneID=" + Int32.Parse(comboBoxCommune.SelectedValue.ToString());
                                        string sConc = conn.connectdb();
                                        SqlConnection dbConnc;
                                        dbConnc = new SqlConnection(sConc);
                                        dbConnc.Open();
                                        SqlCommand cmdc = new SqlCommand(queryc, dbConnc);
                                        rdc = cmdc.ExecuteReader();
                                        if (rdc.Read())
                                        {

                                            // string btnName = nom;
                                            string testc = (rdc["CommuneTotale"].ToString());

                                            totalecommune = Int32.Parse(testc);


                                        }




                                        int totalenationale = 0;

                                        SqlDataReader rdt = null;
                                        string queryt = "select * from View_nationaletotale where RégionID=" + Int32.Parse(comboBoxRégion.SelectedValue.ToString());
                                        string sCont = conn.connectdb();
                                        SqlConnection dbConnt;
                                        dbConnt = new SqlConnection(sCont);
                                        dbConnt.Open();
                                        SqlCommand cmdt = new SqlCommand(queryt, dbConnt);
                                        rdt = cmdt.ExecuteReader();
                                        if (rdt.Read())
                                        {

                                            // string btnName = nom;
                                            string testt = (rdt["nationaleregionTotale"].ToString());

                                            totalenationale = Int32.Parse(testt);


                                        }




                                        int totaleArrondis_ord = 0;

                                        SqlDataReader rdao = null;
                                        string queryao = "select * from View_totalearrondis_ordonnance where ArrondisID=" + Int32.Parse(comboBoxArrondis.SelectedValue.ToString());
                                        string sConao = conn.connectdb();
                                        SqlConnection dbConnao;
                                        dbConnao = new SqlConnection(sConao);
                                        dbConnao.Open();
                                        SqlCommand cmdao = new SqlCommand(queryao, dbConnao);
                                        rdao = cmdao.ExecuteReader();
                                        if (rdao.Read())
                                        {
                                            string testao = (rdao["ArrondisTotale_ord"].ToString());

                                            totaleArrondis_ord = Int32.Parse(testao);


                                        }



                                        int totalecommune_ord = 0;

                                        SqlDataReader rdco = null;
                                        string queryco = "select * from View_totalecommune_ordonnance where CommuneID=" + Int32.Parse(comboBoxCommune.SelectedValue.ToString());
                                        string sConco = conn.connectdb();
                                        SqlConnection dbConnco;
                                        dbConnco = new SqlConnection(sConco);
                                        dbConnco.Open();
                                        SqlCommand cmdco = new SqlCommand(queryco, dbConnco);
                                        rdco = cmdco.ExecuteReader();
                                        if (rdco.Read())
                                        {
                                            string testco = (rdco["CommuneTotale_ord"].ToString());

                                            totalecommune_ord = Int32.Parse(testco);


                                        }


                                        int totalenationale_ord = 0;

                                        SqlDataReader rdto = null;
                                        string queryto = "select * from View_centrevilletotale_ordonnance where RégionID=" + Int32.Parse(comboBoxRégion.SelectedValue.ToString());
                                        string sConto = conn.connectdb();
                                        SqlConnection dbConnto;
                                        dbConnto = new SqlConnection(sConto);
                                        dbConnto.Open();
                                        SqlCommand cmdto = new SqlCommand(queryto, dbConnto);
                                        rdto = cmdto.ExecuteReader();
                                        if (rdto.Read())
                                        {
                                            string testto = (rdto["centrevilleTotale_ord"].ToString());

                                            totalenationale_ord = Int32.Parse(testto);
                                        }

                                        int totalebureau = 0;

                                        SqlDataReader rdb = null;
                                        string queryb = "select * from View_totalebureau where BureauID=" + Int32.Parse(comboBoxBV.SelectedValue.ToString());
                                        string sContb = conn.connectdb();
                                        SqlConnection dbConntb;
                                        dbConntb = new SqlConnection(sContb);
                                        dbConntb.Open();
                                        SqlCommand cmdb = new SqlCommand(queryb, dbConntb);
                                        rdb = cmdb.ExecuteReader();
                                        if (rdb.Read())
                                        {
                                            string testb = (rdb["BureauTotale"].ToString());

                                            totalebureau = Int32.Parse(testb);
                                        }


                                        int totalecentre = 0;

                                        SqlDataReader rdcv = null;
                                        string querycv = "select * from View_totaleCentre where CentreID=" + Int32.Parse(comboBoxCV.SelectedValue.ToString());
                                        string sContcv = conn.connectdb();
                                        SqlConnection dbConntcv;
                                        dbConntcv = new SqlConnection(sContcv);
                                        dbConntcv.Open();
                                        SqlCommand cmdcv = new SqlCommand(querycv, dbConntcv);
                                        rdcv = cmdcv.ExecuteReader();
                                        if (rdcv.Read())
                                        {
                                            string testcv = (rdcv["CentreTotale"].ToString());

                                            totalecentre = Int32.Parse(testcv);
                                        }


                                        string sCon = conn.connectdb();
                                        string insertCmd = "INSERT INTO participant (totalecentrevote,totalebureauvote,totalenationale_ord,nombre_votant_ord,totaleArrondis_ord,totalecommune_ord,totalenationale,totalecommune,totaleArrondis,BUREAU_ID,nombre_votant,HORAIRE_ID,datecreation,datemodification,usercreation,usermodification) VALUES (@totalecentrevote,@totalebureauvote,@totalenationale_ord,@nombre_votant_ord,@totaleArrondis_ord,@totalecommune_ord,@totalenationale,@totalecommune,@totaleArrondis,@BUREAU_ID,@nombre_votant,@HORAIRE_ID,@datecreation,@datemodification,@usercreation,@usermodification)";
                                        SqlConnection dbConn;
                                        dbConn = new SqlConnection(sCon);
                                        dbConn.Open();
                                        Login lg = new Login();
                                        //string str;
                                        string dat = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                        DateTime date4 = DateTime.Parse(dat);

                                        GetValue value = new GetValue();
                                        //getAdress add = new getAdress();
                                        SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);

                                        myCommand.Parameters.AddWithValue("@nombre_votant", textBoxnbr.Text);
                                        myCommand.Parameters.AddWithValue("@nombre_votant_ord", textBox1.Text);
                                        myCommand.Parameters.AddWithValue("@HORAIRE_ID", Int32.Parse(comboBox1.SelectedValue.ToString()));


                                        myCommand.Parameters.AddWithValue("@usermodification", value.getAgentModif());//recupere agent modif
                                        myCommand.Parameters.AddWithValue("@usercreation", value.getAgentCréation());//recupere agent de creation


                                        myCommand.Parameters.AddWithValue("@BUREAU_ID", Int32.Parse(comboBoxBV.SelectedValue.ToString()));
                                        myCommand.Parameters.AddWithValue("@DateCreation", date4);
                                        myCommand.Parameters.AddWithValue("@DateModification", date4);

                                        myCommand.Parameters.AddWithValue("@totaleArrondis", totaleArrondis);
                                        myCommand.Parameters.AddWithValue("@totalecommune", totalecommune);
                                        myCommand.Parameters.AddWithValue("@totalenationale", totalenationale);

                                        myCommand.Parameters.AddWithValue("@totaleArrondis_ord", totaleArrondis_ord);
                                        myCommand.Parameters.AddWithValue("@totalecommune_ord", totalecommune_ord);
                                        myCommand.Parameters.AddWithValue("@totalenationale_ord", totalenationale_ord);

                                        myCommand.Parameters.AddWithValue("@totalecentrevote", totalecentre);
                                        myCommand.Parameters.AddWithValue("@totalebureauvote", totalebureau);


                                        myCommand.ExecuteNonQuery();
                                        MessageBox.Show("Enregistrement effectué avec succès");
                                        RecursiveClearTextBoxes(this.Controls);
                                        this.Hide();
                                    }
                                    else
                                    {
                                        MessageBox.Show("Veuillez respecter le seuil de nombre de votant sur cette Bureau de vote, le nombre de votant sont déjà " + nombrevotants + " et le totale inscrit sont +" + seuilvotant);
                                        textBoxnbr.Focus();
                                    }
                                }

                            }
                            else
                            {


                                int bureauid = 0;
                                int heureid = 0;
                                SqlDataReader rdbbh = null;
                                string querybbh = "select BureauID,horaire_id from View_participant_bureau where BureauID=" + Int32.Parse(comboBoxBureau_dis.SelectedValue.ToString()) + "  and horaire_id=" + Int32.Parse(comboBox1.SelectedValue.ToString());
                                string sContbbh = conn.connectdb();
                                SqlConnection dbConntbbh;
                                dbConntbbh = new SqlConnection(sContbbh);
                                dbConntbbh.Open();
                                SqlCommand cmdbbh = new SqlCommand(querybbh, dbConntbbh);
                                rdbbh = cmdbbh.ExecuteReader();
                                if (rdbbh.Read())
                                {

                                    bureauid = Int32.Parse((rdbbh["BureauID"].ToString()));
                                    heureid = Int32.Parse((rdbbh["horaire_id"].ToString()));

                                }
                                if ((bureauid == Int32.Parse(comboBoxBureau_dis.SelectedValue.ToString())) && (heureid == Int32.Parse(comboBox1.SelectedValue.ToString())))
                                {

                                    MessageBox.Show("cette bureau de vote est deja saisie à cette heure");
                                    comboBox1.Focus();
                                }
                                else
                                {


                                    int totalebureauvotes = 0;
                                    int nombrevotants = 0;
                                    int votantords = 0;
                                    SqlDataReader rdbs = null;
                                    string querybs = "select * from View_verification_nombrevotant_par_bureau where BureauID=" + Int32.Parse(comboBoxBureau_dis.SelectedValue.ToString());
                                    string sContbs = conn.connectdb();
                                    SqlConnection dbConntbs;
                                    dbConntbs = new SqlConnection(sContbs);
                                    dbConntbs.Open();
                                    SqlCommand cmdbs = new SqlCommand(querybs, dbConntbs);
                                    rdbs = cmdbs.ExecuteReader();
                                    if (rdbs.Read())
                                    {

                                        totalebureauvotes = Int32.Parse((rdbs["totalebureauvote"].ToString()));
                                        nombrevotants = Int32.Parse((rdbs["nombrevotant"].ToString()));
                                        votantords = Int32.Parse((rdbs["votantord"].ToString()));

                                    }


                                    int seuilvotant = totalebureauvotes + votantords + Int32.Parse(textBox1.Text.Trim());
                                    int nombrevotant_saisi = nombrevotants + Int32.Parse(textBoxnbr.Text.Trim());

                                    if (seuilvotant >= nombrevotant_saisi)
                                    {




                                        int totaleregion = 0;

                                        SqlDataReader rd = null;
                                        string query = "select * from View_RegionTotale where RégionID=" + Int32.Parse(comboBoxRégion.SelectedValue.ToString());
                                        string sCona = conn.connectdb();
                                        SqlConnection dbConna;
                                        dbConna = new SqlConnection(sCona);
                                        dbConna.Open();
                                        SqlCommand cmd = new SqlCommand(query, dbConna);
                                        rd = cmd.ExecuteReader();
                                        if (rd.Read())
                                        {

                                            // string btnName = nom;
                                            string testri = (rd["RegionTotale"].ToString());

                                            totaleregion = Int32.Parse(testri);


                                        }

                                        int totaleregion_ord = 0;

                                        SqlDataReader rdor = null;
                                        string queryor = "select * from View_regioninterieur_ordonnance where RégionID=" + Int32.Parse(comboBoxRégion.SelectedValue.ToString());
                                        string sConor = conn.connectdb();
                                        SqlConnection dbConnor;
                                        dbConnor = new SqlConnection(sConor);
                                        dbConnor.Open();
                                        SqlCommand cmdor = new SqlCommand(queryor, dbConnor);
                                        rdor = cmdor.ExecuteReader();
                                        if (rdor.Read())
                                        {

                                            // string btnName = nom;
                                            string testro = (rdor["regioninterieur_ord"].ToString());

                                            totaleregion_ord = Int32.Parse(testro);


                                        }



                                        int totalebureau = 0;

                                        SqlDataReader rdb = null;
                                        string queryb = "select * from View_totalebureau where BureauID=" + Int32.Parse(comboBoxBureau_dis.SelectedValue.ToString());
                                        string sContb = conn.connectdb();
                                        SqlConnection dbConntb;
                                        dbConntb = new SqlConnection(sContb);
                                        dbConntb.Open();
                                        SqlCommand cmdb = new SqlCommand(queryb, dbConntb);
                                        rdb = cmdb.ExecuteReader();
                                        if (rdb.Read())
                                        {
                                            string testbr = (rdb["BureauTotale"].ToString());

                                            totalebureau = Int32.Parse(testbr);
                                        }


                                        int totalecentre = 0;

                                        SqlDataReader rdcv = null;
                                        string querycv = "select * from View_totaleCentre where CentreID=" + Int32.Parse(comboBoxCentre_dis.SelectedValue.ToString());
                                        string sContcv = conn.connectdb();
                                        SqlConnection dbConntcv;
                                        dbConntcv = new SqlConnection(sContcv);
                                        dbConntcv.Open();
                                        SqlCommand cmdcv = new SqlCommand(querycv, dbConntcv);
                                        rdcv = cmdcv.ExecuteReader();
                                        if (rdcv.Read())
                                        {
                                            string testcr = (rdcv["CentreTotale"].ToString());

                                            totalecentre = Int32.Parse(testcr);
                                        }


                                        int totaleLocalite = 0;

                                        SqlDataReader rdl = null;
                                        string queryl = "select * from View_LocaliteTotale where LocalitéID=" + Int32.Parse(comboBoxLoc.SelectedValue.ToString());
                                        string sContl = conn.connectdb();
                                        SqlConnection dbConntl;
                                        dbConntl = new SqlConnection(sContl);
                                        dbConntl.Open();
                                        SqlCommand cmdl = new SqlCommand(queryl, dbConntl);
                                        rdl = cmdl.ExecuteReader();
                                        if (rdl.Read())
                                        {
                                            string testl = (rdl["LocaliteTotale"].ToString());

                                            totaleLocalite = Int32.Parse(testl);
                                        }

                                        string sCon = conn.connectdb();
                                        string insertCmd = "INSERT INTO participant (totalelocalite,nombre_votant_ord,totalecentrevote,totalebureauvote,totalenationale_ord,totalenationale,totaleRegion,BUREAU_ID,nombre_votant,HORAIRE_ID,datecreation,datemodification,usercreation,usermodification) VALUES (@totalelocalite,@nombre_votant_ord,@totalecentrevote,@totalebureauvote,@totalenationale_ord,@totalenationale,@totaleRegion,@BUREAU_ID,@nombre_votant,@HORAIRE_ID,@datecreation,@datemodification,@usercreation,@usermodification)";
                                        SqlConnection dbConn;
                                        dbConn = new SqlConnection(sCon);
                                        dbConn.Open();
                                        Login lg = new Login();
                                        //string str;
                                        string dat = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                        DateTime date4 = DateTime.Parse(dat);

                                        GetValue value = new GetValue();
                                        //getAdress add = new getAdress();
                                        SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);

                                        myCommand.Parameters.AddWithValue("@nombre_votant", textBoxnbr.Text);
                                        myCommand.Parameters.AddWithValue("@nombre_votant_ord", textBox1.Text);
                                        myCommand.Parameters.AddWithValue("@HORAIRE_ID", Int32.Parse(comboBox1.SelectedValue.ToString()));


                                        myCommand.Parameters.AddWithValue("@usermodification", value.getAgentModif());//recupere agent modif
                                        myCommand.Parameters.AddWithValue("@usercreation", value.getAgentCréation());//recupere agent de creation

                                        myCommand.Parameters.AddWithValue("@totaleRegion", totaleregion);
                                        myCommand.Parameters.AddWithValue("@totalenationale", totaleregion);
                                        myCommand.Parameters.AddWithValue("@totalenationale_ord", totaleregion_ord);

                                        myCommand.Parameters.AddWithValue("@BUREAU_ID", Int32.Parse(comboBoxBureau_dis.SelectedValue.ToString()));
                                        myCommand.Parameters.AddWithValue("@DateCreation", date4);
                                        myCommand.Parameters.AddWithValue("@DateModification", date4);

                                        myCommand.Parameters.AddWithValue("@totalecentrevote", totalecentre);
                                        myCommand.Parameters.AddWithValue("@totalebureauvote", totalebureau);
                                        myCommand.Parameters.AddWithValue("@totalelocalite", totaleLocalite);



                                        myCommand.ExecuteNonQuery();
                                        MessageBox.Show("Enregistrement effectué avec succès");
                                        RecursiveClearTextBoxes(this.Controls);
                                        this.Hide();

                                    }
                                    else
                                    {
                                        MessageBox.Show("Veuillez respecter le seuil de nombre de votant sur cette Bureau de vote, le nombre de votant sont déjà " + nombrevotants + " et le totale inscrit sont +" + seuilvotant);
                                        textBoxnbr.Focus();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }*/
        private void FillRégion()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT RégionID, NomRégion FROM Région";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            objDs.Tables[0].Rows.Add(30, "");
            con.Close();
            comboBoxRégion.ValueMember = "RégionID";
            comboBoxRégion.DisplayMember = "NomRégion";
            comboBoxRégion.DataSource = objDs.Tables[0];
            comboBoxRégion.SelectedValue = 30;
        }
        //int RégionID;
        /*private void comboBoxRégion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxRégion.SelectedValue.ToString() != "")
            {
                int RégionID = Convert.ToInt32(comboBoxRégion.SelectedValue.ToString());
                FillCommune(RégionID);
                //comboBoxCommune.SelectedIndex = 0;
            }
        }*/
        private void FillCommune(int RégionID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT CommuneID, NomCommune FROM Commune WHERE RégionID =@RégionID";
            if (RégionID == 30)
            {
                int reg = 1;
                cmd.Parameters.AddWithValue("@RégionID", reg);


            }
            else
            {
                cmd.Parameters.AddWithValue("@RégionID", RégionID);
            }

            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            objDs.Tables[0].Rows.Add(30, "");
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxCommune.ValueMember = "CommuneID";
                comboBoxCommune.DisplayMember = "NomCommune";
                comboBoxCommune.DataSource = objDs.Tables[0];
                comboBoxCommune.SelectedValue = 30;
            }
        }
        private void comboBoxCommune_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Int32.Parse(comboBoxCommune.SelectedValue.ToString()) == 30)
            {
                int CommuneID = Convert.ToInt32(comboBoxCommune.SelectedValue.ToString());
                this.FillArrondis(CommuneID);
                this.comboBoxArrondis.Enabled = false;
                //comboBoxArrondis.SelectedIndex = 0;
            }
            else
            {
                int CommuneID = Convert.ToInt32(comboBoxCommune.SelectedValue.ToString());
                this.FillArrondis(CommuneID);
                this.comboBoxArrondis.Enabled = true;
            }
        }
        private void comboBoxArrondis_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Int32.Parse(comboBoxArrondis.SelectedValue.ToString()) == 30)
            {
                int CommuneID = Convert.ToInt32(comboBoxArrondis.SelectedValue.ToString());
                this.FillCV(CommuneID);
                //this.comboBoxAdresse.Enabled = false;
                //comboBoxArrondis.SelectedIndex = 0;
            }
            else
            {
                int CommuneID = Convert.ToInt32(comboBoxArrondis.SelectedValue.ToString());
                this.FillCV(CommuneID);
                //this.comboBoxAdresse.Enabled = true;
            }
        }
        private void FillCV(int ArrondisID)
        {
            string sCon = conn.connectdb(); ;
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            if (comboBoxArrondis.Visible)
            {
                cmd.CommandText = "SET CONCAT_NULL_YIELDS_NULL OFF;SELECT CentreID, ( CONVERT(Nvarchar(50), CodeCentre)) + ' - '+CentreVote as CentreVote FROM View_CV where Actif = 1 and ArrondisID =@ArrondisID ORDER BY CodeCentre";
                if (ArrondisID == 30)
                {
                    int com = 1;
                    //cmd.CommandText = "SELECT CentreID, CentreVote FROM View_CV where Actif = 1 and ArrondisID =@ArrondisID ORDER BY CodeCentre";
                    cmd.Parameters.AddWithValue("@ArrondisID", com);


                }
                else
                {
                    
                    cmd.Parameters.AddWithValue("@ArrondisID", ArrondisID);
                }
            }
            else
            {
                cmd.CommandText = "SELECT CentreID, CentreVote FROM View_CV where Actif = 1 and LocalitéID =@ArrondisID ORDER BY CodeCentre"; 
                if (ArrondisID == 30000)
                {
                                  
                    int com = 1;
                    cmd.Parameters.AddWithValue("@ArrondisID", com);


                }
                else
                {
                    cmd.Parameters.AddWithValue("@ArrondisID", ArrondisID);
                }
            }

            //cmd.Parameters.AddWithValue("@ArrondisID", ArrondisID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            objDs.Tables[0].Rows.Add(3000, "");
            con.Close();
            comboBoxCV.ValueMember = "CentreID";
            comboBoxCV.DisplayMember = "CentreVote";
            comboBoxCV.DataSource = objDs.Tables[0];
            comboBoxCV.SelectedValue = 3000;
        }
        private void FillBureau(int CentreID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SET CONCAT_NULL_YIELDS_NULL OFF;SELECT BureauID, ( CONVERT(Nvarchar(50), CodeBureau)) + ' - '+NomBureau as NomBureau FROM View_LV WHERE CentreID =@CentreID ORDER BY CodeCentre,OrdreCentre";
            cmd.Parameters.AddWithValue("@CentreID", CentreID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            objDs.Tables[0].Rows.Add(3000, "");
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxBV.ValueMember = "BureauID";
                comboBoxBV.DisplayMember = "NomBureau";
                comboBoxBV.DataSource = objDs.Tables[0];
                comboBoxBV.SelectedValue = 3000;
            }
        }
        private void FillArrondis(int CommuneID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT ArrondisID, NomArrondis FROM Arrondissement WHERE CommuneID =@CommuneID";
            if (CommuneID == 30)
            {
                int com = 1;
                cmd.Parameters.AddWithValue("@CommuneID", com);


            }
            else
            {
                cmd.Parameters.AddWithValue("@CommuneID", CommuneID);
            }
            //cmd.Parameters.AddWithValue("@CommuneID", CommuneID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            objDs.Tables[0].Rows.Add(30, "");
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxArrondis.ValueMember = "ArrondisID";
                comboBoxArrondis.DisplayMember = "NomArrondis";
                comboBoxArrondis.DataSource = objDs.Tables[0];
                comboBoxArrondis.SelectedValue = 30;


            }
        }
        private void Filllocalite(int _reg)
        {
            int reg = _reg;
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT LocalitéID, NomLocalité FROM Localité WHERE LocalitéID in(Select LocalitéID from View_CV) and RégionID = " + reg;
            //cmd.Parameters.AddWithValue("@RégionID", RégionID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            objDs.Tables[0].Rows.Add(30000, "");
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxLoc.ValueMember = "LocalitéID";
                comboBoxLoc.DisplayMember = "NomLocalité";
                comboBoxLoc.DataSource = objDs.Tables[0];
                comboBoxLoc.SelectedValue = 30000;
               
            }
        }
        private void comboBoxRégion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Int32.Parse(comboBoxRégion.SelectedValue.ToString()) == 1)
            {
                int RégionID = Convert.ToInt32(comboBoxRégion.SelectedValue.ToString());
                FillCommune(RégionID);
                //comboBoxCommune.SelectedIndex = 0;
                labelCom.Text = "Commune";
                comboBoxLoc.Visible = false;
                //labelAddress.Visible = true;
                labelArrondis.Visible = true;
                //comboBoxAdresse.Visible = true;
                comboBoxArrondis.Visible = true;
                comboBoxCommune.Visible = true;
                //comboBoxAdresse.Enabled = true;
                comboBoxArrondis.Enabled = true;
                comboBoxCommune.Enabled = true;
            }
            else
            {
                if (Int32.Parse(comboBoxRégion.SelectedValue.ToString()) == 30)
                {
                    int RégionID = Convert.ToInt32(comboBoxRégion.SelectedValue.ToString());
                    FillCommune(RégionID);
                    //comboBoxCommune.SelectedIndex = 0;
                    labelCom.Text = "Commune";
                    comboBoxLoc.Visible = false;
                    //labelAddress.Visible = true;
                    labelArrondis.Visible = true;
                    //comboBoxAdresse.Visible = true;
                    comboBoxArrondis.Visible = true;
                    comboBoxCommune.Visible = true;
                    //comboBoxAdresse.Enabled = false;
                    comboBoxArrondis.Enabled = false;
                    comboBoxCommune.Enabled = false;
                }
                else
                {
                    this.Filllocalite(Int32.Parse(comboBoxRégion.SelectedValue.ToString()));
                    comboBoxLoc.SelectedIndex = 0;
                    comboBoxLoc.Visible = true;
                    labelCom.Text = "Localité";
                    comboBoxCommune.Visible = false;
                    labelCom.Visible = true;
                    //labelAddress.Visible = false;
                    labelArrondis.Visible = false;
                    //comboBoxAdresse.Visible = false;
                    comboBoxArrondis.Visible = false;
                }

            }
        }

        private void comboBoxLoc_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Int32.Parse(comboBoxLoc.SelectedValue.ToString()) == 30)
            {
                int CommuneID = Convert.ToInt32(comboBoxLoc.SelectedValue.ToString());
                this.FillCV(CommuneID);
                //this.comboBoxAdresse.Enabled = false;
                //comboBoxArrondis.SelectedIndex = 0;
            }
            else
            {
                int CommuneID = Convert.ToInt32(comboBoxLoc.SelectedValue.ToString());
                this.FillCV(CommuneID);
                //this.comboBoxAdresse.Enabled = true;
            }
        }

        private void textBoxnbr_KeyPress(object sender, KeyPressEventArgs e)
        {


            int isNum = 0;
            if (e.KeyChar == ',' || e.KeyChar == (char)Keys.Back)
                e.Handled = false;
            else if (!int.TryParse(e.KeyChar.ToString(), out isNum))
                e.Handled = true;
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {

            int isNum = 0;
            if (e.KeyChar == ',' || e.KeyChar == (char)Keys.Back)
                e.Handled = false;
            else if (!int.TryParse(e.KeyChar.ToString(), out isNum))
                e.Handled = true;
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void comboBoxBV_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.Fillheure();
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SET CONCAT_NULL_YIELDS_NULL OFF;SELECT * from View_reg_aff where BureauID=@BureauID";
            cmd.Parameters.AddWithValue("@BureauID", Int32.Parse(comboBoxBV.SelectedValue.ToString()));
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            int total = objDs.Tables[0].Rows.Count;
            InscrisBureau = total;
            if (total>0)
            {
                Save.Enabled = true;
                ClearClass1 clr1 = new ClearClass1();
                clr1.RecursiveClearTextBoxes1(this.panel2.Controls);
                button2.Enabled = true;
                
            }
            else
            {
                Save.Enabled = false;
                button2.Enabled = false;
            }
        }
        public bool bur()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SET CONCAT_NULL_YIELDS_NULL OFF;SELECT * from Particip where BUREAU_ID=@BureauID and Horaire_id =@Horaire_Id";
            cmd.Parameters.AddWithValue("@BureauID", Int32.Parse(comboBoxBV.SelectedValue.ToString()));
            cmd.Parameters.AddWithValue("@Horaire_Id", Int32.Parse(comboBox1.SelectedValue.ToString()));
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            int total = objDs.Tables[0].Rows.Count;
            //InscrisBureau = total;
            if (total != 0)
            {
                ctrl = true;
            }
            else
            {
                ctrl = false;
            }
            return ctrl;
        }

        private void Save_Click(object sender, EventArgs e)
        {
            if (this.bur())
            {
                MessageBox.Show("la participation a déjà été saisie pour ce bureau");
            }
            else
            {
             
                try
                {

                    int Tot = Int32.Parse(textBox1.Text.ToString()) + Int32.Parse(textBoxnbr.Text.ToString());
                    if (Tot > (InscrisBureau + (Int32.Parse(textBox1.Text.ToString()))))
                    {
                        MessageBox.Show("le nombre de votant ne doit pas etre superieur au nombre d'électeur inscris");
                    }
                    else
                    {

                        int seuilvotanttot = 0;
                        int seuilvotantinscrit = 0;
                        int seuilvotantord = 0;

                         string SQL = "select MAX(nombre_votantsum) as  nombrevotantmax,MAX(nombre_votantInscris) as votantinscritmax,MAX(nombre_votant_ord) as votant_ordmax  from View_participant_bureau where BureauID=" + Int32.Parse(comboBoxBV.SelectedValue.ToString());
                      
                        string sCon1 = conn.connectdb();
                        SqlConnection dbConn1;
                        dbConn1 = new SqlConnection(sCon1);
                        dbConn1.Open();
                        SqlDataAdapter sqlAdapter = new SqlDataAdapter(SQL, dbConn1);
                        SqlCommandBuilder commandBuilder1 = new SqlCommandBuilder(sqlAdapter);
                        DataTable dt = new DataTable();
                        sqlAdapter.Fill(dt);
                        dbConn1.Close();

                        if (dt.Rows.Count > 0)
                        {
                            DataRow dtRow = dt.Rows[0];
                        
                            seuilvotanttot = Convert.ToInt32((!object.ReferenceEquals(dtRow["nombrevotantmax"], DBNull.Value) ? dtRow["nombrevotantmax"].ToString() :"0"));
                            seuilvotantord = Convert.ToInt32((!object.ReferenceEquals(dtRow["votant_ordmax"], DBNull.Value) ? dtRow["votant_ordmax"].ToString() : "0"));
                            seuilvotantinscrit = Convert.ToInt32((!object.ReferenceEquals(dtRow["votantinscritmax"], DBNull.Value) ? dtRow["votantinscritmax"].ToString() : "0"));
                            
                        }

                        if (Tot < seuilvotanttot || Int32.Parse(textBoxnbr.Text.ToString()) < seuilvotantinscrit || Int32.Parse(textBox1.Text.ToString()) < seuilvotantord)
                        {
                            MessageBox.Show("le nombre à saisir doit-etre superieur ou égale au saisie precedent Totale=" + seuilvotanttot + ", votant_inscrit=" + seuilvotantinscrit + ", votant_ordonnance=" + seuilvotantord);
                        }

                        else
                        {

                            if (comboBoxRégion.SelectedValue.ToString() == "1")
                            {


                                int totaleArrondis = 0;

                                SqlDataReader rd = null;
                                string query = "select * from View_totaleArrondis where ArrondisID=" + Int32.Parse(comboBoxArrondis.SelectedValue.ToString());
                                string sCona = conn.connectdb();
                                SqlConnection dbConna;
                                dbConna = new SqlConnection(sCona);
                                dbConna.Open();
                                SqlCommand cmd = new SqlCommand(query, dbConna);
                                rd = cmd.ExecuteReader();
                                if (rd.Read())
                                {

                                    // string btnName = nom;
                                    string testa = (rd["ArrondisTotale"].ToString());

                                    totaleArrondis = Int32.Parse(testa);


                                }


                                int totalecommune = 0;

                                SqlDataReader rdc = null;
                                string queryc = "select * from View_totalecommune where CommuneID=" + Int32.Parse(comboBoxCommune.SelectedValue.ToString());
                                string sConc = conn.connectdb();
                                SqlConnection dbConnc;
                                dbConnc = new SqlConnection(sConc);
                                dbConnc.Open();
                                SqlCommand cmdc = new SqlCommand(queryc, dbConnc);
                                rdc = cmdc.ExecuteReader();
                                if (rdc.Read())
                                {

                                    // string btnName = nom;
                                    string testc = (rdc["CommuneTotale"].ToString());

                                    totalecommune = Int32.Parse(testc);


                                }


                                int totalenationale = 0;

                                SqlDataReader rdt = null;
                                string queryt = "select * from View_nationaletotale where RégionID=" + Int32.Parse(comboBoxRégion.SelectedValue.ToString());
                                string sCont = conn.connectdb();
                                SqlConnection dbConnt;
                                dbConnt = new SqlConnection(sCont);
                                dbConnt.Open();
                                SqlCommand cmdt = new SqlCommand(queryt, dbConnt);
                                rdt = cmdt.ExecuteReader();
                                if (rdt.Read())
                                {

                                    // string btnName = nom;
                                    string testt = (rdt["nationaleregionTotale"].ToString());

                                    totalenationale = Int32.Parse(testt);


                                }

                                int totalebureau = 0;

                                SqlDataReader rdb = null;
                                string queryb = "select * from View_totalebureau where BureauID=" + Int32.Parse(comboBoxBV.SelectedValue.ToString());
                                string sContb = conn.connectdb();
                                SqlConnection dbConntb;
                                dbConntb = new SqlConnection(sContb);
                                dbConntb.Open();
                                SqlCommand cmdb = new SqlCommand(queryb, dbConntb);
                                rdb = cmdb.ExecuteReader();
                                if (rdb.Read())
                                {
                                    string testb = (rdb["BureauTotale"].ToString());

                                    totalebureau = Int32.Parse(testb);
                                }


                                int totalecentre = 0;

                                SqlDataReader rdcv = null;
                                string querycv = "select * from View_totaleCentre where CentreID=" + Int32.Parse(comboBoxCV.SelectedValue.ToString());
                                string sContcv = conn.connectdb();
                                SqlConnection dbConntcv;
                                dbConntcv = new SqlConnection(sContcv);
                                dbConntcv.Open();
                                SqlCommand cmdcv = new SqlCommand(querycv, dbConntcv);
                                rdcv = cmdcv.ExecuteReader();
                                if (rdcv.Read())
                                {
                                    string testcv = (rdcv["CentreTotale"].ToString());

                                    totalecentre = Int32.Parse(testcv);
                                }


                                string sCon = conn.connectdb();
                                string insertCmd = "INSERT INTO particip (nombre_votantsum,totalecentrevote,totalebureauvote,totalenationale,totalecommune,totaleArrondis,nombre_votant_ord,BUREAU_ID,nombre_votantInscris,Horaire_id,ElectionID,datecreation,datemodification,usercreation,usermodification) VALUES (@nombre_votantsum,@totalecentrevote,@totalebureauvote,@totalenationale,@totalecommune,@totaleArrondis,@nombre_votant_ord,@BUREAU_ID,@nombre_votantInscris,@HORAIRE_ID,@ElectionID,@datecreation,@datemodification,@usercreation,@usermodification)";
                                SqlConnection dbConn;
                                dbConn = new SqlConnection(sCon);
                                dbConn.Open();
                                //Login lg = new Login();
                                //string str;
                                string dat = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                DateTime date4 = DateTime.Parse(dat);
                                GetValue value = new GetValue();
                                SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                                myCommand.Parameters.AddWithValue("@totaleArrondis", totaleArrondis);
                                myCommand.Parameters.AddWithValue("@totalecommune", totalecommune);
                                myCommand.Parameters.AddWithValue("@totalenationale", totalenationale);
                                myCommand.Parameters.AddWithValue("@totalecentrevote", totalecentre);
                                myCommand.Parameters.AddWithValue("@totalebureauvote", totalebureau);

                                myCommand.Parameters.AddWithValue("@nombre_votantInscris", textBoxnbr.Text);
                                myCommand.Parameters.AddWithValue("@nombre_votant_ord", textBox1.Text);
                                myCommand.Parameters.AddWithValue("@nombre_votantsum", Tot);

                                myCommand.Parameters.AddWithValue("@HORAIRE_ID", Int32.Parse(comboBox1.SelectedValue.ToString()));
                                myCommand.Parameters.AddWithValue("@ElectionID", Int32.Parse(comboBoxElect.SelectedValue.ToString()));
                                myCommand.Parameters.AddWithValue("@usermodification", value.getAgentModif());//recupere agent modif
                                myCommand.Parameters.AddWithValue("@usercreation", value.getAgentCréation());//recupere agent de creation
                                myCommand.Parameters.AddWithValue("@BUREAU_ID", Int32.Parse(comboBoxBV.SelectedValue.ToString()));
                                myCommand.Parameters.AddWithValue("@DateCreation", date4);
                                myCommand.Parameters.AddWithValue("@DateModification", date4);
                                myCommand.ExecuteNonQuery();
                                MessageBox.Show("Enregistrement effectué avec succès");
                                ClearClass1 clr1 = new ClearClass1();
                                clr1.RecursiveClearTextBoxes1(this.panel2.Controls);

                            }
                            else
                            {


                                int totaleregion = 0;

                                SqlDataReader rd = null;
                                string query = "select * from View_RegionTotale where RégionID=" + Int32.Parse(comboBoxRégion.SelectedValue.ToString());
                                string sCona = conn.connectdb();
                                SqlConnection dbConna;
                                dbConna = new SqlConnection(sCona);
                                dbConna.Open();
                                SqlCommand cmd = new SqlCommand(query, dbConna);
                                rd = cmd.ExecuteReader();
                                if (rd.Read())
                                {

                                    // string btnName = nom;
                                    string testri = (rd["RegionTotale"].ToString());

                                    totaleregion = Int32.Parse(testri);


                                }


                                int totalebureau = 0;

                                SqlDataReader rdb = null;
                                string queryb = "select * from View_totalebureau where BureauID=" + Int32.Parse(comboBoxBV.SelectedValue.ToString());
                                string sContb = conn.connectdb();
                                SqlConnection dbConntb;
                                dbConntb = new SqlConnection(sContb);
                                dbConntb.Open();
                                SqlCommand cmdb = new SqlCommand(queryb, dbConntb);
                                rdb = cmdb.ExecuteReader();
                                if (rdb.Read())
                                {
                                    string testbr = (rdb["BureauTotale"].ToString());

                                    totalebureau = Int32.Parse(testbr);
                                }


                                int totalecentre = 0;

                                SqlDataReader rdcv = null;
                                string querycv = "select * from View_totaleCentre where CentreID=" + Int32.Parse(comboBoxCV.SelectedValue.ToString());
                                string sContcv = conn.connectdb();
                                SqlConnection dbConntcv;
                                dbConntcv = new SqlConnection(sContcv);
                                dbConntcv.Open();
                                SqlCommand cmdcv = new SqlCommand(querycv, dbConntcv);
                                rdcv = cmdcv.ExecuteReader();
                                if (rdcv.Read())
                                {
                                    string testcr = (rdcv["CentreTotale"].ToString());

                                    totalecentre = Int32.Parse(testcr);
                                }


                                int totaleLocalite = 0;

                                SqlDataReader rdl = null;
                                string queryl = "select * from View_LocaliteTotale where LocalitéID=" + Int32.Parse(comboBoxLoc.SelectedValue.ToString());
                                string sContl = conn.connectdb();
                                SqlConnection dbConntl;
                                dbConntl = new SqlConnection(sContl);
                                dbConntl.Open();
                                SqlCommand cmdl = new SqlCommand(queryl, dbConntl);
                                rdl = cmdl.ExecuteReader();
                                if (rdl.Read())
                                {
                                    string testl = (rdl["LocaliteTotale"].ToString());

                                    totaleLocalite = Int32.Parse(testl);
                                }

                                string sCon = conn.connectdb();
                                string insertCmd = "INSERT INTO particip (nombre_votantsum,totalelocalite,totalecentrevote,totalebureauvote,totalenationale,totaleRegion,nombre_votant_ord,BUREAU_ID,nombre_votantInscris,Horaire_id,ElectionID,datecreation,datemodification,usercreation,usermodification) VALUES (@nombre_votantsum,@totalelocalite,@totalecentrevote,@totalebureauvote,@totalenationale,@totaleRegion,@nombre_votant_ord,@BUREAU_ID,@nombre_votantInscris,@HORAIRE_ID,@ElectionID,@datecreation,@datemodification,@usercreation,@usermodification)";
                                SqlConnection dbConn;
                                dbConn = new SqlConnection(sCon);
                                dbConn.Open();
                                //Login lg = new Login();
                                //string str;
                                string dat = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                DateTime date4 = DateTime.Parse(dat);
                                GetValue value = new GetValue();
                                SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                                myCommand.Parameters.AddWithValue("@nombre_votantInscris", textBoxnbr.Text);
                                myCommand.Parameters.AddWithValue("@nombre_votant_ord", textBox1.Text);
                                myCommand.Parameters.AddWithValue("@nombre_votantsum", Tot);
                                myCommand.Parameters.AddWithValue("@HORAIRE_ID", Int32.Parse(comboBox1.SelectedValue.ToString()));
                                myCommand.Parameters.AddWithValue("@ElectionID", Int32.Parse(comboBoxElect.SelectedValue.ToString()));
                                myCommand.Parameters.AddWithValue("@usermodification", value.getAgentModif());//recupere agent modif
                                myCommand.Parameters.AddWithValue("@usercreation", value.getAgentCréation());//recupere agent de creation
                                myCommand.Parameters.AddWithValue("@BUREAU_ID", Int32.Parse(comboBoxBV.SelectedValue.ToString()));
                                myCommand.Parameters.AddWithValue("@DateCreation", date4);
                                myCommand.Parameters.AddWithValue("@DateModification", date4);


                                myCommand.Parameters.AddWithValue("@totaleRegion", totaleregion);
                                myCommand.Parameters.AddWithValue("@totalenationale", totaleregion);
                                myCommand.Parameters.AddWithValue("@totalecentrevote", totalecentre);
                                myCommand.Parameters.AddWithValue("@totalebureauvote", totalebureau);
                                myCommand.Parameters.AddWithValue("@totalelocalite", totaleLocalite);

                                myCommand.ExecuteNonQuery();
                                MessageBox.Show("Enregistrement effectué avec succès");
                                ClearClass1 clr1 = new ClearClass1();
                                clr1.RecursiveClearTextBoxes1(this.panel2.Controls);
                            }
                        }
                    }
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            
               
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (this.bur())
            {
                MessageBox.Show("la participation a déjà été saisie pour ce bureau");
            }
            else
            {
              
                try
                {

                    int Tot = Int32.Parse(textBox1.Text.ToString()) + Int32.Parse(textBoxnbr.Text.ToString());
                    if (Tot > (InscrisBureau + (Int32.Parse(textBox1.Text.ToString()))))
                    {
                        MessageBox.Show("le nombre d'électeur inscris ne doit pas etre superieur au nombre de votant");
                    }
                    else
                    {

                        int seuilvotanttot = 0;
                        int seuilvotantinscrit = 0;
                        int seuilvotantord = 0;

                        string SQL = "select MAX(nombre_votantsum) as  nombrevotantmax,MAX(nombre_votantInscris) as votantinscritmax,MAX(nombre_votant_ord) as votant_ordmax  from View_participant_bureau where BureauID=" + Int32.Parse(comboBoxBV.SelectedValue.ToString());

                        string sCon1 = conn.connectdb();
                        SqlConnection dbConn1;
                        dbConn1 = new SqlConnection(sCon1);
                        dbConn1.Open();
                        SqlDataAdapter sqlAdapter = new SqlDataAdapter(SQL, dbConn1);
                        SqlCommandBuilder commandBuilder1 = new SqlCommandBuilder(sqlAdapter);
                        DataTable dt = new DataTable();
                        sqlAdapter.Fill(dt);
                        dbConn1.Close();

                        if (dt.Rows.Count > 0)
                        {
                            DataRow dtRow = dt.Rows[0];

                            seuilvotanttot = Convert.ToInt32((!object.ReferenceEquals(dtRow["nombrevotantmax"], DBNull.Value) ? dtRow["nombrevotantmax"].ToString() : "0"));
                            seuilvotantord = Convert.ToInt32((!object.ReferenceEquals(dtRow["votant_ordmax"], DBNull.Value) ? dtRow["votant_ordmax"].ToString() : "0"));
                            seuilvotantinscrit = Convert.ToInt32((!object.ReferenceEquals(dtRow["votantinscritmax"], DBNull.Value) ? dtRow["votantinscritmax"].ToString() : "0"));

                        }


                        if (Tot < seuilvotanttot || Int32.Parse(textBoxnbr.Text.ToString()) < seuilvotantinscrit || Int32.Parse(textBox1.Text.ToString()) < seuilvotantord)
                        {
                            MessageBox.Show("le nombre à saisir doit-etre superieur ou égale au saisie precedent Totale=" + seuilvotanttot + ", votant_inscrit=" + seuilvotantinscrit + ", votant_ordonnance=" + seuilvotantord);
                        }

                        else
                        {



                            if (comboBoxRégion.SelectedValue.ToString() == "1")
                            {


                                int totaleArrondis = 0;

                                SqlDataReader rd = null;
                                string query = "select * from View_totaleArrondis where ArrondisID=" + Int32.Parse(comboBoxArrondis.SelectedValue.ToString());
                                string sCona = conn.connectdb();
                                SqlConnection dbConna;
                                dbConna = new SqlConnection(sCona);
                                dbConna.Open();
                                SqlCommand cmd = new SqlCommand(query, dbConna);
                                rd = cmd.ExecuteReader();
                                if (rd.Read())
                                {

                                    // string btnName = nom;
                                    string testa = (rd["ArrondisTotale"].ToString());

                                    totaleArrondis = Int32.Parse(testa);


                                }



                                int totalecommune = 0;

                                SqlDataReader rdc = null;
                                string queryc = "select * from View_totalecommune where CommuneID=" + Int32.Parse(comboBoxCommune.SelectedValue.ToString());
                                string sConc = conn.connectdb();
                                SqlConnection dbConnc;
                                dbConnc = new SqlConnection(sConc);
                                dbConnc.Open();
                                SqlCommand cmdc = new SqlCommand(queryc, dbConnc);
                                rdc = cmdc.ExecuteReader();
                                if (rdc.Read())
                                {

                                    // string btnName = nom;
                                    string testc = (rdc["CommuneTotale"].ToString());

                                    totalecommune = Int32.Parse(testc);


                                }




                                int totalenationale = 0;

                                SqlDataReader rdt = null;
                                string queryt = "select * from View_nationaletotale where RégionID=" + Int32.Parse(comboBoxRégion.SelectedValue.ToString());
                                string sCont = conn.connectdb();
                                SqlConnection dbConnt;
                                dbConnt = new SqlConnection(sCont);
                                dbConnt.Open();
                                SqlCommand cmdt = new SqlCommand(queryt, dbConnt);
                                rdt = cmdt.ExecuteReader();
                                if (rdt.Read())
                                {

                                    // string btnName = nom;
                                    string testt = (rdt["nationaleregionTotale"].ToString());

                                    totalenationale = Int32.Parse(testt);


                                }



                                int totalebureau = 0;

                                SqlDataReader rdb = null;
                                string queryb = "select * from View_totalebureau where BureauID=" + Int32.Parse(comboBoxBV.SelectedValue.ToString());
                                string sContb = conn.connectdb();
                                SqlConnection dbConntb;
                                dbConntb = new SqlConnection(sContb);
                                dbConntb.Open();
                                SqlCommand cmdb = new SqlCommand(queryb, dbConntb);
                                rdb = cmdb.ExecuteReader();
                                if (rdb.Read())
                                {
                                    string testb = (rdb["BureauTotale"].ToString());

                                    totalebureau = Int32.Parse(testb);
                                }


                                int totalecentre = 0;

                                SqlDataReader rdcv = null;
                                string querycv = "select * from View_totaleCentre where CentreID=" + Int32.Parse(comboBoxCV.SelectedValue.ToString());
                                string sContcv = conn.connectdb();
                                SqlConnection dbConntcv;
                                dbConntcv = new SqlConnection(sContcv);
                                dbConntcv.Open();
                                SqlCommand cmdcv = new SqlCommand(querycv, dbConntcv);
                                rdcv = cmdcv.ExecuteReader();
                                if (rdcv.Read())
                                {
                                    string testcv = (rdcv["CentreTotale"].ToString());

                                    totalecentre = Int32.Parse(testcv);
                                }


                                string sCon = conn.connectdb();
                                string insertCmd = "INSERT INTO particip (nombre_votantsum,totalecentrevote,totalebureauvote,totalenationale,totalecommune,totaleArrondis,nombre_votant_ord,BUREAU_ID,nombre_votantInscris,Horaire_id,ElectionID,datecreation,datemodification,usercreation,usermodification) VALUES (@nombre_votantsum,@totalecentrevote,@totalebureauvote,@totalenationale,@totalecommune,@totaleArrondis,@nombre_votant_ord,@BUREAU_ID,@nombre_votantInscris,@HORAIRE_ID,@ElectionID,@datecreation,@datemodification,@usercreation,@usermodification)";
                                SqlConnection dbConn;
                                dbConn = new SqlConnection(sCon);
                                dbConn.Open();
                                //Login lg = new Login();
                                //string str;
                                string dat = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                DateTime date4 = DateTime.Parse(dat);
                                GetValue value = new GetValue();
                                SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                                myCommand.Parameters.AddWithValue("@totaleArrondis", totaleArrondis);
                                myCommand.Parameters.AddWithValue("@totalecommune", totalecommune);
                                myCommand.Parameters.AddWithValue("@totalenationale", totalenationale);
                                myCommand.Parameters.AddWithValue("@totalecentrevote", totalecentre);
                                myCommand.Parameters.AddWithValue("@totalebureauvote", totalebureau);

                                myCommand.Parameters.AddWithValue("@nombre_votantInscris", textBoxnbr.Text);
                                myCommand.Parameters.AddWithValue("@nombre_votant_ord", textBox1.Text);
                                myCommand.Parameters.AddWithValue("@nombre_votantsum", Tot);

                                myCommand.Parameters.AddWithValue("@HORAIRE_ID", Int32.Parse(comboBox1.SelectedValue.ToString()));
                                myCommand.Parameters.AddWithValue("@ElectionID", Int32.Parse(comboBoxElect.SelectedValue.ToString()));
                                myCommand.Parameters.AddWithValue("@usermodification", value.getAgentModif());//recupere agent modif
                                myCommand.Parameters.AddWithValue("@usercreation", value.getAgentCréation());//recupere agent de creation
                                myCommand.Parameters.AddWithValue("@BUREAU_ID", Int32.Parse(comboBoxBV.SelectedValue.ToString()));
                                myCommand.Parameters.AddWithValue("@DateCreation", date4);
                                myCommand.Parameters.AddWithValue("@DateModification", date4);
                                myCommand.ExecuteNonQuery();
                                MessageBox.Show("Enregistrement effectué avec succès");
                                this.Close();

                            }
                            else
                            {


                                int totaleregion = 0;

                                SqlDataReader rd = null;
                                string query = "select * from View_RegionTotale where RégionID=" + Int32.Parse(comboBoxRégion.SelectedValue.ToString());
                                string sCona = conn.connectdb();
                                SqlConnection dbConna;
                                dbConna = new SqlConnection(sCona);
                                dbConna.Open();
                                SqlCommand cmd = new SqlCommand(query, dbConna);
                                rd = cmd.ExecuteReader();
                                if (rd.Read())
                                {

                                    // string btnName = nom;
                                    string testri = (rd["RegionTotale"].ToString());

                                    totaleregion = Int32.Parse(testri);


                                }


                                int totalebureau = 0;

                                SqlDataReader rdb = null;
                                string queryb = "select * from View_totalebureau where BureauID=" + Int32.Parse(comboBoxBV.SelectedValue.ToString());
                                string sContb = conn.connectdb();
                                SqlConnection dbConntb;
                                dbConntb = new SqlConnection(sContb);
                                dbConntb.Open();
                                SqlCommand cmdb = new SqlCommand(queryb, dbConntb);
                                rdb = cmdb.ExecuteReader();
                                if (rdb.Read())
                                {
                                    string testbr = (rdb["BureauTotale"].ToString());

                                    totalebureau = Int32.Parse(testbr);
                                }


                                int totalecentre = 0;

                                SqlDataReader rdcv = null;
                                string querycv = "select * from View_totaleCentre where CentreID=" + Int32.Parse(comboBoxCV.SelectedValue.ToString());
                                string sContcv = conn.connectdb();
                                SqlConnection dbConntcv;
                                dbConntcv = new SqlConnection(sContcv);
                                dbConntcv.Open();
                                SqlCommand cmdcv = new SqlCommand(querycv, dbConntcv);
                                rdcv = cmdcv.ExecuteReader();
                                if (rdcv.Read())
                                {
                                    string testcr = (rdcv["CentreTotale"].ToString());

                                    totalecentre = Int32.Parse(testcr);
                                }


                                int totaleLocalite = 0;

                                SqlDataReader rdl = null;
                                string queryl = "select * from View_LocaliteTotale where LocalitéID=" + Int32.Parse(comboBoxLoc.SelectedValue.ToString());
                                string sContl = conn.connectdb();
                                SqlConnection dbConntl;
                                dbConntl = new SqlConnection(sContl);
                                dbConntl.Open();
                                SqlCommand cmdl = new SqlCommand(queryl, dbConntl);
                                rdl = cmdl.ExecuteReader();
                                if (rdl.Read())
                                {
                                    string testl = (rdl["LocaliteTotale"].ToString());

                                    totaleLocalite = Int32.Parse(testl);
                                }

                                string sCon = conn.connectdb();
                                string insertCmd = "INSERT INTO particip (nombre_votantsum,totalelocalite,totalecentrevote,totalebureauvote,totalenationale,totaleRegion,nombre_votant_ord,BUREAU_ID,nombre_votantInscris,Horaire_id,ElectionID,datecreation,datemodification,usercreation,usermodification) VALUES (@nombre_votantsum,@totalelocalite,@totalecentrevote,@totalebureauvote,@totalenationale,@totaleRegion,@nombre_votant_ord,@BUREAU_ID,@nombre_votantInscris,@HORAIRE_ID,@ElectionID,@datecreation,@datemodification,@usercreation,@usermodification)";
                                SqlConnection dbConn;
                                dbConn = new SqlConnection(sCon);
                                dbConn.Open();
                                //Login lg = new Login();
                                //string str;
                                string dat = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                DateTime date4 = DateTime.Parse(dat);
                                GetValue value = new GetValue();
                                SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                                myCommand.Parameters.AddWithValue("@nombre_votantInscris", textBoxnbr.Text);
                                myCommand.Parameters.AddWithValue("@nombre_votant_ord", textBox1.Text);
                                myCommand.Parameters.AddWithValue("@nombre_votantsum", Tot);
                                myCommand.Parameters.AddWithValue("@HORAIRE_ID", Int32.Parse(comboBox1.SelectedValue.ToString()));
                                myCommand.Parameters.AddWithValue("@ElectionID", Int32.Parse(comboBoxElect.SelectedValue.ToString()));
                                myCommand.Parameters.AddWithValue("@usermodification", value.getAgentModif());//recupere agent modif
                                myCommand.Parameters.AddWithValue("@usercreation", value.getAgentCréation());//recupere agent de creation
                                myCommand.Parameters.AddWithValue("@BUREAU_ID", Int32.Parse(comboBoxBV.SelectedValue.ToString()));
                                myCommand.Parameters.AddWithValue("@DateCreation", date4);
                                myCommand.Parameters.AddWithValue("@DateModification", date4);


                                myCommand.Parameters.AddWithValue("@totaleRegion", totaleregion);
                                myCommand.Parameters.AddWithValue("@totalenationale", totaleregion);
                                myCommand.Parameters.AddWithValue("@totalecentrevote", totalecentre);
                                myCommand.Parameters.AddWithValue("@totalebureauvote", totalebureau);
                                myCommand.Parameters.AddWithValue("@totalelocalite", totaleLocalite);

                                myCommand.ExecuteNonQuery();
                                MessageBox.Show("Enregistrement effectué avec succès");
                                this.Close();
                            }
                        }
                    }
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearClass1 clr1 = new ClearClass1();
            clr1.RecursiveClearTextBoxes1(this.panel2.Controls);
        }
               
    }
}
