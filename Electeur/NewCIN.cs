﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class NewCIN : Form
    {
        public NewCIN()
        {
            InitializeComponent();
        }
        string clause, clause1;
        bool verif = true;
        int CNI;
        Connection conn = new Connection();
        private void button1_Click(object sender, EventArgs e)
        {
            this.IsEmptyfield();
            if (String.IsNullOrEmpty(clause))
            {
                MessageBox.Show("veuillez remplir un champ au minimum");
                this.Focus();

            }
            else
            {
                string sCon = conn.connectdb();
                SqlConnection con = new SqlConnection(sCon);
                string str = "SET CONCAT_NULL_YIELDS_NULL OFF; SELECT NOM1 +'  '+ NOM2 +'  '+NOM3 AS NOM,NOM1_MERE+'  '+NOM2_MERE AS 'NOM DE LA MERE', SEXE ,DATEDENAISSANCE AS 'DATE DE NAISSANCE',CIN, DATEDEDEPOT AS 'DATE DE DELIVRANCE'FROM NewCIN where " + clause;
                con.Open();
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(str, con);
                SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sqlAdapter);
                con.Close();
                DataTable table = new DataTable();
                table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                sqlAdapter.Fill(table);
                if (table.Rows.Count > 0)
                {
                    dataGridView1.DataSource = table;
                }
                else
                {
                    dataGridView1.DataSource = null;
                }

            }

        }
        private void textBoxNom1_KeyPress(object sender, KeyPressEventArgs e)
        {
            int isNum = 0;
            if (e.KeyChar == ',' || e.KeyChar == (char)Keys.Back)
                e.Handled = false;
            else if (int.TryParse(e.KeyChar.ToString(), out isNum))
                e.Handled = true;
        }
        private void textBoxAncCNI_KeyPress(object sender, KeyPressEventArgs e)
        {
            int isNum = 0;
            if (e.KeyChar == ',' || e.KeyChar == (char)Keys.Back)
                e.Handled = false;
            else if (!int.TryParse(e.KeyChar.ToString(), out isNum))
                e.Handled = true;
        }
        public void IsEmptyfield()
        {
            clause = null;
            foreach (Control X in this.panel1.Controls)
            {

                if (X is TextBox || X is ComboBox)
                {
                    if (X.Tag.ToString() == "CIN")
                    {
                        if (X.Text.Trim().Length != 0)
                        {
                            if (string.IsNullOrEmpty(clause))
                            {
                                clause = clause + X.Tag + " = '" + X.Text.Trim() + "'";
                                //return clause;
                            }
                            else
                            {
                                clause = clause + " And " + X.Tag + " = '" + X.Text.Trim() + "'";
                                //return clause;
                            }
                        }
                    }
                    else
                    {
                        if (X.Text.Trim().Length != 0)
                        {
                            if (string.IsNullOrEmpty(clause))
                            {
                                clause = clause + X.Tag + " Like '%" + X.Text.Trim() + "%'";
                                //return clause;
                            }
                            else
                            {
                                clause = clause + " And " + X.Tag + " Like '%" + X.Text.Trim() + "%'";
                                //return clause;
                            }
                        }
                    }

                }
                else
                {
                    if (X is DateTimePicker)
                    {
                        if (X.Name == "dateTimePicker1")
                        {
                            if (string.IsNullOrEmpty(clause))
                            {
                                if (dateTimePicker1.Checked)
                                {
                                    string dat = dateTimePicker1.Value.ToString("yyyy-MM-dd");
                                    DateTime date4 = DateTime.Parse(dat);
                                    clause = clause + X.Tag + " Like '%" + dat + "%'";
                                }

                            }
                            else
                            {
                                if (dateTimePicker1.Checked)
                                {
                                    string dat = dateTimePicker1.Value.ToString("yyyy-MM-dd");
                                    DateTime date4 = DateTime.Parse(dat);
                                    clause = clause + " AND " + X.Tag + " Like '%" + dat + "%'";
                                }

                            }
                        }
                    }
                }
            }

        }
        public void selectcin()
        {
            int counter;
            clause1 = null;



            if (dataGridView1.Rows.Count > 0)
            {
                // Iterate through all the rows and sum up the appropriate columns. 
                for (counter = 0; counter < (dataGridView1.Rows.Count);
                    counter++)
                {
                    //dataGridView1.Rows.
                    if (dataGridView1.Rows[counter].Selected)
                    {
                        if (dataGridView1.Rows[counter].Cells["CIN"].Value
                        != null)
                        {
                            if (dataGridView1.Rows[counter].
                                Cells["CIN"].Value.ToString().Length != 0)
                            {
                                if (string.IsNullOrEmpty(clause1))
                                {
                                    clause1 = clause1 + int.Parse(dataGridView1.Rows[counter].
                                        Cells["CIN"].Value.ToString());
                                }
                                else
                                {
                                    clause1 = clause1 + "," + int.Parse(dataGridView1.Rows[counter].
                                    Cells["CIN"].Value.ToString());

                                }

                            }
                        }
                    }
                }

                
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            this.selectcin();
            CNI = Int32.Parse(clause1);
            string info = "New";
            SqlDataReader rd = null;
            string query = "select * from Electeur where NewCNI ="+CNI;
            string sCon = conn.connectdb();
            SqlConnection dbConn;
            dbConn = new SqlConnection(sCon);
            dbConn.Open();
            SqlCommand cmd = new SqlCommand(query, dbConn);
            rd = cmd.ExecuteReader();
            if (rd.Read())
            {
                MessageBox.Show("Cet electeur est déjà enregistré");

            }
            else
            {
                SaisieElecteur se = new SaisieElecteur(CNI, verif,info);
                se.MdiParent = this.ParentForm;
                se.Show();
                this.Hide();

            }
           
            



        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {

           // this.selectcin();
            //CNI = Int32.Parse(clause1);
            

        }

        private void NewCIN_Load(object sender, EventArgs e)
        {
            GetValue get = new GetValue();
            int Profil = get.getProfilID();
            if (Profil == 8)
            {
                button2.Enabled = false;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ClearClass1 clr = new ClearClass1();
            clr.RecursiveClearTextBoxes(this.panel1.Controls);
        }
    }
}

