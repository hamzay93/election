﻿namespace Electeur
{
    partial class historicModif
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridViewhist = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewhist)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Berlin Sans FB Demi", 14.25F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(48, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(200, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Historique des Actions";
            // 
            // dataGridViewhist
            // 
            this.dataGridViewhist.AllowUserToAddRows = false;
            this.dataGridViewhist.AllowUserToDeleteRows = false;
            this.dataGridViewhist.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewhist.Location = new System.Drawing.Point(52, 59);
            this.dataGridViewhist.Name = "dataGridViewhist";
            this.dataGridViewhist.ReadOnly = true;
            this.dataGridViewhist.Size = new System.Drawing.Size(564, 150);
            this.dataGridViewhist.TabIndex = 2;
            // 
            // historicModif
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSeaGreen;
            this.ClientSize = new System.Drawing.Size(646, 442);
            this.Controls.Add(this.dataGridViewhist);
            this.Controls.Add(this.label1);
            this.Name = "historicModif";
            this.Text = "historicModif";
            this.Load += new System.EventHandler(this.historicModif_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewhist)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridViewhist;
    }
}