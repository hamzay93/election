﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class RéaffectationMasse : Form
    {
        public RéaffectationMasse()
        {
            InitializeComponent();
        }
        Connection conn = new Connection();
        private void FillCV()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT CentreID, CentreVote FROM CentreVote";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBoxCV.ValueMember = "CentreID";
            comboBoxCV.DisplayMember = "CentreVote";
            comboBoxCV.DataSource = objDs.Tables[0];
        }
        private void FillBureau(int CentreID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT BureauID, NomBureau FROM Bureau WHERE CentreID =@CentreID";
            cmd.Parameters.AddWithValue("@CentreID", CentreID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxBV.ValueMember = "BureauID";
                comboBoxBV.DisplayMember = "NomBureau";
                comboBoxBV.DataSource = objDs.Tables[0];
            }
        }
        private void comboBoxCV_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxCV.SelectedValue.ToString() != "")
            {
                int CentreID = Convert.ToInt32(comboBoxCV.SelectedValue.ToString());
                FillBureau(CentreID);
                comboBoxBV.SelectedIndex = 0;
                IsEmptyfield();
                chargement();
            }
        }
        private void FillCV1()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT CentreID, CentreVote FROM CentreVote";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBoxCV1.ValueMember = "CentreID";
            comboBoxCV1.DisplayMember = "CentreVote";
            comboBoxCV1.DataSource = objDs.Tables[0];
        }
        private void FillBureau1(int CentreID1)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT BureauID, NomBureau FROM Bureau WHERE CentreID =@CentreID";
            cmd.Parameters.AddWithValue("@CentreID", CentreID1);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxBV1.ValueMember = "BureauID";
                comboBoxBV1.DisplayMember = "NomBureau";
                comboBoxBV1.DataSource = objDs.Tables[0];
            }
        }
        private void comboBoxCV1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxCV.SelectedValue.ToString() != "")
            {
                int CentreID1 = Convert.ToInt32(comboBoxCV1.SelectedValue.ToString());
                FillBureau1(CentreID1);
                comboBoxBV1.SelectedIndex = 0;
                
            }
        }
        string clause;
        public void IsEmptyfield()
        {
            if (comboBoxCV.Text.Trim().Length != 0)
            {
                int CentreID = Convert.ToInt32(comboBoxCV.SelectedValue.ToString());
                clause = "CentreID = " + CentreID;
            }
            /*else
            {
                int CentreID = Convert.ToInt32(comboBoxCV.SelectedValue.ToString());
                clause = "BureauID in (select BureauID from Bureau where CentreID=" + CentreID + ")";
            }*/

        }
        public void chargement()
        {
            if (String.IsNullOrEmpty(clause))
            {
                clause = "Nom1='%'";
            }
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            string str = "SELECT ElecteurID, Nom1 +'  '+ Nom2 +'  '+Nom3 AS NOM,NomMère1+'  '+NomMère2 AS 'NOM DE LA MERE',DOB AS 'DATE DE NAISSANCE',POB AS LIEU,CodeElecteur AS 'CODE ELECTEUR',CentreVote AS 'CENTRE DE VOTE' FROM ViewAffectation where " + clause+ " and ElecteurID not in (Select ElecteurID from Radiation where Statut='Actif')";
            con.Open();
            SqlDataAdapter sqlAdapter = new SqlDataAdapter(str, con);
            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sqlAdapter);
            con.Close();
            DataTable table = new DataTable();
            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
            sqlAdapter.Fill(table);
            dataGridView1.DataSource = table;
            this.dataGridView1.Columns["ElecteurID"].Visible = false;
            clause = "";
        }
        private void RéaffectationMasse_Load(object sender, EventArgs e)
        {
            FillCV();
            FillCV1();
            IsEmptyfield();
            chargement();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            
            string sCon = conn.connectdb();
            string updateCmd = "UPDATE Electeur SET CentreID =@CentreID,AgentModif=@AgentModif,DateModification=@DateModification where CentreID =(" + Convert.ToInt32(comboBoxCV.SelectedValue.ToString()) + ")";
            SqlConnection dbConn;
            dbConn = new SqlConnection(sCon);
            dbConn.Open();
            GetValue get = new GetValue();
            //DateTime dt = new DateTime();
            //dt.ToShortDateString();
            string dat = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            DateTime date4 = DateTime.Parse(dat);
            SqlCommand myCommand = new SqlCommand(updateCmd, dbConn);
            // Create parameters for the SqlCommand object
            // initialize with input-form field values
            myCommand.Parameters.AddWithValue("@CentreID", Convert.ToInt32(comboBoxCV1.SelectedValue.ToString()));

            myCommand.Parameters.AddWithValue("@AgentModif", get.getAgentModif());
            myCommand.Parameters.AddWithValue("@DateModification", dat);
            myCommand.ExecuteNonQuery();
            MessageBox.Show("Réaffectation au nouveau Centre de vote effectuée avec succès");
            RéaffectationMasse réaffect = new RéaffectationMasse();
            réaffect.MdiParent = this.ParentForm;
            réaffect.Show();
            this.Hide();
        }

        
    }
}
