﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class Motif : Form
    {
        public Motif()
        {
            InitializeComponent();
        }
        string elect, Name1;
        public Motif(string ID,string name)
        {
            elect = ID;
            Name1 = name;
            InitializeComponent();
            this.label2.Text = Name1.ToString().ToUpper();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!PanelDeces.Visible)
            {
                if (textBoxMotif.Text.Trim().Length == 0)
                {
                    MessageBox.Show("veuillez saisir le motif");
                    this.Focus();

                }
                else
                {
                    historique hist = new historique();
                    Connection cn = new Connection();
                    string sCon = cn.connectdb();
                    string insertCmd = "INSERT INTO Radiation(DateRadiation,ElecteurID,MotifRadiation,AgentID,Statut) VALUES (@DateRadiation,@ElecteurID,@MotifRadiation,@AgentID,@Statut)";
                    SqlConnection dbConn;
                    dbConn = new SqlConnection(sCon);
                    dbConn.Open();
                    //Login lg = new Login();
                    //string str;
                    string dat = DateTime.Now.ToString("dd/MM/yyyy");
                    DateTime date4 = DateTime.Parse(dat);
                    GetValue value = new GetValue();
                    //getAdress add = new getAdress();
                    SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                    // Create parameters for the SqlCommand object
                    // initialize with input-form field values
                    //myCommand.Parameters.AddWithValue("@ElecteurID","");
                    myCommand.Parameters.AddWithValue("@DateRadiation", date4);
                    myCommand.Parameters.AddWithValue("@ElecteurID", elect);
                    myCommand.Parameters.AddWithValue("@MotifRadiation", textBoxMotif.Text.ToUpper());
                    myCommand.Parameters.AddWithValue("@AgentID", value.getAgentID());
                    myCommand.Parameters.AddWithValue("@Statut", "Actif");
                    myCommand.ExecuteNonQuery();
                    int AGentID = value.getAgentID();
                    int IDElect = Int32.Parse(elect);
                    string action = "Radiation";
                    hist.addhistoric(AGentID, IDElect, action);
                    MessageBox.Show("Electeur radié avec succès");

                    Radiation rad = new Radiation();
                    rad.MdiParent = this.ParentForm;
                    rad.Show();
                    this.Hide();
                }
            }
            else
            {
                if ((textBoxLieu.Text.Trim().Length == 0) || (!dateTimePicker1.Checked) || (textBoxNum.Text.Trim().Length == 0))
                {
                    MessageBox.Show("veuillez remplir tous les champs");
                    this.Focus();

                }
                else
                {
                    historique hist = new historique();
                    Connection cn = new Connection();
                    string sCon = cn.connectdb();
                    string insertCmd = "INSERT INTO Radiation(DateRadiation,ElecteurID,MotifRadiation,AgentID,Statut) VALUES (@DateRadiation,@ElecteurID,@MotifRadiation,@AgentID,@Statut)";
                    string insertCmd1 = "INSERT INTO DECES(ElecteurID,Lieu,DATEDEDECES,NUMERO,AgentCréation) VALUES (@ElecteurID,@Lieu,@DATEDEDECES,@NUMERO,@AgentCréation)";
                    SqlConnection dbConn;
                    dbConn = new SqlConnection(sCon);
                    dbConn.Open();
                    //Login lg = new Login();
                    //string str;
                    DateTime Date1 = DateTime.Parse(dateTimePicker1.Text);
                    string dat = DateTime.Now.ToString("dd/MM/yyyy");
                    DateTime date4 = DateTime.Parse(dat);
                    GetValue value = new GetValue();
                    //getAdress add = new getAdress();
                    SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                    SqlCommand myCommand1 = new SqlCommand(insertCmd1, dbConn);
                    // Create parameters for the SqlCommand object
                    // initialize with input-form field values
                    //myCommand.Parameters.AddWithValue("@ElecteurID","");
                    myCommand.Parameters.AddWithValue("@DateRadiation", date4);
                    myCommand.Parameters.AddWithValue("@ElecteurID", elect);
                    myCommand.Parameters.AddWithValue("@MotifRadiation", "DECES");
                    myCommand.Parameters.AddWithValue("@AgentID", value.getAgentID());
                    myCommand.Parameters.AddWithValue("@Statut", "Actif");
                    myCommand1.Parameters.AddWithValue("@Lieu", textBoxLieu.Text);
                    myCommand1.Parameters.AddWithValue("@DATEDEDECES", Date1);
                    myCommand1.Parameters.AddWithValue("@NUMERO", textBoxNum.Text);
                    myCommand1.Parameters.AddWithValue("@ElecteurID", elect);
                    myCommand1.Parameters.AddWithValue("@AgentCréation", value.getAgentID());
                    myCommand.ExecuteNonQuery();
                    myCommand1.ExecuteNonQuery();
                    int AGentID = value.getAgentID();
                    int IDElect = Int32.Parse(elect);
                    string action = "Radiation";
                    hist.addhistoric(AGentID, IDElect, action);
                    MessageBox.Show("Electeur radié avec succès");

                    Radiation rad = new Radiation();
                    rad.MdiParent = this.ParentForm;
                    rad.Show();
                    this.Hide();
                }
                
            }
            
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem.ToString().Trim() == "DECES")
            {
                PanelDeces.Visible = true;
                textBoxMotif.Visible = false;
            }
            else
            {
                textBoxMotif.Visible = true;
                PanelDeces.Visible = false;
            }
        }
    }
}
