﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class testreport : Form
    {
        camembermultiple objRpt;
        Connection conn = new Connection();
        public testreport()
        {
            InitializeComponent();
        }

        private void testreport_Load(object sender, EventArgs e)
        {
             objRpt = new camembermultiple();

            SqlConnection con = new SqlConnection(conn.connectdb());
            string SQLlist = "SET CONCAT_NULL_YIELDS_NULL OFF; SELECT   * " +
                                 " FROM  View_res_detail";

            con.Open();
            SqlDataAdapter sqlAdapter = new SqlDataAdapter(SQLlist, con);
            SqlCommandBuilder commandBuilder1 = new SqlCommandBuilder(sqlAdapter);

            con.Close();
            DataTable dt = new DataTable();
            sqlAdapter.Fill(dt);
            if (dt.Rows.Count == 0)
            {
                MessageBox.Show("Aucun Edition pour ces critères");
            }
            else
            {
                objRpt.SetDataSource(dt);
                crystalReportViewer1.ReportSource = objRpt;
                crystalReportViewer1.Refresh();
            }
        }
    }
}
