﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Electeur
{
    class historique
    {
        public void addhistoric(int agent, int electeur, string action)
        {
            int AgentID = agent;
            int ElecteurID= electeur;
            string Action= action;
            Connection cn = new Connection();
            string sCon = cn.connectdb();
            string insertCmd = "INSERT INTO Historique(AgentID,ElecteurID,TypeAction,DateAction) VALUES (@AgentID,@ElecteurID,@TypeAction,@DateAction)";
            SqlConnection dbConn;
            dbConn = new SqlConnection(sCon);
            dbConn.Open();
            //Login lg = new Login();
            //string str;
            string dat = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            //DateTime date4 = DateTime.Parse(dat);
            GetValue value = new GetValue();
            //getAdress add = new getAdress();
            SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
            // Create parameters for the SqlCommand object
            // initialize with input-form field values
            //myCommand.Parameters.AddWithValue("@ElecteurID","");
            myCommand.Parameters.AddWithValue("@AgentID", AgentID);
            myCommand.Parameters.AddWithValue("@ElecteurID", ElecteurID);
            myCommand.Parameters.AddWithValue("@TypeAction", Action.ToUpper());
            myCommand.Parameters.AddWithValue("@DateAction", dat);
            myCommand.ExecuteNonQuery();
           
        }
        public void addhistoric(int agent, int electeur, string action, int i)
        {
            int AgentID = agent;
            int ElecteurID = electeur;
            string Action = action;
            Connection cn = new Connection();
            string sCon = cn.connectdb();
            string insertCmd = "INSERT INTO Historique(AgentID,ElecteurID,TypeAction,DateAction) VALUES (@AgentID,@ElecteurID,@TypeAction,@DateAction)";
            SqlConnection dbConn;
            dbConn = new SqlConnection(sCon);
            dbConn.Open();
            //Login lg = new Login();
            //string str;
            string dat = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            
            GetValue value = new GetValue();
            //getAdress add = new getAdress();
            SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
            // Create parameters for the SqlCommand object
            // initialize with input-form field values
            //myCommand.Parameters.AddWithValue("@ElecteurID","");
            myCommand.Parameters.AddWithValue("@AgentID", AgentID);
            myCommand.Parameters.AddWithValue("@ElecteurID", ElecteurID);
            myCommand.Parameters.AddWithValue("@TypeAction", Action.ToUpper());
            myCommand.Parameters.AddWithValue("@DateAction", dat);
            myCommand.ExecuteNonQuery();

        }
    }
}
