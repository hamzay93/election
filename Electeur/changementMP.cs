﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class changementMP : Form
    {
        public changementMP()
        {
            InitializeComponent();
        }
        Connection conn = new Connection();
        bool comp;
        bool champsvide;
        string Id;
        private void changementMP_Load(object sender, EventArgs e)
        {
            //this.FillAgent();
        }
        /*private void FillAgent()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT  NomAgent FROM Agent where Statut = 'Actif'";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBoxAgent.ValueMember = "AgentID";
            comboBoxAgent.DisplayMember = "NomAgent";
            comboBoxAgent.DataSource = objDs.Tables[0];
        }*/
        public int getAgentID(string AgentID)
        {
            //SqlDataReader rd = null;
            Id = AgentID;
            string query = "select AgentID from Agent where NomAgent='" + Id + "'";
            string sCon = conn.connectdb();
            SqlConnection dbConn;
            dbConn = new SqlConnection(sCon);
            dbConn.Open();
            SqlCommand cmd = new SqlCommand(query, dbConn);
            int result = ((int)cmd.ExecuteScalar());
            //SaisieElecteur se=new SaisieElecteur();
            int agentID = result;
            return agentID;

        }
        public void CompareMP()
        {
            if (textBox1.Text.Trim().Length == 0 || textBox2.Text.Trim().Length == 0 || textBox3.Text.Trim().Length == 0 || textBox4.Text.Trim().Length == 0)
            {
                champsvide =true;
            }
            else
            {
                champsvide = false;
                string name = textBox4.Text.Trim();
                string query = "select NomAgent from Agent where AgentID='" + this.getAgentID(name) + "' and Password ='" + textBox3.Text.Trim() + "'";
                string sCon = conn.connectdb();
                SqlConnection dbConn;
                SqlDataReader rd = null;
                dbConn = new SqlConnection(sCon);
                dbConn.Open();
                SqlCommand cmd = new SqlCommand(query, dbConn);
                rd = cmd.ExecuteReader();
                if (rd.Read())
                {
                    comp = true;
                }
                else
                {
                    comp = false;
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.CompareMP();
            if (!champsvide)
            {
                if (comp)
                {

                    if (textBox1.Text.Trim() == textBox2.Text.Trim())
                    {


                        try
                        {
                            string sCon = conn.connectdb();
                            int ID = this.getAgentID(textBox4.Text);
                            string updateCmd = "UPDATE Agent SET Password =@Password,AgentModif=@AgentModif,DateModif=@DateModif where AgentID =(" + ID + ")";
                            SqlConnection dbConn;
                            dbConn = new SqlConnection(sCon);
                            dbConn.Open();
                            GetValue value = new GetValue();
                            //DateTime dt = new DateTime();
                            //dt.ToShortDateString();
                            string dat = DateTime.Now.ToString("dd/MM/yyyy");
                            DateTime date4 = DateTime.Parse(dat);
                            SqlCommand myCommand = new SqlCommand(updateCmd, dbConn);
                            // Create parameters for the SqlCommand object
                            // initialize with input-form field values
                            myCommand.Parameters.AddWithValue("@Password", textBox1.Text.Trim());
                            myCommand.Parameters.AddWithValue("@AgentModif", value.getAgentModif());
                            myCommand.Parameters.AddWithValue("@DateModif", date4);
                            myCommand.ExecuteNonQuery();
                            MessageBox.Show("Mot de passe changé avec succès");
                            //this.Hide();

                        }
                        catch (Exception ex)
                        {

                        }
                    }

                    else
                    {
                        MessageBox.Show("veuillez vérifier que les mots soient identiques");
                        textBox1.Focus();
                    }
                }
                else
                {
                    MessageBox.Show("veuillez vérifier votre nom d'utilisateur et votre mot de passe");
                    textBox4.Focus();
                }
            }
            else
            {
                MessageBox.Show("veuillez remplir tous les champs");
                textBox4.Focus();
            }
        }
       
    }
}
