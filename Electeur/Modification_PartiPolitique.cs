﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class Modification_PartiPolitique : Form
    {
        string clause;
        public Modification_PartiPolitique()
        {
            InitializeComponent();
        }
        Connection conn = new Connection();
        private void Modification_PartiPolitique_Load(object sender, EventArgs e)
        {
            this.FillElection();
        }
        private void FillElection()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT ElectionID,ObjetElection from Election where Etat=1";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBoxElec.ValueMember = "ElectionID";
            comboBoxElec.DisplayMember = "ObjetElection";
            comboBoxElec.DataSource = objDs.Tables[0];

            comboBox1.ValueMember = "ElectionID";
            comboBox1.DisplayMember = "ObjetElection";
            comboBox1.DataSource = objDs.Tables[0];
        }

        private void button6_Click(object sender, EventArgs e)
        {
            

            this.IsEmptyfield();
            if (String.IsNullOrEmpty(clause))
            {
                MessageBox.Show("veuillez remplir un champ au minimum");
                this.Focus();

            }
            else
            {
                string sCon = conn.connectdb();
                SqlConnection con = new SqlConnection(sCon);
                string str = "SET CONCAT_NULL_YIELDS_NULL OFF; SELECT ID_parti, NomParti AS 'Partie Politique',ObjetElection as 'Election'  FROM View_PartiPolitique_Election where " + clause + "";
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(str, con);
                SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sqlAdapter);

                DataTable table = new DataTable();
                table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                con.Open();
                sqlAdapter.Fill(table);
                con.Close();
                dataGridView1.DataSource = table;
                if (dataGridView1.Rows.Count == 0)
                {

                    panel1.Visible = false;
                    label1.Visible = false;
                    MessageBox.Show("Aucun Partie Politique retrouvé pour ces critères");

                }
                else
                {
                    panel1.Visible = true;
                    label1.Visible = true;
                }

                this.dataGridView1.Columns["ID_parti"].Visible = false;
              



                clause = "";
            }
        }
        public void IsEmptyfield()
        {
            clause = null;
            foreach (Control X in this.panel7.Controls)
            {

                if (X is TextBox || X is ComboBox)
                {

                    if (X.Text.Trim().Length != 0)
                    {
                        if (string.IsNullOrEmpty(clause))
                        {
                            clause = clause + X.Tag + " Like '" + X.Text.Trim() + "%'";
                            //return clause;
                        }
                        else
                        {
                            clause = clause + " And " + X.Tag + " Like '" + X.Text.Trim() + "%'";
                            //return clause;
                        }
                    }


                }
              
            }
        }
    

        private void button2_Click(object sender, EventArgs e)
        {
            ClearClass1 clr = new ClearClass1();
            clr.RecursiveClearTextBoxes(this.panel7.Controls);
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {

            this.filldetails();
        }
        string clause3;
        private void filldetails()
        {

            this.selection();
            if (String.IsNullOrEmpty(clause3))
            {
                panel1.Visible = false;
                label1.Visible = false;

            }
            else
            {
                panel1.Visible = true;
                label1.Visible = true;
                string sCon = conn.connectdb();
                SqlConnection con = new SqlConnection(sCon);
                string str = "SELECT * FROM View_PartiPolitique_Election where ID_parti= " + clause3;
                SqlCommand cmd = new SqlCommand(str, con);
                con.Open();

                using (SqlDataReader read = cmd.ExecuteReader())

                    while (read.Read())
                    {
                        textBox1.Text = (read["NomParti"].ToString());
                        comboBox1.SelectedValue = (read["ELECTION_ID"].ToString());
                    }
            }

        }
        private void selection()
        {
            int counter3;
            clause3 = null;

            // Iterate through all the rows and sum up the appropriate columns. 
            for (counter3 = 0; counter3 < (dataGridView1.Rows.Count);
                counter3++)
            {
                //dataGridView1.Rows.
                if (dataGridView1.Rows[counter3].Selected)
                {
                    if (dataGridView1.Rows[counter3].Cells["ID_parti"].Value
                    != null)
                    {
                        if (dataGridView1.Rows[counter3].
                            Cells["ID_parti"].Value.ToString().Length != 0)
                        {
                            if (string.IsNullOrEmpty(clause3))
                            {
                                clause3 = clause3 + int.Parse(dataGridView1.Rows[counter3].
                                    Cells["ID_parti"].Value.ToString());
                            }
                            else
                            {
                                clause3 = clause3 + "," + int.Parse(dataGridView1.Rows[counter3].
                                Cells["ID_parti"].Value.ToString());

                            }

                        }
                    }


                }

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (textBox1.Text.Trim().Length == 0)
            {
                MessageBox.Show("veuillez saisir l'inttitulé de la Partie Politique");
                textBox1.Focus();
            }

            else
            {

                if (comboBox1.Text.Length == 0)
                {
                    MessageBox.Show("veuillez choisir l'election en question");
                }
                else
                {

                    //Connection cn = new Connection();
                    string insertCmd = "update Parti_Politique set NomParti=@NomParti,ELECTION_ID=@ELECTION_ID,UserModification=@UserModification,DateModification=@DateModification  where ID_parti=" + clause3 + "";
                    SqlConnection dbConn;
                    dbConn = new SqlConnection(conn.connectdb());
                    dbConn.Open();
                    Login lg = new Login();
                    string dat = DateTime.Now.ToString("yyyy-MM-dd");
                    GetValue value = new GetValue();
                    //getAdress add = new getAdress();
                    SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);


                    int ElectionID = Convert.ToInt32(comboBox1.SelectedValue.ToString());
                    myCommand.Parameters.AddWithValue("@ELECTION_ID", ElectionID);

                    //if (radioButton4.Checked)
                    //{
                    //    myCommand.Parameters.AddWithValue("@Etat", 1);
                    //}
                    //if (radioButton3.Checked)
                    //{
                    //    myCommand.Parameters.AddWithValue("@Etat", 0);
                    //}
                    myCommand.Parameters.AddWithValue("@NomParti", textBox1.Text.ToString());

                 
                    myCommand.Parameters.AddWithValue("@UserModification", value.getAgentModif());
                    myCommand.Parameters.AddWithValue("@DateModification", dat);
                    myCommand.ExecuteNonQuery();

                    MessageBox.Show("Modification avec succès");
                    textBox1.Clear();
                }

            }
            
        }
    }
}
