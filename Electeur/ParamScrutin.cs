﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class ParamScrutin : Form
    {
        public ParamScrutin()
        {
            InitializeComponent();
        }
        Connection conn = new Connection();
        private void button1_Click(object sender, EventArgs e)
        {
            if (textBoxNom1.Text.Trim().Length == 0)
            {
                MessageBox.Show("veuillez saisir le nom du candidat");
                textBoxNom1.Focus();
            }
            else
            {
                Connection cn = new Connection();
                string insertCmd = "INSERT INTO Candidat (Nom1,Nom2,Nom3,ElectionID,TourID,Partie,AgentID,DateCréation,DateModification,Statut) VALUES (@Nom1,@Nom2,@Nom3,@ElectionID,@TourID,@Partie,@AgentID,@DateCréation,@DateModification,@Statut)";
                SqlConnection dbConn;
                dbConn = new SqlConnection(cn.connectdb());
                dbConn.Open();
                
                string dat = DateTime.Now.ToString("dd/MM/yyyy");
                DateTime date4 = DateTime.Parse(dat);
                GetValue value = new GetValue();
                //getAdress add = new getAdress();
                SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                // Create parameters for the SqlCommand object
                // initialize with input-form field values
                //myCommand.Parameters.AddWithValue("@ElecteurID","");
                myCommand.Parameters.AddWithValue("@Nom1", textBoxNom1.Text);
                myCommand.Parameters.AddWithValue("@Nom2", textBoxNom2.Text);
                myCommand.Parameters.AddWithValue("@Nom3", textBoxNom3.Text);
                myCommand.Parameters.AddWithValue("@Partie", textBoxPartie.Text);
                myCommand.Parameters.AddWithValue("@ElectionID", Int32.Parse(comboBoxElec.SelectedValue.ToString()));
                myCommand.Parameters.AddWithValue("@TourID", Int32.Parse(comboBoxTour.SelectedValue.ToString()));
                myCommand.Parameters.AddWithValue("@Statut", "Actif");
                myCommand.Parameters.AddWithValue("@DateCréation", date4);
                myCommand.Parameters.AddWithValue("@DateModification", date4);                
                myCommand.Parameters.AddWithValue("@AgentID", value.getAgentID());
                myCommand.ExecuteNonQuery();

                MessageBox.Show("Ajoutée avec succès");
                textBoxNom1.Clear();
                textBoxNom2.Clear();
                textBoxNom3.Clear();
                textBoxPartie.Clear();
                
            }
        }

        private void ParamScrutin_Load(object sender, EventArgs e)
        {
            this.FillElection();
            this.FillTour();
        }
        private void FillElection()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT ElectionID,ObjetElection from Election where Etat=1";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBoxElec.ValueMember = "ElectionID";
            comboBoxElec.DisplayMember = "ObjetElection";
            comboBoxElec.DataSource = objDs.Tables[0];
        }
        private void FillTour()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT TourElectionID,ObjetTourElection from TourElection";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBoxElec.ValueMember = "ElectionID";
            comboBoxElec.DisplayMember = "ObjetElection";
            comboBoxElec.DataSource = objDs.Tables[0];
        }
    }
}
