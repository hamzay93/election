﻿namespace Electeur
{
    partial class ParamScrutin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.comboBoxElec = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.comboBoxTour = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxNom3 = new System.Windows.Forms.TextBox();
            this.textBoxNom2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxPartie = new System.Windows.Forms.TextBox();
            this.textBoxNom1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.comboBoxElec);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.comboBoxTour);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.textBoxNom3);
            this.panel1.Controls.Add(this.textBoxNom2);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.textBoxPartie);
            this.panel1.Controls.Add(this.textBoxNom1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(65, 56);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(950, 225);
            this.panel1.TabIndex = 0;
            // 
            // comboBoxElec
            // 
            this.comboBoxElec.FormattingEnabled = true;
            this.comboBoxElec.Location = new System.Drawing.Point(128, 94);
            this.comboBoxElec.Name = "comboBoxElec";
            this.comboBoxElec.Size = new System.Drawing.Size(245, 21);
            this.comboBoxElec.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.3F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Location = new System.Drawing.Point(49, 97);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 15);
            this.label5.TabIndex = 11;
            this.label5.Text = "Election:";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.button1.Location = new System.Drawing.Point(371, 165);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(233, 33);
            this.button1.TabIndex = 9;
            this.button1.Text = "Enrégistrer";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // comboBoxTour
            // 
            this.comboBoxTour.FormattingEnabled = true;
            this.comboBoxTour.Location = new System.Drawing.Point(128, 128);
            this.comboBoxTour.Name = "comboBoxTour";
            this.comboBoxTour.Size = new System.Drawing.Size(245, 21);
            this.comboBoxTour.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.3F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(49, 131);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 15);
            this.label3.TabIndex = 7;
            this.label3.Text = "Tour:";
            // 
            // textBoxNom3
            // 
            this.textBoxNom3.Location = new System.Drawing.Point(688, 26);
            this.textBoxNom3.Name = "textBoxNom3";
            this.textBoxNom3.Size = new System.Drawing.Size(245, 20);
            this.textBoxNom3.TabIndex = 5;
            // 
            // textBoxNom2
            // 
            this.textBoxNom2.Location = new System.Drawing.Point(408, 26);
            this.textBoxNom2.Name = "textBoxNom2";
            this.textBoxNom2.Size = new System.Drawing.Size(245, 20);
            this.textBoxNom2.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.3F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(49, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "Partie:";
            // 
            // textBoxPartie
            // 
            this.textBoxPartie.Location = new System.Drawing.Point(128, 60);
            this.textBoxPartie.Name = "textBoxPartie";
            this.textBoxPartie.Size = new System.Drawing.Size(245, 20);
            this.textBoxPartie.TabIndex = 2;
            // 
            // textBoxNom1
            // 
            this.textBoxNom1.Location = new System.Drawing.Point(128, 26);
            this.textBoxNom1.Name = "textBoxNom1";
            this.textBoxNom1.Size = new System.Drawing.Size(245, 20);
            this.textBoxNom1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.3F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(49, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Candidat";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Location = new System.Drawing.Point(90, 42);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(128, 20);
            this.label4.TabIndex = 9;
            this.label4.Text = "Ajout Candidat";
            // 
            // ParamScrutin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSeaGreen;
            this.ClientSize = new System.Drawing.Size(1096, 299);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.panel1);
            this.Name = "ParamScrutin";
            this.Text = "Candidat";
            this.Load += new System.EventHandler(this.ParamScrutin_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBoxNom1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxTour;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxNom3;
        private System.Windows.Forms.TextBox textBoxNom2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxPartie;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox comboBoxElec;
        private System.Windows.Forms.Label label5;
    }
}