﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class AjoutTypeReunion : Form
    {
        Connection conn = new Connection();
        public AjoutTypeReunion()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox2.Text.Trim().Length == 0)
            {
                MessageBox.Show("veuillez saisir l'inttitulé");
                textBox2.Focus();
            }
            else
            {
                //Connection cn = new Connection();
                string insertCmd = "INSERT INTO TypeReunion (ObjetTypeReunion,UserCreation,DateCreation,UserModification,DateModification) VALUES (@Objet,@UserCreation,@DateCreation,@UserModification,@DateModification)";
                SqlConnection dbConn;
                dbConn = new SqlConnection(conn.connectdb());
                dbConn.Open();
                Login lg = new Login();
                string dat = DateTime.Now.ToString("yyyy-MM-dd");
                DateTime date4 = DateTime.Parse(dat);
                GetValue value = new GetValue();
                //getAdress add = new getAdress();
                SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);

                myCommand.Parameters.AddWithValue("@Objet", textBox2.Text);
                myCommand.Parameters.AddWithValue("@UserCreation", value.getAgentCréation());
                myCommand.Parameters.AddWithValue("@DateCreation", dat);
                myCommand.Parameters.AddWithValue("@UserModification", value.getAgentCréation());
                myCommand.Parameters.AddWithValue("@DateModification", dat);
                myCommand.ExecuteNonQuery();

                MessageBox.Show("Ajoutée avec succès");
                textBox2.Clear();

            }
        }
    }
}
