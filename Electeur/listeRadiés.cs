﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class listeRadiés : Form
    {
        public listeRadiés()
        {
            InitializeComponent();
        }
        Connection conn = new Connection();
        private void listeRadiés_Load(object sender, EventArgs e)
        {
            this.filldata();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        public void filldata()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            string str = "SET CONCAT_NULL_YIELDS_NULL OFF; SELECT ElecteurID,Nom1 +'  '+ Nom2 +'  '+Nom3 AS NOM,NomMère1+'  '+NomMère2 AS 'NOM DE LA MERE', DateRadiation AS 'DATE RADIATION',MotifRadiation AS 'MOTIF RADIATION' FROM viewRadiation where Statut ='Actif'";
            con.Open();
            SqlDataAdapter sqlAdapter = new SqlDataAdapter(str, con);
            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sqlAdapter);
            con.Close();
            DataTable table = new DataTable();
            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
            sqlAdapter.Fill(table);
            if (table.Rows.Count > 0)
            {
                dataGridView1.DataSource = table;
                this.dataGridView1.Columns["ElecteurID"].Visible = false;
            }
            else
            {
                btnAnnuler.Enabled = false;
            }
        }

        private void btnAnnuler_Click(object sender, EventArgs e)
        {
             int counter;
            string clause1 = null;
            
            //adresseID = null;
            if (dataGridView1.Rows.Count > 0)
            {
                // Iterate through all the rows and sum up the appropriate columns. 
                for (counter = 0; counter < (dataGridView1.Rows.Count);
                    counter++)
                {
                    //dataGridView1.Rows.
                    if (dataGridView1.Rows[counter].Selected)
                    {
                        if (dataGridView1.Rows[counter].Cells["ElecteurID"].Value
                        != null)
                        {
                            if (dataGridView1.Rows[counter].
                                Cells["ElecteurID"].Value.ToString().Length != 0)
                            {
                                if (string.IsNullOrEmpty(clause1))
                                {
                                    clause1 = clause1 + int.Parse(dataGridView1.Rows[counter].
                                        Cells["ElecteurID"].Value.ToString());
                                }
                                else
                                {
                                    clause1 = clause1 + int.Parse(dataGridView1.Rows[counter].
                                    Cells["ElecteurID"].Value.ToString());

                                }

                            }
                        }
                    }
                }
            }
            else { }
            historique hist = new historique();
            //Connection cn = new Connection();
            string sCon = conn.connectdb();
            string updateCmd = "UPDATE RADIATION SET Statut ='Inactif' where ElecteurID = "+clause1;
            SqlConnection dbConn;
            dbConn = new SqlConnection(sCon);
            dbConn.Open();
            //Login lg = new Login();
            //string str;
            string dat = DateTime.Now.ToString("dd/MM/yyyy");
            DateTime date4 = DateTime.Parse(dat);
            GetValue value = new GetValue();
            //getAdress add = new getAdress();
            SqlCommand myCommand = new SqlCommand(updateCmd, dbConn);                    
            myCommand.ExecuteNonQuery();
            int AGentID = value.getAgentID();
            int IDElect = Int32.Parse(clause1);
            string action = "Annulation Radiation";
            hist.addhistoric(AGentID, IDElect, action);
            MessageBox.Show("Annulation effectuée avec succès");
            this.filldata();

            
        }
    }
}
