﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class CréationAgent : Form
    {
        public CréationAgent()
        {
            //textBoxUser.Validating += textBoxUser_Validating;
            InitializeComponent();
        }
        Connection conn = new Connection();
        private void textBoxUser_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //string errorMsg;
            if (textBoxUser.Text.Trim() != "")
            {
                //dateTimePicker2.Enabled = true;
                string sCon = conn.connectdb();
                SqlConnection con = new SqlConnection(sCon);
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT NomAgent FROM Agent WHERE NomAgent =@NomAgent";
                cmd.Parameters.AddWithValue("@NomAgent", textBoxUser.Text.Trim());
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                con.Open();
                dAdapter.Fill(objDs);
                con.Close();
                if (objDs.Tables[0].Rows.Count > 0)
                {
                    MessageBox.Show("Nom utilisateur non disponible");
                    textBoxUser.Focus();
                }
                else
                {
                    //Connection cn = new Connection();
                    //string sCon = @"Data Source=localhost\SQL2008; Initial Catalog=Electeur; Integrated Security = true";
                    string insertCmd = "INSERT INTO Agent(Nom1,Nom2,Nom3,NomAgent,AgentCréation,AgentModif,Password,DateCréation,DateModif,Statut,ProfilID) VALUES (@Nom1,@Nom2,@Nom3,@NomAgent,@AgentCréation,@AgentModif,@Password,@DateCréation,@DateModif,@Statut,@ProfilID)";
                    //SqlConnection dbConn;
                    con = new SqlConnection(sCon);
                    con.Open();
                    Login lg = new Login();
                    //DateTime dt = new DateTime();
                    DateTime dt = DateTime.Today;


                    GetValue value = new GetValue();
                    //getAdress add = new getAdress();
                    SqlCommand myCommand = new SqlCommand(insertCmd, con);
                    // Create parameters for the SqlCommand object
                    // initialize with input-form field values
                    //myCommand.Parameters.AddWithValue("@ElecteurID","");
                    myCommand.Parameters.AddWithValue("@Nom1", textBoxNom1.Text);
                    myCommand.Parameters.AddWithValue("@Nom2", textBoxNom2.Text);
                    myCommand.Parameters.AddWithValue("@Nom3", textBoxNom3.Text);
                    myCommand.Parameters.AddWithValue("@NomAgent", textBoxUser.Text);
                    myCommand.Parameters.AddWithValue("@AgentCréation", value.getAgentCréation());
                    myCommand.Parameters.AddWithValue("@AgentModif", value.getAgentModif());
                    myCommand.Parameters.AddWithValue("@Password", textBoxPwd.Text);
                    myCommand.Parameters.AddWithValue("@DateCréation", dt);
                    myCommand.Parameters.AddWithValue("@DateModif", dt);
                    myCommand.Parameters.AddWithValue("@Statut", "Actif");
                    if (radioButtonAdmin.Checked)
                    {
                        myCommand.Parameters.AddWithValue("@ProfilID", "1");
                    }
                    else
                    {
                        if (radioButtonStat.Checked)
                        {
                            myCommand.Parameters.AddWithValue("@ProfilID", "2");
                        }
                        else
                        {
                            if (radioButtonSaisie.Checked)
                            {
                                myCommand.Parameters.AddWithValue("@ProfilID", "3");
                            }
                            else
                            {
                                if (radioButtonApprobation.Checked)
                                {
                                    myCommand.Parameters.AddWithValue("@ProfilID", "4");
                                }
                                else
                                {
                                    if (radioButtonValidation.Checked)
                                    {
                                        myCommand.Parameters.AddWithValue("@ProfilID", "5");
                                    }
                                    else
                                    {
                                        if (radioButtonAffection.Checked)
                                        {
                                            myCommand.Parameters.AddWithValue("@ProfilID", "7");
                                        }
                                        else
                                        {
                                            if (radioButtonRadiation.Checked)
                                            {
                                                myCommand.Parameters.AddWithValue("@ProfilID", "6");
                                            }
                                            else
                                            {
                                                if (radioButtonAff.Checked)
                                                {
                                                    myCommand.Parameters.AddWithValue("@ProfilID", "8");
                                                }
                                                else
                                                {
                                                    if (radioButtonImp.Checked)
                                                    {
                                                        myCommand.Parameters.AddWithValue("@ProfilID", "9");
                                                    }
                                                    else
                                                    {
                                                        if (radioButtonRet.Checked)
                                                        {
                                                            myCommand.Parameters.AddWithValue("@ProfilID", "12");
                                                        }


                                                    }

                                                }


                                            }
                                           
                                        }
                                    }
                                }

                            }

                        }
                    }
                    myCommand.ExecuteNonQuery();
                    MessageBox.Show("Utilisateur ajouté avec succès");
                    RecursiveClearTextBoxes(this.Controls);
                }


            }

            else
            {
                MessageBox.Show("Veuillez choisir un nom d'utilisateur");
                textBoxUser.Focus();
            }

            
        }
        private void RecursiveClearTextBoxes(Control.ControlCollection cc)
        {
            foreach (Control ctrl in cc)
            {
                TextBox tb = ctrl as TextBox;
                if (tb != null)
                    tb.Clear();
                else
                    RecursiveClearTextBoxes(ctrl.Controls);
            }
        }

        private void CréationAgent_Load(object sender, EventArgs e)
        {

        }

        private void radioButtonAffection_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
