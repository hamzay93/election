﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class Consultation : Form
    {
        public Consultation()
        {
            InitializeComponent();
        }
        Connection conn = new Connection();
        private void FillCV()
        {
            string sCon = conn.connectdb(); ;
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT CentreID, ( CONVERT(Nvarchar(50), CodeCentre)) + ' - '+CentreVote as CentreVote FROM CentreVote where Actif = 1 ORDER BY CodeCentre";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBoxCV.ValueMember = "CentreID";
            comboBoxCV.DisplayMember = "CentreVote";
            comboBoxCV.DataSource = objDs.Tables[0];
        }
        private void FillBureau(int CentreID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT BureauID, ( CONVERT(Nvarchar(50), CodeBureau)) + ' - '+NomBureau as NomBureau FROM View_Lieu_vote WHERE CentreID =@CentreID ORDER BY CodeCentre,OrdreCentre";
            cmd.Parameters.AddWithValue("@CentreID", CentreID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxBV.ValueMember = "BureauID";
                comboBoxBV.DisplayMember = "NomBureau";
                comboBoxBV.DataSource = objDs.Tables[0];
            }
        }
        private void comboBoxCV_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxCV.SelectedValue.ToString() != "")
            {
                int CentreID = Convert.ToInt32(comboBoxCV.SelectedValue.ToString());
                FillBureau(CentreID);
                comboBoxBV.SelectedIndex = 0;
            }
        }
        string clause,clauseCV;
        //string clause1 = null;
        public void IsEmptyfield()
        {
            if (comboBoxBV.Text.Trim().Length != 0)
            {
                int BureauID = Convert.ToInt32(comboBoxBV.SelectedValue.ToString());
                clause = "BureauID = " + BureauID;
            }
            else
            {
                if (comboBoxCV.Text.Trim().Length != 0)
                {
                    int CentreID = Convert.ToInt32(comboBoxCV.SelectedValue.ToString());
                    /*if (String.IsNullOrEmpty(clauseCV))
                    {
                        
                    }*/
                    clause = "CentreID =" + CentreID;
                    //clause = "BureauID in (select BureauID from Bureau where CentreID=" + CentreID + ")";
                }
                else
                {
                    clause = "CentreID like '%'";
                }
                
            }
           
        }

        private void Consultation_Load(object sender, EventArgs e)
        {
            FillCV();
        }
        private void dataGridView1_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            if (e.Column.Name == "ElecteurID")
                dataGridView1.Columns["ElecteurID"].Visible = false;
        }

        /*public  listBureauCentre()
        {
            string sCon = @"Data Source=localhost\SQL2008; Initial Catalog=Electeur; Integrated Security = true";
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT BureauID, FROM Bureau WHERE CentreID =@CentreID";
            int CentreID = Convert.ToInt32(comboBoxCV.SelectedValue.ToString());
            cmd.Parameters.AddWithValue("@CentreID", CentreID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            string str=null;
            for (int i = 0; i <= (objDs.Tables[0].Rows.Count); i++)
            {
                str= objDs.Tables[0].Rows[i].["BureauID"];
            }
            
            
        }*/

        private void button3_Click(object sender, EventArgs e)
        {
            IsEmptyfield();
            if (String.IsNullOrEmpty(clause))
            {
                clause = "CentreID='%'";
            }
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            string str = "SET CONCAT_NULL_YIELDS_NULL OFF;SELECT  Nom1 +'  '+ Nom2 +'  '+Nom3 AS NOM,NomMère1+'  '+NomMère2 AS 'NOM DE LA MERE',DOB AS 'DATE DE NAISSANCE',POB AS LIEU,AncienCNI AS 'ANCIENNE CNI',DateAncienCNI AS 'DATE DE DELIVRANCE',NEWCNI AS 'NOUVELLE CNI',DateNewCNI AS 'DATE DE DELIVRANCE', NomBureau AS 'BUREAU',CodeBureau AS NuméroBureau,CentreVote AS 'CENTRE DE VOTE' FROM View_Affect where " + clause + "  and ElecteurID not in(select ElecteurID from Radiation where statut='Actif') ORDER BY Nom1,Nom2,Nom3";
            con.Open();
            SqlDataAdapter sqlAdapter = new SqlDataAdapter(str, con);
            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sqlAdapter);
            con.Close();
            DataTable table = new DataTable();
            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
            sqlAdapter.Fill(table);
            dataGridView1.DataSource = table;
            //this.dataGridView1.Columns["ElecteurID"].Visible = false;
            clause = "";
            int total = dataGridView1.RowCount;
            label2.Text = total.ToString();
            //clause1 = "";
        }

        
    }
}
