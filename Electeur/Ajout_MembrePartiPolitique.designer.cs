﻿namespace Electeur
{
    partial class Ajout_MembrePartiPolitique
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label6 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.comboBox_parti = new System.Windows.Forms.ComboBox();
            this.comboBox_Election = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.n3 = new System.Windows.Forms.TextBox();
            this.n2 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.n1 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label6.Location = new System.Drawing.Point(129, 103);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(290, 20);
            this.label6.TabIndex = 15;
            this.label6.Text = "Ajout Membre d\'une Parti_Politique";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.comboBox_parti);
            this.panel2.Controls.Add(this.comboBox_Election);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.button2);
            this.panel2.Controls.Add(this.n3);
            this.panel2.Controls.Add(this.n2);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.n1);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Location = new System.Drawing.Point(104, 117);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(950, 225);
            this.panel2.TabIndex = 14;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.button1.Location = new System.Drawing.Point(604, 166);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(132, 33);
            this.button1.TabIndex = 14;
            this.button1.Text = "Annuler";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // comboBox_parti
            // 
            this.comboBox_parti.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_parti.FormattingEnabled = true;
            this.comboBox_parti.Location = new System.Drawing.Point(128, 60);
            this.comboBox_parti.Name = "comboBox_parti";
            this.comboBox_parti.Size = new System.Drawing.Size(245, 21);
            this.comboBox_parti.TabIndex = 13;
            // 
            // comboBox_Election
            // 
            this.comboBox_Election.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Election.FormattingEnabled = true;
            this.comboBox_Election.Location = new System.Drawing.Point(128, 94);
            this.comboBox_Election.Name = "comboBox_Election";
            this.comboBox_Election.Size = new System.Drawing.Size(245, 21);
            this.comboBox_Election.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.3F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label7.Location = new System.Drawing.Point(49, 97);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 15);
            this.label7.TabIndex = 11;
            this.label7.Text = "Election:";
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold);
            this.button2.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.button2.Location = new System.Drawing.Point(408, 166);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(165, 33);
            this.button2.TabIndex = 9;
            this.button2.Text = "Enrégistrer";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // n3
            // 
            this.n3.Location = new System.Drawing.Point(688, 26);
            this.n3.Name = "n3";
            this.n3.Size = new System.Drawing.Size(245, 20);
            this.n3.TabIndex = 5;
            // 
            // n2
            // 
            this.n2.Location = new System.Drawing.Point(408, 26);
            this.n2.Name = "n2";
            this.n2.Size = new System.Drawing.Size(245, 20);
            this.n2.TabIndex = 4;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.3F, System.Drawing.FontStyle.Bold);
            this.label9.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label9.Location = new System.Drawing.Point(49, 63);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 15);
            this.label9.TabIndex = 3;
            this.label9.Text = "Partie:";
            // 
            // n1
            // 
            this.n1.Location = new System.Drawing.Point(128, 26);
            this.n1.Name = "n1";
            this.n1.Size = new System.Drawing.Size(245, 20);
            this.n1.TabIndex = 1;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.3F, System.Drawing.FontStyle.Bold);
            this.label10.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label10.Location = new System.Drawing.Point(49, 29);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(37, 15);
            this.label10.TabIndex = 0;
            this.label10.Text = "Nom";
            // 
            // Ajout_MembrePartiPolitique
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSeaGreen;
            this.ClientSize = new System.Drawing.Size(1128, 529);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.panel2);
            this.Name = "Ajout_MembrePartiPolitique";
            this.Text = "Ajout_MembrePartiPolitique";
            this.Load += new System.EventHandler(this.Ajout_MembrePartiPolitique_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox comboBox_parti;
        private System.Windows.Forms.ComboBox comboBox_Election;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox n3;
        private System.Windows.Forms.TextBox n2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox n1;
        private System.Windows.Forms.Label label10;
    }
}