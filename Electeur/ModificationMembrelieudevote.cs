﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class ModificationMembrelieudevote : Form
    {
        string clause;

        Connection conn = new Connection();
        GetValue get = new GetValue();

        public ModificationMembrelieudevote()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {

            if (textBox1.Text.Trim().Length == 0 || textBox2.Text.Trim().Length == 0 ||  textBox4.Text.Trim().Length == 0)
            {
                MessageBox.Show("veuillez saisir le lieu ou le nom complet");
                textBox1.Focus(); textBox2.Focus(); textBox3.Focus(); textBox4.Focus();
            }
            else
            {
                if (comboBox5.Text.Length ==0)
                {
                    MessageBox.Show("Veuillez choisir de quel fonction il/elle occupera");
                }
                else
                {

                    if (comboBox4.Text.Length ==0)
                    {
                        MessageBox.Show("Veuillez choisir dans quel bureau de vote");
                    }
                    else
                    {

                        if (comboBox1.Text.Length ==0)
                        {
                            MessageBox.Show("Veuillez choisir le sexe de la personne");
                        }
                        else
                        {

                            if (comboBox2.Text.Length ==0)
                            {
                                MessageBox.Show("Veuillez choisir dans quel centre de vote");
                            }

                            else
                            {

                                if (comboBox3.Text.Length ==0)
                                {
                                    MessageBox.Show("Veuillez choisir sur quel election");
                                }
                                else
                                {

                                    //Connection cn = new Connection();
                                    string insertCmd = "update membreLieuVoteID set RégionID=@RégionID,nom1=@nom1,nom2=@nom2,nom3=@nom3,DOB=@DOB,POB=@POB,Sexe=@Sexe,FonctionID=@FonctionID,ElectionID=@ElectionID,CentreID=@CentreID,BureauID=@BureauID,UserModification=@UserModification,DateModification=@DateModification where membreLieuVoteID=" + clause3 + "";
                                    SqlConnection dbConn;
                                    dbConn = new SqlConnection(conn.connectdb());
                                    dbConn.Open();
                                    Login lg = new Login();
                                    string dat = DateTime.Now.ToString("yyyy-MM-dd");
                                    DateTime datedn = DateTime.Parse(dateTimePicker1.Value.ToString("yyyy-MM-dd"));

                                    GetValue value = new GetValue();
                                    //getAdress add = new getAdress();
                                    SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                                    int fonctionID = Convert.ToInt32(comboBox5.SelectedValue.ToString());
                                        myCommand.Parameters.AddWithValue("@FonctionID", fonctionID);
                                      int BureauID = Convert.ToInt32(comboBox4.SelectedValue.ToString());
                                        myCommand.Parameters.AddWithValue("@BureauID", BureauID);
                                   
                                        int ElectionID = Convert.ToInt32(comboBox3.SelectedValue.ToString());
                                        myCommand.Parameters.AddWithValue("@ElectionID", ElectionID);
                                  
                                        int CentreID = Convert.ToInt32(comboBox2.SelectedValue.ToString());
                                        myCommand.Parameters.AddWithValue("@CentreID", CentreID);

                                        int RégionID = Convert.ToInt32(comboBox12.SelectedValue.ToString());
                                        myCommand.Parameters.AddWithValue("@RégionID", RégionID);


                                        myCommand.Parameters.AddWithValue("@nom1", textBox4.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@nom2", textBox1.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@nom3", textBox2.Text.ToUpper());

                                    myCommand.Parameters.AddWithValue("@POB", textBox3.Text);

                                    myCommand.Parameters.AddWithValue("@DOB", datedn);
                                    myCommand.Parameters.AddWithValue("@Sexe", comboBox1.SelectedItem.ToString());
                                   
                                    myCommand.Parameters.AddWithValue("@UserModification", value.getAgentCréation());
                                    myCommand.Parameters.AddWithValue("@DateModification", dat);
                                    myCommand.ExecuteNonQuery();

                                    MessageBox.Show("Modification avec succès");
                                    textBox1.Clear(); textBox2.Clear(); textBox3.Clear(); textBox4.Clear();
                                }
                            }
                        }
                    }
                }

            }
        }
        private void Fillelection()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT ElectionID,ObjetElection FROM Election";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBox8.ValueMember = "ElectionID";
            comboBox8.DisplayMember = "ObjetElection";
            comboBox8.DataSource = objDs.Tables[0];
        }
        private void Fillelection2()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT ElectionID,ObjetElection FROM Election";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBox3.ValueMember = "ElectionID";
            comboBox3.DisplayMember = "ObjetElection";
            comboBox3.DataSource = objDs.Tables[0];
        }
        private void Fillcv(int RégionID)
        {
            if (RégionID == 1)
            {
                string sCon = conn.connectdb();
                SqlConnection con = new SqlConnection(sCon);
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT CentreID,CentreVote FROM   dbo.Arrondissement INNER JOIN"
                          + " dbo.CentreVote ON dbo.Arrondissement.ArrondisID = dbo.CentreVote.ArrondisID INNER JOIN"
                         + " dbo.Commune ON dbo.Arrondissement.CommuneID = dbo.Commune.CommuneID INNER JOIN"
                         + " dbo.Région ON dbo.Commune.RégionID = dbo.Région.RégionID where dbo.Région.RégionID=" + RégionID;
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                con.Open();
                dAdapter.Fill(objDs);
                con.Close();
                comboBox9.ValueMember = "CentreID";
                comboBox9.DisplayMember = "CentreVote";
                comboBox9.DataSource = objDs.Tables[0];
            }
            else if (RégionID == 9)
            {
                MessageBox.Show("SANAA N'EST PAS ENCORE OPERATIONNEL");
            }
            else
            {
                string sCon = conn.connectdb();
                SqlConnection con = new SqlConnection(sCon);
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT CentreID,CentreVote FROM  dbo.CentreVote INNER JOIN"
                      + " dbo.Localité ON dbo.CentreVote.LocalitéID = dbo.Localité.LocalitéID INNER JOIN"
                      + " dbo.Région ON dbo.Localité.RégionID = dbo.Région.RégionID  where dbo.Région.RégionID=" + RégionID;
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                con.Open();
                dAdapter.Fill(objDs);
                con.Close();
                comboBox9.ValueMember = "CentreID";
                comboBox9.DisplayMember = "CentreVote";
                comboBox9.DataSource = objDs.Tables[0];
            }
        }
        private void Fillcv2(int RégionID)
        {
            if (RégionID == 1)
            {
                string sCon = conn.connectdb();
                SqlConnection con = new SqlConnection(sCon);
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT CentreID,CentreVote FROM   dbo.Arrondissement INNER JOIN"
                          + " dbo.CentreVote ON dbo.Arrondissement.ArrondisID = dbo.CentreVote.ArrondisID INNER JOIN"
                         + " dbo.Commune ON dbo.Arrondissement.CommuneID = dbo.Commune.CommuneID INNER JOIN"
                         + " dbo.Région ON dbo.Commune.RégionID = dbo.Région.RégionID where dbo.Région.RégionID=" + RégionID;
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                con.Open();
                dAdapter.Fill(objDs);
                con.Close();
                comboBox2.ValueMember = "CentreID";
                comboBox2.DisplayMember = "CentreVote";
                comboBox2.DataSource = objDs.Tables[0];
            }
            else if (RégionID == 9)
            {
                MessageBox.Show("SANAA N'EST PAS ENCORE OPERATIONNEL");
            }
            else
            {
                string sCon = conn.connectdb();
                SqlConnection con = new SqlConnection(sCon);
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT CentreID,CentreVote FROM  dbo.CentreVote INNER JOIN"
                      + " dbo.Localité ON dbo.CentreVote.LocalitéID = dbo.Localité.LocalitéID INNER JOIN"
                      + " dbo.Région ON dbo.Localité.RégionID = dbo.Région.RégionID  where dbo.Région.RégionID=" + RégionID;
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                con.Open();
                dAdapter.Fill(objDs);
                con.Close();
                comboBox2.ValueMember = "CentreID";
                comboBox2.DisplayMember = "CentreVote";
                comboBox2.DataSource = objDs.Tables[0];
            }
        }
        private void Fillbv(int centreID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT BureauID,NomBureau FROM Bureau where CentreID=" + centreID;
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBox7.ValueMember = "BureauID";
            comboBox7.DisplayMember = "NomBureau";
            comboBox7.DataSource = objDs.Tables[0];
        }
        private void Fillbv2(int centreID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT BureauID,NomBureau FROM Bureau where CentreID=" + centreID;
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBox4.ValueMember = "BureauID";
            comboBox4.DisplayMember = "NomBureau";
            comboBox4.DataSource = objDs.Tables[0];
        }
        private void Fillfonction()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT FonctionID,ObjetFonction FROM Fonction";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBox6.ValueMember = "FonctionID";
            comboBox6.DisplayMember = "ObjetFonction";
            comboBox6.DataSource = objDs.Tables[0];
        }
        private void Fillfonction2()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT FonctionID,ObjetFonction FROM Fonction";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBox5.ValueMember = "FonctionID";
            comboBox5.DisplayMember = "ObjetFonction";
            comboBox5.DataSource = objDs.Tables[0];
        }
        private void ModificationMembrelieudevote_Load(object sender, EventArgs e)
        {
            Fillfonction2(); Fillfonction();
            FillRégion(); FillRégion2();
            Fillfonction2(); Fillfonction2();
           
            Fillelection(); Fillelection2();
        }
        private void FillRégion()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT RégionID,NomRégion FROM Région";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBox11.ValueMember = "RégionID";
            comboBox11.DisplayMember = "NomRégion";
            comboBox11.DataSource = objDs.Tables[0];
        }
        private void FillRégion2()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT RégionID,NomRégion FROM Région";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBox12.ValueMember = "RégionID";
            comboBox12.DisplayMember = "NomRégion";
            comboBox12.DataSource = objDs.Tables[0];
        }
        private void button1_Click(object sender, EventArgs e)
        {
            ClearClass1 clr = new ClearClass1();
            clr.RecursiveClearTextBoxes(this.panel8.Controls);
        }
        public void IsEmptyfield()
        {
            clause = null;
            foreach (Control X in this.panel8.Controls)
            {

                if (X is TextBox || X is ComboBox)
                {

                    if (X.Text.Trim().Length != 0)
                    {
                        if (string.IsNullOrEmpty(clause))
                        {
                            clause = clause + X.Tag + " Like '%" + X.Text.Trim() + "%'";
                            //return clause;
                        }
                        else
                        {
                            clause = clause + " And " + X.Tag + " Like '%" + X.Text.Trim() + "%'";
                            //return clause;
                        }
                    }


                }
                else
                {
                    if (X is DateTimePicker)
                    {
                    
                        if (X.Name == "dateTimePicker4")
                        {
                            if (string.IsNullOrEmpty(clause))
                            {
                                if (dateTimePicker4.Checked)
                                {
                                    string dat = dateTimePicker1.Value.ToString("yyyy-MM-dd");
                                    DateTime date4 = DateTime.Parse(dat);
                                    clause = clause + X.Tag + " Like '%" + dat + "%'";
                                }

                            }
                            else
                            {
                                if (dateTimePicker4.Checked)
                                {
                                    string dat = dateTimePicker1.Value.ToString("yyyy-MM-dd");
                                    DateTime date4 = DateTime.Parse(dat);
                                    clause = clause + " AND " + X.Tag + " Like '%" + dat + "%'";
                                }

                            }
                        }
                    }
                }


            }

        }
        private void button8_Click(object sender, EventArgs e)
        {


            this.IsEmptyfield();
            if (String.IsNullOrEmpty(clause))
            {
                MessageBox.Show("veuillez remplir un champ au minimum");
                this.Focus();

            }
            else
            {
                string sCon = conn.connectdb();
                SqlConnection con = new SqlConnection(sCon);
                string str = "SET CONCAT_NULL_YIELDS_NULL OFF; SELECT membreLieuVoteID,nom1+' '+nom2+' '+nom3 as 'NOM',DOB as 'DATE DE NAISSANCE',POB as 'LIEU DE NAISSANCE',CentreVote as 'CENTRE DE VOTE',NomBureau as 'BUREAU de VOTE', ObjetElection AS 'Intitulé Election',ObjetFonction as 'Fonction' FROM View_MembreLieuVote where " + clause + "";
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(str, con);
                SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sqlAdapter);

                DataTable table = new DataTable();
                table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                con.Open();
                sqlAdapter.Fill(table);
                con.Close();
                dataGridView1.DataSource = table;
                if (dataGridView1.Rows.Count == 0)
                {

                    panel1.Visible = false;
                    label1.Visible = false;
                    MessageBox.Show("Aucun Membre de Lieu de Vote retrouvé pour ces critères");

                }
                else
                {
                    panel1.Visible = true;
                    label1.Visible = true;
                }

                this.dataGridView1.Columns["membreLieuVoteID"].Visible = false;



                clause = "";
            }
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {

            this.filldetails();
        }
        string clause3;
        private void filldetails()
        {

            this.selection();
            if (String.IsNullOrEmpty(clause3))
            {
                panel1.Visible = false;
                label1.Visible = false;

            }
            else
            {
                panel1.Visible = true;
                label1.Visible = true;
                string sCon = conn.connectdb();
                SqlConnection con = new SqlConnection(sCon);
                string str = "SELECT * FROM View_MembreLieuVote where membreLieuVoteID= " + clause3;
                SqlCommand cmd = new SqlCommand(str, con);
                con.Open();

                using (SqlDataReader read = cmd.ExecuteReader())

                    while (read.Read())
                    {
                        textBox1.Text = (read["nom2"].ToString());
                        textBox2.Text = (read["nom3"].ToString());
                        textBox4.Text = (read["nom1"].ToString());
                        textBox3.Text = (read["POB"].ToString());

                        int colIndex3 = (read.GetOrdinal("DOB"));
                        if (!read.IsDBNull(colIndex3))
                        {
                            dateTimePicker1.Value = (read.GetDateTime(colIndex3));
                        }
                        comboBox12.SelectedValue = (read["RégionID"].ToString());
                        comboBox2.SelectedValue = (read["CentreID"].ToString());
                        comboBox4.SelectedValue = (read["BureauID"].ToString());
                        comboBox5.SelectedValue = (read["FonctionID"].ToString());
                       
                        comboBox3.SelectedValue = (read["ElectionID"].ToString());
                       
                        comboBox1.SelectedItem = (read["Sexe"].ToString());

                    }
            }

        }
        private void selection()
        {
            int counter3;
            clause3 = null;

            // Iterate through all the rows and sum up the appropriate columns. 
            for (counter3 = 0; counter3 < (dataGridView1.Rows.Count);
                counter3++)
            {
                //dataGridView1.Rows.
                if (dataGridView1.Rows[counter3].Selected)
                {
                    if (dataGridView1.Rows[counter3].Cells["membreLieuVoteID"].Value
                    != null)
                    {
                        if (dataGridView1.Rows[counter3].
                            Cells["membreLieuVoteID"].Value.ToString().Length != 0)
                        {
                            if (string.IsNullOrEmpty(clause3))
                            {
                                clause3 = clause3 + int.Parse(dataGridView1.Rows[counter3].
                                    Cells["membreLieuVoteID"].Value.ToString());
                            }
                            else
                            {
                                clause3 = clause3 + "," + int.Parse(dataGridView1.Rows[counter3].
                                Cells["membreLieuVoteID"].Value.ToString());

                            }

                        }
                    }


                }

            }
        }

        private void comboBox9_SelectedIndexChanged(object sender, EventArgs e)
        {
            int centreID = Convert.ToInt32(comboBox9.SelectedValue.ToString());
            Fillbv(centreID);
            comboBox7.SelectedIndex = 0;
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            int centreID = Convert.ToInt32(comboBox2.SelectedValue.ToString());
            Fillbv2(centreID);
            comboBox4.SelectedIndex = 0;
        }

        private void comboBox11_SelectedIndexChanged(object sender, EventArgs e)
        {
            int RégionID = Convert.ToInt32(comboBox11.SelectedValue.ToString());
            Fillcv(RégionID);
            comboBox9.SelectedIndex = 0;
        }

        private void comboBox12_SelectedIndexChanged(object sender, EventArgs e)
        {
            int RégionID = Convert.ToInt32(comboBox12.SelectedValue.ToString());
            Fillcv2(RégionID);
            comboBox2.SelectedIndex = 0;
        }
    }
}
