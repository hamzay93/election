﻿namespace Electeur
{
    partial class AjoutConvocation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label38 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label37 = new System.Windows.Forms.Label();
            this.button10 = new System.Windows.Forms.Button();
            this.comboBox12 = new System.Windows.Forms.ComboBox();
            this.dateTimePicker5 = new System.Windows.Forms.DateTimePicker();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.panel9.SuspendLayout();
            this.SuspendLayout();
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.BackColor = System.Drawing.Color.LightSeaGreen;
            this.label38.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold);
            this.label38.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label38.Location = new System.Drawing.Point(84, 34);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(163, 23);
            this.label38.TabIndex = 15;
            this.label38.Text = "AJOUT Convocation";
            // 
            // panel9
            // 
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel9.Controls.Add(this.label37);
            this.panel9.Controls.Add(this.button10);
            this.panel9.Controls.Add(this.comboBox12);
            this.panel9.Controls.Add(this.dateTimePicker5);
            this.panel9.Controls.Add(this.label40);
            this.panel9.Controls.Add(this.label41);
            this.panel9.Controls.Add(this.textBox16);
            this.panel9.Location = new System.Drawing.Point(71, 50);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(351, 269);
            this.panel9.TabIndex = 14;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.3F, System.Drawing.FontStyle.Bold);
            this.label37.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label37.Location = new System.Drawing.Point(39, 153);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(59, 15);
            this.label37.TabIndex = 39;
            this.label37.Text = "Election";
            // 
            // button10
            // 
            this.button10.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold);
            this.button10.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.button10.Location = new System.Drawing.Point(200, 224);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(114, 38);
            this.button10.TabIndex = 19;
            this.button10.Text = "Enregistrer";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // comboBox12
            // 
            this.comboBox12.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox12.FormattingEnabled = true;
            this.comboBox12.Location = new System.Drawing.Point(117, 147);
            this.comboBox12.Name = "comboBox12";
            this.comboBox12.Size = new System.Drawing.Size(197, 21);
            this.comboBox12.TabIndex = 12;
            // 
            // dateTimePicker5
            // 
            this.dateTimePicker5.CustomFormat = "dd-MM-yyyy HH:mm:ss";
            this.dateTimePicker5.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker5.Location = new System.Drawing.Point(117, 103);
            this.dateTimePicker5.Name = "dateTimePicker5";
            this.dateTimePicker5.Size = new System.Drawing.Size(197, 20);
            this.dateTimePicker5.TabIndex = 10;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.3F, System.Drawing.FontStyle.Bold);
            this.label40.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label40.Location = new System.Drawing.Point(14, 103);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(84, 15);
            this.label40.TabIndex = 9;
            this.label40.Text = "Date_Heure";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.3F, System.Drawing.FontStyle.Bold);
            this.label41.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label41.Location = new System.Drawing.Point(46, 65);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(51, 15);
            this.label41.TabIndex = 8;
            this.label41.Text = "Intitulé";
            // 
            // textBox16
            // 
            this.textBox16.Location = new System.Drawing.Point(117, 60);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(197, 20);
            this.textBox16.TabIndex = 6;
            // 
            // AjoutConvocation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSeaGreen;
            this.ClientSize = new System.Drawing.Size(509, 396);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.panel9);
            this.Name = "AjoutConvocation";
            this.Text = "AjoutConvocation";
            this.Load += new System.EventHandler(this.AjoutConvocation_Load);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.ComboBox comboBox12;
        private System.Windows.Forms.DateTimePicker dateTimePicker5;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox textBox16;
    }
}