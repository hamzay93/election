﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class SaisieResultat : Form
    {
        public SaisieResultat()
        {
            InitializeComponent();
        }
        int[] id = new int[7];
        Connection conn = new Connection();
        ClearClass1 clr = new ClearClass1();
        
        private void SaisieResultat_Load(object sender, EventArgs e)
        {
            this.FillElection();
            this.FillRégion();
            this.FillCandidat();
            
        }
        private void textBoxAncCNI_KeyPress(object sender, KeyPressEventArgs e)
        {
            int isNum = 0;
            if (e.KeyChar == ',' || e.KeyChar == (char)Keys.Back)
                e.Handled = false;
            else if (!int.TryParse(e.KeyChar.ToString(), out isNum))
                e.Handled = true;
        }
        private void FillRégion()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT ID_DifferentCommune,NomDifferentCommune FROM Circonscription";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            cbRegion.ValueMember = "ID_DifferentCommune";
            cbRegion.DisplayMember = "NomDifferentCommune";
            cbRegion.DataSource = objDs.Tables[0];
        }
        private void FillElection()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT ElectionID, ObjetElection  from Election where Etat = 1";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBoxElect.ValueMember = "ElectionID";
            comboBoxElect.DisplayMember = "ObjetElection";
            comboBoxElect.DataSource = objDs.Tables[0];
        }
        private void FillCV(int reg)
        {
            string sCon = conn.connectdb(); ;
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            //if (Int32.Parse(cbRegion.SelectedValue.ToString())==1)
            //{
            //    cmd.CommandText = "SET CONCAT_NULL_YIELDS_NULL OFF;SELECT CentreID, ( CONVERT(Nvarchar(50), CodeCentre)) + ' - '+CentreVote as CentreVote FROM View_CV where RégionID = @Region ORDER BY CodeCentre";
            
            //}
            //else
            //{
            //    cmd.CommandText = "SET CONCAT_NULL_YIELDS_NULL OFF;SELECT CentreID,CentreVote as CentreVote FROM View_CV where RégionID = @Region ORDER BY CodeCentre";
            
            //}
            cmd.CommandText = "SET CONCAT_NULL_YIELDS_NULL OFF;SELECT CentreID,CentreVote as CentreVote FROM View_CV_COMMUNALE where Commun_RegionID = @Commun_RegionID";
            cmd.Parameters.AddWithValue("@Commun_RegionID", reg);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
           
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBoxCV.ValueMember = "CentreID";
            comboBoxCV.DisplayMember = "CentreVote";
            comboBoxCV.DataSource = objDs.Tables[0];
        }
        private void FillBureau(int CentreID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SET CONCAT_NULL_YIELDS_NULL OFF;SELECT BureauID, ( CONVERT(Nvarchar(50), CodeBureau)) + ' - '+NomBureau as NomBureau FROM View_Lieu_vote WHERE CentreID =@CentreID and BureauID not in (select IdBur from ResBureau where Voted !='Rejeté') ORDER BY CodeCentre,OrdreCentre";
            cmd.Parameters.AddWithValue("@CentreID", CentreID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);

            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxBV.ValueMember = "BureauID";
                comboBoxBV.DisplayMember = "NomBureau";
                comboBoxBV.DataSource = objDs.Tables[0];
                con.Close();
            }
            else
            {
                objDs.Tables[0].Rows.Add(3000, "");

                if (objDs.Tables[0].Rows.Count == 1)
                {
                    comboBoxBV.ValueMember = "BureauID";
                    comboBoxBV.DisplayMember = "NomBureau";
                    comboBoxBV.DataSource = objDs.Tables[0];
                    comboBoxBV.SelectedValue = 3000;
                    con.Close();
                }
            }
        }
        public bool isvalide()
        {
            bool empty = false;
            
            string txtnam1 = "txt";
            for (int i = 0; i < 25; i++)
            {
                string nam1 = "Liste";
                nam1 = nam1 + (i + 1).ToString();
                foreach (Control Y in this.panel2.Controls)
                {
                    if (Y is TextBox)
                    {
                        if (Y.Name.ToString() == txtnam1 + nam1)
                        {
                            if (Y.Text.Trim().Length == 0)
                            {
                                empty = true;
                            }
                            
                        }
                       
                    }
                }
            }
            
            return empty;
        }
        public bool isvalide1()
        {
            bool empty1 = false;

            if (txtOROM.Text.Trim().Length == 0 || txtVotant.Text.Trim().Length == 0 || txtNul.Text.Trim().Length == 0)
            {
                empty1 = true;
            }           
            
            return empty1;
        }

            

        private void FillCandidat()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SET CONCAT_NULL_YIELDS_NULL OFF;Select ID_parti,NomParti from Parti_Politique ";//where Statut ='Actif' order by ordre";
            //cmd.Parameters.AddWithValue("@CentreID", CentreID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            DataTable dt = objDs.Tables[0]; 
            //string nam = "Liste";
            for (int i = 0; i < objDs.Tables[0].Rows.Count; i++)
            {
                string nam = "Liste";
                string txtnam = "txt";
                nam = nam + (i + 1).ToString();
                DataRow row = dt.Rows[i];
                //Liste1.Text = (string)row["NOM"] + " (" + (string)row["Partie"] + ")";

                foreach (Control X in this.panel2.Controls)
                {

                    if (X is Label)
                    {
                        if (X.Name.ToString() == nam)
                        {
                            X.Text = (string)row["NomParti"];// +" (" + (string)row["Partie"] + ")";
                            X.Visible = true;

                            foreach (Control Y in this.panel2.Controls)
                            {


                                if (Y is TextBox)
                                {
                                    if (Y.Name.ToString() == txtnam + nam)
                                    {
                                        Y.Visible = true;
                                        Y.Tag = (string)row["ID_parti"].ToString();
                                        id[i] = Int32.Parse((string)row["ID_parti"].ToString());
                                        
                                    }
                                }
                            }
                        }

                    }
                }
                //i++;
            }
        }

        public bool incoherent()
        {
            bool res = false;
            int voix = Int32.Parse(txtListe1.Text.ToString()) + Int32.Parse(txtListe2.Text.ToString()) + Int32.Parse(txtListe3.Text.ToString()) + Int32.Parse(txtListe4.Text.ToString()) + Int32.Parse(txtListe5.Text.ToString()) + Int32.Parse(txtListe6.Text.ToString());
            int suf = Int32.Parse(lblSuffrage.Text.ToString());
            if (suf == voix)
            {
                res = false;
            }
            else
            {
                res = true;
            }
            return res;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (this.isvalide()||this.isvalide1())
            {
                MessageBox.Show("Aucun champs ne doit être vide");
                //clr.RecursiveClearTextBoxes1
            }
            else
            {
                string sCon = conn.connectdb();
                SqlConnection con = new SqlConnection(sCon);
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT IdBur FROM ResBureau where IdBur =@IdBur and IdElection=@IdElection";
                cmd.Parameters.AddWithValue("@idBur", Int32.Parse(comboBoxBV.SelectedValue.ToString()));
                cmd.Parameters.AddWithValue("@IdElection", Int32.Parse(comboBoxElect.SelectedValue.ToString()));
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                con.Open();
                dAdapter.Fill(objDs);
                con.Close();
                bool tst = this.incoherent();
                if (objDs.Tables[0].Rows.Count > 0)
                {
                    MessageBox.Show("Ce Bureau a déjà traité");

                    this.FillBureau(Int32.Parse(comboBoxCV.SelectedValue.ToString()));
                }
                else
                {

                    if (tst)
                    {
                        MessageBox.Show("La sommme des voix doit être égal au nombre de suffrage");
                        txtListe1.Focus();
                    }
                    else
                    {
                        if (Int32.Parse(lblSuffrage.Text.ToString())<1)
                        {
                            MessageBox.Show("Le total des suffrages doit être different de Zero");
                        }
                        else
                        {
                            //Connection cn = new Connection();
                            //string sCon = @"Data Source=localhost\SQL2008; Initial Catalog=Electeur; Integrated Security = true";
                            string insertCmd = "INSERT INTO ResBureau(IdElection, IdBur, IdReg, NbInscrit, NbOMOR, TotalInscrits, NbVotans, NbNuls, SuffragesExprimes, Voted, SavingTime) VALUES (@IdElection, @IdBur, @IdReg, @NbInscrit, @NbOMOR, @TotalInscrits, @NbVotans, @NbNuls, @SuffragesExprimes, @Voted, @SavingTime)";
                            //SqlConnection dbConn;
                            con = new SqlConnection(sCon);
                            con.Open();
                            Login lg = new Login();
                            //DateTime dt = new DateTime();
                            //DateTime dt = DateTime.Now.ToString;
                            string dat = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                            GetValue value = new GetValue();
                            //getAdress add = new getAdress();
                            SqlCommand myCommand = new SqlCommand(insertCmd, con);
                            // Create parameters for the SqlCommand object
                            // initialize with input-form field values
                            //myCommand.Parameters.AddWithValue("@ElecteurID","");
                            myCommand.Parameters.AddWithValue("@IdElection", Int32.Parse(comboBoxElect.SelectedValue.ToString()));
                            myCommand.Parameters.AddWithValue("@IdBur", Int32.Parse(comboBoxBV.SelectedValue.ToString()));
                            myCommand.Parameters.AddWithValue("@IdReg", Int32.Parse(cbRegion.SelectedValue.ToString()));
                            myCommand.Parameters.AddWithValue("@NbInscrit", Int32.Parse(lblInscrit.Text.ToString()));
                            myCommand.Parameters.AddWithValue("@NbOMOR", Int32.Parse(txtOROM.Text.ToString()));
                            myCommand.Parameters.AddWithValue("@TotalInscrits", Int32.Parse(lblTTinscrit.Text.ToString()));
                            myCommand.Parameters.AddWithValue("@NbVotans", Int32.Parse(txtVotant.Text.ToString()));
                            myCommand.Parameters.AddWithValue("@NbNuls", Int32.Parse(txtNul.Text.ToString()));
                            myCommand.Parameters.AddWithValue("@SuffragesExprimes", Int32.Parse(lblSuffrage.Text.ToString()));
                            myCommand.Parameters.AddWithValue("@Voted", "Saisi");
                            myCommand.Parameters.AddWithValue("@SavingTime", dat);
                            myCommand.ExecuteNonQuery();

                            foreach (int element in id)
                            {
                                foreach (Control Z in this.panel2.Controls)
                                {
                                    if (Z is TextBox)
                                    {


                                        if (Z.Visible)
                                        {
                                            if (Z.Tag.ToString() == element.ToString())
                                            {
                                                string insertCmd1 = "INSERT INTO ResCandidat (IdCandidat,IdBur,Voix,SavingTime,IdElection,CVoted) VALUES (@IdCandidat,@IdBur,@Voix,@SavingTime,@IdElection,@CVoted)";
                                                SqlCommand myCommand2 = new SqlCommand(insertCmd1, con);
                                                myCommand2.Parameters.AddWithValue("@IdCandidat", element);
                                                myCommand2.Parameters.AddWithValue("@IdElection", Int32.Parse(comboBoxElect.SelectedValue.ToString()));
                                                myCommand2.Parameters.AddWithValue("@IdBur", Int32.Parse(comboBoxBV.SelectedValue.ToString()));
                                                myCommand2.Parameters.AddWithValue("@Voix", Int32.Parse(Z.Text.ToString()));
                                                myCommand2.Parameters.AddWithValue("@SavingTime", dat);
                                                myCommand2.Parameters.AddWithValue("@CVoted", "Saisi");
                                                myCommand2.ExecuteNonQuery();
                                                //this.FillBureau(Int32.Parse(comboBoxCV.SelectedValue.ToString()));
                                                //MessageBox.Show("Enrégistrement àvec succès");
                                            }
                                        }


                                    }
                                }
                            }
                            MessageBox.Show("Enregistrement avec succès");
                            this.FillBureau(Int32.Parse(comboBoxCV.SelectedValue.ToString()));
                            clr.RecursiveClearTextBoxes1(this.Controls);

                        }
                    }

                }
            }
                
        }
        private void textBoxNewCNI_KeyPress(object sender, KeyPressEventArgs e)
        {
            int isNum = 0;
            if (e.KeyChar == ',' || e.KeyChar == (char)Keys.Back)
                e.Handled = false;
            else if (!int.TryParse(e.KeyChar.ToString(), out isNum))
                e.Handled = true;
        }

        private void Liste2_Click(object sender, EventArgs e)
        {

        }

        private void txtListe2_TextChanged(object sender, EventArgs e)
        {

        }

        private void cbRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.FillCV(Int32.Parse(cbRegion.SelectedValue.ToString()));
        }

        private void comboBoxCV_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.FillBureau(Int32.Parse(comboBoxCV.SelectedValue.ToString()));
        }

        private void txtOROM_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtOROM.Text.Trim() != string.Empty)
                    lblTTinscrit.Text = (Convert.ToInt32(lblInscrit.Text) + Convert.ToInt32(txtOROM.Text)).ToString();
                else
                {

                    //if (txtOROM.Text.Trim() == string.Empty)
                    //    txtOROM.Text = "0";
                    lblTTinscrit.Text = (Convert.ToInt32(lblInscrit.Text) + 0).ToString();

                }
            }
            catch (Exception)
            {

                //throw;
            }
        }

        private void txtVotant_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtOROM.Text.Trim() == string.Empty)
                    txtOROM.Text = "0";
                lblTTinscrit.Text = (Convert.ToInt32(lblInscrit.Text) + Convert.ToInt32(txtOROM.Text)).ToString();
                if (txtNul.Text == string.Empty)
                    txtNul.Text = "0";
                if (Convert.ToInt32(txtVotant.Text) > Convert.ToInt32(lblTTinscrit.Text))

                    MessageBox.Show("LE NOMBRE DE VOTANT SAISI EST SUPERIEUR AU NOMBRE D'INSCRIT! \n", "", MessageBoxButtons.OK, MessageBoxIcon.Stop);

                if (txtVotant.Text != "")

                    lblSuffrage.Text = Convert.ToString(Convert.ToInt32(txtVotant.Text) - Convert.ToInt32(txtNul.Text));

                //if ((txtVotant.Focus) > Convert.ToInt32(lblInscrit.Text))

                //   


                else
                    lblSuffrage.Text = Convert.ToString(0 - Convert.ToInt32(txtNul.Text));
            }
            catch (Exception)
            {

                //throw;
            }
        }

        private void comboBoxBV_SelectedIndexChanged(object sender, EventArgs e)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SET CONCAT_NULL_YIELDS_NULL OFF;SELECT * from View_reg_aff where BureauID=@BureauID";
            cmd.Parameters.AddWithValue("@BureauID", Int32.Parse(comboBoxBV.SelectedValue.ToString()));
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            int total = objDs.Tables[0].Rows.Count;
            lblInscrit.Text = total.ToString();
            clr.RecursiveClearTextBoxes1(this.Controls);
            if (Int32.Parse(comboBoxBV.SelectedValue.ToString()) ==3000)
            {
                btnSave.Enabled = false;
            }
            else
            {
                btnSave.Enabled = true;
            }
        }

        //public void fillcandidat()
        //{
        //    //SELECT IdCandidat,Nom1 +' '+Nom2+' '+Nom3 as Nom,Partie,TourID from Candidat
        //    string sCon = conn.connectdb();
        //    SqlConnection con = new SqlConnection(sCon);
        //    SqlCommand cmd = new SqlCommand();
        //    cmd.Connection = con;
        //    cmd.CommandType = CommandType.Text;
        //    cmd.CommandText = "SET CONCAT_NULL_YIELDS_NULL OFF;SELECT IdCandidat,Nom1 +' '+Nom2+' '+Nom3 as Nom,Partie,TourID from Candidat";
        //    //cmd.Parameters.AddWithValue("@BureauID", Int32.Parse(comboBoxBV.SelectedValue.ToString()));
        //    DataSet objDs = new DataSet();
        //    SqlDataAdapter dAdapter = new SqlDataAdapter();
        //    dAdapter.SelectCommand = cmd;
        //    con.Open();
        //    dAdapter.Fill(objDs);
        //    con.Close();
        //    int total = objDs.Tables[0].Rows.Count;
        //    lblInscrit.Text = total.ToString();
        //}

        private void txtNul_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtVotant.Text == string.Empty)
                    txtVotant.Text = "0";
                if (txtOROM.Text.Trim() == string.Empty)
                    txtOROM.Text = "0";
                lblTTinscrit.Text = (Convert.ToInt32(lblInscrit.Text) + Convert.ToInt32(txtOROM.Text)).ToString();

                //if (txtNul.Text == string.Empty)
                //    txtNul.Text = "0";

                lblSuffrage.Text = Convert.ToString(Convert.ToInt32(txtVotant.Text) - Convert.ToInt32(txtNul.Text));
                //if (txtListe2.Visible == false)
                //{
                //    txtListe1.Text = lblSuffrage.Text;
                //}

            }
            catch (Exception)
            {

                //throw;
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            clr.RecursiveClearTextBoxes1(this.Controls);
        }

      
       
    }
}
