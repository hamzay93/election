﻿using System.Windows.Forms;
namespace Electeur
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.saisiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saisieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ApprouverToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.validerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rechercheToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.electeurToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.parListeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nouveauCINToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rechercheAncienneCINToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.administrationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paramétrageCentreEtBureauToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.créerUnAgentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.réinitialiserLeMotDePasseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.suspendreUnAgentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.réactivationDunAgentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.auditToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paramétrageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.historiqueToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.importNouveauCNIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.affectationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.affectationIndividuelleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.affectationParListeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.réaffectationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.réaffectationEnMasseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultationToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.radiationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.radierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listesDesRadiésToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rechercheDesRadiésToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outilsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changerMonMotDePasseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.retraitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.retraitCarteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listeDeRetraitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.radiéInterregionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.impressionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.carteDélecteurToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listeDaffichageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listeDemargementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.emargementparrégionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.emargementparBureauToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lieuxDeVoteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.electionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ajoutElectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificationElectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.typeElectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ajoutTypeElectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificationTypeElectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.membreLieuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ajoutMembreLieuVoteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificationMembreLieuVoteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.typeReunionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ajoutTypeReunionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificationTypeReunionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reunionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ajoutReunionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificationReunionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.prefetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ajoutPrefetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificationPrefetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.convocationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ajoutConvocationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificationConvocationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fonctionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ajoutFonctionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificationFonctionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tourElectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ajoutTourElectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificationTourElectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editionDeConvocationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.procurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mandaterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scrutinToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paramétrageToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ajoutPartiePolitiqueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ajoutPartiePolitiqueToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificationPartiePolitiqueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.membreDunePartiePolituqeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ajoutMembreDunePartiePolituqeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificationMembreDunePartiePolituqeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.résultatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.validationRésultatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statistiqueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionDesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saisieDeNombreDeVotantToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificationNombreDeVotantDansBureauDeVoteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saisieLheureDeSaisieDeNombreDeVotantToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.affichageTauxDeParticipationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saisiToolStripMenuItem,
            this.rechercheToolStripMenuItem,
            this.administrationToolStripMenuItem,
            this.affectationToolStripMenuItem,
            this.radiationToolStripMenuItem,
            this.outilsToolStripMenuItem,
            this.retraitToolStripMenuItem,
            this.impressionToolStripMenuItem,
            this.lieuxDeVoteToolStripMenuItem,
            this.procurationToolStripMenuItem,
            this.scrutinToolStripMenuItem,
            this.gestionDesToolStripMenuItem,
            this.quitterToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1234, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // saisiToolStripMenuItem
            // 
            this.saisiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saisieToolStripMenuItem,
            this.ApprouverToolStripMenuItem,
            this.validerToolStripMenuItem});
            this.saisiToolStripMenuItem.Name = "saisiToolStripMenuItem";
            this.saisiToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.saisiToolStripMenuItem.Text = "Electeur";
            this.saisiToolStripMenuItem.Visible = false;
            // 
            // saisieToolStripMenuItem
            // 
            this.saisieToolStripMenuItem.Name = "saisieToolStripMenuItem";
            this.saisieToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.saisieToolStripMenuItem.Text = "Saisie";
            this.saisieToolStripMenuItem.Visible = false;
            this.saisieToolStripMenuItem.Click += new System.EventHandler(this.saisieToolStripMenuItem_Click);
            // 
            // ApprouverToolStripMenuItem
            // 
            this.ApprouverToolStripMenuItem.Name = "ApprouverToolStripMenuItem";
            this.ApprouverToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.ApprouverToolStripMenuItem.Text = "Approbation";
            this.ApprouverToolStripMenuItem.Visible = false;
            this.ApprouverToolStripMenuItem.Click += new System.EventHandler(this.ApprouverToolStripMenuItem_Click);
            // 
            // validerToolStripMenuItem
            // 
            this.validerToolStripMenuItem.Name = "validerToolStripMenuItem";
            this.validerToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.validerToolStripMenuItem.Text = "Validation";
            this.validerToolStripMenuItem.Visible = false;
            this.validerToolStripMenuItem.Click += new System.EventHandler(this.validerToolStripMenuItem_Click);
            // 
            // rechercheToolStripMenuItem
            // 
            this.rechercheToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.electeurToolStripMenuItem,
            this.parListeToolStripMenuItem,
            this.nouveauCINToolStripMenuItem,
            this.rechercheAncienneCINToolStripMenuItem});
            this.rechercheToolStripMenuItem.Name = "rechercheToolStripMenuItem";
            this.rechercheToolStripMenuItem.Size = new System.Drawing.Size(74, 20);
            this.rechercheToolStripMenuItem.Text = "Recherche";
            this.rechercheToolStripMenuItem.Visible = false;
            // 
            // electeurToolStripMenuItem
            // 
            this.electeurToolStripMenuItem.Name = "electeurToolStripMenuItem";
            this.electeurToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.electeurToolStripMenuItem.Text = "Recherche d\'electeur";
            this.electeurToolStripMenuItem.Visible = false;
            this.electeurToolStripMenuItem.Click += new System.EventHandler(this.electeurToolStripMenuItem_Click);
            // 
            // parListeToolStripMenuItem
            // 
            this.parListeToolStripMenuItem.Name = "parListeToolStripMenuItem";
            this.parListeToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.parListeToolStripMenuItem.Text = "Recherche statistique";
            this.parListeToolStripMenuItem.Visible = false;
            this.parListeToolStripMenuItem.Click += new System.EventHandler(this.parListeToolStripMenuItem_Click);
            // 
            // nouveauCINToolStripMenuItem
            // 
            this.nouveauCINToolStripMenuItem.Name = "nouveauCINToolStripMenuItem";
            this.nouveauCINToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.nouveauCINToolStripMenuItem.Text = "Recherche nouvelle CIN";
            this.nouveauCINToolStripMenuItem.Click += new System.EventHandler(this.nouveauCINToolStripMenuItem_Click);
            // 
            // rechercheAncienneCINToolStripMenuItem
            // 
            this.rechercheAncienneCINToolStripMenuItem.Name = "rechercheAncienneCINToolStripMenuItem";
            this.rechercheAncienneCINToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.rechercheAncienneCINToolStripMenuItem.Text = "Recherche ancienne CIN";
            this.rechercheAncienneCINToolStripMenuItem.Click += new System.EventHandler(this.rechercheAncienneCINToolStripMenuItem_Click);
            // 
            // administrationToolStripMenuItem
            // 
            this.administrationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.paramétrageCentreEtBureauToolStripMenuItem,
            this.créerUnAgentToolStripMenuItem,
            this.réinitialiserLeMotDePasseToolStripMenuItem,
            this.suspendreUnAgentToolStripMenuItem,
            this.réactivationDunAgentToolStripMenuItem,
            this.auditToolStripMenuItem,
            this.paramétrageToolStripMenuItem,
            this.historiqueToolStripMenuItem1,
            this.importNouveauCNIToolStripMenuItem});
            this.administrationToolStripMenuItem.Name = "administrationToolStripMenuItem";
            this.administrationToolStripMenuItem.Size = new System.Drawing.Size(98, 20);
            this.administrationToolStripMenuItem.Text = "Administration";
            this.administrationToolStripMenuItem.Visible = false;
            // 
            // paramétrageCentreEtBureauToolStripMenuItem
            // 
            this.paramétrageCentreEtBureauToolStripMenuItem.Name = "paramétrageCentreEtBureauToolStripMenuItem";
            this.paramétrageCentreEtBureauToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.paramétrageCentreEtBureauToolStripMenuItem.Text = "Paramétrage Centre et Bureau";
            this.paramétrageCentreEtBureauToolStripMenuItem.Click += new System.EventHandler(this.paramétrageCentreEtBureauToolStripMenuItem_Click);
            // 
            // créerUnAgentToolStripMenuItem
            // 
            this.créerUnAgentToolStripMenuItem.Name = "créerUnAgentToolStripMenuItem";
            this.créerUnAgentToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.créerUnAgentToolStripMenuItem.Text = "Création d\'un agent";
            this.créerUnAgentToolStripMenuItem.Click += new System.EventHandler(this.créerUnAgentToolStripMenuItem_Click);
            // 
            // réinitialiserLeMotDePasseToolStripMenuItem
            // 
            this.réinitialiserLeMotDePasseToolStripMenuItem.Name = "réinitialiserLeMotDePasseToolStripMenuItem";
            this.réinitialiserLeMotDePasseToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.réinitialiserLeMotDePasseToolStripMenuItem.Text = "Réinitialiser le mot de passe";
            this.réinitialiserLeMotDePasseToolStripMenuItem.Click += new System.EventHandler(this.réinitialiserLeMotDePasseToolStripMenuItem_Click);
            // 
            // suspendreUnAgentToolStripMenuItem
            // 
            this.suspendreUnAgentToolStripMenuItem.Name = "suspendreUnAgentToolStripMenuItem";
            this.suspendreUnAgentToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.suspendreUnAgentToolStripMenuItem.Text = "Suspension d\'un agent";
            this.suspendreUnAgentToolStripMenuItem.Click += new System.EventHandler(this.suspendreUnAgentToolStripMenuItem_Click);
            // 
            // réactivationDunAgentToolStripMenuItem
            // 
            this.réactivationDunAgentToolStripMenuItem.Name = "réactivationDunAgentToolStripMenuItem";
            this.réactivationDunAgentToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.réactivationDunAgentToolStripMenuItem.Text = "Réactivation d\'un agent";
            this.réactivationDunAgentToolStripMenuItem.Click += new System.EventHandler(this.réactivationDunAgentToolStripMenuItem_Click);
            // 
            // auditToolStripMenuItem
            // 
            this.auditToolStripMenuItem.Name = "auditToolStripMenuItem";
            this.auditToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.auditToolStripMenuItem.Text = "Audit";
            this.auditToolStripMenuItem.Visible = false;
            this.auditToolStripMenuItem.Click += new System.EventHandler(this.auditToolStripMenuItem_Click);
            // 
            // paramétrageToolStripMenuItem
            // 
            this.paramétrageToolStripMenuItem.Name = "paramétrageToolStripMenuItem";
            this.paramétrageToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.paramétrageToolStripMenuItem.Text = "Paramétrage";
            this.paramétrageToolStripMenuItem.Click += new System.EventHandler(this.paramétrageToolStripMenuItem_Click);
            // 
            // historiqueToolStripMenuItem1
            // 
            this.historiqueToolStripMenuItem1.Name = "historiqueToolStripMenuItem1";
            this.historiqueToolStripMenuItem1.Size = new System.Drawing.Size(232, 22);
            this.historiqueToolStripMenuItem1.Text = "Historique d\'un agent";
            this.historiqueToolStripMenuItem1.Click += new System.EventHandler(this.historiqueToolStripMenuItem_Click);
            // 
            // importNouveauCNIToolStripMenuItem
            // 
            this.importNouveauCNIToolStripMenuItem.Name = "importNouveauCNIToolStripMenuItem";
            this.importNouveauCNIToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.importNouveauCNIToolStripMenuItem.Text = "Import Nouveau CNI";
            this.importNouveauCNIToolStripMenuItem.Click += new System.EventHandler(this.importNouveauCNIToolStripMenuItem_Click);
            // 
            // affectationToolStripMenuItem
            // 
            this.affectationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.affectationIndividuelleToolStripMenuItem,
            this.affectationParListeToolStripMenuItem,
            this.réaffectationToolStripMenuItem,
            this.réaffectationEnMasseToolStripMenuItem,
            this.consultationToolStripMenuItem1});
            this.affectationToolStripMenuItem.Name = "affectationToolStripMenuItem";
            this.affectationToolStripMenuItem.Size = new System.Drawing.Size(78, 20);
            this.affectationToolStripMenuItem.Text = "Affectation";
            this.affectationToolStripMenuItem.Visible = false;
            // 
            // affectationIndividuelleToolStripMenuItem
            // 
            this.affectationIndividuelleToolStripMenuItem.Name = "affectationIndividuelleToolStripMenuItem";
            this.affectationIndividuelleToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.affectationIndividuelleToolStripMenuItem.Text = "Affectation Individuelle";
            this.affectationIndividuelleToolStripMenuItem.Visible = false;
            this.affectationIndividuelleToolStripMenuItem.Click += new System.EventHandler(this.affectationIndividuelleToolStripMenuItem_Click);
            // 
            // affectationParListeToolStripMenuItem
            // 
            this.affectationParListeToolStripMenuItem.Name = "affectationParListeToolStripMenuItem";
            this.affectationParListeToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.affectationParListeToolStripMenuItem.Text = "Affectation d\'electeur";
            this.affectationParListeToolStripMenuItem.Visible = false;
            this.affectationParListeToolStripMenuItem.Click += new System.EventHandler(this.affectationParListeToolStripMenuItem_Click);
            // 
            // réaffectationToolStripMenuItem
            // 
            this.réaffectationToolStripMenuItem.Name = "réaffectationToolStripMenuItem";
            this.réaffectationToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.réaffectationToolStripMenuItem.Text = "Réaffectation d\'electeur";
            this.réaffectationToolStripMenuItem.Visible = false;
            this.réaffectationToolStripMenuItem.Click += new System.EventHandler(this.réaffectationToolStripMenuItem_Click);
            // 
            // réaffectationEnMasseToolStripMenuItem
            // 
            this.réaffectationEnMasseToolStripMenuItem.Name = "réaffectationEnMasseToolStripMenuItem";
            this.réaffectationEnMasseToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.réaffectationEnMasseToolStripMenuItem.Text = "Réaffectation de centre";
            this.réaffectationEnMasseToolStripMenuItem.Visible = false;
            this.réaffectationEnMasseToolStripMenuItem.Click += new System.EventHandler(this.réaffectationEnMasseToolStripMenuItem_Click);
            // 
            // consultationToolStripMenuItem1
            // 
            this.consultationToolStripMenuItem1.Name = "consultationToolStripMenuItem1";
            this.consultationToolStripMenuItem1.Size = new System.Drawing.Size(199, 22);
            this.consultationToolStripMenuItem1.Text = "Consultation";
            this.consultationToolStripMenuItem1.Click += new System.EventHandler(this.consultationToolStripMenuItem_Click);
            // 
            // radiationToolStripMenuItem
            // 
            this.radiationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.radierToolStripMenuItem,
            this.listesDesRadiésToolStripMenuItem,
            this.rechercheDesRadiésToolStripMenuItem});
            this.radiationToolStripMenuItem.Name = "radiationToolStripMenuItem";
            this.radiationToolStripMenuItem.Size = new System.Drawing.Size(69, 20);
            this.radiationToolStripMenuItem.Text = "Radiation";
            this.radiationToolStripMenuItem.Visible = false;
            this.radiationToolStripMenuItem.Click += new System.EventHandler(this.radiationToolStripMenuItem_Click);
            // 
            // radierToolStripMenuItem
            // 
            this.radierToolStripMenuItem.Name = "radierToolStripMenuItem";
            this.radierToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.radierToolStripMenuItem.Text = "Radiation d\'un electeur";
            this.radierToolStripMenuItem.Click += new System.EventHandler(this.radierToolStripMenuItem_Click);
            // 
            // listesDesRadiésToolStripMenuItem
            // 
            this.listesDesRadiésToolStripMenuItem.Name = "listesDesRadiésToolStripMenuItem";
            this.listesDesRadiésToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.listesDesRadiésToolStripMenuItem.Text = "Listes des radiés";
            this.listesDesRadiésToolStripMenuItem.Click += new System.EventHandler(this.listesDesRadiésToolStripMenuItem_Click);
            // 
            // rechercheDesRadiésToolStripMenuItem
            // 
            this.rechercheDesRadiésToolStripMenuItem.Name = "rechercheDesRadiésToolStripMenuItem";
            this.rechercheDesRadiésToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.rechercheDesRadiésToolStripMenuItem.Text = "Recherche des radiés";
            this.rechercheDesRadiésToolStripMenuItem.Click += new System.EventHandler(this.rechercheDesRadiésToolStripMenuItem_Click);
            // 
            // outilsToolStripMenuItem
            // 
            this.outilsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.changerMonMotDePasseToolStripMenuItem});
            this.outilsToolStripMenuItem.Name = "outilsToolStripMenuItem";
            this.outilsToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.outilsToolStripMenuItem.Text = "Outils";
            // 
            // changerMonMotDePasseToolStripMenuItem
            // 
            this.changerMonMotDePasseToolStripMenuItem.Name = "changerMonMotDePasseToolStripMenuItem";
            this.changerMonMotDePasseToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.changerMonMotDePasseToolStripMenuItem.Text = "Changer mon Mot de passe";
            this.changerMonMotDePasseToolStripMenuItem.Click += new System.EventHandler(this.changerMonMotDePasseToolStripMenuItem_Click);
            // 
            // retraitToolStripMenuItem
            // 
            this.retraitToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.retraitCarteToolStripMenuItem,
            this.listeDeRetraitToolStripMenuItem,
            this.radiéInterregionToolStripMenuItem});
            this.retraitToolStripMenuItem.Name = "retraitToolStripMenuItem";
            this.retraitToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.retraitToolStripMenuItem.Text = "Retrait";
            this.retraitToolStripMenuItem.Visible = false;
            this.retraitToolStripMenuItem.Click += new System.EventHandler(this.retraitToolStripMenuItem_Click);
            // 
            // retraitCarteToolStripMenuItem
            // 
            this.retraitCarteToolStripMenuItem.Name = "retraitCarteToolStripMenuItem";
            this.retraitCarteToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.retraitCarteToolStripMenuItem.Text = "Retrait Carte";
            this.retraitCarteToolStripMenuItem.Click += new System.EventHandler(this.retraitCarteToolStripMenuItem_Click);
            // 
            // listeDeRetraitToolStripMenuItem
            // 
            this.listeDeRetraitToolStripMenuItem.Name = "listeDeRetraitToolStripMenuItem";
            this.listeDeRetraitToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.listeDeRetraitToolStripMenuItem.Text = "Liste de retrait";
            this.listeDeRetraitToolStripMenuItem.Click += new System.EventHandler(this.listeDeRetraitToolStripMenuItem_Click);
            // 
            // radiéInterregionToolStripMenuItem
            // 
            this.radiéInterregionToolStripMenuItem.Name = "radiéInterregionToolStripMenuItem";
            this.radiéInterregionToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.radiéInterregionToolStripMenuItem.Text = "RadiéInterregion";
            this.radiéInterregionToolStripMenuItem.Click += new System.EventHandler(this.radiéInterregionToolStripMenuItem_Click);
            // 
            // impressionToolStripMenuItem
            // 
            this.impressionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.carteDélecteurToolStripMenuItem,
            this.listeDaffichageToolStripMenuItem,
            this.listeDemargementToolStripMenuItem});
            this.impressionToolStripMenuItem.Name = "impressionToolStripMenuItem";
            this.impressionToolStripMenuItem.Size = new System.Drawing.Size(77, 20);
            this.impressionToolStripMenuItem.Text = "Impression";
            this.impressionToolStripMenuItem.Visible = false;
            this.impressionToolStripMenuItem.Click += new System.EventHandler(this.impressionToolStripMenuItem_Click);
            // 
            // carteDélecteurToolStripMenuItem
            // 
            this.carteDélecteurToolStripMenuItem.Name = "carteDélecteurToolStripMenuItem";
            this.carteDélecteurToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.carteDélecteurToolStripMenuItem.Text = "Carte d\'électeur";
            this.carteDélecteurToolStripMenuItem.Click += new System.EventHandler(this.carteDélecteurToolStripMenuItem_Click);
            // 
            // listeDaffichageToolStripMenuItem
            // 
            this.listeDaffichageToolStripMenuItem.Name = "listeDaffichageToolStripMenuItem";
            this.listeDaffichageToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.listeDaffichageToolStripMenuItem.Text = "Liste d\'affichage";
            this.listeDaffichageToolStripMenuItem.Click += new System.EventHandler(this.listeDaffichageToolStripMenuItem_Click);
            // 
            // listeDemargementToolStripMenuItem
            // 
            this.listeDemargementToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.emargementparrégionToolStripMenuItem,
            this.emargementparBureauToolStripMenuItem});
            this.listeDemargementToolStripMenuItem.Name = "listeDemargementToolStripMenuItem";
            this.listeDemargementToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.listeDemargementToolStripMenuItem.Text = "Liste d\'emargement";
            this.listeDemargementToolStripMenuItem.Click += new System.EventHandler(this.listeDemargementToolStripMenuItem_Click);
            // 
            // emargementparrégionToolStripMenuItem
            // 
            this.emargementparrégionToolStripMenuItem.Name = "emargementparrégionToolStripMenuItem";
            this.emargementparrégionToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.emargementparrégionToolStripMenuItem.Text = "Emargement_par_Région";
            this.emargementparrégionToolStripMenuItem.Visible = false;
            this.emargementparrégionToolStripMenuItem.Click += new System.EventHandler(this.emargementparrégionToolStripMenuItem_Click);
            // 
            // emargementparBureauToolStripMenuItem
            // 
            this.emargementparBureauToolStripMenuItem.Name = "emargementparBureauToolStripMenuItem";
            this.emargementparBureauToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.emargementparBureauToolStripMenuItem.Text = "Emargement_par_Bureau";
            this.emargementparBureauToolStripMenuItem.Click += new System.EventHandler(this.emargementparBureauToolStripMenuItem_Click);
            // 
            // lieuxDeVoteToolStripMenuItem
            // 
            this.lieuxDeVoteToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.electionToolStripMenuItem,
            this.typeElectionToolStripMenuItem,
            this.membreLieuToolStripMenuItem,
            this.typeReunionToolStripMenuItem,
            this.reunionToolStripMenuItem,
            this.prefetToolStripMenuItem,
            this.convocationToolStripMenuItem,
            this.fonctionToolStripMenuItem,
            this.tourElectionToolStripMenuItem,
            this.editionDeConvocationToolStripMenuItem});
            this.lieuxDeVoteToolStripMenuItem.Name = "lieuxDeVoteToolStripMenuItem";
            this.lieuxDeVoteToolStripMenuItem.Size = new System.Drawing.Size(89, 20);
            this.lieuxDeVoteToolStripMenuItem.Text = "Lieux de Vote";
            this.lieuxDeVoteToolStripMenuItem.Visible = false;
            // 
            // electionToolStripMenuItem
            // 
            this.electionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ajoutElectionToolStripMenuItem,
            this.modificationElectionToolStripMenuItem});
            this.electionToolStripMenuItem.Name = "electionToolStripMenuItem";
            this.electionToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.electionToolStripMenuItem.Text = "Election";
            // 
            // ajoutElectionToolStripMenuItem
            // 
            this.ajoutElectionToolStripMenuItem.Name = "ajoutElectionToolStripMenuItem";
            this.ajoutElectionToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.ajoutElectionToolStripMenuItem.Text = "Ajout Election";
            this.ajoutElectionToolStripMenuItem.Click += new System.EventHandler(this.ajoutElectionToolStripMenuItem_Click);
            // 
            // modificationElectionToolStripMenuItem
            // 
            this.modificationElectionToolStripMenuItem.Name = "modificationElectionToolStripMenuItem";
            this.modificationElectionToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.modificationElectionToolStripMenuItem.Text = "Modification Election";
            this.modificationElectionToolStripMenuItem.Click += new System.EventHandler(this.modificationElectionToolStripMenuItem_Click);
            // 
            // typeElectionToolStripMenuItem
            // 
            this.typeElectionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ajoutTypeElectionToolStripMenuItem,
            this.modificationTypeElectionToolStripMenuItem});
            this.typeElectionToolStripMenuItem.Name = "typeElectionToolStripMenuItem";
            this.typeElectionToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.typeElectionToolStripMenuItem.Text = "Type Election";
            this.typeElectionToolStripMenuItem.Visible = false;
            // 
            // ajoutTypeElectionToolStripMenuItem
            // 
            this.ajoutTypeElectionToolStripMenuItem.Name = "ajoutTypeElectionToolStripMenuItem";
            this.ajoutTypeElectionToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.ajoutTypeElectionToolStripMenuItem.Text = "Ajout Type Election";
            this.ajoutTypeElectionToolStripMenuItem.Click += new System.EventHandler(this.ajoutTypeElectionToolStripMenuItem_Click);
            // 
            // modificationTypeElectionToolStripMenuItem
            // 
            this.modificationTypeElectionToolStripMenuItem.Name = "modificationTypeElectionToolStripMenuItem";
            this.modificationTypeElectionToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.modificationTypeElectionToolStripMenuItem.Text = "Modification Type Election";
            this.modificationTypeElectionToolStripMenuItem.Click += new System.EventHandler(this.modificationTypeElectionToolStripMenuItem_Click);
            // 
            // membreLieuToolStripMenuItem
            // 
            this.membreLieuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ajoutMembreLieuVoteToolStripMenuItem,
            this.modificationMembreLieuVoteToolStripMenuItem});
            this.membreLieuToolStripMenuItem.Name = "membreLieuToolStripMenuItem";
            this.membreLieuToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.membreLieuToolStripMenuItem.Text = "Membre Lieu de Vote";
            this.membreLieuToolStripMenuItem.Visible = false;
            this.membreLieuToolStripMenuItem.Click += new System.EventHandler(this.membreLieuToolStripMenuItem_Click);
            // 
            // ajoutMembreLieuVoteToolStripMenuItem
            // 
            this.ajoutMembreLieuVoteToolStripMenuItem.Name = "ajoutMembreLieuVoteToolStripMenuItem";
            this.ajoutMembreLieuVoteToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            this.ajoutMembreLieuVoteToolStripMenuItem.Text = "Ajout Membre Lieu Vote";
            this.ajoutMembreLieuVoteToolStripMenuItem.Click += new System.EventHandler(this.ajoutMembreLieuVoteToolStripMenuItem_Click);
            // 
            // modificationMembreLieuVoteToolStripMenuItem
            // 
            this.modificationMembreLieuVoteToolStripMenuItem.Name = "modificationMembreLieuVoteToolStripMenuItem";
            this.modificationMembreLieuVoteToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            this.modificationMembreLieuVoteToolStripMenuItem.Text = "Modification Membre Lieu Vote";
            this.modificationMembreLieuVoteToolStripMenuItem.Click += new System.EventHandler(this.modificationMembreLieuVoteToolStripMenuItem_Click);
            // 
            // typeReunionToolStripMenuItem
            // 
            this.typeReunionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ajoutTypeReunionToolStripMenuItem,
            this.modificationTypeReunionToolStripMenuItem});
            this.typeReunionToolStripMenuItem.Name = "typeReunionToolStripMenuItem";
            this.typeReunionToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.typeReunionToolStripMenuItem.Text = "Type Reunion";
            this.typeReunionToolStripMenuItem.Visible = false;
            // 
            // ajoutTypeReunionToolStripMenuItem
            // 
            this.ajoutTypeReunionToolStripMenuItem.Name = "ajoutTypeReunionToolStripMenuItem";
            this.ajoutTypeReunionToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.ajoutTypeReunionToolStripMenuItem.Text = "Ajout Type Reunion";
            this.ajoutTypeReunionToolStripMenuItem.Click += new System.EventHandler(this.ajoutTypeReunionToolStripMenuItem_Click);
            // 
            // modificationTypeReunionToolStripMenuItem
            // 
            this.modificationTypeReunionToolStripMenuItem.Name = "modificationTypeReunionToolStripMenuItem";
            this.modificationTypeReunionToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.modificationTypeReunionToolStripMenuItem.Text = "Modification Type Reunion";
            this.modificationTypeReunionToolStripMenuItem.Click += new System.EventHandler(this.modificationTypeReunionToolStripMenuItem_Click);
            // 
            // reunionToolStripMenuItem
            // 
            this.reunionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ajoutReunionToolStripMenuItem,
            this.modificationReunionToolStripMenuItem});
            this.reunionToolStripMenuItem.Name = "reunionToolStripMenuItem";
            this.reunionToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.reunionToolStripMenuItem.Text = "Reunion";
            this.reunionToolStripMenuItem.Visible = false;
            // 
            // ajoutReunionToolStripMenuItem
            // 
            this.ajoutReunionToolStripMenuItem.Name = "ajoutReunionToolStripMenuItem";
            this.ajoutReunionToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.ajoutReunionToolStripMenuItem.Text = "Ajout Reunion";
            this.ajoutReunionToolStripMenuItem.Click += new System.EventHandler(this.ajoutReunionToolStripMenuItem_Click);
            // 
            // modificationReunionToolStripMenuItem
            // 
            this.modificationReunionToolStripMenuItem.Name = "modificationReunionToolStripMenuItem";
            this.modificationReunionToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.modificationReunionToolStripMenuItem.Text = "Modification Reunion";
            this.modificationReunionToolStripMenuItem.Click += new System.EventHandler(this.modificationReunionToolStripMenuItem_Click);
            // 
            // prefetToolStripMenuItem
            // 
            this.prefetToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ajoutPrefetToolStripMenuItem,
            this.modificationPrefetToolStripMenuItem});
            this.prefetToolStripMenuItem.Name = "prefetToolStripMenuItem";
            this.prefetToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.prefetToolStripMenuItem.Text = "Prefet";
            // 
            // ajoutPrefetToolStripMenuItem
            // 
            this.ajoutPrefetToolStripMenuItem.Name = "ajoutPrefetToolStripMenuItem";
            this.ajoutPrefetToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.ajoutPrefetToolStripMenuItem.Text = "Ajout Prefet";
            this.ajoutPrefetToolStripMenuItem.Click += new System.EventHandler(this.ajoutPrefetToolStripMenuItem_Click);
            // 
            // modificationPrefetToolStripMenuItem
            // 
            this.modificationPrefetToolStripMenuItem.Name = "modificationPrefetToolStripMenuItem";
            this.modificationPrefetToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.modificationPrefetToolStripMenuItem.Text = "Modification Prefet";
            this.modificationPrefetToolStripMenuItem.Click += new System.EventHandler(this.modificationPrefetToolStripMenuItem_Click);
            // 
            // convocationToolStripMenuItem
            // 
            this.convocationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ajoutConvocationToolStripMenuItem,
            this.modificationConvocationToolStripMenuItem});
            this.convocationToolStripMenuItem.Name = "convocationToolStripMenuItem";
            this.convocationToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.convocationToolStripMenuItem.Text = "Convocation";
            this.convocationToolStripMenuItem.Visible = false;
            // 
            // ajoutConvocationToolStripMenuItem
            // 
            this.ajoutConvocationToolStripMenuItem.Name = "ajoutConvocationToolStripMenuItem";
            this.ajoutConvocationToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.ajoutConvocationToolStripMenuItem.Text = "Ajout Convocation";
            this.ajoutConvocationToolStripMenuItem.Click += new System.EventHandler(this.ajoutConvocationToolStripMenuItem_Click);
            // 
            // modificationConvocationToolStripMenuItem
            // 
            this.modificationConvocationToolStripMenuItem.Name = "modificationConvocationToolStripMenuItem";
            this.modificationConvocationToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.modificationConvocationToolStripMenuItem.Text = "Modification Convocation";
            this.modificationConvocationToolStripMenuItem.Click += new System.EventHandler(this.modificationConvocationToolStripMenuItem_Click);
            // 
            // fonctionToolStripMenuItem
            // 
            this.fonctionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ajoutFonctionToolStripMenuItem,
            this.modificationFonctionToolStripMenuItem});
            this.fonctionToolStripMenuItem.Name = "fonctionToolStripMenuItem";
            this.fonctionToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.fonctionToolStripMenuItem.Text = "Fonction";
            this.fonctionToolStripMenuItem.Visible = false;
            // 
            // ajoutFonctionToolStripMenuItem
            // 
            this.ajoutFonctionToolStripMenuItem.Name = "ajoutFonctionToolStripMenuItem";
            this.ajoutFonctionToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.ajoutFonctionToolStripMenuItem.Text = "Ajout Fonction";
            this.ajoutFonctionToolStripMenuItem.Click += new System.EventHandler(this.ajoutFonctionToolStripMenuItem_Click);
            // 
            // modificationFonctionToolStripMenuItem
            // 
            this.modificationFonctionToolStripMenuItem.Name = "modificationFonctionToolStripMenuItem";
            this.modificationFonctionToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.modificationFonctionToolStripMenuItem.Text = "Modification Fonction";
            this.modificationFonctionToolStripMenuItem.Click += new System.EventHandler(this.modificationFonctionToolStripMenuItem_Click);
            // 
            // tourElectionToolStripMenuItem
            // 
            this.tourElectionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ajoutTourElectionToolStripMenuItem,
            this.modificationTourElectionToolStripMenuItem});
            this.tourElectionToolStripMenuItem.Name = "tourElectionToolStripMenuItem";
            this.tourElectionToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.tourElectionToolStripMenuItem.Text = "Tour Election";
            // 
            // ajoutTourElectionToolStripMenuItem
            // 
            this.ajoutTourElectionToolStripMenuItem.Name = "ajoutTourElectionToolStripMenuItem";
            this.ajoutTourElectionToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.ajoutTourElectionToolStripMenuItem.Text = "Ajout Tour Election";
            this.ajoutTourElectionToolStripMenuItem.Click += new System.EventHandler(this.ajoutTourElectionToolStripMenuItem_Click);
            // 
            // modificationTourElectionToolStripMenuItem
            // 
            this.modificationTourElectionToolStripMenuItem.Name = "modificationTourElectionToolStripMenuItem";
            this.modificationTourElectionToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.modificationTourElectionToolStripMenuItem.Text = "Modification Tour Election";
            this.modificationTourElectionToolStripMenuItem.Click += new System.EventHandler(this.modificationTourElectionToolStripMenuItem_Click);
            // 
            // editionDeConvocationToolStripMenuItem
            // 
            this.editionDeConvocationToolStripMenuItem.Name = "editionDeConvocationToolStripMenuItem";
            this.editionDeConvocationToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.editionDeConvocationToolStripMenuItem.Text = "Edition de convocation";
            this.editionDeConvocationToolStripMenuItem.Visible = false;
            this.editionDeConvocationToolStripMenuItem.Click += new System.EventHandler(this.editionDeConvocationToolStripMenuItem_Click);
            // 
            // procurationToolStripMenuItem
            // 
            this.procurationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mandaterToolStripMenuItem});
            this.procurationToolStripMenuItem.Name = "procurationToolStripMenuItem";
            this.procurationToolStripMenuItem.Size = new System.Drawing.Size(81, 20);
            this.procurationToolStripMenuItem.Text = "Procuration";
            this.procurationToolStripMenuItem.Visible = false;
            // 
            // mandaterToolStripMenuItem
            // 
            this.mandaterToolStripMenuItem.Name = "mandaterToolStripMenuItem";
            this.mandaterToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.mandaterToolStripMenuItem.Text = "Mandater";
            this.mandaterToolStripMenuItem.Click += new System.EventHandler(this.mandaterToolStripMenuItem_Click);
            // 
            // scrutinToolStripMenuItem
            // 
            this.scrutinToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.paramétrageToolStripMenuItem1,
            this.résultatToolStripMenuItem,
            this.validationRésultatToolStripMenuItem,
            this.statistiqueToolStripMenuItem});
            this.scrutinToolStripMenuItem.Name = "scrutinToolStripMenuItem";
            this.scrutinToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.scrutinToolStripMenuItem.Text = "Scrutin";
            this.scrutinToolStripMenuItem.Visible = false;
            this.scrutinToolStripMenuItem.Click += new System.EventHandler(this.scrutinToolStripMenuItem_Click);
            // 
            // paramétrageToolStripMenuItem1
            // 
            this.paramétrageToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ajoutPartiePolitiqueToolStripMenuItem,
            this.membreDunePartiePolituqeToolStripMenuItem});
            this.paramétrageToolStripMenuItem1.Name = "paramétrageToolStripMenuItem1";
            this.paramétrageToolStripMenuItem1.Size = new System.Drawing.Size(169, 22);
            this.paramétrageToolStripMenuItem1.Text = "Paramétrage";
            this.paramétrageToolStripMenuItem1.Click += new System.EventHandler(this.paramétrageToolStripMenuItem1_Click);
            // 
            // ajoutPartiePolitiqueToolStripMenuItem
            // 
            this.ajoutPartiePolitiqueToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ajoutPartiePolitiqueToolStripMenuItem1,
            this.modificationPartiePolitiqueToolStripMenuItem});
            this.ajoutPartiePolitiqueToolStripMenuItem.Name = "ajoutPartiePolitiqueToolStripMenuItem";
            this.ajoutPartiePolitiqueToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.ajoutPartiePolitiqueToolStripMenuItem.Text = "Partie Politique";
            // 
            // ajoutPartiePolitiqueToolStripMenuItem1
            // 
            this.ajoutPartiePolitiqueToolStripMenuItem1.Name = "ajoutPartiePolitiqueToolStripMenuItem1";
            this.ajoutPartiePolitiqueToolStripMenuItem1.Size = new System.Drawing.Size(225, 22);
            this.ajoutPartiePolitiqueToolStripMenuItem1.Text = "Ajout Partie Politique";
            this.ajoutPartiePolitiqueToolStripMenuItem1.Click += new System.EventHandler(this.ajoutPartiePolitiqueToolStripMenuItem1_Click);
            // 
            // modificationPartiePolitiqueToolStripMenuItem
            // 
            this.modificationPartiePolitiqueToolStripMenuItem.Name = "modificationPartiePolitiqueToolStripMenuItem";
            this.modificationPartiePolitiqueToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.modificationPartiePolitiqueToolStripMenuItem.Text = "Modification Partie Politique";
            this.modificationPartiePolitiqueToolStripMenuItem.Click += new System.EventHandler(this.modificationPartiePolitiqueToolStripMenuItem_Click);
            // 
            // membreDunePartiePolituqeToolStripMenuItem
            // 
            this.membreDunePartiePolituqeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ajoutMembreDunePartiePolituqeToolStripMenuItem,
            this.modificationMembreDunePartiePolituqeToolStripMenuItem});
            this.membreDunePartiePolituqeToolStripMenuItem.Name = "membreDunePartiePolituqeToolStripMenuItem";
            this.membreDunePartiePolituqeToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.membreDunePartiePolituqeToolStripMenuItem.Text = "Membre d\'une Partie Polituqe";
            // 
            // ajoutMembreDunePartiePolituqeToolStripMenuItem
            // 
            this.ajoutMembreDunePartiePolituqeToolStripMenuItem.Name = "ajoutMembreDunePartiePolituqeToolStripMenuItem";
            this.ajoutMembreDunePartiePolituqeToolStripMenuItem.Size = new System.Drawing.Size(303, 22);
            this.ajoutMembreDunePartiePolituqeToolStripMenuItem.Text = "Ajout Membre d\'une Partie Polituqe";
            this.ajoutMembreDunePartiePolituqeToolStripMenuItem.Click += new System.EventHandler(this.ajoutMembreDunePartiePolituqeToolStripMenuItem_Click);
            // 
            // modificationMembreDunePartiePolituqeToolStripMenuItem
            // 
            this.modificationMembreDunePartiePolituqeToolStripMenuItem.Name = "modificationMembreDunePartiePolituqeToolStripMenuItem";
            this.modificationMembreDunePartiePolituqeToolStripMenuItem.Size = new System.Drawing.Size(303, 22);
            this.modificationMembreDunePartiePolituqeToolStripMenuItem.Text = "Modification Membre d\'une Partie Polituqe";
            this.modificationMembreDunePartiePolituqeToolStripMenuItem.Click += new System.EventHandler(this.modificationMembreDunePartiePolituqeToolStripMenuItem_Click);
            // 
            // résultatToolStripMenuItem
            // 
            this.résultatToolStripMenuItem.Name = "résultatToolStripMenuItem";
            this.résultatToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.résultatToolStripMenuItem.Text = "Saisie résultat";
            this.résultatToolStripMenuItem.Click += new System.EventHandler(this.résultatToolStripMenuItem_Click);
            // 
            // validationRésultatToolStripMenuItem
            // 
            this.validationRésultatToolStripMenuItem.Name = "validationRésultatToolStripMenuItem";
            this.validationRésultatToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.validationRésultatToolStripMenuItem.Text = "Validation résultat";
            this.validationRésultatToolStripMenuItem.Click += new System.EventHandler(this.validationRésultatToolStripMenuItem_Click);
            // 
            // statistiqueToolStripMenuItem
            // 
            this.statistiqueToolStripMenuItem.Name = "statistiqueToolStripMenuItem";
            this.statistiqueToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.statistiqueToolStripMenuItem.Text = "Statistique";
            this.statistiqueToolStripMenuItem.Click += new System.EventHandler(this.statistiqueToolStripMenuItem_Click);
            // 
            // gestionDesToolStripMenuItem
            // 
            this.gestionDesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saisieDeNombreDeVotantToolStripMenuItem,
            this.modificationNombreDeVotantDansBureauDeVoteToolStripMenuItem,
            this.saisieLheureDeSaisieDeNombreDeVotantToolStripMenuItem,
            this.affichageTauxDeParticipationToolStripMenuItem});
            this.gestionDesToolStripMenuItem.Name = "gestionDesToolStripMenuItem";
            this.gestionDesToolStripMenuItem.Size = new System.Drawing.Size(145, 20);
            this.gestionDesToolStripMenuItem.Text = "Gestion de participation";
            this.gestionDesToolStripMenuItem.Visible = false;
            // 
            // saisieDeNombreDeVotantToolStripMenuItem
            // 
            this.saisieDeNombreDeVotantToolStripMenuItem.Name = "saisieDeNombreDeVotantToolStripMenuItem";
            this.saisieDeNombreDeVotantToolStripMenuItem.Size = new System.Drawing.Size(367, 22);
            this.saisieDeNombreDeVotantToolStripMenuItem.Text = "Saisie de nombre de votant";
            this.saisieDeNombreDeVotantToolStripMenuItem.Click += new System.EventHandler(this.saisieDeNombreDeVotantToolStripMenuItem_Click);
            // 
            // modificationNombreDeVotantDansBureauDeVoteToolStripMenuItem
            // 
            this.modificationNombreDeVotantDansBureauDeVoteToolStripMenuItem.Name = "modificationNombreDeVotantDansBureauDeVoteToolStripMenuItem";
            this.modificationNombreDeVotantDansBureauDeVoteToolStripMenuItem.Size = new System.Drawing.Size(367, 22);
            this.modificationNombreDeVotantDansBureauDeVoteToolStripMenuItem.Text = "modification nombre de votant dans un bureau de vote";
            this.modificationNombreDeVotantDansBureauDeVoteToolStripMenuItem.Click += new System.EventHandler(this.modificationNombreDeVotantDansBureauDeVoteToolStripMenuItem_Click);
            // 
            // saisieLheureDeSaisieDeNombreDeVotantToolStripMenuItem
            // 
            this.saisieLheureDeSaisieDeNombreDeVotantToolStripMenuItem.Name = "saisieLheureDeSaisieDeNombreDeVotantToolStripMenuItem";
            this.saisieLheureDeSaisieDeNombreDeVotantToolStripMenuItem.Size = new System.Drawing.Size(367, 22);
            this.saisieLheureDeSaisieDeNombreDeVotantToolStripMenuItem.Text = "Saisie l\'heure de saisie de nombre de votant";
            this.saisieLheureDeSaisieDeNombreDeVotantToolStripMenuItem.Click += new System.EventHandler(this.saisieLheureDeSaisieDeNombreDeVotantToolStripMenuItem_Click);
            // 
            // affichageTauxDeParticipationToolStripMenuItem
            // 
            this.affichageTauxDeParticipationToolStripMenuItem.Name = "affichageTauxDeParticipationToolStripMenuItem";
            this.affichageTauxDeParticipationToolStripMenuItem.Size = new System.Drawing.Size(367, 22);
            this.affichageTauxDeParticipationToolStripMenuItem.Text = "Affichage taux de participation";
            this.affichageTauxDeParticipationToolStripMenuItem.Click += new System.EventHandler(this.affichageTauxDeParticipationToolStripMenuItem_Click);
            // 
            // quitterToolStripMenuItem
            // 
            this.quitterToolStripMenuItem.Name = "quitterToolStripMenuItem";
            this.quitterToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.quitterToolStripMenuItem.Text = "Quitter";
            this.quitterToolStripMenuItem.Click += new System.EventHandler(this.quitterToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.aboutToolStripMenuItem.Text = "A propos";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(1234, 406);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "S.G.E - Système de Gestion Electoral";
            this.TransparencyKey = System.Drawing.Color.Red;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_Closing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem saisiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rechercheToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem administrationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem créerUnAgentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem auditToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem electeurToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem parListeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saisieToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ApprouverToolStripMenuItem;
        private ToolStripMenuItem validerToolStripMenuItem;
        private ToolStripMenuItem affectationToolStripMenuItem;
        private ToolStripMenuItem affectationParListeToolStripMenuItem;
        private ToolStripMenuItem réaffectationToolStripMenuItem;
        private ToolStripMenuItem réaffectationEnMasseToolStripMenuItem;
        private ToolStripMenuItem paramétrageToolStripMenuItem;
        private ToolStripMenuItem radiationToolStripMenuItem;
        private ToolStripMenuItem radierToolStripMenuItem;
        private ToolStripMenuItem listesDesRadiésToolStripMenuItem;
        private ToolStripMenuItem consultationToolStripMenuItem1;
        private ToolStripMenuItem historiqueToolStripMenuItem1;
        private ToolStripMenuItem suspendreUnAgentToolStripMenuItem;
        private ToolStripMenuItem importNouveauCNIToolStripMenuItem;
        private ToolStripMenuItem nouveauCINToolStripMenuItem;
        private ToolStripMenuItem réactivationDunAgentToolStripMenuItem;
        private ToolStripMenuItem outilsToolStripMenuItem;
        private ToolStripMenuItem changerMonMotDePasseToolStripMenuItem;
        private ToolStripMenuItem réinitialiserLeMotDePasseToolStripMenuItem;
        private ToolStripMenuItem rechercheAncienneCINToolStripMenuItem;
        private ToolStripMenuItem quitterToolStripMenuItem;
        private ToolStripMenuItem affectationIndividuelleToolStripMenuItem;
        private ToolStripMenuItem paramétrageCentreEtBureauToolStripMenuItem;
        private ToolStripMenuItem rechercheDesRadiésToolStripMenuItem;
        private ToolStripMenuItem impressionToolStripMenuItem;
        private ToolStripMenuItem carteDélecteurToolStripMenuItem;
        private ToolStripMenuItem listeDaffichageToolStripMenuItem;
        private ToolStripMenuItem listeDemargementToolStripMenuItem;
        private ToolStripMenuItem emargementparrégionToolStripMenuItem;
        private ToolStripMenuItem emargementparBureauToolStripMenuItem;
        private ToolStripMenuItem retraitToolStripMenuItem;
        private ToolStripMenuItem retraitCarteToolStripMenuItem;
        private ToolStripMenuItem listeDeRetraitToolStripMenuItem;
        private ToolStripMenuItem radiéInterregionToolStripMenuItem;
        private ToolStripMenuItem lieuxDeVoteToolStripMenuItem;
        private ToolStripMenuItem electionToolStripMenuItem;
        private ToolStripMenuItem typeElectionToolStripMenuItem;
        private ToolStripMenuItem reunionToolStripMenuItem;
        private ToolStripMenuItem typeReunionToolStripMenuItem;
        private ToolStripMenuItem membreLieuToolStripMenuItem;
        private ToolStripMenuItem ajoutElectionToolStripMenuItem;
        private ToolStripMenuItem modificationElectionToolStripMenuItem;
        private ToolStripMenuItem ajoutTypeElectionToolStripMenuItem;
        private ToolStripMenuItem modificationTypeElectionToolStripMenuItem;
        private ToolStripMenuItem ajoutReunionToolStripMenuItem;
        private ToolStripMenuItem modificationReunionToolStripMenuItem;
        private ToolStripMenuItem ajoutTypeReunionToolStripMenuItem;
        private ToolStripMenuItem modificationTypeReunionToolStripMenuItem;
        private ToolStripMenuItem ajoutMembreLieuVoteToolStripMenuItem;
        private ToolStripMenuItem modificationMembreLieuVoteToolStripMenuItem;
        private ToolStripMenuItem prefetToolStripMenuItem;
        private ToolStripMenuItem ajoutPrefetToolStripMenuItem;
        private ToolStripMenuItem modificationPrefetToolStripMenuItem;
        private ToolStripMenuItem convocationToolStripMenuItem;
        private ToolStripMenuItem ajoutConvocationToolStripMenuItem;
        private ToolStripMenuItem modificationConvocationToolStripMenuItem;
        private ToolStripMenuItem fonctionToolStripMenuItem;
        private ToolStripMenuItem ajoutFonctionToolStripMenuItem;
        private ToolStripMenuItem modificationFonctionToolStripMenuItem;
        private ToolStripMenuItem tourElectionToolStripMenuItem;
        private ToolStripMenuItem ajoutTourElectionToolStripMenuItem;
        private ToolStripMenuItem modificationTourElectionToolStripMenuItem;
        private ToolStripMenuItem editionDeConvocationToolStripMenuItem;
        private ToolStripMenuItem scrutinToolStripMenuItem;
        private ToolStripMenuItem paramétrageToolStripMenuItem1;
        private ToolStripMenuItem résultatToolStripMenuItem;
        private ToolStripMenuItem statistiqueToolStripMenuItem;
        private ToolStripMenuItem procurationToolStripMenuItem;
        private ToolStripMenuItem mandaterToolStripMenuItem;
        private ToolStripMenuItem gestionDesToolStripMenuItem;
        private ToolStripMenuItem saisieDeNombreDeVotantToolStripMenuItem;
        private ToolStripMenuItem modificationNombreDeVotantDansBureauDeVoteToolStripMenuItem;
        private ToolStripMenuItem saisieLheureDeSaisieDeNombreDeVotantToolStripMenuItem;
        private ToolStripMenuItem affichageTauxDeParticipationToolStripMenuItem;
        private ToolStripMenuItem validationRésultatToolStripMenuItem;
        private ToolStripMenuItem ajoutPartiePolitiqueToolStripMenuItem;
        private ToolStripMenuItem ajoutPartiePolitiqueToolStripMenuItem1;
        private ToolStripMenuItem modificationPartiePolitiqueToolStripMenuItem;
        private ToolStripMenuItem membreDunePartiePolituqeToolStripMenuItem;
        private ToolStripMenuItem ajoutMembreDunePartiePolituqeToolStripMenuItem;
        private ToolStripMenuItem modificationMembreDunePartiePolituqeToolStripMenuItem;
    }
}

