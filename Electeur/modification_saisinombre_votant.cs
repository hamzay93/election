﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class modification_saisinombre_votant : Form
    {
        public modification_saisinombre_votant()
        {
            InitializeComponent();
        }
        string clause;
        //int RégionName, ArrondisName, CommuneName, strID;
        Connection conn = new Connection();
        GetValue get = new GetValue();
        private void button1_Click(object sender, EventArgs e)
        {

           
           
            if ((comboBoxBV.Text.Length!=0 ) && comboBox1.Text.Length!=0)
            {

                //if (comboBoxRégion.SelectedValue.ToString() == "1")
                //{
                    string sCon = conn.connectdb();
                    SqlConnection con = new SqlConnection(sCon);
                    string str = "SET CONCAT_NULL_YIELDS_NULL OFF; SELECT BureauID,id_particip, NomBureau  AS 'Bureau de vote',nombre_votantInscris AS 'Nombre de votant',heure FROM View_participant_bureau where  BureauID =" + Int32.Parse(comboBoxBV.SelectedValue.ToString()) + " and horaire_id=" + Int32.Parse(comboBox1.SelectedValue.ToString()) + "  and ElectionID=" + Int32.Parse(comboBoxElect.SelectedValue.ToString()) + "";
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter(str, con);
                    SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sqlAdapter);

                    DataTable table = new DataTable();
                    table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                    sqlAdapter.Fill(table);
                    dataGridView1.DataSource = table;
                    if (dataGridView1.Rows.Count == 0)
                    {

                        panel1.Visible = false;
                        button2.Enabled = false;

                        MessageBox.Show("Aucun nombre de votant d'un bureau donnée retrouvé pour ces critères");

                    }
                    else
                    {

                        panel1.Visible = true;
                        button2.Enabled = true;
                    }

                    this.dataGridView1.Columns["id_particip"].Visible = false;
                    this.dataGridView1.Columns["BureauID"].Visible = false;


                    con.Open();
                    clause = "";
                //}
                //else
                //{
                //    string sCon = conn.connectdb();
                //    SqlConnection con = new SqlConnection(sCon);
                //    string str = "SET CONCAT_NULL_YIELDS_NULL OFF; SELECT BureauID,id_participant, NomBureau  AS 'Bureau de vote',nombre_votant AS 'Nombre de votant',heure FROM View_participant_bureau where  BureauID =" + Int32.Parse(comboBoxBV.SelectedValue.ToString()) + " and horaire_id=" + Int32.Parse(comboBox1.SelectedValue.ToString()) + "";
                //    SqlDataAdapter sqlAdapter = new SqlDataAdapter(str, con);
                //    SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sqlAdapter);

                //    DataTable table = new DataTable();
                //    table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                //    sqlAdapter.Fill(table);
                //    dataGridView1.DataSource = table;
                //    if (dataGridView1.Rows.Count == 0)
                //    {

                //        panel1.Visible = false;
                //        button2.Enabled = false;

                //        MessageBox.Show("Aucun nombre de votant d'un bureau donnée retrouvé pour ces critères");

                //    }
                //    else
                //    {

                //        panel1.Visible = true;
                //        button2.Enabled = true;
                //    }

                //    this.dataGridView1.Columns["id_participant"].Visible = false;
                //    this.dataGridView1.Columns["BureauID"].Visible = false;


                //    con.Open();
                //    clause = "";
                //}
            }
            else
            {
                MessageBox.Show("veuillez remplir un champ au minimum");
                this.Focus();

            }
        }
        public void IsEmptyfield()
        {
            clause = null;
            foreach (Control X in this.panel2.Controls)
            {

                if (X is TextBox || X is ComboBox)
                {
                    if (X.Text.Trim().Length != 0)
                        {

                            if (string.IsNullOrEmpty(clause))
                            {
                                clause = clause + X.Tag + " = '" + X.Text.Trim() + "'";
                                //return clause;
                            }
                            else
                            {
                                clause = clause + " And " + X.Tag + " = '" + X.Text.Trim() + "'";
                                //return clause;
                            }
                        }
                  
                }
            }

        }
         string clause3;
         private void filldetails()
         {

             this.selection();
             if (String.IsNullOrEmpty(clause3))
             {

                 this.panel1.Visible = false;

             }
             else
             {
                 this.Fillheure2();

                 this.panel1.Visible = true;
                 string sCon = conn.connectdb();
                 SqlConnection con = new SqlConnection(sCon);
                 string str = "SELECT * FROM View_participant_bureau where id_particip= " + clause3;
                 SqlCommand cmd = new SqlCommand(str, con);
                 con.Open();

                 using (SqlDataReader read = cmd.ExecuteReader())

                     while (read.Read())
                     {
                         textBoxnbr.Text = (read["nombre_votantInscris"].ToString());
                         textBox1.Text = (read["nombre_votant_ord"].ToString());
                         comboBox2.SelectedValue = (read["horaire_id"].ToString()); ;
                         InscrisBureau = Convert.ToInt32((read["totalebureauvote"].ToString()));
                         
                     }
             }
                       
         }
        private void FillBureau()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT NomBureau, BureauID FROM Bureau ";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxBV.ValueMember = "BureauID";
                comboBoxBV.DisplayMember = "NomBureau";
                comboBoxBV.DataSource = objDs.Tables[0];
            }
        }
        private void button3_Click(object sender, EventArgs e)
        {
            ClearClass1 clr = new ClearClass1();
            clr.RecursiveClearTextBoxes(this.panel2.Controls);
        }

        private void modification_saisinombre_votant_Load(object sender, EventArgs e)
        {
             this.FillRégion();
             //this.Fillheure();
             //this.Fillheure2();
             this.FillElection();
        }
        private void FillElection()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT ElectionID, ObjetElection +' - '+ObjetTourElection as NomElection from View_Elect where Etat= 1";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBoxElect.ValueMember = "ElectionID";
            comboBoxElect.DisplayMember = "NomElection";
            comboBoxElect.DataSource = objDs.Tables[0];
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
             
            ClearClass1 clr1 = new ClearClass1();
            clr1.RecursiveClearTextBoxes(this.panel1.Controls);
          
            this.filldetails();
        }
         //string adresseID1, StatutID;
        private void selection()
        {
            int counter3;
            clause3 = null;
           
            // Iterate through all the rows and sum up the appropriate columns. 
            for (counter3 = 0; counter3 < (dataGridView1.Rows.Count);
                counter3++)
            {
                //dataGridView1.Rows.
                if (dataGridView1.Rows[counter3].Selected)
                {
                    if (dataGridView1.Rows[counter3].Cells["id_particip"].Value
                    != null)
                    {
                        if (dataGridView1.Rows[counter3].
                            Cells["id_particip"].Value.ToString().Length != 0)
                        {
                            if (string.IsNullOrEmpty(clause3))
                            {
                                clause3 = clause3 + int.Parse(dataGridView1.Rows[counter3].
                                    Cells["id_particip"].Value.ToString());
                            }
                            else
                            {
                                clause3 = clause3 + "," + int.Parse(dataGridView1.Rows[counter3].
                                Cells["id_particip"].Value.ToString());

                            }

                        }
                    }
                  
                }

            }
        }
        int InscrisBureau = 0;
        private void button2_Click(object sender, EventArgs e)
        {
            if (textBoxnbr.Text.Length != 0 && comboBox2.Text.Length != 0  && textBox1.Text.Length != 0 )
            {
                try
                {

                    int Tot = Int32.Parse(textBox1.Text.ToString()) + Int32.Parse(textBoxnbr.Text.ToString());
                    if (Tot > (InscrisBureau + (Int32.Parse(textBox1.Text.ToString()))))
                    {
                        MessageBox.Show("le nombre de votant ne doit pas etre superieur au nombre d'électeur inscris");
                    }
                    else
                    {

                        string sConh = conn.connectdb();
                        SqlConnection con = new SqlConnection(sConh);
                        SqlCommand cmdh = new SqlCommand();
                        cmdh.Connection = con;
                        cmdh.CommandType = CommandType.Text;
                        cmdh.CommandText = "SELECT horaire_id, heure FROM horaire where horaire_id="+Convert.ToInt32(comboBox2.SelectedValue.ToString())+"-"+1;
                        DataSet objDsh = new DataSet();
                        SqlDataAdapter dAdapterh = new SqlDataAdapter();
                        dAdapterh.SelectCommand = cmdh;
                        con.Open();
                        dAdapterh.Fill(objDsh);
                        con.Close();
                        if (objDsh.Tables[0].Rows.Count > 0)
                        {

                            int seuilvotanttot = 0;
                            int seuilvotantinscrit = 0;
                            int seuilvotantord = 0;

                            string SQL = "select MAX(nombre_votantsum) as  nombrevotantmax,MAX(nombre_votantInscris) as votantinscritmax,MAX(nombre_votant_ord) as votant_ordmax   from View_participant_bureau where BureauID=" + Convert.ToInt32(comboBoxBV.SelectedValue.ToString()) + "and Horaire_id =" + Convert.ToInt32(comboBox2.SelectedValue.ToString())+"-"+1;

                            string sCon1 = conn.connectdb();
                            SqlConnection dbConn1;
                            dbConn1 = new SqlConnection(sCon1);
                            dbConn1.Open();
                            SqlDataAdapter sqlAdapter = new SqlDataAdapter(SQL, dbConn1);
                            SqlCommandBuilder commandBuilder1 = new SqlCommandBuilder(sqlAdapter);
                            DataTable dt = new DataTable();
                            sqlAdapter.Fill(dt);
                            dbConn1.Close();

                            if (dt.Rows.Count > 0)
                            {
                                DataRow dtRow = dt.Rows[0];

                                seuilvotanttot = Convert.ToInt32((!object.ReferenceEquals(dtRow["nombrevotantmax"], DBNull.Value) ? dtRow["nombrevotantmax"].ToString() : "0"));
                                seuilvotantord = Convert.ToInt32((!object.ReferenceEquals(dtRow["votant_ordmax"], DBNull.Value) ? dtRow["votant_ordmax"].ToString() : "0"));
                                seuilvotantinscrit = Convert.ToInt32((!object.ReferenceEquals(dtRow["votantinscritmax"], DBNull.Value) ? dtRow["votantinscritmax"].ToString() : "0"));
                            
                            }


                            if (Tot < seuilvotanttot || Int32.Parse(textBoxnbr.Text.ToString()) < seuilvotantinscrit || Int32.Parse(textBox1.Text.ToString()) < seuilvotantord)
                            {
                                MessageBox.Show("le nombre à saisir doit-etre superieur ou égale au saisie precedent Totale=" + seuilvotanttot + ", votant_inscrit=" + seuilvotantinscrit + ", votant_ordonnance=" + seuilvotantord);
                            }

                            else
                            {

                                string sCon = conn.connectdb();
                                string insertCmd = "update particip set nombre_votant_ord=@nombre_votant_ord,Horaire_id=@horaire_id,nombre_votantInscris=@nombre_votantInscris where id_particip=" + clause3;
                                SqlConnection dbConn;
                                dbConn = new SqlConnection(sCon);
                                dbConn.Open();
                                Login lg = new Login();
                                //string str;
                                string dat = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                DateTime date4 = DateTime.Parse(dat);

                                GetValue value = new GetValue();
                                //getAdress add = new getAdress();
                                SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                                // Create parameters for the SqlCommand object
                                // initialize with input-form field values
                                //myCommand.Parameters.AddWithValue("@ElecteurID","");
                                myCommand.Parameters.AddWithValue("@nombre_votantInscris", textBoxnbr.Text);
                                myCommand.Parameters.AddWithValue("@nombre_votant_ord", textBox1.Text);
                                myCommand.Parameters.AddWithValue("@horaire_id", Int32.Parse(comboBox2.SelectedValue.ToString()));

                                myCommand.Parameters.AddWithValue("@AgentModif", value.getAgentModif());//recupere agent modif



                                myCommand.Parameters.AddWithValue("@DateModification", dat);


                                myCommand.ExecuteNonQuery();
                                MessageBox.Show("Modification effectué avec succès");
                                ClearClass1 clr1 = new ClearClass1();
                                clr1.RecursiveClearTextBoxes(this.panel1.Controls);
                            }
                        }
                        else {


                            string sCon = conn.connectdb();
                            string insertCmd = "update particip set nombre_votant_ord=@nombre_votant_ord,Horaire_id=@horaire_id,nombre_votantInscris=@nombre_votantInscris where id_particip=" + clause3;
                            SqlConnection dbConn;
                            dbConn = new SqlConnection(sCon);
                            dbConn.Open();
                            Login lg = new Login();
                            //string str;
                            string dat = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                            DateTime date4 = DateTime.Parse(dat);

                            GetValue value = new GetValue();
                            //getAdress add = new getAdress();
                            SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                            // Create parameters for the SqlCommand object
                            // initialize with input-form field values
                            //myCommand.Parameters.AddWithValue("@ElecteurID","");
                            myCommand.Parameters.AddWithValue("@nombre_votantInscris", textBoxnbr.Text);
                            myCommand.Parameters.AddWithValue("@nombre_votant_ord", textBox1.Text);
                            myCommand.Parameters.AddWithValue("@horaire_id", Int32.Parse(comboBox2.SelectedValue.ToString()));

                            myCommand.Parameters.AddWithValue("@AgentModif", value.getAgentModif());//recupere agent modif



                            myCommand.Parameters.AddWithValue("@DateModification", dat);


                            myCommand.ExecuteNonQuery();
                            MessageBox.Show("Modification effectué avec succès");
                            ClearClass1 clr1 = new ClearClass1();
                            clr1.RecursiveClearTextBoxes(this.panel1.Controls);

                        }
                    }
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.ToString());
                }
               
            }
            else
            {
                MessageBox.Show("aucun champs ne doit etre vide!");
            }
        }
        private void FillRégion()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT RégionID, NomRégion FROM Région";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            objDs.Tables[0].Rows.Add(30, "");
            con.Close();
            comboBoxRégion.ValueMember = "RégionID";
            comboBoxRégion.DisplayMember = "NomRégion";
            comboBoxRégion.DataSource = objDs.Tables[0];
            comboBoxRégion.SelectedValue = 30;
        }
        private void Fillheure()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT horaire_id, heure FROM horaire where horaire_id in(select horaire_id from Particip where BUREAU_ID = " + Int32.Parse(comboBoxBV.SelectedValue.ToString()) + ")";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBox1.ValueMember = "horaire_id";
            comboBox1.DisplayMember = "heure";
            comboBox1.DataSource = objDs.Tables[0];
        }
        private void Fillheure2()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT horaire_id, heure FROM horaire";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBox2.ValueMember = "horaire_id";
            comboBox2.DisplayMember = "heure";
            comboBox2.DataSource = objDs.Tables[0];
        }
        private void FillCommune(int RégionID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT CommuneID, NomCommune FROM Commune WHERE RégionID =@RégionID";
            if (RégionID == 30)
            {
                int reg = 1;
                cmd.Parameters.AddWithValue("@RégionID", reg);


            }
            else
            {
                cmd.Parameters.AddWithValue("@RégionID", RégionID);
            }

            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            objDs.Tables[0].Rows.Add(30, "");
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxCommune.ValueMember = "CommuneID";
                comboBoxCommune.DisplayMember = "NomCommune";
                comboBoxCommune.DataSource = objDs.Tables[0];
                comboBoxCommune.SelectedValue = 30;
            }
        }
        private void comboBoxCommune_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Int32.Parse(comboBoxCommune.SelectedValue.ToString()) == 30)
            {
                int CommuneID = Convert.ToInt32(comboBoxCommune.SelectedValue.ToString());
                this.FillArrondis(CommuneID);
                this.comboBoxArrondis.Enabled = false;
                //comboBoxArrondis.SelectedIndex = 0;
            }
            else
            {
                int CommuneID = Convert.ToInt32(comboBoxCommune.SelectedValue.ToString());
                this.FillArrondis(CommuneID);
                this.comboBoxArrondis.Enabled = true;
            }
        }
        private void comboBoxArrondis_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Int32.Parse(comboBoxArrondis.SelectedValue.ToString()) == 30)
            {
                int CommuneID = Convert.ToInt32(comboBoxArrondis.SelectedValue.ToString());
                this.FillCV(CommuneID);
                //this.comboBoxAdresse.Enabled = false;
                //comboBoxArrondis.SelectedIndex = 0;
            }
            else
            {
                int CommuneID = Convert.ToInt32(comboBoxArrondis.SelectedValue.ToString());
                this.FillCV(CommuneID);
                //this.comboBoxAdresse.Enabled = true;
            }
        }
        private void FillCV(int ArrondisID)
        {
            string sCon = conn.connectdb(); ;
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            if (comboBoxArrondis.Visible)
            {
                cmd.CommandText = "SET CONCAT_NULL_YIELDS_NULL OFF;SELECT CentreID, ( CONVERT(Nvarchar(50), CodeCentre)) + ' - '+CentreVote as CentreVote FROM View_CV where Actif = 1 and ArrondisID =@ArrondisID ORDER BY CodeCentre";
                if (ArrondisID == 30)
                {
                    int com = 1;
                    //cmd.CommandText = "SELECT CentreID, CentreVote FROM View_CV where Actif = 1 and ArrondisID =@ArrondisID ORDER BY CodeCentre";
                    cmd.Parameters.AddWithValue("@ArrondisID", com);


                }
                else
                {

                    cmd.Parameters.AddWithValue("@ArrondisID", ArrondisID);
                }
            }
            else
            {
                cmd.CommandText = "SELECT CentreID, CentreVote FROM View_CV where Actif = 1 and LocalitéID =@ArrondisID ORDER BY CodeCentre";
                if (ArrondisID == 30000)
                {

                    int com = 1;
                    cmd.Parameters.AddWithValue("@ArrondisID", com);


                }
                else
                {
                    cmd.Parameters.AddWithValue("@ArrondisID", ArrondisID);
                }
            }

            //cmd.Parameters.AddWithValue("@ArrondisID", ArrondisID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            objDs.Tables[0].Rows.Add(3000, "");
            con.Close();
            comboBoxCV.ValueMember = "CentreID";
            comboBoxCV.DisplayMember = "CentreVote";
            comboBoxCV.DataSource = objDs.Tables[0];
            comboBoxCV.SelectedValue = 3000;
        }
        private void FillBureau(int CentreID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SET CONCAT_NULL_YIELDS_NULL OFF;SELECT BureauID, ( CONVERT(Nvarchar(50), CodeBureau)) + ' - '+NomBureau as NomBureau FROM View_LV WHERE CentreID =@CentreID ORDER BY CodeCentre,OrdreCentre";
            cmd.Parameters.AddWithValue("@CentreID", CentreID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            objDs.Tables[0].Rows.Add(3000, "");
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxBV.ValueMember = "BureauID";
                comboBoxBV.DisplayMember = "NomBureau";
                comboBoxBV.DataSource = objDs.Tables[0];
                comboBoxBV.SelectedValue = 3000;
            }
        }
        private void FillArrondis(int CommuneID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT ArrondisID, NomArrondis FROM Arrondissement WHERE CommuneID =@CommuneID";
            if (CommuneID == 30)
            {
                int com = 1;
                cmd.Parameters.AddWithValue("@CommuneID", com);


            }
            else
            {
                cmd.Parameters.AddWithValue("@CommuneID", CommuneID);
            }
            //cmd.Parameters.AddWithValue("@CommuneID", CommuneID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            objDs.Tables[0].Rows.Add(30, "");
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxArrondis.ValueMember = "ArrondisID";
                comboBoxArrondis.DisplayMember = "NomArrondis";
                comboBoxArrondis.DataSource = objDs.Tables[0];
                comboBoxArrondis.SelectedValue = 30;


            }
        }
        private void Filllocalite(int _reg)
        {
            int reg = _reg;
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT LocalitéID, NomLocalité FROM Localité WHERE LocalitéID in(Select LocalitéID from View_CV) and RégionID = " + reg;
            //cmd.Parameters.AddWithValue("@RégionID", RégionID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            objDs.Tables[0].Rows.Add(30000, "");
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxLoc.ValueMember = "LocalitéID";
                comboBoxLoc.DisplayMember = "NomLocalité";
                comboBoxLoc.DataSource = objDs.Tables[0];
                comboBoxLoc.SelectedValue = 30000;
            }
        }
        private void comboBoxRégion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Int32.Parse(comboBoxRégion.SelectedValue.ToString()) == 1)
            {
                int RégionID = Convert.ToInt32(comboBoxRégion.SelectedValue.ToString());
                FillCommune(RégionID);
                //comboBoxCommune.SelectedIndex = 0;
                labelCom.Text = "Commune";
                comboBoxLoc.Visible = false;
                //labelAddress.Visible = true;
                labelArrondis.Visible = true;
                //comboBoxAdresse.Visible = true;
                comboBoxArrondis.Visible = true;
                comboBoxCommune.Visible = true;
                //comboBoxAdresse.Enabled = true;
                comboBoxArrondis.Enabled = true;
                comboBoxCommune.Enabled = true;
            }
            else
            {
                if (Int32.Parse(comboBoxRégion.SelectedValue.ToString()) == 30)
                {
                    int RégionID = Convert.ToInt32(comboBoxRégion.SelectedValue.ToString());
                    FillCommune(RégionID);
                    //comboBoxCommune.SelectedIndex = 0;
                    labelCom.Text = "Commune";
                    comboBoxLoc.Visible = false;
                    //labelAddress.Visible = true;
                    labelArrondis.Visible = true;
                    //comboBoxAdresse.Visible = true;
                    comboBoxArrondis.Visible = true;
                    comboBoxCommune.Visible = true;
                    //comboBoxAdresse.Enabled = false;
                    comboBoxArrondis.Enabled = false;
                    comboBoxCommune.Enabled = false;
                }
                else
                {
                    this.Filllocalite(Int32.Parse(comboBoxRégion.SelectedValue.ToString()));
                    comboBoxLoc.SelectedIndex = 0;
                    comboBoxLoc.Visible = true;
                    labelCom.Text = "Localité";
                    comboBoxCommune.Visible = false;
                    labelCom.Visible = true;
                    //labelAddress.Visible = false;
                    labelArrondis.Visible = false;
                    //comboBoxAdresse.Visible = false;
                    comboBoxArrondis.Visible = false;
                }

            }
        }
        private void comboBoxCV_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.FillBureau(Int32.Parse(comboBoxCV.SelectedValue.ToString()));
        }

        private void comboBoxLoc_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Int32.Parse(comboBoxLoc.SelectedValue.ToString()) == 30)
            {
                int CommuneID = Convert.ToInt32(comboBoxLoc.SelectedValue.ToString());
                this.FillCV(CommuneID);
                //this.comboBoxAdresse.Enabled = false;
                //comboBoxArrondis.SelectedIndex = 0;
            }
            else
            {
                int CommuneID = Convert.ToInt32(comboBoxLoc.SelectedValue.ToString());
                this.FillCV(CommuneID);
                //this.comboBoxAdresse.Enabled = true;
            }
        }

        private void textBoxnbr_KeyPress(object sender, KeyPressEventArgs e)
        {
            int isNum = 0;
            if (e.KeyChar == ',' || e.KeyChar == (char)Keys.Back)
                e.Handled = false;
            else if (!int.TryParse(e.KeyChar.ToString(), out isNum))
                e.Handled = true;
        }

        private void comboBoxBV_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.Fillheure();
            ClearClass1 clr1 = new ClearClass1();
            clr1.RecursiveClearTextBoxes(this.panel1.Controls);
        }
    }
}
