﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class Affectation : Form
    {
        public Affectation()
        {
            InitializeComponent();
        }
        Connection conn = new Connection();
        private void FillRégion()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT RégionID, NomRégion FROM Région";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBoxRégion.ValueMember = "RégionID";
            comboBoxRégion.DisplayMember = "NomRégion";
            comboBoxRégion.DataSource = objDs.Tables[0];
        }
        //int RégionID;
        private void comboBoxRégion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxRégion.SelectedValue.ToString() != "")
            {
                int RégionID = Convert.ToInt32(comboBoxRégion.SelectedValue.ToString());
                FillCommune(RégionID);
                comboBoxCommune.SelectedIndex = 0;
            }
        }
        private void FillCommune(int RégionID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT CommuneID, NomCommune FROM Commune WHERE RégionID =@RégionID";
            cmd.Parameters.AddWithValue("@RégionID", RégionID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxCommune.ValueMember = "CommuneID";
                comboBoxCommune.DisplayMember = "NomCommune";
                comboBoxCommune.DataSource = objDs.Tables[0];
            }
        }
        private void comboBoxCommune_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxCommune.SelectedValue.ToString() != "")
            {
                int CommuneID = Convert.ToInt32(comboBoxCommune.SelectedValue.ToString());
                this.FillArrondis(CommuneID);
                comboBoxArrondis.SelectedIndex = 0;
            }
        }
        private void comboBoxArrondis_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxArrondis.SelectedValue.ToString() != "")
            {
                int ArrondisID = Convert.ToInt32(comboBoxArrondis.SelectedValue.ToString());
                this.FillAdresse(ArrondisID);
                //comboBoxAdresse.SelectedIndex = 0;
            }
        }
        public int getAdresseID()
        {
            //SqlDataReader rd = null;
            string query = "select AdresseID from Adresse where NomAdresse='" + comboBoxAdresse.Text + "'";
            string sCon = conn.connectdb();
            SqlConnection dbConn;
            dbConn = new SqlConnection(sCon);
            dbConn.Open();
            SqlCommand cmd = new SqlCommand(query, dbConn);
            int result = ((int)cmd.ExecuteScalar());
            //SaisieElecteur se=new SaisieElecteur();
            int adress = result;
            return adress;
            //dbConn.Close();

        }
        private void FillArrondis(int CommuneID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT ArrondisID, NomArrondis FROM Arrondissement WHERE CommuneID =@CommuneID";
            cmd.Parameters.AddWithValue("@CommuneID", CommuneID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxArrondis.ValueMember = "ArrondisID";
                comboBoxArrondis.DisplayMember = "NomArrondis";
                comboBoxArrondis.DataSource = objDs.Tables[0];
            }
        }
        private void FillAdresse(int ArrondisID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT AdresseID, NomAdresse FROM Adresse WHERE ArrondisID =@ArrondisID";
            cmd.Parameters.AddWithValue("@ArrondisID", ArrondisID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxAdresse.ValueMember = "AdresseID";
                comboBoxAdresse.DisplayMember = "NomAdresse";
                comboBoxAdresse.DataSource = objDs.Tables[0];
            }
        }
        string clause = null;
        public void IsEmptyfield()
        {

            foreach (Control X in this.panel1.Controls)
            {
                if (X is TextBox || X is ComboBox)
                {
                    if (X.Text.Trim().Length != 0)
                    {
                        if (string.IsNullOrEmpty(clause))
                        {
                            if (X.Name == "comboBoxAdresse")
                            {
                                clause = clause + X.Tag + " Like '%" + this.getAdresseID() + "%'";
                            }
                            else
                            {
                                clause = clause + X.Tag + " Like '%" + X.Text.Trim() + "%'";
                            }
                                
                            
                        }
                        else
                        {
                            if (X.Name == "comboBoxAdresse")
                            {
                                clause = clause + "And " + X.Tag + " Like '%" + this.getAdresseID() + "%'";
                            }
                            else
                            {
                                clause = clause + "And " + X.Tag + " Like '%" + X.Text.Trim() + "%'";
                            }
                           
                               // clause = clause + "And " + X.Tag + " Like '%" + X.Text.Trim() + "%'";
                            
                        }
                    }

                }
                
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.IsEmptyfield();
            //this.setRowNumber(dataGridView1);
            this.load1();
            //clause1 = "";

            
        }
        public void load1()
        {
            this.dataGridView1.RowHeadersVisible = true;
            if (String.IsNullOrEmpty(clause))
            {
                clause = "Nom1 like'%'";
            }
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            string str = "SET CONCAT_NULL_YIELDS_NULL OFF; SELECT ElecteurID, Nom1 +'  '+ Nom2 +'  '+Nom3 AS NOM,NomMère1+'  '+NomMère2+'  '+NomMère3 AS 'NOM DE LA MERE',DOB AS 'DATE DE NAISSANCE',POB AS LIEU,CodeElecteur AS 'CODE ELECTEUR',AncienCNI AS 'ANCIEN CNI',DateAncienCNI AS 'DATE DELIVRANCE',NewCNI AS 'NOUVEAU CNI',DateNewCNI AS 'DATE DELIVRANCE',Sexe AS 'SEXE' FROM Elect where " + clause + " and CentreID IS NULL and ElecteurID not in (Select ElecteurID from Radiation where Statut ='Actif') ";
            //string  str = "
            SqlDataAdapter sqlAdapter = new SqlDataAdapter(str, con);
            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sqlAdapter);

            DataTable table = new DataTable();
            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
            sqlAdapter.Fill(table);
            dataGridView1.DataSource = AutoNumberedTable(table);
            if (dataGridView1.Rows.Count == 0)
            {
                button2.Enabled = false;

            }
            else
            {
                button2.Enabled = true;
            }
            this.dataGridView1.Columns["ElecteurID"].Visible = false;
            int total = dataGridView1.RowCount;
            label14.Visible = true;
            label14.Text = total.ToString();
            clause = "";
        }
        private DataTable AutoNumberedTable(DataTable SourceTable)
        {

            DataTable ResultTable = new DataTable();

            DataColumn AutoNumberColumn = new DataColumn();

            AutoNumberColumn.ColumnName = "Num.";

            AutoNumberColumn.DataType = typeof(int);

            AutoNumberColumn.AutoIncrement = true;

            AutoNumberColumn.AutoIncrementSeed = 1;

            AutoNumberColumn.AutoIncrementStep = 1;

            ResultTable.Columns.Add(AutoNumberColumn);

            ResultTable.Merge(SourceTable);

            return ResultTable;

        }
        private void FillCV()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT CentreID, CentreVote FROM CentreVote where Actif = 1 ORDER BY CodeCentre";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBoxCV.ValueMember = "CentreID";
            comboBoxCV.DisplayMember = "CentreVote";
            comboBoxCV.DataSource = objDs.Tables[0];
        }

        private void FillBureau(int CentreID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT BureauID, NomBureau FROM Bureau WHERE CentreID =@CentreID";
            cmd.Parameters.AddWithValue("@CentreID", CentreID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
               // comboBoxBV.ValueMember = "BureauID";
                //comboBoxBV.DisplayMember = "NomBureau";
                //comboBoxBV.DataSource = objDs.Tables[0];
            }
        }
        /*private void comboBoxCV_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxCV.SelectedValue.ToString() != "")
            {
                int CentreID = Convert.ToInt32(comboBoxCV.SelectedValue.ToString());
                FillBureau(CentreID);
                comboBoxBV.SelectedIndex = 0;
            }
        }*/

        private void Affectation_Load(object sender, EventArgs e)
        {
            FillRégion();
            FillCV();
        }
        
        private void button2_Click(object sender, EventArgs e)
        {
            string clause1 = null;
            if (IndexDu.Text.Trim().Length == 0 || IndexAu.Text.Trim().Length == 0)
            {
                MessageBox.Show("veuillez saisir la tranche de numéro");
                IndexDu.Focus();
            }
            else
            {
                if (((int.Parse(IndexDu.Text) - 1) < 0) || (int.Parse(IndexAu.Text) - 1) > (dataGridView1.Rows.Count) || ((int.Parse(IndexDu.Text) - 1) > (int.Parse(IndexAu.Text) - 1)))
                {
                    MessageBox.Show("veuillez verifier l'index");
                    IndexDu.Focus();
                }
                else
                {
                    for (int counter = (int.Parse(IndexDu.Text) - 1); counter <= (int.Parse(IndexAu.Text) - 1);
                        counter++)
                    {
                        //dataGridView1.Rows.

                        if (dataGridView1.Rows[counter].Cells["ElecteurID"].Value
                        != null)
                        {
                            if (dataGridView1.Rows[counter].
                                Cells["ElecteurID"].Value.ToString().Length != 0)
                            {
                                if (string.IsNullOrEmpty(clause1))
                                {
                                    clause1 = clause1 + int.Parse(dataGridView1.Rows[counter].
                                        Cells["ElecteurID"].Value.ToString());
                                    //counter1++;
                                }
                                else
                                {
                                    clause1 = clause1 + "," + int.Parse(dataGridView1.Rows[counter].
                                    Cells["ElecteurID"].Value.ToString());
                                    //counter1++;

                                }

                            }
                        }

                    }

                    //update bureau de vote
                    Connection cn = new Connection();
                    string sCon = conn.connectdb();
                    string updateCmd = "UPDATE Electeur SET CentreID =@CentreID,AgentModif=@AgentModif,DateModification=@DateModification, BureauID=@BureauID where ElecteurID in(" + clause1 + ")";
                    SqlConnection dbConn;
                    dbConn = new SqlConnection(sCon);
                    dbConn.Open();
                    GetValue get = new GetValue();
                    //DateTime dt = new DateTime();
                    //dt.ToShortDateString();
                    string dat = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    DateTime date4 = DateTime.Parse(dat);
                    SqlCommand myCommand = new SqlCommand(updateCmd, dbConn);
                    // Create parameters for the SqlCommand object
                    // initialize with input-form field values
                    myCommand.Parameters.AddWithValue("@CentreID", Convert.ToInt32(comboBoxCV.SelectedValue.ToString()));
                    myCommand.Parameters.AddWithValue("@BureauID", DBNull.Value);
                    myCommand.Parameters.AddWithValue("@AgentModif", get.getAgentModif());
                    myCommand.Parameters.AddWithValue("@DateModification", dat);
                    myCommand.ExecuteNonQuery();
                    MessageBox.Show("Affectation effectuée avec succès");
                    this.IsEmptyfield();
                    this.load1();
                }
            }
        }
        
    }
}
