﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class AjoutMembrelieuvote : Form
    {
        Connection conn = new Connection();
        public AjoutMembrelieuvote()
        {
            InitializeComponent();
        }
        private void FillCENTREVOTE(int RégionID)
        {
            if (RégionID == 1)
            {
                string sCon = conn.connectdb();
                SqlConnection con = new SqlConnection(sCon);
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT CentreID,CentreVote FROM   dbo.Arrondissement INNER JOIN"
                          + " dbo.CentreVote ON dbo.Arrondissement.ArrondisID = dbo.CentreVote.ArrondisID INNER JOIN"
                         + " dbo.Commune ON dbo.Arrondissement.CommuneID = dbo.Commune.CommuneID INNER JOIN"
                         + " dbo.Région ON dbo.Commune.RégionID = dbo.Région.RégionID where dbo.Région.RégionID=" + RégionID;
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                con.Open();
                dAdapter.Fill(objDs);
                con.Close();
                comboBox9.ValueMember = "CentreID";
                comboBox9.DisplayMember = "CentreVote";
                comboBox9.DataSource = objDs.Tables[0];
            }
            else if (RégionID == 9)
            {
                MessageBox.Show("SANAA N'EST PAS ENCORE OPERATIONNEL");
            }
            else
            {
                string sCon = conn.connectdb();
                SqlConnection con = new SqlConnection(sCon);
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT CentreID,CentreVote FROM  dbo.CentreVote INNER JOIN"
                      + " dbo.Localité ON dbo.CentreVote.LocalitéID = dbo.Localité.LocalitéID INNER JOIN"
                      + " dbo.Région ON dbo.Localité.RégionID = dbo.Région.RégionID  where dbo.Région.RégionID=" + RégionID;
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                con.Open();
                dAdapter.Fill(objDs);
                con.Close();
                comboBox9.ValueMember = "CentreID";
                comboBox9.DisplayMember = "CentreVote";
                comboBox9.DataSource = objDs.Tables[0];
            }
        }
        private void FillBUREAUVOTE(int CentreID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT BureauID,NomBureau FROM Bureau where CentreID="+CentreID;
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBox7.ValueMember = "BureauID";
            comboBox7.DisplayMember = "NomBureau";
            comboBox7.DataSource = objDs.Tables[0];
        }
        private void FillFONCTION()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT FonctionID,ObjetFonction FROM Fonction";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBox6.ValueMember = "FonctionID";
            comboBox6.DisplayMember = "ObjetFonction";
            comboBox6.DataSource = objDs.Tables[0];
        }
        private void FillELECTION()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT ElectionID,ObjetElection FROM Election";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBox8.ValueMember = "ElectionID";
            comboBox8.DisplayMember = "ObjetElection";
            comboBox8.DataSource = objDs.Tables[0];
        }
          private void FillRégion()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT RégionID,NomRégion FROM Région";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBox1.ValueMember = "RégionID";
            comboBox1.DisplayMember = "NomRégion";
            comboBox1.DataSource = objDs.Tables[0];
        }
        private void button8_Click(object sender, EventArgs e)
        {
            if (textBox11.Text.Trim().Length == 0 || textBox12.Text.Trim().Length == 0 ||  textBox14.Text.Trim().Length == 0)
            {
                MessageBox.Show("Veuillez remplir tous les champs vides");
                textBox11.Focus(); textBox12.Focus(); textBox13.Focus(); textBox14.Focus();
            }
            else
            {
                if (comboBox6.Text == "")
                {
                    MessageBox.Show("Veuillez choisir de quel fonction il/elle occupera");
                }
                else
                {

                    if (comboBox7.Text == "")
                    {
                        MessageBox.Show("Veuillez choisir dans quel bureau de vote");
                    }
                    else
                    {

                        if (comboBox10.Text == "")
                        {
                            MessageBox.Show("Veuillez choisir le sexe de la personne");
                        }
                        else
                        {

                            if (comboBox9.Text == "")
                            {
                                MessageBox.Show("Veuillez choisir dans quel centre de vote");
                            }

                            else
                            {

                                if (comboBox8.Text == "")
                                {
                                    MessageBox.Show("Veuillez choisir sur quel election");
                                }
                                else
                                {

                                    //Connection cn = new Connection();
                                    string insertCmd = "INSERT INTO membreLieuVoteID (RégionID,nom1,nom2,nom3,DOB,POB,Sexe,FonctionID,ElectionID,CentreID,BureauID,UserCreation,UserModification,DateCreation,DateModification) VALUES (@RégionID,@nom1,@nom2,@nom3,@DOB,@POB,@Sexe,@FonctionID,@ElectionID,@CentreID,@BureauID,@UserCreation,@UserModification,@DateCreation,@DateModification)";
                                    SqlConnection dbConn;
                                    dbConn = new SqlConnection(conn.connectdb());
                                    dbConn.Open();
                                    Login lg = new Login();
                                    string dat = DateTime.Now.ToString("yyyy-MM-dd");
                                    DateTime datedn = DateTime.Parse(dateTimePicker4.Value.ToString("yyyy-MM-dd"));

                                    GetValue value = new GetValue();
                                    //getAdress add = new getAdress();
                                    SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                                    if (comboBox6.Text != "")
                                    {
                                        int fonctionID = Convert.ToInt32(comboBox6.SelectedValue.ToString());
                                        myCommand.Parameters.AddWithValue("@FonctionID", fonctionID);
                                    }
                                    if (comboBox7.Text != "")
                                    {
                                        int BureauID = Convert.ToInt32(comboBox7.SelectedValue.ToString());
                                        myCommand.Parameters.AddWithValue("@BureauID", BureauID);
                                    }
                                    if (comboBox8.Text != "")
                                    {
                                        int ElectionID = Convert.ToInt32(comboBox8.SelectedValue.ToString());
                                        myCommand.Parameters.AddWithValue("@ElectionID", ElectionID);
                                    }
                                    if (comboBox9.Text != "")
                                    {
                                        int CentreID = Convert.ToInt32(comboBox9.SelectedValue.ToString());
                                        myCommand.Parameters.AddWithValue("@CentreID", CentreID);
                                    }
                                    if (comboBox1.Text != "")
                                    {
                                        int RégionID = Convert.ToInt32(comboBox1.SelectedValue.ToString());
                                        myCommand.Parameters.AddWithValue("@RégionID", RégionID);
                                    }

                                    myCommand.Parameters.AddWithValue("@nom1", textBox12.Text.ToUpper());
                                    myCommand.Parameters.AddWithValue("@nom2", textBox14.Text.ToUpper());
                                    myCommand.Parameters.AddWithValue("@nom3", textBox13.Text.ToUpper());

                                    myCommand.Parameters.AddWithValue("@POB", textBox13.Text);

                                    myCommand.Parameters.AddWithValue("@DOB", datedn);
                                    myCommand.Parameters.AddWithValue("@Sexe", comboBox10.SelectedItem.ToString());
                                    myCommand.Parameters.AddWithValue("@UserCreation", value.getAgentCréation());
                                    myCommand.Parameters.AddWithValue("@DateCreation", dat);
                                    myCommand.Parameters.AddWithValue("@UserModification", value.getAgentCréation());
                                    myCommand.Parameters.AddWithValue("@DateModification", dat);
                                    myCommand.ExecuteNonQuery();

                                    MessageBox.Show("Ajoutée avec succès");
                                    textBox11.Clear(); textBox12.Clear(); textBox13.Clear(); textBox14.Clear();
                                }
                            }
                        }
                    }
                }

            }
        }

        private void AjoutMembrelieuvote_Load(object sender, EventArgs e)
        {
            this.FillRégion();
          
            this.FillELECTION();
            this.FillFONCTION();
        }

        private void comboBox9_SelectedIndexChanged(object sender, EventArgs e)
        {
            int centreID = Convert.ToInt32(comboBox9.SelectedValue.ToString());
            FillBUREAUVOTE(centreID);
            comboBox7.SelectedIndex = 0;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int RégionID = Convert.ToInt32(comboBox1.SelectedValue.ToString());
            FillCENTREVOTE(RégionID);
            comboBox9.SelectedIndex = 0;
        }
    }
}
