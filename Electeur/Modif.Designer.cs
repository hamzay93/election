﻿namespace Electeur
{
    partial class Modif
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.dateTimePickerDOB = new System.Windows.Forms.DateTimePicker();
            this.comboBoxSexe = new System.Windows.Forms.ComboBox();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label19 = new System.Windows.Forms.Label();
            this.textBoxCodeElecteur = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.textBoxNewCNI = new System.Windows.Forms.TextBox();
            this.textBoxNomMère3 = new System.Windows.Forms.TextBox();
            this.textBoxNomMère2 = new System.Windows.Forms.TextBox();
            this.textBoxNomMère1 = new System.Windows.Forms.TextBox();
            this.textBoxAncCNI = new System.Windows.Forms.TextBox();
            this.textBoxPOB = new System.Windows.Forms.TextBox();
            this.textBoxNom3 = new System.Windows.Forms.TextBox();
            this.textBoxNom2 = new System.Windows.Forms.TextBox();
            this.textBoxNom1 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label26 = new System.Windows.Forms.Label();
            this.textBoxAdresseCompl = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.comboBoxAdresse = new System.Windows.Forms.ComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.comboBoxCommune = new System.Windows.Forms.ComboBox();
            this.comboBoxArrondis = new System.Windows.Forms.ComboBox();
            this.comboBoxRégion = new System.Windows.Forms.ComboBox();
            this.textBoxPhone = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.LabelStatut = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dateTimePickerDOB);
            this.panel1.Controls.Add(this.comboBoxSexe);
            this.panel1.Controls.Add(this.dateTimePicker2);
            this.panel1.Controls.Add(this.dateTimePicker1);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.textBoxCodeElecteur);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.textBoxNewCNI);
            this.panel1.Controls.Add(this.textBoxNomMère3);
            this.panel1.Controls.Add(this.textBoxNomMère2);
            this.panel1.Controls.Add(this.textBoxNomMère1);
            this.panel1.Controls.Add(this.textBoxAncCNI);
            this.panel1.Controls.Add(this.textBoxPOB);
            this.panel1.Controls.Add(this.textBoxNom3);
            this.panel1.Controls.Add(this.textBoxNom2);
            this.panel1.Controls.Add(this.textBoxNom1);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(109, 168);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(791, 200);
            this.panel1.TabIndex = 31;
            // 
            // dateTimePickerDOB
            // 
            this.dateTimePickerDOB.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerDOB.Location = new System.Drawing.Point(193, 52);
            this.dateTimePickerDOB.Name = "dateTimePickerDOB";
            this.dateTimePickerDOB.Size = new System.Drawing.Size(192, 20);
            this.dateTimePickerDOB.TabIndex = 11;
            this.dateTimePickerDOB.Value = new System.DateTime(1997, 8, 23, 8, 15, 1, 773);
            // 
            // comboBoxSexe
            // 
            this.comboBoxSexe.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSexe.FormattingEnabled = true;
            this.comboBoxSexe.Items.AddRange(new object[] {
            "FEMININ",
            "MASCULIN"});
            this.comboBoxSexe.Location = new System.Drawing.Point(551, 170);
            this.comboBoxSexe.Name = "comboBoxSexe";
            this.comboBoxSexe.Size = new System.Drawing.Size(189, 21);
            this.comboBoxSexe.TabIndex = 21;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Enabled = false;
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker2.Location = new System.Drawing.Point(551, 135);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(189, 20);
            this.dateTimePicker2.TabIndex = 19;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Enabled = false;
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(551, 107);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(189, 20);
            this.dateTimePicker1.TabIndex = 17;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label19.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label19.Location = new System.Drawing.Point(395, 170);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(51, 19);
            this.label19.TabIndex = 22;
            this.label19.Text = "Sexe :";
            // 
            // textBoxCodeElecteur
            // 
            this.textBoxCodeElecteur.Enabled = false;
            this.textBoxCodeElecteur.Location = new System.Drawing.Point(193, 168);
            this.textBoxCodeElecteur.MaxLength = 9;
            this.textBoxCodeElecteur.Name = "textBoxCodeElecteur";
            this.textBoxCodeElecteur.Size = new System.Drawing.Size(192, 20);
            this.textBoxCodeElecteur.TabIndex = 20;
            this.textBoxCodeElecteur.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxAncCNI_KeyPress);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label16.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label16.Location = new System.Drawing.Point(12, 168);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(119, 19);
            this.label16.TabIndex = 99;
            this.label16.Text = "Code Electeur :";
            // 
            // textBoxNewCNI
            // 
            this.textBoxNewCNI.Location = new System.Drawing.Point(193, 139);
            this.textBoxNewCNI.Name = "textBoxNewCNI";
            this.textBoxNewCNI.Size = new System.Drawing.Size(192, 20);
            this.textBoxNewCNI.TabIndex = 18;
            this.textBoxNewCNI.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxNewCNI_KeyPress);
            // 
            // textBoxNomMère3
            // 
            this.textBoxNomMère3.Location = new System.Drawing.Point(551, 81);
            this.textBoxNomMère3.Name = "textBoxNomMère3";
            this.textBoxNomMère3.Size = new System.Drawing.Size(189, 20);
            this.textBoxNomMère3.TabIndex = 15;
            this.textBoxNomMère3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxNom1_KeyPress);
            // 
            // textBoxNomMère2
            // 
            this.textBoxNomMère2.Location = new System.Drawing.Point(347, 81);
            this.textBoxNomMère2.Name = "textBoxNomMère2";
            this.textBoxNomMère2.Size = new System.Drawing.Size(189, 20);
            this.textBoxNomMère2.TabIndex = 14;
            this.textBoxNomMère2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxNom1_KeyPress);
            // 
            // textBoxNomMère1
            // 
            this.textBoxNomMère1.Location = new System.Drawing.Point(193, 81);
            this.textBoxNomMère1.Name = "textBoxNomMère1";
            this.textBoxNomMère1.Size = new System.Drawing.Size(148, 20);
            this.textBoxNomMère1.TabIndex = 13;
            this.textBoxNomMère1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxNom1_KeyPress);
            // 
            // textBoxAncCNI
            // 
            this.textBoxAncCNI.Location = new System.Drawing.Point(193, 110);
            this.textBoxAncCNI.Name = "textBoxAncCNI";
            this.textBoxAncCNI.Size = new System.Drawing.Size(192, 20);
            this.textBoxAncCNI.TabIndex = 16;
            this.textBoxAncCNI.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxAncCNI_KeyPress);
            // 
            // textBoxPOB
            // 
            this.textBoxPOB.Location = new System.Drawing.Point(551, 51);
            this.textBoxPOB.Name = "textBoxPOB";
            this.textBoxPOB.Size = new System.Drawing.Size(189, 20);
            this.textBoxPOB.TabIndex = 12;
            this.textBoxPOB.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxNom1_KeyPress);
            // 
            // textBoxNom3
            // 
            this.textBoxNom3.Location = new System.Drawing.Point(592, 23);
            this.textBoxNom3.Name = "textBoxNom3";
            this.textBoxNom3.Size = new System.Drawing.Size(148, 20);
            this.textBoxNom3.TabIndex = 10;
            this.textBoxNom3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxNom1_KeyPress);
            // 
            // textBoxNom2
            // 
            this.textBoxNom2.Location = new System.Drawing.Point(408, 23);
            this.textBoxNom2.Name = "textBoxNom2";
            this.textBoxNom2.Size = new System.Drawing.Size(162, 20);
            this.textBoxNom2.TabIndex = 9;
            this.textBoxNom2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxNom1_KeyPress);
            // 
            // textBoxNom1
            // 
            this.textBoxNom1.Location = new System.Drawing.Point(193, 23);
            this.textBoxNom1.Name = "textBoxNom1";
            this.textBoxNom1.Size = new System.Drawing.Size(193, 20);
            this.textBoxNom1.TabIndex = 8;
            this.textBoxNom1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxNom1_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label9.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label9.Location = new System.Drawing.Point(395, 137);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(151, 19);
            this.label9.TabIndex = 7;
            this.label9.Text = "Date Nouvelle CNI :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label8.Location = new System.Drawing.Point(12, 140);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(175, 19);
            this.label8.TabIndex = 6;
            this.label8.Text = "Numéro Nouvelle CNI :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label7.Location = new System.Drawing.Point(395, 111);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(155, 19);
            this.label7.TabIndex = 5;
            this.label7.Text = "Date Ancienne CNI :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label6.Location = new System.Drawing.Point(12, 110);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(175, 19);
            this.label6.TabIndex = 4;
            this.label6.Text = "Numéro Ancienne CNI:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Location = new System.Drawing.Point(12, 81);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(130, 19);
            this.label5.TabIndex = 3;
            this.label5.Text = "Nom de la mère :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Location = new System.Drawing.Point(395, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(141, 19);
            this.label4.TabIndex = 2;
            this.label4.Text = "Lieu de naissance :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(12, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(145, 19);
            this.label3.TabIndex = 1;
            this.label3.Text = "Date de naissance :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(12, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 19);
            this.label2.TabIndex = 0;
            this.label2.Text = "Nom               :";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.button1.Location = new System.Drawing.Point(161, 622);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(197, 35);
            this.button1.TabIndex = 36;
            this.button1.Text = "Modifier et Approuver";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label26.Location = new System.Drawing.Point(736, 85);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(136, 27);
            this.label26.TabIndex = 39;
            this.label26.Text = "Date du Jour";
            // 
            // textBoxAdresseCompl
            // 
            this.textBoxAdresseCompl.Location = new System.Drawing.Point(432, 132);
            this.textBoxAdresseCompl.Multiline = true;
            this.textBoxAdresseCompl.Name = "textBoxAdresseCompl";
            this.textBoxAdresseCompl.Size = new System.Drawing.Size(308, 37);
            this.textBoxAdresseCompl.TabIndex = 29;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label17.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label17.Location = new System.Drawing.Point(428, 110);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(197, 19);
            this.label17.TabIndex = 10;
            this.label17.Text = "Adresse Complémentaire :";
            // 
            // comboBoxAdresse
            // 
            this.comboBoxAdresse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxAdresse.FormattingEnabled = true;
            this.comboBoxAdresse.Location = new System.Drawing.Point(532, 76);
            this.comboBoxAdresse.Name = "comboBoxAdresse";
            this.comboBoxAdresse.Size = new System.Drawing.Size(208, 21);
            this.comboBoxAdresse.TabIndex = 28;
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.Controls.Add(this.textBoxAdresseCompl);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.comboBoxAdresse);
            this.panel2.Controls.Add(this.comboBoxCommune);
            this.panel2.Controls.Add(this.comboBoxArrondis);
            this.panel2.Controls.Add(this.comboBoxRégion);
            this.panel2.Controls.Add(this.textBoxPhone);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Location = new System.Drawing.Point(109, 405);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(791, 185);
            this.panel2.TabIndex = 33;
            // 
            // comboBoxCommune
            // 
            this.comboBoxCommune.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCommune.FormattingEnabled = true;
            this.comboBoxCommune.Location = new System.Drawing.Point(532, 46);
            this.comboBoxCommune.Name = "comboBoxCommune";
            this.comboBoxCommune.Size = new System.Drawing.Size(208, 21);
            this.comboBoxCommune.TabIndex = 26;
            this.comboBoxCommune.SelectedIndexChanged += new System.EventHandler(this.comboBoxCommune_SelectedIndexChanged);
            // 
            // comboBoxArrondis
            // 
            this.comboBoxArrondis.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxArrondis.FormattingEnabled = true;
            this.comboBoxArrondis.Location = new System.Drawing.Point(156, 80);
            this.comboBoxArrondis.Name = "comboBoxArrondis";
            this.comboBoxArrondis.Size = new System.Drawing.Size(183, 21);
            this.comboBoxArrondis.TabIndex = 27;
            this.comboBoxArrondis.SelectedIndexChanged += new System.EventHandler(this.comboBoxArrondis_SelectedIndexChanged);
            // 
            // comboBoxRégion
            // 
            this.comboBoxRégion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxRégion.Enabled = false;
            this.comboBoxRégion.FormattingEnabled = true;
            this.comboBoxRégion.Location = new System.Drawing.Point(156, 50);
            this.comboBoxRégion.Name = "comboBoxRégion";
            this.comboBoxRégion.Size = new System.Drawing.Size(183, 21);
            this.comboBoxRégion.TabIndex = 25;
            this.comboBoxRégion.SelectedIndexChanged += new System.EventHandler(this.comboBoxRégion_SelectedIndexChanged);
            // 
            // textBoxPhone
            // 
            this.textBoxPhone.Location = new System.Drawing.Point(156, 21);
            this.textBoxPhone.Name = "textBoxPhone";
            this.textBoxPhone.Size = new System.Drawing.Size(185, 20);
            this.textBoxPhone.TabIndex = 22;
            this.textBoxPhone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxPhone_KeyPress);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label15.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label15.Location = new System.Drawing.Point(428, 76);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(74, 19);
            this.label15.TabIndex = 4;
            this.label15.Text = "Adresse :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label14.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label14.Location = new System.Drawing.Point(20, 80);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(136, 19);
            this.label14.TabIndex = 3;
            this.label14.Text = "Arrondissement : ";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label13.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label13.Location = new System.Drawing.Point(428, 46);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(91, 19);
            this.label13.TabIndex = 2;
            this.label13.Text = "Commune :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label12.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label12.Location = new System.Drawing.Point(23, 50);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(83, 19);
            this.label12.TabIndex = 1;
            this.label12.Text = "Région     :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label11.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label11.Location = new System.Drawing.Point(20, 24);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(89, 19);
            this.label11.TabIndex = 0;
            this.label11.Text = "Téléphone:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Berlin Sans FB Demi", 14.25F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(140, 152);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 23);
            this.label1.TabIndex = 32;
            this.label1.Text = "Identification";
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold);
            this.button2.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.button2.Location = new System.Drawing.Point(619, 622);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(197, 35);
            this.button2.TabIndex = 37;
            this.button2.Text = "Fermer";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold);
            this.label25.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label25.Location = new System.Drawing.Point(136, 85);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(353, 23);
            this.label25.TabIndex = 38;
            this.label25.Text = "MODIFICATION D\'UNE FICHE ELECTEUR";
            // 
            // LabelStatut
            // 
            this.LabelStatut.AutoSize = true;
            this.LabelStatut.Font = new System.Drawing.Font("Berlin Sans FB Demi", 14.25F, System.Drawing.FontStyle.Bold);
            this.LabelStatut.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.LabelStatut.Location = new System.Drawing.Point(757, 152);
            this.LabelStatut.Name = "LabelStatut";
            this.LabelStatut.Size = new System.Drawing.Size(51, 23);
            this.LabelStatut.TabIndex = 35;
            this.LabelStatut.Text = "Crée";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Berlin Sans FB Demi", 14.25F, System.Drawing.FontStyle.Bold);
            this.label10.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label10.Location = new System.Drawing.Point(137, 389);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(124, 23);
            this.label10.TabIndex = 34;
            this.label10.Text = "Coordonnées";
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.label18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label18.Location = new System.Drawing.Point(828, 40);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(91, 20);
            this.label18.TabIndex = 40;
            this.label18.Text = "Historique";
            this.label18.Click += new System.EventHandler(this.label18_Click);
            // 
            // Modif
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSeaGreen;
            this.ClientSize = new System.Drawing.Size(1008, 730);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.LabelStatut);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label25);
            this.Name = "Modif";
            this.Text = "Modif";
            this.Load += new System.EventHandler(this.Modif_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBoxCodeElecteur;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBoxNewCNI;
        private System.Windows.Forms.TextBox textBoxNomMère3;
        private System.Windows.Forms.TextBox textBoxNomMère2;
        private System.Windows.Forms.TextBox textBoxNomMère1;
        private System.Windows.Forms.TextBox textBoxAncCNI;
        private System.Windows.Forms.TextBox textBoxPOB;
        private System.Windows.Forms.TextBox textBoxNom3;
        private System.Windows.Forms.TextBox textBoxNom2;
        private System.Windows.Forms.TextBox textBoxNom1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox textBoxAdresseCompl;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox comboBoxAdresse;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox comboBoxCommune;
        private System.Windows.Forms.ComboBox comboBoxArrondis;
        private System.Windows.Forms.ComboBox comboBoxRégion;
        private System.Windows.Forms.TextBox textBoxPhone;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label LabelStatut;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox comboBoxSexe;
        private System.Windows.Forms.DateTimePicker dateTimePickerDOB;
        private System.Windows.Forms.Label label18;
    }
}