﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class Edition_procuration : Form
    {
        int mandatéID;
        CrystalReport_edtion_procuration objRptc;
        Connection conn = new Connection();
        //public static string strcon = @"Data Source=GUINALEH-PC\SQL2008;Initial Catalog=Djibouti;User ID=sa;Password=651251;";
        //public static string strcon = @"Data Source=SRVDNEVM1\SQL2008;Initial Catalog=DJIBOUTI;User ID=sa;Password=election;";
        
        public Edition_procuration(int ID)
        {
            InitializeComponent();
            mandatéID = ID;
        }

        private void Edition_procuration_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(conn.connectdb());
            objRptc = new CrystalReport_edtion_procuration();


            string SQLlist = "SET CONCAT_NULL_YIELDS_NULL OFF; SELECT   * " +
                                 " FROM  View_edition_mandat where mandatéID=" + mandatéID;

            con.Open();
            SqlDataAdapter sqlAdapter = new SqlDataAdapter(SQLlist, con);
            SqlCommandBuilder commandBuilder1 = new SqlCommandBuilder(sqlAdapter);

            con.Close();
            DataTable dt = new DataTable();
            sqlAdapter.Fill(dt);
            objRptc.SetDataSource(dt);
            crystalReportViewer1.ReportSource = objRptc;
            crystalReportViewer1.Refresh();
        }

        private void crystalReportViewer1_Load(object sender, EventArgs e)
        {

        }
    }
}
