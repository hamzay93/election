﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class Modification_Membre_parti_politique : Form
    {
        string clause;
        public Modification_Membre_parti_politique()
        {
            InitializeComponent();
        }
        Connection conn = new Connection();
        private void button6_Click(object sender, EventArgs e)
        {

            this.IsEmptyfield();
            if (String.IsNullOrEmpty(clause))
            {
                MessageBox.Show("veuillez remplir un champ au minimum");
                this.Focus();

            }
            else
            {
                string sCon = conn.connectdb();
                SqlConnection con = new SqlConnection(sCon);
                string str = "SET CONCAT_NULL_YIELDS_NULL OFF; SELECT ID_List_Parti,Prenom+' '+nom1+' '+nom2 as NOM , NomParti AS 'Partie Politique',ObjetElection as Election  FROM View_Membre_Parti_Election where " + clause + "";
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(str, con);
                SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sqlAdapter);

                DataTable table = new DataTable();
                table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                con.Open();
                sqlAdapter.Fill(table);
                con.Close();
                dataGridView1.DataSource = table;
                if (dataGridView1.Rows.Count == 0)
                {

                    panel1.Visible = false;
                    label1.Visible = false;
                    MessageBox.Show("Aucun Membre d'une Partie Politique retrouvé pour ces critères");

                }
                else
                {
                    panel1.Visible = true;
                    label1.Visible = true;
                }

                this.dataGridView1.Columns["ID_List_Parti"].Visible = false;
              



                clause = "";
            }
        }
        public void IsEmptyfield()
        {
            clause = null;
            foreach (Control X in this.panel7.Controls)
            {

                if (X is TextBox || X is ComboBox)
                {

                    if (X.Text.Trim().Length != 0)
                    {
                        if (string.IsNullOrEmpty(clause))
                        {
                            clause = clause + X.Tag + " Like '" + X.Text.Trim() + "%'";
                            //return clause;
                        }
                        else
                        {
                            clause = clause + " And " + X.Tag + " Like '" + X.Text.Trim() + "%'";
                            //return clause;
                        }
                    }


                }

            }
        }
        private void FillElection()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT ElectionID,ObjetElection from Election where Etat=1";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBox_Election.ValueMember = "ElectionID";
            comboBox_Election.DisplayMember = "ObjetElection";
            comboBox_Election.DataSource = objDs.Tables[0];

            comboBox2.ValueMember = "ElectionID";
            comboBox2.DisplayMember = "ObjetElection";
            comboBox2.DataSource = objDs.Tables[0];
        }
        private void FillParti()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT NomParti,ID_parti from Parti_Politique ";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBox_parti.ValueMember = "ID_parti";
            comboBox_parti.DisplayMember = "NomParti";
            comboBox_parti.DataSource = objDs.Tables[0];

            comboBox1.ValueMember = "ID_parti";
            comboBox1.DisplayMember = "NomParti";
            comboBox1.DataSource = objDs.Tables[0];
        }

        private void Modification_Membre_parti_politique_Load(object sender, EventArgs e)
        {
            this.FillElection();
            this.FillParti();
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            this.filldetails();
        }
        string clause3;
        private void filldetails()
        {

            this.selection();
            if (String.IsNullOrEmpty(clause3))
            {
                panel1.Visible = false;
                label1.Visible = false;

            }
            else
            {
                panel1.Visible = true;
                label1.Visible = true;
                string sCon = conn.connectdb();
                SqlConnection con = new SqlConnection(sCon);
                string str = "SELECT * FROM View_Membre_Parti_Election where ID_List_Parti= " + clause3;
                SqlCommand cmd = new SqlCommand(str, con);
                con.Open();

                using (SqlDataReader read = cmd.ExecuteReader())

                    while (read.Read())
                    {
                        textBox1.Text = (read["nom2"].ToString());
                        textBox2.Text = (read["nom1"].ToString());
                        textBox3.Text = (read["Prenom"].ToString());
                        comboBox1.SelectedValue = (read["ID_PARTI"].ToString());
                        comboBox2.SelectedValue = (read["ID_ELECTION"].ToString());
                    }
            }

        }
        private void selection()
        {
            int counter3;
            clause3 = null;

            // Iterate through all the rows and sum up the appropriate columns. 
            for (counter3 = 0; counter3 < (dataGridView1.Rows.Count);
                counter3++)
            {
                //dataGridView1.Rows.
                if (dataGridView1.Rows[counter3].Selected)
                {
                    if (dataGridView1.Rows[counter3].Cells["ID_List_Parti"].Value
                    != null)
                    {
                        if (dataGridView1.Rows[counter3].
                            Cells["ID_List_Parti"].Value.ToString().Length != 0)
                        {
                            if (string.IsNullOrEmpty(clause3))
                            {
                                clause3 = clause3 + int.Parse(dataGridView1.Rows[counter3].
                                    Cells["ID_List_Parti"].Value.ToString());
                            }
                            else
                            {
                                clause3 = clause3 + "," + int.Parse(dataGridView1.Rows[counter3].
                                Cells["ID_List_Parti"].Value.ToString());

                            }

                        }
                    }


                }

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (textBox1.Text.Trim().Length == 0 && textBox2.Text.Trim().Length == 0 && textBox3.Text.Trim().Length == 0)
            {
                MessageBox.Show("veuillez saisir le nom complet");
                textBox1.Focus(); textBox2.Focus(); textBox3.Focus();
            }

            else
            {

                if (comboBox1.Text.Length == 0 && comboBox2.Text.Length == 0)
                {
                    MessageBox.Show("veuillez choisir l'election et la partie politique");
                }
                else
                {

                    //Connection cn = new Connection();
                    string insertCmd = "update List_de_Parti set Prenom=@Prenom,nom1=@nom1,nom2=@nom2,ID_PARTI=@ID_PARTI,ID_ELECTION=@ID_ELECTION,UserModification=@UserModification,DateModification=@DateModification  where ID_List_Parti=" + clause3 + "";
                    SqlConnection dbConn;
                    dbConn = new SqlConnection(conn.connectdb());
                    dbConn.Open();
                    Login lg = new Login();
                    string dat = DateTime.Now.ToString("yyyy-MM-dd");
                    GetValue value = new GetValue();
                
                    SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);


                    int partiID = Convert.ToInt32(comboBox1.SelectedValue.ToString());
                    myCommand.Parameters.AddWithValue("@ID_PARTI", partiID);
                    int ElectionID = Convert.ToInt32(comboBox2.SelectedValue.ToString());
                    myCommand.Parameters.AddWithValue("@ID_ELECTION", ElectionID);

                 
                    myCommand.Parameters.AddWithValue("@Prenom", textBox3.Text.ToString());
                    myCommand.Parameters.AddWithValue("@nom1", textBox2.Text.ToString());
                    myCommand.Parameters.AddWithValue("@nom2", textBox1.Text.ToString());


                    myCommand.Parameters.AddWithValue("@UserModification", value.getAgentModif());
                    myCommand.Parameters.AddWithValue("@DateModification", dat);
                    myCommand.ExecuteNonQuery();

                    MessageBox.Show("Modification avec succès");
                    textBox1.Clear(); textBox2.Clear(); textBox3.Clear();
                }

            }
        }

    }
}
