﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;

namespace Electeur
{
    public partial class Import : Form
    {
        public Import()
        {
            InitializeComponent();
        }
        Connection conn = new Connection();
        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog fdlg = new OpenFileDialog();
            fdlg.Title = "Selection du fichier à importer";
            fdlg.InitialDirectory = @"c:\";
            fdlg.Filter = "All files (*.xlsx*)|*.xlsx*|All files (*.*)|*.*";
            fdlg.FilterIndex = 2;
            fdlg.RestoreDirectory = true;
            if (fdlg.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = fdlg.FileName;
            }
        }
        public void importdatafromexcel(string excelfilepath)
{
    //declare variables - edit these based on your particular situation
    string ssqltable = "NEWCNI";
    // make sure your sheet name is correct, here sheet name is sheet1, so you can change your sheet name if have
    //different
    string myexceldataquery = "select CIN,NOM1,NOM2,NOM3,SEXE,DATEDENAISSANCE,NUMERODOSSIER,DATEDEDEPOT,NOM1_MERE,NOM2_MERE from [Feuil$] WHERE CIN IS NOT NULL";
    try
    {
        //create our connection strings
        string excelConnectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 8.0", excelfilepath);
        
        string ssqlconnectionstring = conn.connectdb();
        //execute a query to erase any previous data from our destination table
        /*string CreateTemptable = "create table NEWCNI1( CIN int, NOM1 Nvarchar(255) null, NOM2 Nvarchar(255) null, NOM3 Nvarchar(255) null,"
             + "SEXE Nvarchar(255) null, DATEDENAISSANCE datetime null, NUMERODOSSIER float null, DATEDEDEPOT datetime null, NOM1_MERE Nvarchar(255) null,"
            + "NOM2_MERE Nvarchar(255) null,)";*/
        string sqlinsert = "INSERT INTO NEWCIN SELECT * FROM NEWCNI where CIN not in (select CIN from NEWCIN)";
        string sqldelete = "DELETE FROM NEWCNI";
        OleDbConnection oledbconn = new OleDbConnection(excelConnectionString);
        OleDbCommand oledbcmd = new OleDbCommand(myexceldataquery, oledbconn);
        oledbconn.Open();
        OleDbDataReader dr = oledbcmd.ExecuteReader();
        SqlConnection sqlconn = new SqlConnection(ssqlconnectionstring);
        sqlconn.Open();
        //series of commands to bulk copy data from the excel file into our sql table
       
        SqlBulkCopy bulkcopy = new SqlBulkCopy(ssqlconnectionstring);
        //sqlconn.Open();
        bulkcopy.BulkCopyTimeout = 600;
        bulkcopy.DestinationTableName = ssqltable;
        
        while (dr.Read())
        {
            
            bulkcopy.WriteToServer(dr);
           
            
            
            //MessageBox.Show(dr.ToString());
          
        }
        SqlCommand sqlcmd1 = new SqlCommand(sqlinsert, sqlconn);
        SqlCommand sqlcmd2 = new SqlCommand(sqldelete, sqlconn);
        sqlcmd1.CommandTimeout = 150;
        sqlcmd1.ExecuteNonQuery();
        
        sqlcmd2.ExecuteNonQuery();
        //sqlconn.Close();

        label1.Text = "The data has been exported succefuly from Excel to SQL";
        oledbconn.Close();
        sqlconn.Close();
    }
    catch (Exception ex)
    {
        //handle exception
    }
}

        private void button2_Click(object sender, EventArgs e)
        {
            this.importdatafromexcel(textBox1.Text);
        }
    }
}
