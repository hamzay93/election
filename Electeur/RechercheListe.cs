﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class RechercheListe : Form
    {
        public RechercheListe()
        {
            InitializeComponent();
        }
        Connection conn = new Connection();
        private void button1_Click(object sender, EventArgs e)
        {
            this.RoleAgent();
            this.IsEmptyfield();
            if (String.IsNullOrEmpty(clause))
            {
                clause = "Nom1='%'";
            }
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            string str = "SET CONCAT_NULL_YIELDS_NULL OFF; SELECT ElecteurID, Nom1 +'  '+ Nom2 +'  '+Nom3 AS NOM,NomMère1+'  '+NomMère2+'  '+NomMère3 AS 'NOM DE LA MERE',DOB AS 'DATE DE NAISSANCE',POB AS LIEU,CodeElecteur AS 'CODE ELECTEUR',AncienCNI AS 'ANCIEN CNI',DateAncienCNI AS 'DATE DELIVRANCE',NewCNI AS 'NOUVEAU CNI',DateNewCNI AS 'DATE DELIVRANCE',CentreVote as 'Centre de Vote',NomBureau as 'Bureau',CodeBureau AS 'Numéro Bureau',Sexe AS 'SEXE',NomRégion,NomCommune,NomArrondis,NomAdresse FROM Elect1 where " + clause + clause1 + clause2 + clause4;
            con.Open();
            SqlDataAdapter sqlAdapter = new SqlDataAdapter(str, con);
            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sqlAdapter);
            con.Close();
            DataTable table = new DataTable();
            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
            sqlAdapter.Fill(table);
            dataGridView1.DataSource = table;
            dataGridView1.Columns["ElecteurID"].Visible = false;
            int total = dataGridView1.RowCount;
            label14.Visible = true;
            label14.Text = total.ToString();
            clause = "";
            clause1 = "";
            clause2 = "";

           
        }
        public int getAdresseID()
        {
            //SqlDataReader rd = null;
            string query = "select AdresseID from Adresse where NomAdresse='" + comboBoxAdresse.Text +"'";
            string sCon = conn.connectdb();
            SqlConnection dbConn;
            dbConn = new SqlConnection(sCon);
            dbConn.Open();
            SqlCommand cmd = new SqlCommand(query, dbConn);
            
            int result = ((int)cmd.ExecuteScalar());
            dbConn.Close();
            //SaisieElecteur se=new SaisieElecteur();
            int adress = result;
            return adress;

        }
        private void FillAgent()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT  NomAgent FROM Agent order by NomAgent";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            objDs.Tables[0].Rows.Add("");
            con.Close();
            comboBoxAgent.ValueMember = "NomAgent";
            comboBoxAgent.DisplayMember = "NomAgent";
            comboBoxAgent.DataSource = objDs.Tables[0];
            comboBoxAgent.SelectedValue = "";
            //comboBoxAgent.DataSource = 
        //    comboBoxAgent.Items.q
            //comboBoxAgent.Items.Clear();
        }
        private void FillRégion()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT RégionID, NomRégion FROM Région";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBoxRégion.ValueMember = "RégionID";
            comboBoxRégion.DisplayMember = "NomRégion";
            comboBoxRégion.DataSource = objDs.Tables[0];
        }
        //int RégionID;
        /*private void comboBoxRégion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxRégion.SelectedValue.ToString() != "")
            {
                int RégionID = Convert.ToInt32(comboBoxRégion.SelectedValue.ToString());
                FillCommune(RégionID);
                //comboBoxCommune.SelectedIndex = 0;
            }
        }*/
        private void FillCommune(int RégionID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT CommuneID, NomCommune FROM Commune WHERE RégionID =@RégionID";
            cmd.Parameters.AddWithValue("@RégionID", RégionID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            objDs.Tables[0].Rows.Add(30,"");
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxCommune.ValueMember = "CommuneID";
                comboBoxCommune.DisplayMember = "NomCommune";
                comboBoxCommune.DataSource = objDs.Tables[0];
                comboBoxCommune.SelectedValue = 30;
            }
        }
        private void comboBoxCommune_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxCommune.SelectedValue.ToString() != "")
            {
                int CommuneID = Convert.ToInt32(comboBoxCommune.SelectedValue.ToString());
                this.FillArrondis(CommuneID);
                //comboBoxArrondis.SelectedIndex = 0;
            }
        }
        private void comboBoxArrondis_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxArrondis.SelectedValue.ToString() != "")
            {
                int ArrondisID = Convert.ToInt32(comboBoxArrondis.SelectedValue.ToString());
                this.FillAdresse(ArrondisID);
                //comboBoxAdresse.SelectedIndex = 0;
            }
        }
        private void FillArrondis(int CommuneID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT ArrondisID, NomArrondis FROM Arrondissement WHERE CommuneID =@CommuneID";
            cmd.Parameters.AddWithValue("@CommuneID", CommuneID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            if (CommuneID == 30)
            {
                objDs.Tables[0].Rows.Add(30, "");
            }
            
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxArrondis.ValueMember = "ArrondisID";
                comboBoxArrondis.DisplayMember = "NomArrondis";
                comboBoxArrondis.DataSource = objDs.Tables[0];
                if (CommuneID==30)
                {
                    comboBoxArrondis.SelectedValue = 30;
                }
                
            }
        }
        private void FillAdresse(int ArrondisID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT AdresseID, NomAdresse FROM Adresse WHERE ArrondisID =@ArrondisID";
            cmd.Parameters.AddWithValue("@ArrondisID", ArrondisID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            if (ArrondisID == 30)
            {
                objDs.Tables[0].Rows.Add(30, "");
            }
            //objDs.Tables[0].Rows.Add(30, "");
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxAdresse.ValueMember = "AdresseID";
                comboBoxAdresse.DisplayMember = "NomAdresse";
                comboBoxAdresse.DataSource = objDs.Tables[0];
                if (ArrondisID == 30)
                {
                    comboBoxAdresse.SelectedValue = 30;
                }
                //comboBoxArrondis.SelectedValue = 30;
            }
        }
        //string Champ;
        string clause;
        string clause1=null;
        string clause2 = null;
        string clause4 = null;
        public void IsEmptyfield()
        {
            
            foreach (Control X in this.panel1.Controls)
            {
                if (X is TextBox || X is ComboBox)
                {
                    if (X.Text.Trim().Length != 0)
                    {
                        if (string.IsNullOrEmpty(clause))
                        {
                            if (X.Name == "comboBoxAdresse")
                            {
                                clause = clause + X.Tag + " Like '%" + this.getAdresseID() + "%'";
                            }
                            else
                            {
                                clause = clause + X.Tag + " Like '%" + X.Text.Trim() + "%'";
                            }
                        }
                        else
                        {
                            if (X.Name == "comboBoxAdresse")
                            {
                                clause = clause + "And "+ X.Tag + " Like '%" + this.getAdresseID() + "%'";
                            }
                            else
                            {
                                clause = clause + "And " + X.Tag + " Like '%" + X.Text.Trim() + "%'";
                            }
                        }
                    }

                }
                else 
                {
                    if (X is DateTimePicker)
                    {
                        if (X.Name == "dateTimePicker1")
                        {
                            if (string.IsNullOrEmpty(clause1))
                            {
                                if (dateTimePicker1.Checked)
                                {
                                    if (TimePicker1.Checked && dateTimePicker1.Checked)
                                    {
                                        string time = TimePicker1.Value.TimeOfDay.ToString();
                                        string daate = dateTimePicker1.Value.Date.ToString("yyyy/MM/dd") + " " + time;
                                        DateTime datefull = DateTime.Parse(daate);
                                        clause1 = clause1 + " AND " + X.Tag + " >= '" + daate + "'";
                                    }
                                    else
                                    {
                                        //string time = "00:00:00:000";
                                        string dat = dateTimePicker1.Value.ToString("yyyy/MM/dd");
                                        DateTime date4 = DateTime.Parse(dat);
                                        clause1 = clause1 + " AND " + X.Tag + " >= '" + dat + "'";
                                    }
                                    
                                }
                                

                            }
                            else
                            {
                                if (dateTimePicker1.Checked)
                                {
                                    if (TimePicker1.Checked && dateTimePicker1.Checked)
                                    {
                                        string time = TimePicker1.Value.TimeOfDay.ToString();
                                        string daate = dateTimePicker1.Value.Date.ToString("yyyy/MM/dd")+" "+time;
                                        DateTime datefull = DateTime.Parse( daate);
                                        clause1 = clause1 + " AND " + X.Tag + " >= '" + daate + "'";
                                    }
                                    else
                                    {
                                        string dat = dateTimePicker1.Value.ToString("yyyy/MM/dd");
                                        clause1 = clause1 + " AND " + X.Tag + " >= '" + dat + "'";
                                    }
                                    
                                }
                                
                            }
                        }
                        if (X.Name == "dateTimePicker2")
                        {
                            if (string.IsNullOrEmpty(clause1))
                            {
                                if (dateTimePicker2.Checked)
                                {
                                    if (TimePicker2.Checked && dateTimePicker2.Checked)
                                    {
                                        string time = TimePicker2.Value.TimeOfDay.ToString();
                                        string daate = dateTimePicker2.Value.Date.ToString("yyyy/MM/dd") + " " + time;
                                        DateTime datefull = DateTime.Parse(daate);
                                        clause1 = clause1 + " AND " + X.Tag + " <= '" + daate + "'";
                                    }
                                    else
                                    {
                                        string dat = dateTimePicker2.Value.ToString("yyyy/MM/dd");
                                        clause1 = clause1 + " AND " + X.Tag + " <= '" + dat + "'";
                                    }
                                }

                            }
                            else
                            {
                                if (dateTimePicker2.Checked)
                                {
                                    if (TimePicker2.Checked && dateTimePicker2.Checked)
                                    {
                                        string time = TimePicker2.Value.TimeOfDay.ToString();
                                        string daate = dateTimePicker2.Value.Date.ToString("yyyy/MM/dd") + " " + time;
                                        DateTime datefull = DateTime.Parse(daate);
                                        clause1 = clause1 + " AND " + X.Tag + " <= '" + daate + "'";
                                    }
                                    else
                                    {
                                        string dat = dateTimePicker2.Value.ToString("yyyy/MM/dd");
                                        clause1 = clause1 + " AND " + X.Tag + " <= '" + dat + "'";
                                    }
                                }
                            }
                        }
                       /* if (X.Name == "TimePicker1")
                        {
                            if (string.IsNullOrEmpty(clause1))
                            {
                                if (TimePicker1.Checked && dateTimePicker1.Checked)
                                {
                                    //string time = TimePicker1.Value.TimeOfDay;
                                    DateTime datefull = dateTimePicker1.Value.Date+TimePicker1.Value.TimeOfDay;
                                    clause1 = clause1 + " AND " + X.Tag + " <= '" + datefull + "'";
                                }

                            }
                            else
                            {
                                if (dateTimePicker2.Checked)
                                {
                                    string dat = dateTimePicker2.Value.ToString("yyyy/MM/dd");
                                    clause1 = clause1 + " AND " + X.Tag + " <= '" + dat + "'";
                                }
                            }
                        }*/


                    }
                    if (X is CheckBox)
                    {
                        if (checkBox1.Checked)
                        {
                            clause2 = "and electeurID not in (select electeurID from Radiation where Statut='Actif')";
                        }
                        else
                        {
                            clause2 = null;
                        }
                        if (checkBox2.Checked)
                        {
                            clause4 = "and CentreID is null";
                        }
                        else
                        {
                            clause4 = null;
                        }

                    }
                }
            }
        }
        public void RoleAgent()
        {
            if (Saisie.Checked)
            {
                dateTimePicker1.Tag = "DateCréation";
                dateTimePicker2.Tag = "DateCréation";
                comboBoxAgent.Tag = "AgentSAisie";
            }
            if (radioButtonApprobation.Checked)
            {
                dateTimePicker1.Tag = "DateApprobation";
                dateTimePicker2.Tag = "DateApprobation";
                comboBoxAgent.Tag = "AgentApprobation";
            }
            if(radioButtonValidation.Checked)
            {
                dateTimePicker1.Tag = "DateValidation";
                dateTimePicker2.Tag = "DateValidation";
                comboBoxAgent.Tag = "AgentValidation";
            }
            if (radiobuttonAll.Checked)
            {
                dateTimePicker1.Checked = false;
                dateTimePicker2.Checked = false;
                comboBoxAgent.Tag = "AgentSAisie";
            }

        }
        private void RechercheListe_Load(object sender, EventArgs e)
        {
            int regionID=1;
            FillRégion();
            FillCommune(regionID);
            FillAgent();
            // TODO: This line of code loads data into the 'electeurDataSet1.Electeur' table. You can move, or remove it, as needed.
            //this.electeurTableAdapter.Fill(this.electeurDataSet1.Electeur);
            // TODO: This line of code loads data into the 'electeurDataSet1.Adresse' table. You can move, or remove it, as needed.
            //this.adresseTableAdapter.Fill(this.electeurDataSet1.Adresse);
            // TODO: This line of code loads data into the 'electeurDataSet1.Localité' table. You can move, or remove it, as needed.
            //this.localitéTableAdapter.Fill(this.electeurDataSet1.Localité);
            // TODO: This line of code loads data into the 'electeurDataSet1.Arrondissement' table. You can move, or remove it, as needed.
            //this.arrondissementTableAdapter.Fill(this.electeurDataSet1.Arrondissement);
            // TODO: This line of code loads data into the 'electeurDataSet1.Commune' table. You can move, or remove it, as needed.
            //this.communeTableAdapter.Fill(this.electeurDataSet1.Commune);
            // TODO: This line of code loads data into the 'electeurDataSet.Région' table. You can move, or remove it, as needed.
            //this.régionTableAdapter.Fill(this.electeurDataSet.Région);
            // TODO: This line of code loads data into the 'electeurDataSet1.Agent' table. You can move, or remove it, as needed.
            //this.agentTableAdapter.Fill(this.electeurDataSet1.Agent);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            comboBox10.SelectedValue = "";
            comboBoxAgent.SelectedValue = "";
            comboBoxCommune.SelectedValue = 30;
            radiobuttonAll.Checked = true;
            checkBox1.Checked = false;
            dateTimePicker1.Checked = false;
            dateTimePicker2.Checked = false;
            TimePicker1.Checked = false;
            TimePicker2.Checked = false;
        }
    }
}
