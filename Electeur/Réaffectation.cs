﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class Réaffectation : Form
    {
        public Réaffectation()
        {
            InitializeComponent();
        }
        Connection conn = new Connection();
        private void FillCV()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT CentreID, CentreVote FROM CentreVote where Actif = 1 ORDER BY CodeCentre";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBoxCV.ValueMember = "CentreID";
            comboBoxCV.DisplayMember = "CentreVote";
            comboBoxCV.DataSource = objDs.Tables[0];
        }
        private void FillBureau(int CentreID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT BureauID, NomBureau FROM Bureau WHERE CentreID =@CentreID";
            cmd.Parameters.AddWithValue("@CentreID", CentreID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxBV.ValueMember = "BureauID";
                comboBoxBV.DisplayMember = "NomBureau";
                comboBoxBV.DataSource = objDs.Tables[0];
            }
        }
        private void comboBoxCV_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxCV.SelectedValue.ToString() != "")
            {
                int CentreID = Convert.ToInt32(comboBoxCV.SelectedValue.ToString());
                //FillBureau(CentreID);
                //comboBoxBV.SelectedIndex = 0;
            }
        }
        string clause = null;
        int[] arr= new int[50];
        int point = 0;
        public void IsEmptyfield()
        {

            foreach (Control X in this.panel1.Controls)
            {

                if (X is TextBox || X is ComboBox )
                {
                    if (X.Text.Trim().Length != 0)
                    {
                        if (string.IsNullOrEmpty(clause))
                        {
                            clause = clause + X.Tag + " Like '" + X.Text + "'";
                            //return clause;
                        }
                        else
                        {
                            clause = clause + " And " + X.Tag + " Like '" + X.Text + "'";
                            //return clause;
                        }
                    }

                }
                else
                {
                    if (X is DateTimePicker)
                    {
                        if (X.Name == "dateTimePicker1")
                        {
                            if (string.IsNullOrEmpty(clause))
                            {
                                if (dateTimePicker1.Checked)
                                {
                                    string dat = dateTimePicker1.Value.ToString("yyyy-MM-dd");
                                    DateTime date4 = DateTime.Parse(dat);
                                    clause = clause + X.Tag + " Like '" + dat + "'";
                                }

                            }
                            else
                            {
                                if (dateTimePicker1.Checked)
                                {
                                    string dat = dateTimePicker1.Value.ToString("yyyy-MM-dd");
                                    DateTime date4 = DateTime.Parse(dat);
                                    clause = clause + " AND " + X.Tag + " Like '" + dat + "'";
                                }

                            }
                        }
                    }
                }
            }
        }
        string clause1 = null;
        
        private void button3_Click(object sender, EventArgs e)
        {
            for (int counter = 0; counter < (dataGridView1.Rows.Count);
                counter++)
            {
                //dataGridView1.Rows.
                if (dataGridView1.Rows[counter].Selected)
                {
                    //dataGridView1.Rows.

                    if (dataGridView1.Rows[counter].Cells["ElecteurID"].Value
                    != null)
                    {
                        if (dataGridView1.Rows[counter].
                            Cells["ElecteurID"].Value.ToString().Length != 0)
                        {
                            if (string.IsNullOrEmpty(clause1))
                            {
                                clause1 = clause1 + int.Parse(dataGridView1.Rows[counter].
                                    Cells["ElecteurID"].Value.ToString());
                                arr[point] = Int32.Parse(dataGridView1.Rows[counter].
                                    Cells["ElecteurID"].Value.ToString());
                                point++;
                                
                                //counter1++;
                            }
                            else
                            {
                                clause1 = clause1 + "," + int.Parse(dataGridView1.Rows[counter].
                                Cells["ElecteurID"].Value.ToString());
                                arr[point] = Int32.Parse(dataGridView1.Rows[counter].
                                    Cells["ElecteurID"].Value.ToString());
                                point++;
                                //counter1++;

                            }

                        }
                    }

                }
                //update bureau de vote
                
            }
            Connection cn = new Connection();
            string sCon = conn.connectdb();
            string updateCmd = "UPDATE Electeur SET CentreID =@CentreID ,AgentModif=@AgentModif,DateModification=@DateModification,BureauID=@BureauID where ElecteurID in(" + clause1 + ")";
            SqlConnection dbConn;
            dbConn = new SqlConnection(sCon);
            dbConn.Open();
            GetValue get = new GetValue();
            //DateTime dt = new DateTime();
            //dt.ToShortDateString();
            string dat = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            DateTime date4 = DateTime.Parse(dat);
            SqlCommand myCommand = new SqlCommand(updateCmd, dbConn);
            // Create parameters for the SqlCommand object
            // initialize with input-form field values
            myCommand.Parameters.AddWithValue("@CentreID", Convert.ToInt32(comboBoxCV.SelectedValue.ToString()));
            myCommand.Parameters.AddWithValue("@AgentModif", get.getAgentModif());
            myCommand.Parameters.AddWithValue("@DateModification", dat);
            myCommand.Parameters.AddWithValue("@BureauID", DBNull.Value);
            myCommand.ExecuteNonQuery();
            foreach (int ID in arr)
            {
                if (ID != 0)
                {
                    string action = "Réaffectation";
                    historique hist = new historique();
                    int AgentID = get.getAgentID();
                    int none = 0;
                    hist.addhistoric(AgentID, ID, action, none);
                }
            }
            
            MessageBox.Show("Réaffectation effectuée avec succès");
            Réaffectation affect = new Réaffectation();
            affect.MdiParent = this.ParentForm;
            affect.Show();
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
             this.IsEmptyfield();
             if (String.IsNullOrEmpty(clause))
             {
                 clause = "Nom1 like '%'";
             }
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            string str = "SET CONCAT_NULL_YIELDS_NULL OFF; SELECT ElecteurID, Nom1 +'  '+ Nom2 +'  '+Nom3 AS NOM,NomMère1+'  '+NomMère2+'  '+NomMère3 AS 'NOM DE LA MERE',DOB AS 'DATE DE NAISSANCE',POB AS LIEU,CodeElecteur AS 'CODE ELECTEUR',AncienCNI AS 'ANCIEN CNI',DateAncienCNI AS 'DATE DELIVRANCE',NewCNI AS 'NOUVEAU CNI',DateNewCNI AS 'DATE DELIVRANCE',Sexe AS 'SEXE', CentreVote.CentreVote FROM Electeur inner join CentreVote on Electeur.CentreID =CentreVote.CentreID where " + clause + "and StatutID ='4' and ElecteurID not in (Select ElecteurID from Radiation  where Statut ='Actif')";
            SqlDataAdapter sqlAdapter = new SqlDataAdapter(str, con);
            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sqlAdapter);
            DataTable table = new DataTable();
            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
            sqlAdapter.Fill(table);
            dataGridView1.DataSource = table;
            this.dataGridView1.Columns["ElecteurID"].Visible = false;
            if (dataGridView1.Rows.Count == 0)
            {
                button3.Enabled = false;

            }
            else
            {
                button3.Enabled = true;
            }
            
            con.Open();
            clause = "";
        }

        private void Réaffectation_Load(object sender, EventArgs e)
        {
            FillCV();
        }

       
        //string clause = "StatutID > 3";
        
    }
}
