﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class AjoutPrefet : Form
    {
        Connection conn = new Connection();
        public AjoutPrefet()
        {
            InitializeComponent();
        }
        private void FillCv()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT RégionID,NomRégion FROM Région";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBox4.ValueMember = "RégionID";
            comboBox4.DisplayMember = "NomRégion";
            comboBox4.DataSource = objDs.Tables[0];
        }
        private void button6_Click(object sender, EventArgs e)
        {
            if (textBox8.Text.Trim().Length == 0)
            {
                MessageBox.Show("veuillez saisir le nom du prefet");
                textBox8.Focus();
            }
            else
            {
                if (comboBox4.Text == "")
                {
                    MessageBox.Show("veuillez choisir de quel région il/elle est");
                }
                else{

                    //Connection cn = new Connection();
                    string insertCmd = "INSERT INTO Prefet (Nom,RégionID,Etat,UserCreation,DateCreation,UserModification,DateModification) VALUES (@Nom,@RégionID,@Etat,@UserCreation,@DateCreation,@UserModification,@DateModification)";
                    SqlConnection dbConn;
                    dbConn = new SqlConnection(conn.connectdb());
                    dbConn.Open();
                    Login lg = new Login();
                    string dat = DateTime.Now.ToString("yyyy-MM-dd");
                    GetValue value = new GetValue();
                    //getAdress add = new getAdress();
                    SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                    int RegionID = Convert.ToInt32(comboBox4.SelectedValue.ToString());
                        myCommand.Parameters.AddWithValue("@RégionID", RegionID);
                    

                    if (radioButton1.Checked)
                    {
                        myCommand.Parameters.AddWithValue("@Etat", 1);
                    }
                    if (radioButton2.Checked)
                    {
                        myCommand.Parameters.AddWithValue("@Etat", 0);
                    }
                    myCommand.Parameters.AddWithValue("@Nom", textBox8.Text.ToUpper());
                    myCommand.Parameters.AddWithValue("@UserCreation", value.getAgentCréation());
                    myCommand.Parameters.AddWithValue("@DateCreation", dat);
                    myCommand.Parameters.AddWithValue("@UserModification", value.getAgentCréation());
                    myCommand.Parameters.AddWithValue("@DateModification", dat);
                    myCommand.ExecuteNonQuery();

                    MessageBox.Show("Ajoutée avec succès");
                    textBox8.Clear();
                }


            }
        }

        private void AjoutPrefet_Load(object sender, EventArgs e)
        {
            this.FillCv();
        }
    }
}
