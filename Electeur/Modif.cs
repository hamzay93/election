﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class Modif : Form
    {
        public string adresse, statutID, AgentID, statutID1, action;
        Connection conn = new Connection();
        //int adresseID ;
        public Modif()
        {
            InitializeComponent();
            //textBoxAncCNI.Validating += textBoxAncCNI_Validating;
            //textBoxNewCNI.Validating += textBoxNewCNI_Validating;

        }
        string elect, addressID, adressName,BtnNom;
        int RégionName, ArrondisName, CommuneName, IDElect;
        /*public Modif(string ID,string add)
        {
            elect = ID;
            addressID = add;
            InitializeComponent();

        }*/
        public Modif(string ID, string add, string btnname)
        {
            elect = ID;
            IDElect = Int32.Parse(ID);
            addressID = add;
            BtnNom = btnname;
            InitializeComponent();
            textBoxAncCNI.Validating += textBoxAncCNI_Validating;
            textBoxNewCNI.Validating += textBoxNewCNI_Validating;

        }
        private void FillRégion()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT RégionID, NomRégion FROM Région";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBoxRégion.ValueMember = "RégionID";
            comboBoxRégion.DisplayMember = "NomRégion";
            comboBoxRégion.DataSource = objDs.Tables[0];
        }

        private void Modif_Load(object sender, EventArgs e)
        {
            this.FillRégion();
            label26.Text = DateTime.Today.ToString();
            
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            string str = "SELECT * FROM Electeur where ElecteurID= " + elect;
            SqlCommand cmd = new SqlCommand(str, con);
            con.Open();
            //cmd.CommandType = CommandType.Text;
            GetValue get = new GetValue();
            //int profil = get.getProfilID();
            if (String.IsNullOrEmpty(BtnNom))
            {

            }
            else
            {
                if (BtnNom == "Modifier et Valider")
                {
                    button1.Text = BtnNom.ToString();
                }
                else
                {
                    button1.Text = BtnNom.ToString();
                }
            }

            using (SqlDataReader read = cmd.ExecuteReader())
                      
                while (read.Read())
                {
                    textBoxNom1.Text = (read["Nom1"].ToString());
                    textBoxNom2.Text = (read["Nom2"].ToString());
                    textBoxNom3.Text = (read["Nom3"].ToString());
                    textBoxNomMère1.Text = (read["NomMère1"].ToString());
                    textBoxNomMère2.Text = (read["NomMère2"].ToString());
                    textBoxNomMère3.Text = (read["NomMère3"].ToString());
                    int colIndex3 = (read.GetOrdinal("DOB"));
                    if (!read.IsDBNull(colIndex3))
                        dateTimePickerDOB.Value = (read.GetDateTime(colIndex3));
                    //dateTimePickerDOB.TextDOB.Text = (read["DOB"].ToString());
                    textBoxPOB.Text = (read["POB"].ToString());
                    textBoxCodeElecteur.Text = (read["CodeElecteur"].ToString());
                    
                    int colIndexAcni = (read.GetOrdinal("AncienCNI"));
                    if (!read.IsDBNull(colIndexAcni))
                    {
                        textBoxAncCNI.Text = (read["AncienCNI"].ToString());
                    }
                    int colIndex = (read.GetOrdinal("DateAncienCNI"));
                    if (!read.IsDBNull(colIndex))
                    {
                        dateTimePicker1.Value = (read.GetDateTime(colIndex));
                        dateTimePicker1.Enabled = true;
                    }
                    int colIndex2 = (read.GetOrdinal("DateNewCNI"));
                    if (!read.IsDBNull(colIndex2))
                    {
                        dateTimePicker2.Value = (read.GetDateTime(colIndex2));
                        dateTimePicker2.Enabled = true;
                    }
                    int colIndexNcni = (read.GetOrdinal("NEWCNI"));
                    if (!read.IsDBNull(colIndexNcni))
                    {
                        textBoxNewCNI.Text = (read["NewCNI"].ToString());
                    }
                    
                    textBoxCodeElecteur.Text = (read["CodeElecteur"].ToString());
                    comboBoxSexe.SelectedItem = (read["Sexe"].ToString());
                    textBoxPhone.Text = (read["Téléphone"].ToString());
                    statutID = (read["StatutID"].ToString());
                    LabelStatut.Text = this.getStatutName();
                    //adresseID = this.getAdresseID();
                    //IDRégion = (read["RégionID"].ToString());
                    comboBoxRégion.SelectedValue = this.getRégionName().ToString();
                    comboBoxCommune.SelectedValue = this.getCommune().ToString();
                    comboBoxArrondis.SelectedValue = this.getArrondissement().ToString();
                    //comboBoxAdresse.text = this.getadress().ToString();
                    
            
                    



                }
            //this.fill();
            
        }
        //public DataGridView Dgv { get; set; }
        

        public void fill()
        {
                        
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            historique hist = new historique();
            
            string dtappr=null;
            if (button1.Text == "Modifier et Approuver")
            {
                dtappr = ",DateApprobation =@DateApprobation, AgentApprobation =@AgentApprobation";
            }
            else
            {
                if (button1.Text == "Modifier et Valider")
                {
                    dtappr = ",DateValidation =@DateValidation, AgentValidation =@AgentValidation";
                }
                else
                {
                    if (button1.Text == "Mettre à jour")
                    {
                        dtappr = "";
                    }
                }
            }
            Connection cn = new Connection();
            string sCon = conn.connectdb();
            string updateCmd = "UPDATE Electeur SET Nom1=@Nom1, Nom2=@Nom2, Nom3=@Nom3, NomMère1=@NomMère1, NomMère2=@NomMère2, NomMère3=@NomMère3,DOB=@DOB,POB=@POB,CodeElecteur=@CodeElecteur,AncienCNI=@AncienCNI,DateAncienCNI=@DateAncienCNI,NewCNI=@NewCNI,DateNewCNI =@DateNewCNI,StatutID =@StatutID,Téléphone=@Téléphone,AdresseID=@AdresseID,AgentID=@AgentID,Sexe=@Sexe,DateModification=@DateModification,AgentModif=@AgentModif"+dtappr+" where ElecteurID='" + elect + "'";
            SqlConnection dbConn;
            dbConn = new SqlConnection(sCon);
            dbConn.Open();
            Login lg = new Login();
            Approuver Appr = new Approuver();
            Valider valid = new Valider();
            Rechecher src = new Rechecher();

            string msg;
            adresse = comboBoxAdresse.Text;
            if (button1.Text == "Modifier")
            {
                statutID = "Créé";
                msg = "Mise à Jour Effectuée avec Succès";
                action = "Modification";
                
            }
            else
            {
                if (button1.Text == "Modifier et Valider")
                {
                    statutID = "Validé";
                    msg = "Electeur Validé avec Succès";
                    action = "Modification et validation";
                }
                else
                {
                    if (button1.Text == "Mettre à jour")
                    {
                        //statutID = "Validé";
                        msg = "Mise à jour avec Succès";
                        action = "Mise à jour";
                    }
                    else
                    {
                        statutID = "Approuvé";
                        msg = "Electeur Approuvé avec Succès";
                        action = "Modification et Approbation";
                    }
                }
            }
            int AGentID;
            string dat = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            DateTime date4 = DateTime.Parse(dat);
            DateTime Date3 = DateTime.Parse(dateTimePickerDOB.Text.ToUpper());
            DateTime Date1 = DateTime.Parse(dateTimePicker1.Text.ToUpper());
            DateTime Date2 = DateTime.Parse(dateTimePicker2.Text.ToUpper());
                
            GetValue value = new GetValue();
            
            //getAdress add = new getAdress();
            SqlCommand myCommand = new SqlCommand(updateCmd, dbConn);
            // Create parameters for the SqlCommand object
            // initialize with input-form field values
            //myCommand.Parameters.AddWithValue("@ElecteurID","");
            myCommand.Parameters.AddWithValue("@Nom1", textBoxNom1.Text.ToUpper());
            myCommand.Parameters.AddWithValue("@Nom2", textBoxNom2.Text.ToUpper());
            myCommand.Parameters.AddWithValue("@Nom3", textBoxNom3.Text.ToUpper());
            myCommand.Parameters.AddWithValue("@NomMère1", textBoxNomMère1.Text.ToUpper());
            myCommand.Parameters.AddWithValue("@NomMère2", textBoxNomMère2.Text.ToUpper());
            myCommand.Parameters.AddWithValue("@NomMère3", textBoxNomMère3.Text.ToUpper());
            myCommand.Parameters.AddWithValue("@DOB", Date3);
            myCommand.Parameters.AddWithValue("@POB", textBoxPOB.Text.ToUpper());
            myCommand.Parameters.AddWithValue("@CodeElecteur", textBoxCodeElecteur.Text);
            if (dateTimePicker1.Enabled)
            {
                myCommand.Parameters.AddWithValue("@AncienCNI", textBoxAncCNI.Text);
                myCommand.Parameters.AddWithValue("@DateAncienCNI", Date1);
            }
            else
            {
                myCommand.Parameters.AddWithValue("@AncienCNI", DBNull.Value);
                myCommand.Parameters.AddWithValue("@DateAncienCNI", DBNull.Value);
            }

            if (dateTimePicker2.Enabled)
            {
                myCommand.Parameters.AddWithValue("@NewCNI", textBoxNewCNI.Text);
                myCommand.Parameters.AddWithValue("@DateNewCNI", Date2);
            }
            else
            {
                myCommand.Parameters.AddWithValue("@NewCNI", DBNull.Value);
                myCommand.Parameters.AddWithValue("@DateNewCNI", DBNull.Value);
            }
            /*myCommand.Parameters.AddWithValue("@AncienCNI", textBoxAncCNI.Text);
            myCommand.Parameters.AddWithValue("@DateAncienCNI", Date1);
            myCommand.Parameters.AddWithValue("@NewCNI", textBoxNewCNI.Text);
            myCommand.Parameters.AddWithValue("@DateNewCNI", Date2);*/
            //myCommand.Parameters.AddWithValue("@AgentSaisie", value.getAgentSaisie());//recupere agent saisie
            myCommand.Parameters.AddWithValue("@AgentModif", value.getAgentModif());//recupere agent modif
            //myCommand.Parameters.AddWithValue("@AgentCréation", value.getAgentCréation());//recupere agent de creation
            if (button1.Text == "Mettre à jour")
            {
                myCommand.Parameters.AddWithValue("@StatutID", this.getstatut());
            }
            else
            { myCommand.Parameters.AddWithValue("@StatutID", this.getStatutID() /*add.getStatutID()*/); }//Recupere le statut
            myCommand.Parameters.AddWithValue("@Téléphone", textBoxPhone.Text);
            myCommand.Parameters.AddWithValue("@AdresseID", this.getAdresseID());//recupere l'ID
            myCommand.Parameters.AddWithValue("@AgentID", value.getAgentID());//recupere l'ID agent
            myCommand.Parameters.AddWithValue("@Sexe", comboBoxSexe.Text);
            myCommand.Parameters.AddWithValue("@Région", comboBoxRégion.Text);
            myCommand.Parameters.AddWithValue("@Commune", comboBoxCommune.Text);
            myCommand.Parameters.AddWithValue("@Arrondissement", comboBoxArrondis.Text);
            myCommand.Parameters.AddWithValue("@DateModification", dat);
            
            if (dtappr == ",DateApprobation =@DateApprobation, AgentApprobation =@AgentApprobation")
            {
                myCommand.Parameters.AddWithValue("@DateApprobation", dat);
                myCommand.Parameters.AddWithValue("@AgentApprobation", value.getAgentModif());
            }
            else
            {
                if (dtappr == ",DateValidation =@DateValidation, AgentValidation =@AgentValidation")
                {
                    myCommand.Parameters.AddWithValue("@DateValidation", dat);
                    myCommand.Parameters.AddWithValue("@AgentValidation", value.getAgentModif());
                }
            }

            myCommand.ExecuteNonQuery();
            AGentID = value.getAgentID();
            hist.addhistoric(AGentID,IDElect,action);
            MessageBox.Show(msg);
            //RecursiveClearTextBoxes(this.Controls);
            Form1 fr = new Form1();
            if (button1.Text == "Modifier")
            {
                
                
                src.MdiParent = this.ParentForm;
                src.Show();
                this.Hide();
            }
            else
            {
                if (button1.Text == "Modifier et Valider")
                {
                    
                    valid.MdiParent = this.ParentForm;
                    valid.Show();
                    this.Hide();
                    
                }
                else
                {
                    if (button1.Text == "Mettre à jour")
                    {
                        src.MdiParent = this.ParentForm;
                        src.Show();
                        this.Hide();
                    }
                    else
                    {
                        Appr.MdiParent = this.ParentForm;
                        Appr.Show();
                        this.Hide();
                    }
                }
            }
            
           
           
        }

        public int getStatutID()
        {
            //SqlDataReader rd = null;
            string query = "select StatutID from Statut where Statut='" + statutID + "'";
            string sCon = conn.connectdb();
            SqlConnection dbConn;
            dbConn = new SqlConnection(sCon);
            dbConn.Open();
            SqlCommand cmd = new SqlCommand(query, dbConn);
            int result = (int)cmd.ExecuteScalar();
            //SaisieElecteur se=new SaisieElecteur();
            int statut = result;
            return statut;

        }
        public string getStatutName()
        {
            //SqlDataReader rd = null;
            string query = "select Statut from Statut where StatutID='" + statutID + "'";
            string sCon = conn.connectdb();
            SqlConnection dbConn;
            dbConn = new SqlConnection(sCon);
            dbConn.Open();
            SqlCommand cmd = new SqlCommand(query, dbConn);
            string result = cmd.ExecuteScalar().ToString();
            //SaisieElecteur se=new SaisieElecteur();
            string statut = result;
            return statut;

        }
        public int getAdresseID()
        {
            //SqlDataReader rd = null;
            string query = "select AdresseID from Adresse where NomAdresse='" + adresse + "'";
            string sCon = conn.connectdb();
            SqlConnection dbConn;
            dbConn = new SqlConnection(sCon);
            dbConn.Open();
            SqlCommand cmd = new SqlCommand(query, dbConn);
            int result = ((int)cmd.ExecuteScalar());
            //SaisieElecteur se=new SaisieElecteur();
            int adress = result;
            return adress;

        }
        public int getRégionName()
        {
            //SqlDataReader rd = null;
            string query = "select RégionID from Région where RégionID = (Select RégionID from Commune where CommuneID= (select CommuneID from Arrondissement where ArrondisID=(select ArrondisID from Adresse where AdresseID ='" + addressID + "')))";
            string sCon = conn.connectdb();
            SqlConnection dbConn;
            dbConn = new SqlConnection(sCon);
            dbConn.Open();
            SqlCommand cmd = new SqlCommand(query, dbConn);
            int result = ((int)cmd.ExecuteScalar());
            //SaisieElecteur se=new SaisieElecteur();
            RégionName = result;
            return RégionName;

        }
        public int getArrondissement()
        {
            //SqlDataReader rd = null;
            string query = "select ArrondisID from Arrondissement where ArrondisID=(select ArrondisID from Adresse where AdresseID ='" + addressID + "')";
            string sCon = conn.connectdb();
            SqlConnection dbConn;
            dbConn = new SqlConnection(sCon);
            dbConn.Open();
            SqlCommand cmd = new SqlCommand(query, dbConn);
            int result = ((int)cmd.ExecuteScalar());
            //SaisieElecteur se=new SaisieElecteur();
            ArrondisName= result;
            return ArrondisName;

        }
        public int getCommune()
        {
            //SqlDataReader rd = null;
            string query = "Select CommuneID from Commune where CommuneID= (select CommuneID from Arrondissement where ArrondisID=(select ArrondisID from Adresse where AdresseID ='" + addressID + "'))";
            string sCon = conn.connectdb();
            SqlConnection dbConn;
            dbConn = new SqlConnection(sCon);
            dbConn.Open();
            SqlCommand cmd = new SqlCommand(query, dbConn);
            int result = ((int)cmd.ExecuteScalar());
            //SaisieElecteur se=new SaisieElecteur();
            CommuneName = result;
            return CommuneName;

        }
        public string getadress()
        {
            //SqlDataReader rd = null;
            string query = "Select NomAdresse from Adresse where AdresseID= '" + addressID + "'";
            string sCon = conn.connectdb();
            SqlConnection dbConn;
            dbConn = new SqlConnection(sCon);
            dbConn.Open();
            SqlCommand cmd = new SqlCommand(query, dbConn);
            string result = (cmd.ExecuteScalar().ToString());
            //SaisieElecteur se=new SaisieElecteur();
            adressName = result;
            return adressName;

        }
        public int getstatut()
        {
            //SqlDataReader rd = null;
            string query = "select StatutID from Electeur where ElecteurID='" + elect + "'";
            string sCon = conn.connectdb();
            SqlConnection dbConn;
            dbConn = new SqlConnection(sCon);
            dbConn.Open();
            SqlCommand cmd = new SqlCommand(query, dbConn);
            int result = ((int)cmd.ExecuteScalar());
            //SaisieElecteur se=new SaisieElecteur();
            int stat = result;
            return stat;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void textBoxAncCNI_KeyPress(object sender, KeyPressEventArgs e)
        {
            int isNum = 0;
            if (e.KeyChar == ',' || e.KeyChar == (char)Keys.Back)
                e.Handled = false;
            else if (!int.TryParse(e.KeyChar.ToString(), out isNum))
                e.Handled = true;
        }
        private void textBoxNewCNI_KeyPress(object sender, KeyPressEventArgs e)
        {
            int isNum = 0;
            if (e.KeyChar == ',' || e.KeyChar == (char)Keys.Back)
                e.Handled = false;
            else if (!int.TryParse(e.KeyChar.ToString(), out isNum))
                e.Handled = true;
        }
        private void textBoxPhone_KeyPress(object sender, KeyPressEventArgs e)
        {
            int isNum = 0;
            if (e.KeyChar == ',' || e.KeyChar == (char)Keys.Back)
                e.Handled = false;
            else if (!int.TryParse(e.KeyChar.ToString(), out isNum))
                e.Handled = true;
        }
        private void textBoxCodeElecteur_KeyPress(object sender, KeyPressEventArgs e)
        {
            int isNum = 0;
            if (e.KeyChar == ',')
                e.Handled = false;
            else if (!int.TryParse(e.KeyChar.ToString(), out isNum))
                e.Handled = true;
        }
        private void textBoxCodeElecteur_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Back)
            {
                e.SuppressKeyPress = true;
            }
        }

        private void label10_Click(object sender, EventArgs e)
        {

        }
        private void textBoxNom1_KeyPress(object sender, KeyPressEventArgs e)
        {
            int isNum = 0;
            if (e.KeyChar == ',' || e.KeyChar == (char)Keys.Back)
                e.Handled = false;
            else if (int.TryParse(e.KeyChar.ToString(), out isNum))
                e.Handled = true;
        }
        private void textBoxAncCNI_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //string errorMsg;
            if (textBoxAncCNI.Text.Trim() != "")
            {
                dateTimePicker1.Enabled = true;
                string sCon = conn.connectdb();
                SqlConnection con = new SqlConnection(sCon);
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT AncienCNI FROM Electeur WHERE AncienCNI =@AncienCNI";
                cmd.Parameters.AddWithValue("@AncienCNI", textBoxAncCNI.Text.Trim());
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                con.Open();
                dAdapter.Fill(objDs);
                con.Close();
                if (objDs.Tables[0].Rows.Count > 0)
                {
                    MessageBox.Show("Numéro déjà enregistré");
                    textBoxAncCNI.Focus();
                }
            }
        }

        private void textBoxNewCNI_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //string errorMsg;
            if (textBoxNewCNI.Text.Trim() != "")
            {
                dateTimePicker2.Enabled = true;
                string sCon = conn.connectdb();
                SqlConnection con = new SqlConnection(sCon);
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT NewCNI FROM Electeur WHERE NewCNI =@NewCNI";
                cmd.Parameters.AddWithValue("@NewCNI", textBoxNewCNI.Text.Trim());
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                con.Open();
                dAdapter.Fill(objDs);
                con.Close();
                if (objDs.Tables[0].Rows.Count > 0)
                {
                    MessageBox.Show("Numéro déjà enregistré");
                    textBoxAncCNI.Focus();
                }
            }
        }

        private void FillArrondis(int CommuneID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT ArrondisID, NomArrondis FROM Arrondissement WHERE CommuneID =@CommuneID";
            cmd.Parameters.AddWithValue("@CommuneID", CommuneID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxArrondis.ValueMember = "ArrondisID";
                comboBoxArrondis.DisplayMember = "NomArrondis";
                comboBoxArrondis.DataSource = objDs.Tables[0];
            }
        }
        private void FillAdresse(int ArrondisID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT AdresseID, NomAdresse FROM Adresse WHERE ArrondisID =@ArrondisID";
            cmd.Parameters.AddWithValue("@ArrondisID", ArrondisID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxAdresse.ValueMember = "AdresseID";
                comboBoxAdresse.DisplayMember = "NomAdresse";
                comboBoxAdresse.DataSource = objDs.Tables[0];
                
            }
            comboBoxAdresse.SelectedValue = addressID;
            //comboBoxAdresse.SelectedValue = this.getadress();
        }
        private void FillCommune(int RégionID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT CommuneID, NomCommune FROM Commune WHERE RégionID =@RégionID";
            cmd.Parameters.AddWithValue("@RégionID", RégionID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxCommune.ValueMember = "CommuneID";
                comboBoxCommune.DisplayMember = "NomCommune";
                comboBoxCommune.DataSource = objDs.Tables[0];
            }
        }
        private void comboBoxCommune_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxCommune.SelectedValue.ToString() != "")
            {
                int CommuneID = Convert.ToInt32(comboBoxCommune.SelectedValue.ToString());
                this.FillArrondis(CommuneID);
                comboBoxArrondis.SelectedIndex = 0;
            }
        }
        private void comboBoxArrondis_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxArrondis.SelectedValue.ToString() != "")
            {
                int ArrondisID = Convert.ToInt32(comboBoxArrondis.SelectedValue.ToString());
                this.FillAdresse(ArrondisID);
                //comboBoxAdresse.SelectedValue.ToString() = this.getadress().ToString();
                //comboBoxAdresse.SelectedValue.= this.getadress();
            }
        }
        private void comboBoxRégion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxRégion.SelectedValue.ToString() != "")
            {
                int RégionID = Convert.ToInt32(comboBoxRégion.SelectedValue.ToString());
                FillCommune(RégionID);
                comboBoxCommune.SelectedIndex = 0;
            }
        }

        private void label18_Click(object sender, EventArgs e)
        {
            Label clickedLabel = sender as Label;
            historicModif histModif = new historicModif(IDElect);
            histModif.Show();

        }
       

        

        
    }
}
