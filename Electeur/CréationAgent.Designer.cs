﻿namespace Electeur
{
    partial class CréationAgent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.textBoxPwd = new System.Windows.Forms.TextBox();
            this.textBoxUser = new System.Windows.Forms.TextBox();
            this.textBoxNom3 = new System.Windows.Forms.TextBox();
            this.textBoxNom2 = new System.Windows.Forms.TextBox();
            this.textBoxNom1 = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.radioButtonRet = new System.Windows.Forms.RadioButton();
            this.radioButtonSrc = new System.Windows.Forms.RadioButton();
            this.radioButtonImp = new System.Windows.Forms.RadioButton();
            this.radioButtonAff = new System.Windows.Forms.RadioButton();
            this.radioButtonRadiation = new System.Windows.Forms.RadioButton();
            this.radioButtonAffection = new System.Windows.Forms.RadioButton();
            this.radioButtonValidation = new System.Windows.Forms.RadioButton();
            this.radioButtonApprobation = new System.Windows.Forms.RadioButton();
            this.radioButtonSaisie = new System.Windows.Forms.RadioButton();
            this.radioButtonStat = new System.Windows.Forms.RadioButton();
            this.radioButtonAdmin = new System.Windows.Forms.RadioButton();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.textBoxPwd);
            this.panel1.Controls.Add(this.textBoxUser);
            this.panel1.Controls.Add(this.textBoxNom3);
            this.panel1.Controls.Add(this.textBoxNom2);
            this.panel1.Controls.Add(this.textBoxNom1);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(65, 116);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(848, 411);
            this.panel1.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.button1.Location = new System.Drawing.Point(323, 368);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(178, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "Créer";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBoxPwd
            // 
            this.textBoxPwd.Location = new System.Drawing.Point(428, 67);
            this.textBoxPwd.Name = "textBoxPwd";
            this.textBoxPwd.Size = new System.Drawing.Size(137, 20);
            this.textBoxPwd.TabIndex = 9;
            // 
            // textBoxUser
            // 
            this.textBoxUser.Location = new System.Drawing.Point(145, 67);
            this.textBoxUser.Name = "textBoxUser";
            this.textBoxUser.Size = new System.Drawing.Size(154, 20);
            this.textBoxUser.TabIndex = 8;
            // 
            // textBoxNom3
            // 
            this.textBoxNom3.Location = new System.Drawing.Point(456, 29);
            this.textBoxNom3.Name = "textBoxNom3";
            this.textBoxNom3.Size = new System.Drawing.Size(190, 20);
            this.textBoxNom3.TabIndex = 7;
            // 
            // textBoxNom2
            // 
            this.textBoxNom2.Location = new System.Drawing.Point(309, 29);
            this.textBoxNom2.Name = "textBoxNom2";
            this.textBoxNom2.Size = new System.Drawing.Size(137, 20);
            this.textBoxNom2.TabIndex = 6;
            // 
            // textBoxNom1
            // 
            this.textBoxNom1.Location = new System.Drawing.Point(145, 29);
            this.textBoxNom1.Name = "textBoxNom1";
            this.textBoxNom1.Size = new System.Drawing.Size(154, 20);
            this.textBoxNom1.TabIndex = 5;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.radioButtonRet);
            this.panel2.Controls.Add(this.radioButtonSrc);
            this.panel2.Controls.Add(this.radioButtonImp);
            this.panel2.Controls.Add(this.radioButtonAff);
            this.panel2.Controls.Add(this.radioButtonRadiation);
            this.panel2.Controls.Add(this.radioButtonAffection);
            this.panel2.Controls.Add(this.radioButtonValidation);
            this.panel2.Controls.Add(this.radioButtonApprobation);
            this.panel2.Controls.Add(this.radioButtonSaisie);
            this.panel2.Controls.Add(this.radioButtonStat);
            this.panel2.Controls.Add(this.radioButtonAdmin);
            this.panel2.Location = new System.Drawing.Point(19, 121);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(776, 197);
            this.panel2.TabIndex = 4;
            // 
            // radioButtonRet
            // 
            this.radioButtonRet.AutoSize = true;
            this.radioButtonRet.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonRet.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.radioButtonRet.Location = new System.Drawing.Point(61, 149);
            this.radioButtonRet.Name = "radioButtonRet";
            this.radioButtonRet.Size = new System.Drawing.Size(63, 17);
            this.radioButtonRet.TabIndex = 10;
            this.radioButtonRet.TabStop = true;
            this.radioButtonRet.Text = "Rétrait";
            this.radioButtonRet.UseVisualStyleBackColor = true;
            // 
            // radioButtonSrc
            // 
            this.radioButtonSrc.AutoSize = true;
            this.radioButtonSrc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonSrc.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.radioButtonSrc.Location = new System.Drawing.Point(328, 149);
            this.radioButtonSrc.Name = "radioButtonSrc";
            this.radioButtonSrc.Size = new System.Drawing.Size(95, 17);
            this.radioButtonSrc.TabIndex = 9;
            this.radioButtonSrc.TabStop = true;
            this.radioButtonSrc.Text = "Consultation";
            this.radioButtonSrc.UseVisualStyleBackColor = true;
            this.radioButtonSrc.Visible = false;
            // 
            // radioButtonImp
            // 
            this.radioButtonImp.AutoSize = true;
            this.radioButtonImp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonImp.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.radioButtonImp.Location = new System.Drawing.Point(567, 108);
            this.radioButtonImp.Name = "radioButtonImp";
            this.radioButtonImp.Size = new System.Drawing.Size(85, 17);
            this.radioButtonImp.TabIndex = 8;
            this.radioButtonImp.TabStop = true;
            this.radioButtonImp.Text = "Impression";
            this.radioButtonImp.UseVisualStyleBackColor = true;
            // 
            // radioButtonAff
            // 
            this.radioButtonAff.AutoSize = true;
            this.radioButtonAff.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonAff.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.radioButtonAff.Location = new System.Drawing.Point(327, 108);
            this.radioButtonAff.Name = "radioButtonAff";
            this.radioButtonAff.Size = new System.Drawing.Size(79, 17);
            this.radioButtonAff.TabIndex = 7;
            this.radioButtonAff.TabStop = true;
            this.radioButtonAff.Text = "Affichage";
            this.radioButtonAff.UseVisualStyleBackColor = true;
            // 
            // radioButtonRadiation
            // 
            this.radioButtonRadiation.AutoSize = true;
            this.radioButtonRadiation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonRadiation.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.radioButtonRadiation.Location = new System.Drawing.Point(61, 108);
            this.radioButtonRadiation.Name = "radioButtonRadiation";
            this.radioButtonRadiation.Size = new System.Drawing.Size(79, 17);
            this.radioButtonRadiation.TabIndex = 6;
            this.radioButtonRadiation.TabStop = true;
            this.radioButtonRadiation.Text = "Radiation";
            this.radioButtonRadiation.UseVisualStyleBackColor = true;
            // 
            // radioButtonAffection
            // 
            this.radioButtonAffection.AutoSize = true;
            this.radioButtonAffection.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonAffection.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.radioButtonAffection.Location = new System.Drawing.Point(567, 71);
            this.radioButtonAffection.Name = "radioButtonAffection";
            this.radioButtonAffection.Size = new System.Drawing.Size(87, 17);
            this.radioButtonAffection.TabIndex = 5;
            this.radioButtonAffection.TabStop = true;
            this.radioButtonAffection.Text = "Affectation";
            this.radioButtonAffection.UseVisualStyleBackColor = true;
            this.radioButtonAffection.CheckedChanged += new System.EventHandler(this.radioButtonAffection_CheckedChanged);
            // 
            // radioButtonValidation
            // 
            this.radioButtonValidation.AutoSize = true;
            this.radioButtonValidation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonValidation.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.radioButtonValidation.Location = new System.Drawing.Point(327, 71);
            this.radioButtonValidation.Name = "radioButtonValidation";
            this.radioButtonValidation.Size = new System.Drawing.Size(158, 17);
            this.radioButtonValidation.TabIndex = 4;
            this.radioButtonValidation.TabStop = true;
            this.radioButtonValidation.Text = "Responsable Validation";
            this.radioButtonValidation.UseVisualStyleBackColor = true;
            // 
            // radioButtonApprobation
            // 
            this.radioButtonApprobation.AutoSize = true;
            this.radioButtonApprobation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonApprobation.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.radioButtonApprobation.Location = new System.Drawing.Point(61, 71);
            this.radioButtonApprobation.Name = "radioButtonApprobation";
            this.radioButtonApprobation.Size = new System.Drawing.Size(170, 17);
            this.radioButtonApprobation.TabIndex = 3;
            this.radioButtonApprobation.TabStop = true;
            this.radioButtonApprobation.Text = "Responsable Approbation";
            this.radioButtonApprobation.UseVisualStyleBackColor = true;
            // 
            // radioButtonSaisie
            // 
            this.radioButtonSaisie.AutoSize = true;
            this.radioButtonSaisie.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonSaisie.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.radioButtonSaisie.Location = new System.Drawing.Point(567, 31);
            this.radioButtonSaisie.Name = "radioButtonSaisie";
            this.radioButtonSaisie.Size = new System.Drawing.Size(59, 17);
            this.radioButtonSaisie.TabIndex = 2;
            this.radioButtonSaisie.TabStop = true;
            this.radioButtonSaisie.Text = "Saisie";
            this.radioButtonSaisie.UseVisualStyleBackColor = true;
            // 
            // radioButtonStat
            // 
            this.radioButtonStat.AutoSize = true;
            this.radioButtonStat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonStat.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.radioButtonStat.Location = new System.Drawing.Point(328, 31);
            this.radioButtonStat.Name = "radioButtonStat";
            this.radioButtonStat.Size = new System.Drawing.Size(85, 17);
            this.radioButtonStat.TabIndex = 1;
            this.radioButtonStat.TabStop = true;
            this.radioButtonStat.Text = "Statistique";
            this.radioButtonStat.UseVisualStyleBackColor = true;
            // 
            // radioButtonAdmin
            // 
            this.radioButtonAdmin.AutoSize = true;
            this.radioButtonAdmin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonAdmin.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.radioButtonAdmin.Location = new System.Drawing.Point(61, 31);
            this.radioButtonAdmin.Name = "radioButtonAdmin";
            this.radioButtonAdmin.Size = new System.Drawing.Size(105, 17);
            this.radioButtonAdmin.TabIndex = 0;
            this.radioButtonAdmin.TabStop = true;
            this.radioButtonAdmin.Text = "Administrateur";
            this.radioButtonAdmin.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Location = new System.Drawing.Point(27, 105);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 17);
            this.label5.TabIndex = 3;
            this.label5.Text = "Profil :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Location = new System.Drawing.Point(306, 67);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 17);
            this.label4.TabIndex = 2;
            this.label4.Text = "Mot de Passe :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(27, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(129, 17);
            this.label3.TabIndex = 1;
            this.label3.Text = "Nom Utilisateur :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(27, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "Nom :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(89, 104);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Créer un agent";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label25.Location = new System.Drawing.Point(88, 27);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(213, 23);
            this.label25.TabIndex = 18;
            this.label25.Text = "CREATION D\'UN AGENT";
            // 
            // CréationAgent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSeaGreen;
            this.ClientSize = new System.Drawing.Size(1008, 730);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Name = "CréationAgent";
            this.Text = "Rétrait";
            this.TransparencyKey = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Load += new System.EventHandler(this.CréationAgent_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBoxPwd;
        private System.Windows.Forms.TextBox textBoxUser;
        private System.Windows.Forms.TextBox textBoxNom3;
        private System.Windows.Forms.TextBox textBoxNom2;
        private System.Windows.Forms.TextBox textBoxNom1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton radioButtonSaisie;
        private System.Windows.Forms.RadioButton radioButtonStat;
        private System.Windows.Forms.RadioButton radioButtonAdmin;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton radioButtonValidation;
        private System.Windows.Forms.RadioButton radioButtonApprobation;
        private System.Windows.Forms.RadioButton radioButtonRadiation;
        private System.Windows.Forms.RadioButton radioButtonAffection;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.RadioButton radioButtonAff;
        private System.Windows.Forms.RadioButton radioButtonSrc;
        private System.Windows.Forms.RadioButton radioButtonImp;
        private System.Windows.Forms.RadioButton radioButtonRet;
    }
}