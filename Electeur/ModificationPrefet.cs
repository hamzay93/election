﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class ModificationPrefet : Form
    {
        string clause;
        
        Connection conn = new Connection();
        GetValue get = new GetValue();
        public ModificationPrefet()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (textBox1.Text.Trim().Length == 0)
            {
                MessageBox.Show("veuillez saisir le nom du prefet");
                textBox1.Focus();
            }
            else
            {
                if (comboBox1.Text.Length==0)
                {
                    MessageBox.Show("veuillez choisir de quel région il/elle est");
                }
                else
                {

                    //Connection cn = new Connection();
                    string insertCmd = "update Prefet set Nom=@Nom,RégionID=@RégionID,Etat=@Etat,UserModification=@UserModification,DateModification=@DateModification where PrefetID="+ clause3 +"";
                    SqlConnection dbConn;
                    dbConn = new SqlConnection(conn.connectdb());
                    dbConn.Open();
                    Login lg = new Login();
                    string dat = DateTime.Now.ToString("yyyy-MM-dd");
                    GetValue value = new GetValue();
                    //getAdress add = new getAdress();
                    SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);

                    int RegionID = Convert.ToInt32(comboBox1.SelectedValue.ToString());
                        myCommand.Parameters.AddWithValue("@RégionID", RegionID);
                   

                    if (radioButton4.Checked)
                    {
                        myCommand.Parameters.AddWithValue("@Etat", 1);
                    }
                    if (radioButton3.Checked)
                    {
                        myCommand.Parameters.AddWithValue("@Etat", 0);
                    }
                    myCommand.Parameters.AddWithValue("@Nom", textBox1.Text.ToUpper());
                 
                    myCommand.Parameters.AddWithValue("@UserModification", value.getAgentCréation());
                    myCommand.Parameters.AddWithValue("@DateModification", dat);
                    myCommand.ExecuteNonQuery();

                    MessageBox.Show("Modification effectuée avec succès");
                    textBox1.Clear();
                }


            }
        }
        public void IsEmptyfield()
        {
            clause = null;
            foreach (Control X in this.panel7.Controls)
            {

                if (X is TextBox || X is ComboBox)
                {
                    
                        if (X.Text.Trim().Length != 0)
                        {
                            if (string.IsNullOrEmpty(clause))
                            {
                                clause = clause + X.Tag + " Like '%" + X.Text.Trim() + "%'";
                                //return clause;
                            }
                            else
                            {
                                clause = clause + " And " + X.Tag + " Like '%" + X.Text.Trim() + "%'";
                                //return clause;
                            }
                        }
                    

                }
               
            }

        }
        private void FillRégion()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT RégionID, NomRégion FROM Région";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBox4.ValueMember = "RégionID";
            comboBox4.DisplayMember = "NomRégion";
            comboBox4.DataSource = objDs.Tables[0];
        }
        private void FillRégion2()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT RégionID, NomRégion FROM Région";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBox1.ValueMember = "RégionID";
            comboBox1.DisplayMember = "NomRégion";
            comboBox1.DataSource = objDs.Tables[0];
        }
        private void button6_Click(object sender, EventArgs e)
        {


            this.IsEmptyfield();
            if (String.IsNullOrEmpty(clause))
            {
                MessageBox.Show("veuillez remplir un champ au minimum");
                this.Focus();

            }
            else
            {
                string sCon = conn.connectdb();
                SqlConnection con = new SqlConnection(sCon);
                string str = "SET CONCAT_NULL_YIELDS_NULL OFF; SELECT PrefetID, Nom AS 'NOM',NomRégion AS 'Région' FROM View_prefet_region where " + clause + "";
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(str, con);
                SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sqlAdapter);

                DataTable table = new DataTable();
                table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                con.Open();
                sqlAdapter.Fill(table);
                con.Close();
                dataGridView1.DataSource = table;
                if (dataGridView1.Rows.Count == 0)
                {
                   
                    panel1.Visible = false;
                     label1.Enabled = false;
                    MessageBox.Show("Aucun Prefet retrouvé pour ces critères");

                }
                else
                {
                    panel1.Visible = true;
                    label1.Enabled = true;
                }

                this.dataGridView1.Columns["PrefetID"].Visible = false;
             

                con.Open();
                clause = "";
            }
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            
            this.filldetails();
        }
        string clause3;
        private void filldetails()
        {

            this.selection();
            if (String.IsNullOrEmpty(clause3))
            {
                this.panel1.Visible = false;
                
            }
            else
            {
                this.panel1.Visible = true;
                string sCon = conn.connectdb();
                SqlConnection con = new SqlConnection(sCon);
                string str = "SELECT * FROM View_prefet_region where PrefetID=" + clause3;
                SqlCommand cmd = new SqlCommand(str, con);
                con.Open();

                using (SqlDataReader read = cmd.ExecuteReader())

                    while (read.Read())
                    {
                        textBox1.Text = (read["Nom"].ToString());
                     
                        if ((read["Etat"].ToString()) == "1")
                        {
                            radioButton4.Checked = true;
                        }
                        if ((read["Etat"].ToString()) == "0")
                        {
                            radioButton3.Checked = true;
                        }
                        
                        comboBox1.SelectedValue = (read["RégionID"].ToString());
                       


                    }
            }

        }
        private void selection()
        {
            int counter3;
            clause3 = null;
          
            // Iterate through all the rows and sum up the appropriate columns. 
            for (counter3 = 0; counter3 < (dataGridView1.Rows.Count);
                counter3++)
            {
                //dataGridView1.Rows.
                if (dataGridView1.Rows[counter3].Selected)
                {
                    if (dataGridView1.Rows[counter3].Cells["PrefetID"].Value
                    != null)
                    {
                        if (dataGridView1.Rows[counter3].
                            Cells["PrefetID"].Value.ToString().Length != 0)
                        {
                            if (string.IsNullOrEmpty(clause3))
                            {
                                clause3 = clause3 + int.Parse(dataGridView1.Rows[counter3].
                                    Cells["PrefetID"].Value.ToString());
                            }
                            else
                            {
                                clause3 = clause3 + "," + int.Parse(dataGridView1.Rows[counter3].
                                Cells["PrefetID"].Value.ToString());

                            }

                        }
                    }
                   
                   
                }

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ClearClass1 clr = new ClearClass1();
            clr.RecursiveClearTextBoxes(this.panel7.Controls);
        }

        private void ModificationPrefet_Load(object sender, EventArgs e)
        {
            this.FillRégion2();
            this.FillRégion();

        }
    }
}
