﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class Modifitypereunion : Form
    {
        Connection conn = new Connection();
        public Modifitypereunion()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {


            if (textBox1.Text.Trim().Length == 0)
            {
                MessageBox.Show("veuillez saisir l'inttitulé");
                textBox1.Focus();
            }
            else
            {
                //Connection cn = new Connection();
                string insertCmd = "update  TypeReunion set ObjetTypeReunion=@Objet,UserModification=@UserModification,DateModification=@DateModification where TypeReunionID=" + clause3 + "";
                SqlConnection dbConn;
                dbConn = new SqlConnection(conn.connectdb());
                dbConn.Open();
                Login lg = new Login();
                string dat = DateTime.Now.ToString("yyyy-MM-dd");
                DateTime date4 = DateTime.Parse(dat);
                GetValue value = new GetValue();
                //getAdress add = new getAdress();
                SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);

                myCommand.Parameters.AddWithValue("@Objet", textBox1.Text);
                myCommand.Parameters.AddWithValue("@UserModification", value.getAgentModif());
                myCommand.Parameters.AddWithValue("@DateModification", dat);
                myCommand.ExecuteNonQuery();

                MessageBox.Show("Modification avec succès");
                textBox1.Clear();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

            if (textBox2.Text.Trim().Length == 0)
            {
                MessageBox.Show("veuillez saisir l'inttitulé");
                textBox2.Focus();
            }
            else
            {
                string sCon = conn.connectdb();
                SqlConnection con = new SqlConnection(sCon);
                string str = "SET CONCAT_NULL_YIELDS_NULL OFF; SELECT TypeReunionID,ObjetTypeReunion as 'TypeReunion' FROM TypeReunion where ObjetTypeReunion='" + textBox2.Text.Trim() + "%'";
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(str, con);
                SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sqlAdapter);

                DataTable table = new DataTable();
                table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                con.Open();
                sqlAdapter.Fill(table);
                con.Close();
                dataGridView1.DataSource = table;
                if (dataGridView1.Rows.Count == 0)
                {
                    button3.Enabled = false;
                    panel1.Visible = false;
                    label1.Visible = false;
                    MessageBox.Show("Aucun TypeReunion retrouvé pour ces critères");

                }
                else
                {
                    button3.Enabled = true;
                    panel1.Visible = true;
                    label1.Visible = true;
                }

                this.dataGridView1.Columns["TypeReunionID"].Visible = false;
            }
          
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ClearClass1 clr = new ClearClass1();
            clr.RecursiveClearTextBoxes(this.panel3.Controls);
        }
        private void selection()
        {
            int counter3;
            clause3 = null;

            // Iterate through all the rows and sum up the appropriate columns. 
            for (counter3 = 0; counter3 < (dataGridView1.Rows.Count);
                counter3++)
            {
                //dataGridView1.Rows.
                if (dataGridView1.Rows[counter3].Selected)
                {
                    if (dataGridView1.Rows[counter3].Cells["TypeReunionID"].Value
                    != null)
                    {
                        if (dataGridView1.Rows[counter3].
                            Cells["TypeReunionID"].Value.ToString().Length != 0)
                        {
                            if (string.IsNullOrEmpty(clause3))
                            {
                                clause3 = clause3 + int.Parse(dataGridView1.Rows[counter3].
                                    Cells["TypeReunionID"].Value.ToString());
                            }
                            else
                            {
                                clause3 = clause3 + "," + int.Parse(dataGridView1.Rows[counter3].
                                Cells["TypeReunionID"].Value.ToString());

                            }

                        }
                    }


                }

            }
        }
        string clause3;
        private void filldetails()
        {

            this.selection();
            if (String.IsNullOrEmpty(clause3))
            {
                button3.Enabled = false;
                panel1.Visible = false;
                label1.Visible = false;
            }
            else
            {
                button3.Enabled = true;
                panel1.Visible = true;
                label1.Visible = true;
                string sCon = conn.connectdb();
                SqlConnection con = new SqlConnection(sCon);
                string str = " SELECT ObjetTypeReunion FROM TypeReunion where TypeReunionID=" + clause3;
                SqlCommand cmd = new SqlCommand(str, con);
                con.Open();

                using (SqlDataReader read = cmd.ExecuteReader())

                    while (read.Read())
                    {
                        textBox1.Text = (read["ObjetTypeReunion"].ToString());



                    }
            }

        }
        private void Modifitypereunion_Load(object sender, EventArgs e)
        {

        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            this.filldetails();
        }
    }
}
