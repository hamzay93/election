﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class consultation_par_Bureauvote : Form
    {
        Connection conn = new Connection();
        public consultation_par_Bureauvote()
        {
            InitializeComponent();
        }

        private void consultation_par_Bureauvote_Load(object sender, EventArgs e)
        {
            this.FillRégion();
            this.FillElection();
        }
        private void FillElection()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT ElectionID, ObjetElection +' - '+ObjetTourElection as NomElection from View_Elect where Etat= 1";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBoxElect.ValueMember = "ElectionID";
            comboBoxElect.DisplayMember = "NomElection";
            comboBoxElect.DataSource = objDs.Tables[0];
        }

        private void FillRégion()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT RégionID, NomRégion FROM Région";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            objDs.Tables[0].Rows.Add(30, "");
            con.Close();
            comboBoxRégion.ValueMember = "RégionID";
            comboBoxRégion.DisplayMember = "NomRégion";
            comboBoxRégion.DataSource = objDs.Tables[0];
            comboBoxRégion.SelectedValue = 30;
        }
        //private void Fillheure()
        //{
        //    string sCon = conn.connectdb();
        //    SqlConnection con = new SqlConnection(sCon);
        //    SqlCommand cmd = new SqlCommand();
        //    cmd.Connection = con;
        //    cmd.CommandType = CommandType.Text;
        //    cmd.CommandText = "SELECT horaire_id, heure FROM horaire where horaire_id in(select horaire_id from Particip where BUREAU_ID = " + Int32.Parse(comboBoxBV.SelectedValue.ToString()) + ")";
        //    DataSet objDs = new DataSet();
        //    SqlDataAdapter dAdapter = new SqlDataAdapter();
        //    dAdapter.SelectCommand = cmd;
        //    con.Open();
        //    dAdapter.Fill(objDs);
        //    con.Close();
        //    comboBox1.ValueMember = "horaire_id";
        //    comboBox1.DisplayMember = "heure";
        //    comboBox1.DataSource = objDs.Tables[0];
        //}
        private void FillCommune(int RégionID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT CommuneID, NomCommune FROM Commune WHERE RégionID =@RégionID";
            if (RégionID == 30)
            {
                int reg = 1;
                cmd.Parameters.AddWithValue("@RégionID", reg);


            }
            else
            {
                cmd.Parameters.AddWithValue("@RégionID", RégionID);
            }

            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            objDs.Tables[0].Rows.Add(30, "");
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxCommune.ValueMember = "CommuneID";
                comboBoxCommune.DisplayMember = "NomCommune";
                comboBoxCommune.DataSource = objDs.Tables[0];
                comboBoxCommune.SelectedValue = 30;
            }
        }
        private void FillCV(int ArrondisID)
        {
            string sCon = conn.connectdb(); ;
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            if (comboBoxArrondis.Visible)
            {
                cmd.CommandText = "SET CONCAT_NULL_YIELDS_NULL OFF;SELECT CentreID, ( CONVERT(Nvarchar(50), CodeCentre)) + ' - '+CentreVote as CentreVote FROM View_CV where Actif = 1 and ArrondisID =@ArrondisID ORDER BY CodeCentre";
                if (ArrondisID == 30)
                {
                    int com = 1;
                    //cmd.CommandText = "SELECT CentreID, CentreVote FROM View_CV where Actif = 1 and ArrondisID =@ArrondisID ORDER BY CodeCentre";
                    cmd.Parameters.AddWithValue("@ArrondisID", com);


                }
                else
                {

                    cmd.Parameters.AddWithValue("@ArrondisID", ArrondisID);
                }
            }
            else
            {
                cmd.CommandText = "SELECT CentreID, CentreVote FROM View_CV where Actif = 1 and LocalitéID =@ArrondisID ORDER BY CodeCentre";
                if (ArrondisID == 30000)
                {

                    int com = 1;
                    cmd.Parameters.AddWithValue("@ArrondisID", com);


                }
                else
                {
                    cmd.Parameters.AddWithValue("@ArrondisID", ArrondisID);
                }
            }

            //cmd.Parameters.AddWithValue("@ArrondisID", ArrondisID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            objDs.Tables[0].Rows.Add(3000, "");
            con.Close();
            comboBoxCV.ValueMember = "CentreID";
            comboBoxCV.DisplayMember = "CentreVote";
            comboBoxCV.DataSource = objDs.Tables[0];
            comboBoxCV.SelectedValue = 3000;
        }
        private void FillBureau(int CentreID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SET CONCAT_NULL_YIELDS_NULL OFF;SELECT BureauID, ( CONVERT(Nvarchar(50), CodeBureau)) + ' - '+NomBureau as NomBureau FROM View_LV WHERE CentreID =@CentreID ORDER BY CodeCentre,OrdreCentre";
            cmd.Parameters.AddWithValue("@CentreID", CentreID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            objDs.Tables[0].Rows.Add(3000, "");
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxBV.ValueMember = "BureauID";
                comboBoxBV.DisplayMember = "NomBureau";
                comboBoxBV.DataSource = objDs.Tables[0];
                comboBoxBV.SelectedValue = 3000;
            }
        }
        private void FillArrondis(int CommuneID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT ArrondisID, NomArrondis FROM Arrondissement WHERE CommuneID =@CommuneID";
            if (CommuneID == 30)
            {
                int com = 1;
                cmd.Parameters.AddWithValue("@CommuneID", com);


            }
            else
            {
                cmd.Parameters.AddWithValue("@CommuneID", CommuneID);
            }
            //cmd.Parameters.AddWithValue("@CommuneID", CommuneID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            objDs.Tables[0].Rows.Add(30, "");
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxArrondis.ValueMember = "ArrondisID";
                comboBoxArrondis.DisplayMember = "NomArrondis";
                comboBoxArrondis.DataSource = objDs.Tables[0];
                comboBoxArrondis.SelectedValue = 30;


            }
        }
        private void Filllocalite(int _reg)
        {
            int reg = _reg;
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT LocalitéID, NomLocalité FROM Localité WHERE LocalitéID in(Select LocalitéID from View_CV) and RégionID = " + reg;
            //cmd.Parameters.AddWithValue("@RégionID", RégionID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            objDs.Tables[0].Rows.Add(30000, "");
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxLoc.ValueMember = "LocalitéID";
                comboBoxLoc.DisplayMember = "NomLocalité";
                comboBoxLoc.DataSource = objDs.Tables[0];
                comboBoxLoc.SelectedValue = 30000;
            }
        }
        int InscrisBureau = 0;
        private void comboBoxBV_SelectedIndexChanged(object sender, EventArgs e)
        {
            //this.Fillheure();
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SET CONCAT_NULL_YIELDS_NULL OFF;SELECT * from View_reg_aff where BureauID=@BureauID";
            cmd.Parameters.AddWithValue("@BureauID", Int32.Parse(comboBoxBV.SelectedValue.ToString()));
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            int total = objDs.Tables[0].Rows.Count;
            InscrisBureau = total;
            if (total > 0)
            {
               
                button1.Enabled = true;

            }
            else
            {
                button1.Enabled = false;
            }
        }

       

        private void comboBoxLoc_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Int32.Parse(comboBoxLoc.SelectedValue.ToString()) == 30)
            {
                int CommuneID = Convert.ToInt32(comboBoxLoc.SelectedValue.ToString());
                this.FillCV(CommuneID);
                //this.comboBoxAdresse.Enabled = false;
                //comboBoxArrondis.SelectedIndex = 0;
            }
            else
            {
                int CommuneID = Convert.ToInt32(comboBoxLoc.SelectedValue.ToString());
                this.FillCV(CommuneID);
                //this.comboBoxAdresse.Enabled = true;
            }
        }


        private void comboBoxCommune_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Int32.Parse(comboBoxCommune.SelectedValue.ToString()) == 30)
            {
                int CommuneID = Convert.ToInt32(comboBoxCommune.SelectedValue.ToString());
                this.FillArrondis(CommuneID);
                this.comboBoxArrondis.Enabled = false;
                //comboBoxArrondis.SelectedIndex = 0;
            }
            else
            {
                int CommuneID = Convert.ToInt32(comboBoxCommune.SelectedValue.ToString());
                this.FillArrondis(CommuneID);
                this.comboBoxArrondis.Enabled = true;
            }
        }
        private void comboBoxArrondis_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Int32.Parse(comboBoxArrondis.SelectedValue.ToString()) == 30)
            {
                int CommuneID = Convert.ToInt32(comboBoxArrondis.SelectedValue.ToString());
                this.FillCV(CommuneID);
                //this.comboBoxAdresse.Enabled = false;
                //comboBoxArrondis.SelectedIndex = 0;
            }
            else
            {
                int CommuneID = Convert.ToInt32(comboBoxArrondis.SelectedValue.ToString());
                this.FillCV(CommuneID);
                //this.comboBoxAdresse.Enabled = true;
            }
        }

        private void comboBoxRégion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Int32.Parse(comboBoxRégion.SelectedValue.ToString()) == 1)
            {
                int RégionID = Convert.ToInt32(comboBoxRégion.SelectedValue.ToString());
                FillCommune(RégionID);
                //comboBoxCommune.SelectedIndex = 0;
                labelCom.Text = "Commune";
                comboBoxLoc.Visible = false;
                //labelAddress.Visible = true;
                labelArrondis.Visible = true;
                //comboBoxAdresse.Visible = true;
                comboBoxArrondis.Visible = true;
                comboBoxCommune.Visible = true;
                //comboBoxAdresse.Enabled = true;
                comboBoxArrondis.Enabled = true;
                comboBoxCommune.Enabled = true;
            }
            else
            {
                if (Int32.Parse(comboBoxRégion.SelectedValue.ToString()) == 30)
                {
                    int RégionID = Convert.ToInt32(comboBoxRégion.SelectedValue.ToString());
                    FillCommune(RégionID);
                    //comboBoxCommune.SelectedIndex = 0;
                    labelCom.Text = "Commune";
                    comboBoxLoc.Visible = false;
                    //labelAddress.Visible = true;
                    labelArrondis.Visible = true;
                    //comboBoxAdresse.Visible = true;
                    comboBoxArrondis.Visible = true;
                    comboBoxCommune.Visible = true;
                    //comboBoxAdresse.Enabled = false;
                    comboBoxArrondis.Enabled = false;
                    comboBoxCommune.Enabled = false;
                }
                else
                {
                    this.Filllocalite(Int32.Parse(comboBoxRégion.SelectedValue.ToString()));
                    comboBoxLoc.SelectedIndex = 0;
                    comboBoxLoc.Visible = true;
                    labelCom.Text = "Localité";
                    comboBoxCommune.Visible = false;
                    labelCom.Visible = true;
                    //labelAddress.Visible = false;
                    labelArrondis.Visible = false;
                    //comboBoxAdresse.Visible = false;
                    comboBoxArrondis.Visible = false;
                }

            }
        }
        private void comboBoxCV_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.FillBureau(Int32.Parse(comboBoxCV.SelectedValue.ToString()));
        }

        private void button1_Click(object sender, EventArgs e)
        {




            if ((comboBoxBV.Text.Length != 0) && comboBoxElect.Text.Length != 0)
            {

              
                string sCon = conn.connectdb();
                SqlConnection con = new SqlConnection(sCon);
                string str = "SET CONCAT_NULL_YIELDS_NULL OFF; SELECT BureauID,id_particip, NomBureau  AS 'Bureau de vote',totalebureauvote AS 'Totale Bureau de vote',nombre_votantInscris AS 'Nombre de votant',nombre_votant_ord AS 'Votant (OR/OM)',heure FROM View_participant_bureau where  BureauID =" + Int32.Parse(comboBoxBV.SelectedValue.ToString()) + " and ElectionID=" + Int32.Parse(comboBoxElect.SelectedValue.ToString()) + "";
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(str, con);
                SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sqlAdapter);

                DataTable table = new DataTable();
                table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                sqlAdapter.Fill(table);
                dataGridView1.DataSource = table;
                if (dataGridView1.Rows.Count == 0)
                {


                    MessageBox.Show("Aucun nombre de votant d'un bureau donnée retrouvé pour ces critères");

                }
                else
                {

                   
                }

                this.dataGridView1.Columns["id_particip"].Visible = false;
                this.dataGridView1.Columns["BureauID"].Visible = false;


                con.Open();
           
              
            }
            else
            {
                MessageBox.Show("veuillez remplir un champ au minimum");
                this.Focus();

            }
        }
    }
}
