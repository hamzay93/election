﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class AjoutReunion : Form
    {
        Connection conn = new Connection();
        public AjoutReunion()
        {
            InitializeComponent();
        }
        private void FillCv()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT RégionID,NomRégion FROM Région";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBox1.ValueMember = "RégionID";
            comboBox1.DisplayMember = "NomRégion";
            comboBox1.DataSource = objDs.Tables[0];
        }
        private void FillCv2()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT TypeReunionID,ObjetTypeReunion FROM TypeReunion";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBox2.ValueMember = "TypeReunionID";
            comboBox2.DisplayMember = "ObjetTypeReunion";
            comboBox2.DataSource = objDs.Tables[0];
        }
        private void button5_Click(object sender, EventArgs e)
        {
            if (textBox5.Text.Trim().Length == 0 )
            {
                MessageBox.Show("veuillez saisir l'inttitulé");
                textBox5.Focus(); 
            }
            else
            {
                if (textBox6.Text.Trim().Length == 0)
                {
                    MessageBox.Show("veuillez saisir le Lieu");
                    textBox6.Focus();
                }
                else
                {

                    if (comboBox1.Text == "")
                    {
                        MessageBox.Show("Veuillez choisir dans quel région");
                    }
                    else
                    {

                        if (comboBox2.Text == "")
                        {
                            MessageBox.Show("Veuillez choisir sur quel type de reunion");
                        }
                        else
                        {

                            //Connection cn = new Connection();
                            string insertCmd = "INSERT INTO Reunion (ObjetReunion,Date_Heure,RégionID,TypeReunionID,Lieu,UserCreation,DateCreation,UserModification,DateModification) VALUES (@Objet,@Date_Heure,@RégionID,@TypeReunionID,@Lieu,@UserCreation,@DateCreation,@UserModification,@DateModification)";
                            SqlConnection dbConn;
                            dbConn = new SqlConnection(conn.connectdb());
                            dbConn.Open();
                            Login lg = new Login();
                            string dat = DateTime.Now.ToString("yyyy-MM-dd");
                            DateTime date4 = DateTime.Parse(dateTimePicker1.Value.ToString("yyyy-MM-dd HH:mm:ss"));
                            GetValue value = new GetValue();
                            //getAdress add = new getAdress();
                            SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                            if (comboBox1.Text != "")
                            {
                                int regionID = Convert.ToInt32(comboBox1.SelectedValue.ToString());
                                myCommand.Parameters.AddWithValue("@RégionID", regionID);
                            }
                            if (comboBox2.Text != "")
                            {
                                int TypereunionID = Convert.ToInt32(comboBox2.SelectedValue.ToString());
                                myCommand.Parameters.AddWithValue("@TypeReunionID", TypereunionID);
                            }
                            myCommand.Parameters.AddWithValue("@Objet", textBox5.Text);
                            myCommand.Parameters.AddWithValue("@Date_Heure", date4);
                            myCommand.Parameters.AddWithValue("@Lieu", textBox6.Text);
                            myCommand.Parameters.AddWithValue("@UserCreation", value.getAgentCréation());
                            myCommand.Parameters.AddWithValue("@DateCreation", dat);
                            myCommand.Parameters.AddWithValue("@UserModification", value.getAgentCréation());
                            myCommand.Parameters.AddWithValue("@DateModification", dat);
                            myCommand.ExecuteNonQuery();

                            MessageBox.Show("Ajoutée avec succès");
                            textBox5.Clear(); textBox6.Clear();
                        }
                    }
                }
            }
        }

        private void AjoutReunion_Load(object sender, EventArgs e)
        {
            this.FillCv();
            this.FillCv2();
        }
    }
}
