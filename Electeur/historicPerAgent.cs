﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class historicPerAgent : Form
    {
        public historicPerAgent()
        {
            InitializeComponent();
        }
        Connection conn = new Connection();
        string clause,str;
        private void historicPerAgent_Load(object sender, EventArgs e)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT NomAgent from Agent ";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            objDs.Tables[0].Rows.Add("-Tout les agents-");
            con.Close();
            comboBoxAgent.ValueMember = "NomAgent";
            comboBoxAgent.DisplayMember = "NomAgent";
            comboBoxAgent.DataSource = objDs.Tables[0];
            comboBoxAgent.SelectedValue = "-Tout les agents-";
            comboBox1.SelectedItem = "-Toutes Actions-";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.IsEmptyfield();            
            string sCon = conn.connectdb();
            if (String.IsNullOrEmpty(clause))
            {
                clause = "NomAgent like '%'";

            }
            
            
                SqlConnection con = new SqlConnection(sCon);
                string str = "SET CONCAT_NULL_YIELDS_NULL OFF; SELECT Nom1 +' '+Nom2+' '+Nom3 AS 'NOM ELECTEUR',NomMère1+' '+NomMère2 AS 'NOM DE LA MERE', DateAction as 'DATE ACTION', TypeAction as 'TYPE D ACTION', NomAgent as PAR  FROM hist where " + clause + " ORDER BY DateAction, NomAgent,ElecteurID";
                con.Open();
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(str, con);
                SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sqlAdapter);
                con.Close();
                DataTable table = new DataTable();
                table.Locale = System.Globalization.CultureInfo.CurrentCulture;
                sqlAdapter.Fill(table);
                dataGridView1.DataSource = table;
                int total = dataGridView1.RowCount;
                label14.Visible = true;
                label14.Text = total.ToString();
            
        }
        public void IsEmptyfield()
        {
            clause = null;
            foreach (Control X in this.panel1.Controls)
            {

                if (X is ComboBox)
                {
                    str = X.Text;
                    if (X.Tag.ToString() == "NomAgent")
                    {
                        if (X.Text.Trim().Length != 0)
                        {
                            if (string.IsNullOrEmpty(clause))
                            {
                                
                                if (!(str == "-Tout les agents-"))
                                {
                                    clause = clause + X.Tag + " like '%" + X.Text.Trim() + "%'";
                                }


                                //return clause;
                            }
                            else
                            {
                                if (!(str == "-Tout les agents-"))
                                {
                                    clause = clause + " And " + X.Tag + " like '%" + X.Text.Trim() + "%'";
                                }

                                //return clause;
                            }
                        }
                    }
                    else
                    {
                        if (X.Tag.ToString() == "TypeAction")
                        {
                            if (X.Text.Trim().Length != 0)
                            {
                                if (string.IsNullOrEmpty(clause))
                                {
                                    if (!(str == "-Toutes Actions-"))
                                    {
                                        clause = clause + X.Tag + " like '%" + X.Text.Trim() + "%'";
                                    }
                                    
                                    //return clause;
                                }
                                else
                                {
                                    if (!(str == "-Toutes Actions-"))
                                    {
                                        clause = clause + " And " + X.Tag + " Like '%" + X.Text.Trim() + "%'";
                                    }
                                    
                                    //return clause;
                                }
                            }
                        }
                    }

                }
                else
                {
                    if (X is DateTimePicker)
                    {
                        if (X.Name == "dateTimePicker1")
                        {
                            if (string.IsNullOrEmpty(clause))
                            {
                                if (dateTimePicker1.Checked)
                                {
                                    string dat = dateTimePicker1.Value.ToString("yyyy-MM-dd");
                                    DateTime date4 = DateTime.Parse(dat);
                                    clause = clause + "datediff(day, " + X.Tag + " ,'" + dat + "') = 0";
                                }

                            }
                            else
                            {
                                if (dateTimePicker1.Checked)
                                {
                                    string dat = dateTimePicker1.Value.ToString("yyyy-MM-dd");
                                    DateTime date4 = DateTime.Parse(dat);
                                    clause = clause + " AND " + "datediff(day, " + X.Tag + " ,'" + dat + "') = 0";
                                }

                            }
                        }
                    }
                }
            }

        }
    }
}
