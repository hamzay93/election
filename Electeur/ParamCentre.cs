﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class ParamCentre : Form
    {
        public ParamCentre()
        {
            InitializeComponent();
        }
        Connection conn = new Connection();
        private void ParamCentre_Load(object sender, EventArgs e)
        {
            this.FillCv();
        }
        private void FillCv()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT CentreID, CentreVote FROM CentreVote";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBoxCV.ValueMember = "CentreID";
            comboBoxCV.DisplayMember = "CentreVote";
            comboBoxCV.DataSource = objDs.Tables[0];
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Trim().Length == 0)
            {
                MessageBox.Show("veuillez saisir le Centre de Vote");
                textBox1.Focus();
            }
            else
            {
                //Connection cn = new Connection();
                string insertCmd = "INSERT INTO CentreVote (CentreVote,Staut,DateCréation,AgentCréation) VALUES (@CentreVote,@Staut,@DateCréation,@AgentCréation)";
                SqlConnection dbConn;
                dbConn = new SqlConnection(conn.connectdb());
                dbConn.Open();
                string dat = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                DateTime date4 = DateTime.Parse(dat);
                GetValue value = new GetValue();
                //getAdress add = new getAdress();
                SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                // Create parameters for the SqlCommand object
                // initialize with input-form field values
                //myCommand.Parameters.AddWithValue("@ElecteurID","");
                myCommand.Parameters.AddWithValue("@CentreVote", textBox1.Text);
                myCommand.Parameters.AddWithValue("@Staut", "Actif");
                myCommand.Parameters.AddWithValue("@DateCréation", dat);
                myCommand.Parameters.AddWithValue("@AgentCréation", value.getAgentSaisie());
                myCommand.Parameters.AddWithValue("@AgentModif", value.getAgentModif());
                myCommand.ExecuteNonQuery();
                
                MessageBox.Show("Ajoutée avec succès");
                textBox1.Clear();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox2.Text.Trim().Length == 0)
            {
                MessageBox.Show("veuillez saisir le bureau");
                textBox2.Focus();
            }
            else
            {
                //Connection cn = new Connection();
                string insertCmd = "INSERT INTO Bureau (Bureau,Statut,DateCréation,AgentCréation,CentreID) VALUES (@Bureau,@Statut,@DateCréation,@AgentCréation,@CentreID)";
                SqlConnection dbConn;
                dbConn = new SqlConnection(conn.connectdb());
                dbConn.Open();
                Login lg = new Login();
                string dat = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                DateTime date4 = DateTime.Parse(dat);
                GetValue value = new GetValue();
                //getAdress add = new getAdress();
                SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                // Create parameters for the SqlCommand object
                // initialize with input-form field values
                //myCommand.Parameters.AddWithValue("@ElecteurID","");
                myCommand.Parameters.AddWithValue("@NomBureau", textBox2.Text);
                myCommand.Parameters.AddWithValue("@Statut", "Actif");
                myCommand.Parameters.AddWithValue("@DateCréation", dat);
                myCommand.Parameters.AddWithValue("@AgentCréation", value.getAgentSaisie());
                int CentreID = Convert.ToInt32(comboBoxCV.SelectedValue.ToString());
                myCommand.Parameters.AddWithValue("@CentreID", CentreID);
                myCommand.ExecuteNonQuery();

                MessageBox.Show("Ajoutée avec succès");
                textBox2.Clear();

            }
        }
    }
}
