﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class Radiation : Form
    {
        public Radiation()
        {
            InitializeComponent();
        }
        Connection conn = new Connection();
        string clause, adresseID;
        public void IsEmptyfield()
        {

            foreach (Control X in this.panel1.Controls)
            {


                if (X is TextBox || X is ComboBox)
                {
                    if (X.Tag.ToString() == "AncienCNI" || X.Tag.ToString() == "NewCNI")
                    {
                        if (X.Text.Trim().Length != 0)
                        {

                            if (string.IsNullOrEmpty(clause))
                            {
                                clause = clause + " And " + X.Tag + " = '" + X.Text.Trim() + "'";
                                //return clause;
                            }
                            else
                            {
                                clause = clause + " And " + X.Tag + " = '" + X.Text.Trim() + "'";
                                //return clause;
                            }
                        }
                    }
                    else
                    {
                        if (X.Text.Trim().Length != 0)
                        {
                            if (string.IsNullOrEmpty(clause))
                            {
                                clause = clause + " And " + X.Tag + " Like '%" + X.Text.Trim() + "%'";
                                //return clause;
                            }
                            else
                            {
                                clause = clause + " And " + X.Tag + " Like '%" + X.Text.Trim() + "%'";
                                //return clause;
                            }
                        }
                    }

                }
                else
                {
                    if (X is DateTimePicker)
                    {
                        if (X.Name == "dateTimePicker1")
                        {
                            if (string.IsNullOrEmpty(clause))
                            {
                                if (dateTimePicker1.Checked)
                                {
                                    string dat = dateTimePicker1.Value.ToString("yyyy-MM-dd");
                                    DateTime date4 = DateTime.Parse(dat);
                                    clause = clause + " And " + X.Tag + " Like '%" + dat + "%'";
                                }

                            }
                            else
                            {
                                if (dateTimePicker1.Checked)
                                {
                                    string dat = dateTimePicker1.Value.ToString("yyyy-MM-dd");
                                    DateTime date4 = DateTime.Parse(dat);
                                    clause = clause + " AND " + X.Tag + " Like '%" + dat + "%'";
                                }

                            }
                        }
                    }
                }
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            this.IsEmptyfield();
            this.charger();
        }
        public void charger()
        {
            if (String.IsNullOrEmpty(clause))
            {
                MessageBox.Show("veuillez remplir un champs au minimmum");
                this.Focus();

            }
            else
            {
                string sCon = conn.connectdb();
                SqlConnection con = new SqlConnection(sCon);
                string str = "SET CONCAT_NULL_YIELDS_NULL OFF; SELECT ElecteurID, Nom1 +'  '+ Nom2 +'  '+Nom3 AS NOM,NomMère1+'  '+NomMère2+'  '+NomMère3 AS 'NOM DE LA MERE',DOB AS 'DATE DE NAISSANCE',POB AS LIEU,CodeElecteur AS 'CODE ELECTEUR',AncienCNI AS 'ANCIEN CNI',DateAncienCNI AS 'DATE DELIVRANCE',NewCNI AS 'NOUVEAU CNI',DateNewCNI AS 'DATE DELIVRANCE',Sexe AS 'SEXE' FROM Electeur where StatutID <= 4 " + clause + " and ElecteurID not in(select electeurID from Radiation where Statut='Actif')";
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(str, con);
                SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sqlAdapter);

                DataTable table = new DataTable();
                table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                sqlAdapter.Fill(table);
                dataGridViewrad.DataSource = table;
                if (dataGridViewrad.Rows.Count == 0)
                {
                    button2.Enabled = false;

                }
                else
                {
                    button2.Enabled = true;
                }
                this.dataGridViewrad.Columns[0].Visible = false;

                con.Open();
                clause = "";
            }
        }

        private void dataGridViewValid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int counter;
            string clause1 = null;
            string nom = null;
            adresseID = null;

            if (dataGridViewrad.Rows.Count > -1)
            {
                // Iterate through all the rows and sum up the appropriate columns. 
                for (counter = 0; counter < (dataGridViewrad.Rows.Count);
                    counter++)
                {
                    //dataGridViewrad.Rows.
                    if (dataGridViewrad.Rows[counter].Selected)
                    {
                        if (dataGridViewrad.Rows[counter].Cells["ElecteurID"].Value
                        != null)
                        {
                            if (dataGridViewrad.Rows[counter].
                                Cells["ElecteurID"].Value.ToString().Length != 0)
                            {
                                if (string.IsNullOrEmpty(clause1))
                                {
                                    clause1 = clause1 + int.Parse(dataGridViewrad.Rows[counter].
                                        Cells["ElecteurID"].Value.ToString());
                                }
                                else
                                {
                                    clause1 = clause1 + "," + int.Parse(dataGridViewrad.Rows[counter].
                                    Cells["ElecteurID"].Value.ToString());

                                }

                            }
                        }


                        nom = dataGridViewrad.Rows[counter].Cells["NOM"].Value.ToString();



                    }

                }
                string btnName = nom;
                string test = clause1;
                //string add1 = adresseID;
                //Form1 fr = new Form1();
                Motif mod = new Motif(test, btnName);
                mod.MdiParent = this.ParentForm;
                mod.Show();
                mod.TopMost = true;
                mod.Activate();
                this.Hide();
            }
        }
    }
}
