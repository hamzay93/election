﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class saisie_horaire : Form
    {
        Connection conn = new Connection();
        public saisie_horaire()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBoxheure.Text.Length != 0)
            {
                string sCon = conn.connectdb();
                string insertCmd = "INSERT INTO horaire (heure) VALUES (@heure)";
                SqlConnection dbConn;
                dbConn = new SqlConnection(sCon);
                dbConn.Open();
                Login lg = new Login();

                //getAdress add = new getAdress();
                SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);

                myCommand.Parameters.AddWithValue("@heure", textBoxheure.Text);



                myCommand.ExecuteNonQuery();
                MessageBox.Show("Enregistrement effectué avec succès");
                RecursiveClearTextBoxes(this.Controls);
            }
            else
            {
                MessageBox.Show("Veuillé saisir l'heure d'enregistrement");
            }
        }
        private void RecursiveClearTextBoxes(Control.ControlCollection cc)
        {
            foreach (Control ctrl in cc)
            {
                TextBox tb = ctrl as TextBox;
                if (tb != null)
                    tb.Clear();
                else
                    RecursiveClearTextBoxes(ctrl.Controls);
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            if (textBoxheure.Text.Length != 0)
            {
                string sCon = conn.connectdb();
                string insertCmd = "INSERT INTO horaire (heure) VALUES (@heure)";
                SqlConnection dbConn;
                dbConn = new SqlConnection(sCon);
                dbConn.Open();
                Login lg = new Login();

                //getAdress add = new getAdress();
                SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);

                myCommand.Parameters.AddWithValue("@heure", textBoxheure.Text);



                myCommand.ExecuteNonQuery();
                MessageBox.Show("Enregistrement effectué avec succès");
                RecursiveClearTextBoxes(this.Controls);
                this.Close();
            }
            else
            {
                MessageBox.Show("Veuillé saisir l'heure d'enregistrement");
            }
        }

        private void saisie_horaire_Load(object sender, EventArgs e)
        {

        }
    }
}
