﻿namespace Electeur
{
    partial class RechercheListe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.button2 = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.TimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.TimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.radiobuttonAll = new System.Windows.Forms.RadioButton();
            this.radioButtonValidation = new System.Windows.Forms.RadioButton();
            this.radioButtonApprobation = new System.Windows.Forms.RadioButton();
            this.Saisie = new System.Windows.Forms.RadioButton();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.comboBox10 = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.comboBoxAdresse = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.comboBoxAgent = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.comboBoxArrondis = new System.Windows.Forms.ComboBox();
            this.comboBoxCommune = new System.Windows.Forms.ComboBox();
            this.comboBoxRégion = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.agentBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.electeurDataSet1 = new Electeur.ElecteurDataSet1();
            this.adresseBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.arrondissementBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.communeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.régionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.electeurDataSet = new Electeur.ElecteurDataSet();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.agentTableAdapter = new Electeur.ElecteurDataSet1TableAdapters.AgentTableAdapter();
            this.régionTableAdapter = new Electeur.ElecteurDataSetTableAdapters.RégionTableAdapter();
            this.communeTableAdapter = new Electeur.ElecteurDataSet1TableAdapters.CommuneTableAdapter();
            this.arrondissementTableAdapter = new Electeur.ElecteurDataSet1TableAdapters.ArrondissementTableAdapter();
            this.localitéBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.localitéTableAdapter = new Electeur.ElecteurDataSet1TableAdapters.LocalitéTableAdapter();
            this.adresseTableAdapter = new Electeur.ElecteurDataSet1TableAdapters.AdresseTableAdapter();
            this.electeurBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.electeurTableAdapter = new Electeur.ElecteurDataSet1TableAdapters.ElecteurTableAdapter();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.agentBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.electeurDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.adresseBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arrondissementBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.communeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.régionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.electeurDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.localitéBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.electeurBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.checkBox2);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.checkBox1);
            this.panel1.Controls.Add(this.TimePicker2);
            this.panel1.Controls.Add(this.TimePicker1);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.radiobuttonAll);
            this.panel1.Controls.Add(this.radioButtonValidation);
            this.panel1.Controls.Add(this.radioButtonApprobation);
            this.panel1.Controls.Add(this.Saisie);
            this.panel1.Controls.Add(this.dateTimePicker2);
            this.panel1.Controls.Add(this.dateTimePicker1);
            this.panel1.Controls.Add(this.comboBox10);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.comboBoxAdresse);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.comboBoxAgent);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.comboBoxArrondis);
            this.panel1.Controls.Add(this.comboBoxCommune);
            this.panel1.Controls.Add(this.comboBoxRégion);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(64, 78);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(863, 286);
            this.panel1.TabIndex = 0;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.checkBox2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.checkBox2.Location = new System.Drawing.Point(38, 225);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(113, 23);
            this.checkBox2.TabIndex = 42;
            this.checkBox2.Text = "Non Affecté";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold);
            this.button2.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.button2.Location = new System.Drawing.Point(466, 248);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(176, 33);
            this.button2.TabIndex = 41;
            this.button2.Text = "Effacer";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.checkBox1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.checkBox1.Location = new System.Drawing.Point(38, 187);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(154, 23);
            this.checkBox1.TabIndex = 40;
            this.checkBox1.Text = "Exclure les radiés";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // TimePicker2
            // 
            this.TimePicker2.Checked = false;
            this.TimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.TimePicker2.Location = new System.Drawing.Point(724, 63);
            this.TimePicker2.Name = "TimePicker2";
            this.TimePicker2.ShowCheckBox = true;
            this.TimePicker2.ShowUpDown = true;
            this.TimePicker2.Size = new System.Drawing.Size(92, 20);
            this.TimePicker2.TabIndex = 10;
            // 
            // TimePicker1
            // 
            this.TimePicker1.Checked = false;
            this.TimePicker1.CustomFormat = "dd/MM/yyyy";
            this.TimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.TimePicker1.Location = new System.Drawing.Point(529, 62);
            this.TimePicker1.Name = "TimePicker1";
            this.TimePicker1.ShowCheckBox = true;
            this.TimePicker1.ShowUpDown = true;
            this.TimePicker1.Size = new System.Drawing.Size(113, 20);
            this.TimePicker1.TabIndex = 9;
            this.TimePicker1.Value = new System.DateTime(2015, 6, 11, 0, 0, 0, 0);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label8.Location = new System.Drawing.Point(690, 65);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(20, 19);
            this.label8.TabIndex = 37;
            this.label8.Text = "A";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label9.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label9.Location = new System.Drawing.Point(436, 65);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(74, 19);
            this.label9.TabIndex = 36;
            this.label9.Text = "Heure de";
            // 
            // radiobuttonAll
            // 
            this.radiobuttonAll.AutoSize = true;
            this.radiobuttonAll.Checked = true;
            this.radiobuttonAll.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.radiobuttonAll.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.radiobuttonAll.Location = new System.Drawing.Point(38, 146);
            this.radiobuttonAll.Name = "radiobuttonAll";
            this.radiobuttonAll.Size = new System.Drawing.Size(63, 23);
            this.radiobuttonAll.TabIndex = 35;
            this.radiobuttonAll.TabStop = true;
            this.radiobuttonAll.Text = "Tout";
            this.radiobuttonAll.UseVisualStyleBackColor = true;
            // 
            // radioButtonValidation
            // 
            this.radioButtonValidation.AutoSize = true;
            this.radioButtonValidation.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.radioButtonValidation.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.radioButtonValidation.Location = new System.Drawing.Point(38, 105);
            this.radioButtonValidation.Name = "radioButtonValidation";
            this.radioButtonValidation.Size = new System.Drawing.Size(99, 23);
            this.radioButtonValidation.TabIndex = 34;
            this.radioButtonValidation.Text = "Validation";
            this.radioButtonValidation.UseVisualStyleBackColor = true;
            // 
            // radioButtonApprobation
            // 
            this.radioButtonApprobation.AutoSize = true;
            this.radioButtonApprobation.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.radioButtonApprobation.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.radioButtonApprobation.Location = new System.Drawing.Point(38, 64);
            this.radioButtonApprobation.Name = "radioButtonApprobation";
            this.radioButtonApprobation.Size = new System.Drawing.Size(117, 23);
            this.radioButtonApprobation.TabIndex = 33;
            this.radioButtonApprobation.Text = "Approbation";
            this.radioButtonApprobation.UseVisualStyleBackColor = true;
            // 
            // Saisie
            // 
            this.Saisie.AutoSize = true;
            this.Saisie.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.Saisie.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Saisie.Location = new System.Drawing.Point(38, 23);
            this.Saisie.Name = "Saisie";
            this.Saisie.Size = new System.Drawing.Size(67, 23);
            this.Saisie.TabIndex = 32;
            this.Saisie.Text = "Saisie";
            this.Saisie.UseVisualStyleBackColor = true;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Checked = false;
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker2.Location = new System.Drawing.Point(724, 20);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.ShowCheckBox = true;
            this.dateTimePicker2.Size = new System.Drawing.Size(92, 20);
            this.dateTimePicker2.TabIndex = 7;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Checked = false;
            this.dateTimePicker1.CustomFormat = "dd/MM/yyyy";
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(529, 19);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.ShowCheckBox = true;
            this.dateTimePicker1.Size = new System.Drawing.Size(113, 20);
            this.dateTimePicker1.TabIndex = 6;
            this.dateTimePicker1.Value = new System.DateTime(2015, 6, 11, 0, 0, 0, 0);
            // 
            // comboBox10
            // 
            this.comboBox10.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox10.FormattingEnabled = true;
            this.comboBox10.Items.AddRange(new object[] {
            "",
            "FEMININ",
            "MASCULIN"});
            this.comboBox10.Location = new System.Drawing.Point(268, 65);
            this.comboBox10.Name = "comboBox10";
            this.comboBox10.Size = new System.Drawing.Size(144, 21);
            this.comboBox10.TabIndex = 8;
            this.comboBox10.Tag = "Sexe";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label13.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label13.Location = new System.Drawing.Point(175, 66);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(43, 19);
            this.label13.TabIndex = 28;
            this.label13.Text = "Sexe";
            // 
            // comboBoxAdresse
            // 
            this.comboBoxAdresse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxAdresse.FormattingEnabled = true;
            this.comboBoxAdresse.Location = new System.Drawing.Point(529, 148);
            this.comboBoxAdresse.Name = "comboBoxAdresse";
            this.comboBoxAdresse.Size = new System.Drawing.Size(113, 21);
            this.comboBoxAdresse.TabIndex = 14;
            this.comboBoxAdresse.Tag = "AdresseID";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label11.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label11.Location = new System.Drawing.Point(436, 148);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(66, 19);
            this.label11.TabIndex = 24;
            this.label11.Text = "Adresse";
            // 
            // comboBoxAgent
            // 
            this.comboBoxAgent.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxAgent.FormattingEnabled = true;
            this.comboBoxAgent.Items.AddRange(new object[] {
            "         "});
            this.comboBoxAgent.Location = new System.Drawing.Point(268, 22);
            this.comboBoxAgent.Name = "comboBoxAgent";
            this.comboBoxAgent.Size = new System.Drawing.Size(144, 21);
            this.comboBoxAgent.TabIndex = 5;
            this.comboBoxAgent.Tag = "AgentSaisie";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.button1.Location = new System.Drawing.Point(247, 248);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(176, 33);
            this.button1.TabIndex = 22;
            this.button1.Text = "Rechercher";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // comboBoxArrondis
            // 
            this.comboBoxArrondis.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxArrondis.FormattingEnabled = true;
            this.comboBoxArrondis.Location = new System.Drawing.Point(529, 107);
            this.comboBoxArrondis.Name = "comboBoxArrondis";
            this.comboBoxArrondis.Size = new System.Drawing.Size(113, 21);
            this.comboBoxArrondis.TabIndex = 12;
            this.comboBoxArrondis.Tag = "NomArrondis";
            this.comboBoxArrondis.SelectedIndexChanged += new System.EventHandler(this.comboBoxArrondis_SelectedIndexChanged);
            // 
            // comboBoxCommune
            // 
            this.comboBoxCommune.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCommune.FormattingEnabled = true;
            this.comboBoxCommune.Location = new System.Drawing.Point(268, 148);
            this.comboBoxCommune.Name = "comboBoxCommune";
            this.comboBoxCommune.Size = new System.Drawing.Size(144, 21);
            this.comboBoxCommune.TabIndex = 13;
            this.comboBoxCommune.Tag = "NomCommune";
            this.comboBoxCommune.SelectedIndexChanged += new System.EventHandler(this.comboBoxCommune_SelectedIndexChanged);
            // 
            // comboBoxRégion
            // 
            this.comboBoxRégion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxRégion.Enabled = false;
            this.comboBoxRégion.FormattingEnabled = true;
            this.comboBoxRégion.Location = new System.Drawing.Point(268, 107);
            this.comboBoxRégion.Name = "comboBoxRégion";
            this.comboBoxRégion.Size = new System.Drawing.Size(144, 21);
            this.comboBoxRégion.TabIndex = 11;
            this.comboBoxRégion.Tag = "NomRégion";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label7.Location = new System.Drawing.Point(436, 107);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 19);
            this.label7.TabIndex = 5;
            this.label7.Text = "Arrondis";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label6.Location = new System.Drawing.Point(175, 148);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 19);
            this.label6.TabIndex = 4;
            this.label6.Text = "Commune";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Location = new System.Drawing.Point(175, 107);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 19);
            this.label5.TabIndex = 3;
            this.label5.Text = "Région";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Location = new System.Drawing.Point(690, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 19);
            this.label4.TabIndex = 2;
            this.label4.Text = "Au";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(436, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 19);
            this.label3.TabIndex = 1;
            this.label3.Text = "Date du";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(175, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 19);
            this.label2.TabIndex = 0;
            this.label2.Text = "Agent";
            // 
            // agentBindingSource
            // 
            this.agentBindingSource.DataMember = "Agent";
            this.agentBindingSource.DataSource = this.electeurDataSet1;
            // 
            // electeurDataSet1
            // 
            this.electeurDataSet1.DataSetName = "ElecteurDataSet1";
            this.electeurDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // adresseBindingSource
            // 
            this.adresseBindingSource.DataMember = "Adresse";
            this.adresseBindingSource.DataSource = this.electeurDataSet1;
            // 
            // arrondissementBindingSource
            // 
            this.arrondissementBindingSource.DataMember = "Arrondissement";
            this.arrondissementBindingSource.DataSource = this.electeurDataSet1;
            // 
            // communeBindingSource
            // 
            this.communeBindingSource.DataMember = "Commune";
            this.communeBindingSource.DataSource = this.electeurDataSet1;
            // 
            // régionBindingSource
            // 
            this.régionBindingSource.DataMember = "Région";
            this.régionBindingSource.DataSource = this.electeurDataSet;
            // 
            // electeurDataSet
            // 
            this.electeurDataSet.DataSetName = "ElecteurDataSet";
            this.electeurDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Berlin Sans FB Demi", 14.25F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(99, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(194, 23);
            this.label1.TabIndex = 2;
            this.label1.Text = "Critères de recherche";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(60, 389);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(863, 287);
            this.dataGridView1.TabIndex = 3;
            // 
            // agentTableAdapter
            // 
            this.agentTableAdapter.ClearBeforeFill = true;
            // 
            // régionTableAdapter
            // 
            this.régionTableAdapter.ClearBeforeFill = true;
            // 
            // communeTableAdapter
            // 
            this.communeTableAdapter.ClearBeforeFill = true;
            // 
            // arrondissementTableAdapter
            // 
            this.arrondissementTableAdapter.ClearBeforeFill = true;
            // 
            // localitéBindingSource
            // 
            this.localitéBindingSource.DataMember = "Localité";
            this.localitéBindingSource.DataSource = this.electeurDataSet1;
            // 
            // localitéTableAdapter
            // 
            this.localitéTableAdapter.ClearBeforeFill = true;
            // 
            // adresseTableAdapter
            // 
            this.adresseTableAdapter.ClearBeforeFill = true;
            // 
            // electeurBindingSource
            // 
            this.electeurBindingSource.DataMember = "Electeur";
            this.electeurBindingSource.DataSource = this.electeurDataSet1;
            // 
            // electeurTableAdapter
            // 
            this.electeurTableAdapter.ClearBeforeFill = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label12.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label12.Location = new System.Drawing.Point(738, 367);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(74, 19);
            this.label12.TabIndex = 4;
            this.label12.Text = "TOTAL :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Bell MT", 16F, System.Drawing.FontStyle.Bold);
            this.label14.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label14.Location = new System.Drawing.Point(802, 363);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(84, 26);
            this.label14.TabIndex = 5;
            this.label14.Text = "label14";
            this.label14.Visible = false;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label25.Location = new System.Drawing.Point(99, 9);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(232, 23);
            this.label25.TabIndex = 18;
            this.label25.Text = "RECHERCHE STATISTIQUE";
            // 
            // RechercheListe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSeaGreen;
            this.ClientSize = new System.Drawing.Size(1008, 730);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Name = "RechercheListe";
            this.Text = "RerchercheListe";
            this.Load += new System.EventHandler(this.RechercheListe_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.agentBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.electeurDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.adresseBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arrondissementBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.communeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.régionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.electeurDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.localitéBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.electeurBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxArrondis;
        private System.Windows.Forms.ComboBox comboBoxCommune;
        private System.Windows.Forms.ComboBox comboBoxRégion;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ComboBox comboBoxAgent;
        private System.Windows.Forms.ComboBox comboBoxAdresse;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.ComboBox comboBox10;
        private System.Windows.Forms.Label label13;
        private ElecteurDataSet1 electeurDataSet1;
        private System.Windows.Forms.BindingSource agentBindingSource;
        private ElecteurDataSet1TableAdapters.AgentTableAdapter agentTableAdapter;
        private ElecteurDataSet electeurDataSet;
        private System.Windows.Forms.BindingSource régionBindingSource;
        private ElecteurDataSetTableAdapters.RégionTableAdapter régionTableAdapter;
        private System.Windows.Forms.BindingSource communeBindingSource;
        private ElecteurDataSet1TableAdapters.CommuneTableAdapter communeTableAdapter;
        private System.Windows.Forms.BindingSource arrondissementBindingSource;
        private ElecteurDataSet1TableAdapters.ArrondissementTableAdapter arrondissementTableAdapter;
        private System.Windows.Forms.BindingSource localitéBindingSource;
        private ElecteurDataSet1TableAdapters.LocalitéTableAdapter localitéTableAdapter;
        private System.Windows.Forms.BindingSource adresseBindingSource;
        private ElecteurDataSet1TableAdapters.AdresseTableAdapter adresseTableAdapter;
        private System.Windows.Forms.BindingSource electeurBindingSource;
        private ElecteurDataSet1TableAdapters.ElecteurTableAdapter electeurTableAdapter;
        private System.Windows.Forms.RadioButton radioButtonValidation;
        private System.Windows.Forms.RadioButton radioButtonApprobation;
        private System.Windows.Forms.RadioButton Saisie;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.RadioButton radiobuttonAll;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.DateTimePicker TimePicker2;
        private System.Windows.Forms.DateTimePicker TimePicker1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.CheckBox checkBox2;
    }
}