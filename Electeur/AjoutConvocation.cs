﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class AjoutConvocation : Form
    {
        public AjoutConvocation()
        {
            InitializeComponent();
        }
       
        private void button10_Click(object sender, EventArgs e)
        {
            if (textBox16.Text.Trim().Length == 0  )
            {
                MessageBox.Show("veuillez saisir l'inttitulé");
                textBox16.Focus();
            }
            else
            {
                if (comboBox12.Text == "")
                {
                    MessageBox.Show("veuillez choisir un election");
                    
                }
                else
                {
                    //Connection cn = new Connection();
                    string insertCmd = "INSERT INTO Convocation (ObjetConvocation,UserCreation,ElectionID,Date_Heure,UserModification,DateCreation,DateModification) VALUES (@Objet,@UserCreation,@ElectionID,@Date_Heure,@UserModification,@DateCreation,@DateModification)";
                    SqlConnection dbConn;
                    dbConn = new SqlConnection(conn.connectdb());
                    dbConn.Open();
                    Login lg = new Login();
                    string dat = DateTime.Now.ToString("yyyy-MM-dd");
                    DateTime date4 = DateTime.Parse(dateTimePicker5.Value.ToString("yyyy-MM-dd HH:mm:ss"));
                    GetValue value = new GetValue();
                    //getAdress add = new getAdress();
                    SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                    if (comboBox12.Text != "")
                    {
                        int electionID = Convert.ToInt32(comboBox12.SelectedValue.ToString());
                        myCommand.Parameters.AddWithValue("@ElectionID", electionID);
                    }
                    myCommand.Parameters.AddWithValue("@Objet", textBox16.Text);

                    myCommand.Parameters.AddWithValue("@Date_Heure", date4);
                    myCommand.Parameters.AddWithValue("@UserCreation", value.getAgentCréation());
                    myCommand.Parameters.AddWithValue("@DateCreation", dat);
                    myCommand.Parameters.AddWithValue("@UserModification", value.getAgentCréation());
                    myCommand.Parameters.AddWithValue("@DateModification", dat);
                    myCommand.ExecuteNonQuery();

                    MessageBox.Show("Ajoutée avec succès");
                    textBox16.Clear();
                }

            }
        }
        Connection conn = new Connection();
        private void AjoutConvocation_Load(object sender, EventArgs e)
        {
            this.FillCv();
        }
        private void FillCv()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT ElectionID,ObjetElection FROM Election";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBox12.ValueMember = "ElectionID";
            comboBox12.DisplayMember = "ObjetElection";
            comboBox12.DataSource = objDs.Tables[0];
        }
    }
}
