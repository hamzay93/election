﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class ModifReunion : Form
    {
        string clause;

        Connection conn = new Connection();
        GetValue get = new GetValue();

        public ModifReunion()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Trim().Length == 0 || textBox2.Text.Trim().Length == 0)
            {
                MessageBox.Show("veuillez saisir l'inttitulé ou le Lieu");
                this.Focus();
            }
            else
            {
                if (comboBox4.Text.Length ==0)
                {
                    MessageBox.Show("Veuillez choisir dans quel région");
                }
                else
                {

                    if (comboBox2.Text.Length ==0)
                    {
                        MessageBox.Show("Veuillez choisir sur quel type de reunion");
                    }
                    else
                    {

                        //Connection cn = new Connection();
                        string insertCmd = "update Reunion set ObjetReunion=@Objet,Date_Heure=@Date_Heure,RégionID=@RégionID,TypeReunionID=@TypeReunionID,Lieu=@Lieu,UserModification=@UserModification,DateModification=@DateModification where ReunionID=" + clause3 + "";
                        SqlConnection dbConn;
                        dbConn = new SqlConnection(conn.connectdb());
                        dbConn.Open();
                        Login lg = new Login();
                        string dat = DateTime.Now.ToString("yyyy-MM-dd");
                        DateTime date4 = DateTime.Parse(dateTimePicker1.Value.ToString("yyyy-MM-dd HH:mm:ss"));
                        GetValue value = new GetValue();
                        //getAdress add = new getAdress();
                        SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                       
                            int regionID = Convert.ToInt32(comboBox4.SelectedValue.ToString());
                            myCommand.Parameters.AddWithValue("@RégionID", regionID);
                        
                            int TypereunionID = Convert.ToInt32(comboBox2.SelectedValue.ToString());
                            myCommand.Parameters.AddWithValue("@TypeReunionID", TypereunionID);
                        
                        myCommand.Parameters.AddWithValue("@Objet", textBox2.Text);
                        myCommand.Parameters.AddWithValue("@Date_Heure", date4);
                        myCommand.Parameters.AddWithValue("@Lieu", textBox1.Text);
                        myCommand.Parameters.AddWithValue("@UserCreation", value.getAgentCréation());
                        myCommand.Parameters.AddWithValue("@DateCreation", dat);
                        myCommand.Parameters.AddWithValue("@UserModification", value.getAgentCréation());
                        myCommand.Parameters.AddWithValue("@DateModification", dat);
                        myCommand.ExecuteNonQuery();

                        MessageBox.Show("Modification avec succès");
                        textBox2.Clear(); textBox1.Clear();
                    }
                }

            }
        }

        private void button5_Click(object sender, EventArgs e)
        {

            this.IsEmptyfield();
            if (String.IsNullOrEmpty(clause))
            {
                MessageBox.Show("veuillez remplir un champ au minimum");
                this.Focus();

            }
            else
            {
                string sCon = conn.connectdb();
                SqlConnection con = new SqlConnection(sCon);
                string str = "SET CONCAT_NULL_YIELDS_NULL OFF; SELECT ReunionID, ObjetReunion AS 'Intitulé du reunion',ObjetTypeReunion,NomRégion FROM View_reunion_region_typereunion where " + clause + "";
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(str, con);
                SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sqlAdapter);

                DataTable table = new DataTable();
                table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                con.Open();
                sqlAdapter.Fill(table);
                con.Close();
                dataGridView1.DataSource = table;
                if (dataGridView1.Rows.Count == 0)
                {

                    panel1.Visible = false;
                    label1.Visible = false;
                    MessageBox.Show("Aucun Reunion retrouvé pour ces critères");

                }
                else
                {
                    panel1.Visible = true;
                    label1.Visible = true;
                }

                this.dataGridView1.Columns["ReunionID"].Visible = false;



                clause = "";
            }
        }
        public void IsEmptyfield()
        {
            clause = null;
            foreach (Control X in this.panel5.Controls)
            {

                if (X is TextBox || X is ComboBox)
                {

                    if (X.Text.Trim().Length != 0)
                    {
                        if (string.IsNullOrEmpty(clause))
                        {
                            clause = clause + X.Tag + " Like '%" + X.Text.Trim() + "%'";
                            //return clause;
                        }
                        else
                        {
                            clause = clause + " And " + X.Tag + " Like '%" + X.Text.Trim() + "%'";
                            //return clause;
                        }
                    }


                }
                else
                {
                    if (X is DateTimePicker)
                    {
                        
                        if (X.Name == "dateTimePicker1")
                        {
                            if (string.IsNullOrEmpty(clause))
                            {
                                if (dateTimePicker1.Checked)
                                {
                                    string dat = dateTimePicker1.Value.ToString("yyyy-MM-dd HH:mm:ss");
                                    DateTime date4 = DateTime.Parse(dat);
                                    clause = clause + X.Tag + " Like '%" + dat + "%'";
                                }

                            }
                            else
                            {
                                if (dateTimePicker1.Checked)
                                {
                                    string dat = dateTimePicker1.Value.ToString("yyyy-MM-dd HH:mm:ss");
                                    DateTime date4 = DateTime.Parse(dat);
                                    clause = clause + " AND " + X.Tag + " Like '%" + dat + "%'";
                                }

                            }
                        }
                    }
                }


            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            ClearClass1 clr = new ClearClass1();
            clr.RecursiveClearTextBoxes(this.panel5.Controls);
        }
        private void FillRégion()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT RégionID, NomRégion FROM Région";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBox1.ValueMember = "RégionID";
            comboBox1.DisplayMember = "NomRégion";
            comboBox1.DataSource = objDs.Tables[0];
        }
        private void FillRégion2()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT RégionID, NomRégion FROM Région";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBox4.ValueMember = "RégionID";
            comboBox4.DisplayMember = "NomRégion";
            comboBox4.DataSource = objDs.Tables[0];
        }
        private void Filltypereunion()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT TypeReunionID,ObjetTypeReunion FROM TypeReunion";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBox2.ValueMember = "TypeReunionID";
            comboBox2.DisplayMember = "ObjetTypeReunion";
            comboBox2.DataSource = objDs.Tables[0];
        }
        private void Filltypereunion2()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT TypeReunionID,ObjetTypeReunion FROM TypeReunion";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBox3.ValueMember = "TypeReunionID";
            comboBox3.DisplayMember = "ObjetTypeReunion";
            comboBox3.DataSource = objDs.Tables[0];
        }
        private void ModifReunion_Load(object sender, EventArgs e)
        {
            FillRégion();
            FillRégion2();
            Filltypereunion();
            Filltypereunion2();
        }
        string clause3;
        private void filldetails()
        {

            this.selection();
            if (String.IsNullOrEmpty(clause3))
            {
                panel1.Visible = false;
                label1.Visible = false;

            }
            else
            {
                panel1.Visible = true;
                label1.Visible = true;
                string sCon = conn.connectdb();
                SqlConnection con = new SqlConnection(sCon);
                string str = "SELECT * FROM View_reunion_region_typereunion where ReunionID= " + clause3;
                SqlCommand cmd = new SqlCommand(str, con);
                con.Open();

                using (SqlDataReader read = cmd.ExecuteReader())

                    while (read.Read())
                    {
                        textBox1.Text = (read["Lieu"].ToString());
                        textBox2.Text = (read["ObjetReunion"].ToString());

                        int colIndex3 = (read.GetOrdinal("Date_Heure"));
                        if (!read.IsDBNull(colIndex3))
                        {
                            dateTimePicker2.Value = (read.GetDateTime(colIndex3));
                        }


                        comboBox4.SelectedValue = (read["RégionID"].ToString());
                        comboBox3.SelectedValue = (read["TypeReunionID"].ToString());



                    }
            }

        }
        private void selection()
        {
            int counter3;
            clause3 = null;

            // Iterate through all the rows and sum up the appropriate columns. 
            for (counter3 = 0; counter3 < (dataGridView1.Rows.Count);
                counter3++)
            {
                //dataGridView1.Rows.
                if (dataGridView1.Rows[counter3].Selected)
                {
                    if (dataGridView1.Rows[counter3].Cells["ReunionID"].Value
                    != null)
                    {
                        if (dataGridView1.Rows[counter3].
                            Cells["ReunionID"].Value.ToString().Length != 0)
                        {
                            if (string.IsNullOrEmpty(clause3))
                            {
                                clause3 = clause3 + int.Parse(dataGridView1.Rows[counter3].
                                    Cells["ReunionID"].Value.ToString());
                            }
                            else
                            {
                                clause3 = clause3 + "," + int.Parse(dataGridView1.Rows[counter3].
                                Cells["ReunionID"].Value.ToString());

                            }

                        }
                    }


                }

            }
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            this.filldetails();
        }

    }
}
