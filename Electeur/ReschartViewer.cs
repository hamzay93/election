﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class ReschartViewer : Form
    {
        public ReschartViewer()
        {
            InitializeComponent();
        }
        Connection conn = new Connection();
        int idreg;
        string elec = null;
        private void ReschartViewer_Load(object sender, EventArgs e)
        {
            this.FillRégion();
            this.FillElection();
        }
        private void FillElection()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT ElectionID, ObjetElection +' - '+ObjetTourElection as NomElection from View_Elect";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBoxElec.ValueMember = "ElectionID";
            comboBoxElec.DisplayMember = "NomElection";
            comboBoxElec.DataSource = objDs.Tables[0];
        }
        private void FillRégion()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT RégionID, NomRégion FROM Région";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            objDs.Tables[0].Rows.Add(30, "");
            con.Close();
            comboBoxRégion.ValueMember = "RégionID";
            comboBoxRégion.DisplayMember = "NomRégion";
            comboBoxRégion.DataSource = objDs.Tables[0];
            comboBoxRégion.SelectedValue = 30;
        }
        //int RégionID;
        /*private void comboBoxRégion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxRégion.SelectedValue.ToString() != "")
            {
                int RégionID = Convert.ToInt32(comboBoxRégion.SelectedValue.ToString());
                FillCommune(RégionID);
                //comboBoxCommune.SelectedIndex = 0;
            }
        }*/
        private void FillCommune(int RégionID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT CommuneID, NomCommune FROM Commune WHERE RégionID =@RégionID";
            if (RégionID == 30)
            {
                int reg = 1;
                cmd.Parameters.AddWithValue("@RégionID", reg);


            }
            else
            {
                cmd.Parameters.AddWithValue("@RégionID", RégionID);
            }

            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            objDs.Tables[0].Rows.Add(30, "");
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxCommune.ValueMember = "CommuneID";
                comboBoxCommune.DisplayMember = "NomCommune";
                comboBoxCommune.DataSource = objDs.Tables[0];
                comboBoxCommune.SelectedValue = 30;
            }
        }
        private void comboBoxCommune_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Int32.Parse(comboBoxCommune.SelectedValue.ToString()) == 30)
            {
                int CommuneID = Convert.ToInt32(comboBoxCommune.SelectedValue.ToString());
                this.FillArrondis(CommuneID);
                this.comboBoxArrondis.Enabled = false;
                //comboBoxArrondis.SelectedIndex = 0;
            }
            else
            {
                int CommuneID = Convert.ToInt32(comboBoxCommune.SelectedValue.ToString());
                this.FillArrondis(CommuneID);
                this.comboBoxArrondis.Enabled = true;
            }
        }
        private void comboBoxArrondis_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Int32.Parse(comboBoxArrondis.SelectedValue.ToString()) == 30)
            {
                int CommuneID = Convert.ToInt32(comboBoxArrondis.SelectedValue.ToString());
                this.FillCV(CommuneID);
                //this.comboBoxAdresse.Enabled = false;
                //comboBoxArrondis.SelectedIndex = 0;
            }
            else
            {
                int CommuneID = Convert.ToInt32(comboBoxArrondis.SelectedValue.ToString());
                this.FillCV(CommuneID);
                //this.comboBoxAdresse.Enabled = true;
            }
        }
        private void FillCV(int ArrondisID)
        {
            string sCon = conn.connectdb(); ;
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            if (comboBoxArrondis.Visible)
            {
                cmd.CommandText = "SELECT CentreID, ( CONVERT(Nvarchar(50), CodeCentre)) + ' - '+CentreVote as CentreVote FROM View_CV where Actif = 1 and ArrondisID =@ArrondisID ORDER BY CodeCentre";
                if (ArrondisID == 30)
                {
                    int com = 1;
                    cmd.Parameters.AddWithValue("@ArrondisID", com);


                }
                else
                {
                    cmd.Parameters.AddWithValue("@ArrondisID", ArrondisID);
                }
            }
            else
            {
                cmd.CommandText = "SELECT CentreID, ( CONVERT(Nvarchar(50), CodeCentre)) + ' - '+CentreVote as CentreVote FROM View_CV where Actif = 1 and LocalitéID =@ArrondisID ORDER BY CodeCentre";
                if (ArrondisID == 30000)
                {
                    int com = 1;
                    cmd.Parameters.AddWithValue("@ArrondisID", com);


                }
                else
                {
                    cmd.Parameters.AddWithValue("@ArrondisID", ArrondisID);
                }
            }
            
            //cmd.Parameters.AddWithValue("@ArrondisID", ArrondisID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            objDs.Tables[0].Rows.Add(3000, "");
            con.Close();
            comboBoxCV.ValueMember = "CentreID";
            comboBoxCV.DisplayMember = "CentreVote";
            comboBoxCV.DataSource = objDs.Tables[0];
            comboBoxCV.SelectedValue = 3000;
        }
        private void FillBureau(int CentreID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT BureauID, ( CONVERT(Nvarchar(50), CodeBureau)) + ' - '+NomBureau as NomBureau FROM View_LV WHERE CentreID =@CentreID ORDER BY CodeCentre,OrdreCentre";
            cmd.Parameters.AddWithValue("@CentreID", CentreID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            objDs.Tables[0].Rows.Add(3000, "");
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxBV.ValueMember = "BureauID";
                comboBoxBV.DisplayMember = "NomBureau";
                comboBoxBV.DataSource = objDs.Tables[0];
                comboBoxBV.SelectedValue = 3000;
            }
        }
        private void FillArrondis(int CommuneID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT ArrondisID, NomArrondis FROM Arrondissement WHERE CommuneID =@CommuneID";
            if (CommuneID == 30)
            {
                int com = 1;
                cmd.Parameters.AddWithValue("@CommuneID", com);


            }
            else
            {
                cmd.Parameters.AddWithValue("@CommuneID", CommuneID);
            }
            //cmd.Parameters.AddWithValue("@CommuneID", CommuneID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            objDs.Tables[0].Rows.Add(30, "");
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxArrondis.ValueMember = "ArrondisID";
                comboBoxArrondis.DisplayMember = "NomArrondis";
                comboBoxArrondis.DataSource = objDs.Tables[0];
                comboBoxArrondis.SelectedValue = 30;


            }
        }
        private void Filllocalite(int _reg)
        {
            int reg = _reg;
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT LocalitéID, NomLocalité FROM Localité WHERE RégionID = " + reg;
            //cmd.Parameters.AddWithValue("@RégionID", RégionID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            objDs.Tables[0].Rows.Add(30000, "");
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxLoc.ValueMember = "LocalitéID";
                comboBoxLoc.DisplayMember = "NomLocalité";
                comboBoxLoc.DataSource = objDs.Tables[0];
                comboBoxLoc.SelectedValue = 30000;
            }
        }
        private void comboBoxRégion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Int32.Parse(comboBoxRégion.SelectedValue.ToString()) == 1)
            {
                int RégionID = Convert.ToInt32(comboBoxRégion.SelectedValue.ToString());
                FillCommune(RégionID);
                //comboBoxCommune.SelectedIndex = 0;
                labelArrondis.Text = "Arrondis";
                comboBoxLoc.Visible = false;
                //labelAddress.Visible = true;
                labelCom.Visible = true;
                //comboBoxAdresse.Visible = true;
                comboBoxArrondis.Visible = true;
                comboBoxCommune.Visible = true;
                //comboBoxAdresse.Enabled = true;
                comboBoxArrondis.Enabled = true;
                comboBoxCommune.Enabled = true;
            }
            else
            {
                if (Int32.Parse(comboBoxRégion.SelectedValue.ToString()) == 30)
                {
                    int RégionID = Convert.ToInt32(comboBoxRégion.SelectedValue.ToString());
                    FillCommune(RégionID);
                    //comboBoxCommune.SelectedIndex = 0;
                    labelArrondis.Text = "Arrondis";
                    comboBoxLoc.Visible = false;
                    //labelAddress.Visible = true;
                    labelCom.Visible = true;
                    //comboBoxAdresse.Visible = true;
                    comboBoxArrondis.Visible = true;
                    comboBoxCommune.Visible = true;
                    //comboBoxAdresse.Enabled = false;
                    comboBoxArrondis.Enabled = false;
                    comboBoxCommune.Enabled = false;
                }
                else
                {
                    this.Filllocalite(Int32.Parse(comboBoxRégion.SelectedValue.ToString()));
                    comboBoxLoc.SelectedIndex = 0;
                    comboBoxLoc.Visible = true;
                    labelArrondis.Text = "Localité";
                    comboBoxCommune.Visible = false;
                    labelCom.Visible = false;
                    //labelAddress.Visible = false;
                    labelArrondis.Visible = true;
                    //comboBoxAdresse.Visible = false;
                    comboBoxArrondis.Visible = false;
                }

            }
        }

        private void comboBoxLoc_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Int32.Parse(comboBoxLoc.SelectedValue.ToString()) == 30)
            {
                int CommuneID = Convert.ToInt32(comboBoxLoc.SelectedValue.ToString());
                this.FillCV(CommuneID);
                //this.comboBoxAdresse.Enabled = false;
                //comboBoxArrondis.SelectedIndex = 0;
            }
            else
            {
                int CommuneID = Convert.ToInt32(comboBoxLoc.SelectedValue.ToString());
                this.FillCV(CommuneID);
                //this.comboBoxAdresse.Enabled = true;
            }
        }
        string clause = null;
        public void IsEmptyfield()
        {
            clause = null;
            foreach (Control X in this.Controls)
            {
                if (X is ComboBox)
                {
                    if (X.Text.Trim().Length != 0 && X.Visible)
                    {
                        if (string.IsNullOrEmpty(clause))
                        {
                            if (X is ComboBox)
                            {
                                if (X.Name == "comboBoxCV")
                                {
                                    clause = X.Tag + " = '" + this.comboBoxCV.SelectedValue + "'";
                                }
                                if (X.Name == "comboBoxArrondis")
                                {
                                    clause = clause + X.Tag + " = '" + this.comboBoxArrondis.SelectedValue + "'";
                                }
                                if (X.Name == "comboBoxCommune")
                                {
                                    clause = clause + X.Tag + " = '" + this.comboBoxCommune.SelectedValue + "'";
                                }
                                if (X.Name == "comboBoxRégion")
                                {
                                    clause = clause + X.Tag + " = '" + this.comboBoxRégion.SelectedValue +"'";
                                    idreg = Int32.Parse(comboBoxRégion.SelectedValue.ToString());
                                }

                                if (X.Name == "comboBoxLoc")
                                {
                                    clause = clause + X.Tag + " = '" + this.comboBoxLoc.SelectedValue + "'";
                                }
                                
                            }

                        }
                        else
                        {
                            if (X is ComboBox)
                            {
                                if (X.Name == "comboBoxCV")
                                {
                                    clause = clause + "And " + X.Tag + " = '" + this.comboBoxCV.SelectedValue +"'";
                                }
                                if (X.Name == "comboBoxArrondis")
                                {
                                    clause = clause + "And " + X.Tag + " = '" + this.comboBoxArrondis.SelectedValue + "'";
                                }
                                if (X.Name == "comboBoxCommune")
                                {
                                    clause = clause + "And " + X.Tag + " = '" + this.comboBoxCommune.SelectedValue + "'";
                                }
                                if (X.Name == "comboBoxRégion")
                                {
                                    clause = clause + "And " + X.Tag + " = '" + this.comboBoxRégion.SelectedValue +"'";
                                    idreg = Int32.Parse(comboBoxRégion.SelectedValue.ToString());
                                }
                                if (X.Name == "comboBoxLoc")
                                {
                                    clause = clause + "And " + X.Tag + " =' " + this.comboBoxLoc.SelectedValue + "'";
                                }
                            }


                        }

                    }

                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(clause))
            {
                elec = " IdElection= '"+comboBoxElec.SelectedValue.ToString()+" ' ";
            }
            else
            {
                elec = " and IdElection= '" + comboBoxElec.SelectedValue.ToString() + " ' ";
            }
            
            this.IsEmptyfield();
            SqlConnection con = new SqlConnection(conn.connectdb());
            string req = null;
            //objRpt = new CrystalReport_Reunion();
            if (comboBoxEtat.SelectedItem.ToString() == "Editions des résultats détaillés")
            {
                req = "select * from View_res_detail where " + clause + elec ;
                resultat_details rpt = new resultat_details();
                con.Open();
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(req, con);
                SqlCommandBuilder commandBuilder1 = new SqlCommandBuilder(sqlAdapter);
                resdetails res = new resdetails();
                con.Close();
                DataTable dt = new DataTable();
                sqlAdapter.Fill(dt);
                rpt.SetDataSource(dt);
                crystalReportViewer1.ReportSource = rpt;
                crystalReportViewer1.Refresh();


            }
            if (comboBoxEtat.SelectedItem.ToString() == "Editions des résultats détaillés en pourcentage")
            {
                req = "select * from View_res_detail where "+clause + elec;
                resultat_detail_pourcentage rpt = new resultat_detail_pourcentage();
                con.Open();
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(req, con);
                SqlCommandBuilder commandBuilder1 = new SqlCommandBuilder(sqlAdapter);
                resdetails res = new resdetails();
                con.Close();
                DataTable dt = new DataTable();
                sqlAdapter.Fill(dt);
                rpt.SetDataSource(dt);
                crystalReportViewer1.ReportSource = rpt;
                crystalReportViewer1.Refresh();


            }
            if (comboBoxEtat.SelectedItem.ToString() == "Editions des résultats en camemberg")
            {
                req = "select * from View_res_candidat where "+clause + elec;
                res_camemberg rpt = new res_camemberg();
                con.Open();
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(req, con);
                SqlCommandBuilder commandBuilder1 = new SqlCommandBuilder(sqlAdapter);
                voix_dts res = new voix_dts();
                con.Close();
                DataTable dt = new DataTable();
                sqlAdapter.Fill(dt);
                rpt.SetDataSource(dt);
                crystalReportViewer1.ReportSource = rpt;
                crystalReportViewer1.Refresh();


            }
            string SQLlist = "SET CONCAT_NULL_YIELDS_NULL OFF;  SELECT     dbo.ResCandidat.IdRescand, dbo.ResCandidat.IdCandidat, dbo.ResCandidat.IdBur, dbo.ResCandidat.Voix, dbo.ResBureau.IdElection, dbo.ResBureau.IdReg, dbo.ResBureau.NbInscrit, " +
                     " dbo.ResBureau.NbOMOR, dbo.ResBureau.TotalInscrits, dbo.ResBureau.NbVotans, dbo.ResBureau.NbNuls, dbo.ResBureau.SuffragesExprimes, dbo.ResBureau.Voted, dbo.ResBureau.SavingTime, " +
                      "dbo.Candidat.Nom1 + ' ' + dbo.Candidat.Nom2 + ' ' + dbo.Candidat.Nom3 + ' (' + dbo.Candidat.Partie + ' )' AS Nom, dbo.Candidat.Statut " +
                        "FROM         dbo.ResBureau INNER JOIN " +
                         "dbo.ResCandidat ON dbo.ResBureau.IdBur = dbo.ResCandidat.IdBur INNER JOIN " +
                          "dbo.Candidat ON dbo.ResCandidat.IdCandidat = dbo.Candidat.IdCandidat Order by Nom";

            req = "select * from ResCandidat";

        }
    }
}
