﻿namespace Electeur
{
    partial class Valider
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dateTimePickerDOB = new System.Windows.Forms.DateTimePicker();
            this.label18 = new System.Windows.Forms.Label();
            this.textBoxCodeElecteur = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.textBoxAdresseCompl = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.comboBoxAdresse = new System.Windows.Forms.ComboBox();
            this.comboBoxCommune = new System.Windows.Forms.ComboBox();
            this.comboBoxArrondis = new System.Windows.Forms.ComboBox();
            this.comboBoxRégion = new System.Windows.Forms.ComboBox();
            this.textBoxPhone = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.comboBoxSexe = new System.Windows.Forms.ComboBox();
            this.textBoxNewCNI = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBoxNomMère3 = new System.Windows.Forms.TextBox();
            this.textBoxNomMère2 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBoxNomMère1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBoxAncCNI = new System.Windows.Forms.TextBox();
            this.textBoxPOB = new System.Windows.Forms.TextBox();
            this.textBoxNom3 = new System.Windows.Forms.TextBox();
            this.textBoxNom2 = new System.Windows.Forms.TextBox();
            this.textBoxNom1 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dateTimePicker4 = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.dataGridViewValid = new System.Windows.Forms.DataGridView();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewValid)).BeginInit();
            this.SuspendLayout();
            // 
            // dateTimePickerDOB
            // 
            this.dateTimePickerDOB.Enabled = false;
            this.dateTimePickerDOB.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerDOB.Location = new System.Drawing.Point(146, 47);
            this.dateTimePickerDOB.MaxDate = new System.DateTime(2000, 7, 4, 0, 0, 0, 0);
            this.dateTimePickerDOB.Name = "dateTimePickerDOB";
            this.dateTimePickerDOB.Size = new System.Drawing.Size(118, 20);
            this.dateTimePickerDOB.TabIndex = 11;
            this.dateTimePickerDOB.Value = new System.DateTime(2000, 7, 4, 0, 0, 0, 0);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Bell MT", 9F, System.Drawing.FontStyle.Bold);
            this.label18.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label18.Location = new System.Drawing.Point(254, 170);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(39, 14);
            this.label18.TabIndex = 22;
            this.label18.Text = "Sexe :";
            // 
            // textBoxCodeElecteur
            // 
            this.textBoxCodeElecteur.Enabled = false;
            this.textBoxCodeElecteur.Location = new System.Drawing.Point(146, 166);
            this.textBoxCodeElecteur.Name = "textBoxCodeElecteur";
            this.textBoxCodeElecteur.Size = new System.Drawing.Size(99, 20);
            this.textBoxCodeElecteur.TabIndex = 20;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Bell MT", 9F, System.Drawing.FontStyle.Bold);
            this.label20.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label20.Location = new System.Drawing.Point(17, 170);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(91, 14);
            this.label20.TabIndex = 23;
            this.label20.Text = "Code Electeur :";
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.textBoxAdresseCompl);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.comboBoxAdresse);
            this.panel2.Controls.Add(this.comboBoxCommune);
            this.panel2.Controls.Add(this.comboBoxArrondis);
            this.panel2.Controls.Add(this.comboBoxRégion);
            this.panel2.Controls.Add(this.textBoxPhone);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Location = new System.Drawing.Point(468, 459);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(467, 189);
            this.panel2.TabIndex = 27;
            // 
            // textBoxAdresseCompl
            // 
            this.textBoxAdresseCompl.Enabled = false;
            this.textBoxAdresseCompl.Location = new System.Drawing.Point(257, 131);
            this.textBoxAdresseCompl.Multiline = true;
            this.textBoxAdresseCompl.Name = "textBoxAdresseCompl";
            this.textBoxAdresseCompl.Size = new System.Drawing.Size(189, 37);
            this.textBoxAdresseCompl.TabIndex = 11;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Bell MT", 9F, System.Drawing.FontStyle.Bold);
            this.label17.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label17.Location = new System.Drawing.Point(260, 114);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(150, 14);
            this.label17.TabIndex = 10;
            this.label17.Text = "Adresse Complémentaire :";
            // 
            // comboBoxAdresse
            // 
            this.comboBoxAdresse.DisplayMember = "NomAdresse";
            this.comboBoxAdresse.Enabled = false;
            this.comboBoxAdresse.FormattingEnabled = true;
            this.comboBoxAdresse.Location = new System.Drawing.Point(332, 83);
            this.comboBoxAdresse.Name = "comboBoxAdresse";
            this.comboBoxAdresse.Size = new System.Drawing.Size(114, 21);
            this.comboBoxAdresse.TabIndex = 27;
            this.comboBoxAdresse.ValueMember = "NomAdresse";
            // 
            // comboBoxCommune
            // 
            this.comboBoxCommune.Enabled = false;
            this.comboBoxCommune.FormattingEnabled = true;
            this.comboBoxCommune.Location = new System.Drawing.Point(332, 52);
            this.comboBoxCommune.Name = "comboBoxCommune";
            this.comboBoxCommune.Size = new System.Drawing.Size(114, 21);
            this.comboBoxCommune.TabIndex = 25;
            this.comboBoxCommune.SelectedIndexChanged += new System.EventHandler(this.comboBoxCommune_SelectedIndexChanged);
            // 
            // comboBoxArrondis
            // 
            this.comboBoxArrondis.Enabled = false;
            this.comboBoxArrondis.FormattingEnabled = true;
            this.comboBoxArrondis.Location = new System.Drawing.Point(131, 79);
            this.comboBoxArrondis.Name = "comboBoxArrondis";
            this.comboBoxArrondis.Size = new System.Drawing.Size(114, 21);
            this.comboBoxArrondis.TabIndex = 26;
            this.comboBoxArrondis.SelectedIndexChanged += new System.EventHandler(this.comboBoxArrondis_SelectedIndexChanged);
            // 
            // comboBoxRégion
            // 
            this.comboBoxRégion.Enabled = false;
            this.comboBoxRégion.FormattingEnabled = true;
            this.comboBoxRégion.Location = new System.Drawing.Point(131, 52);
            this.comboBoxRégion.Name = "comboBoxRégion";
            this.comboBoxRégion.Size = new System.Drawing.Size(114, 21);
            this.comboBoxRégion.TabIndex = 24;
            this.comboBoxRégion.SelectedIndexChanged += new System.EventHandler(this.comboBoxRégion_SelectedIndexChanged);
            // 
            // textBoxPhone
            // 
            this.textBoxPhone.Enabled = false;
            this.textBoxPhone.Location = new System.Drawing.Point(131, 20);
            this.textBoxPhone.Name = "textBoxPhone";
            this.textBoxPhone.Size = new System.Drawing.Size(114, 20);
            this.textBoxPhone.TabIndex = 22;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Bell MT", 9F, System.Drawing.FontStyle.Bold);
            this.label15.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label15.Location = new System.Drawing.Point(260, 87);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(56, 14);
            this.label15.TabIndex = 4;
            this.label15.Text = "Adresse :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Bell MT", 9F, System.Drawing.FontStyle.Bold);
            this.label14.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label14.Location = new System.Drawing.Point(20, 80);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(104, 14);
            this.label14.TabIndex = 3;
            this.label14.Text = "Arrondissement : ";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Bell MT", 9F, System.Drawing.FontStyle.Bold);
            this.label13.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label13.Location = new System.Drawing.Point(260, 53);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(68, 14);
            this.label13.TabIndex = 2;
            this.label13.Text = "Commune :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Bell MT", 9F, System.Drawing.FontStyle.Bold);
            this.label12.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label12.Location = new System.Drawing.Point(23, 50);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(64, 14);
            this.label12.TabIndex = 1;
            this.label12.Text = "Région     :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Bell MT", 9F, System.Drawing.FontStyle.Bold);
            this.label11.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label11.Location = new System.Drawing.Point(20, 24);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(69, 14);
            this.label11.TabIndex = 0;
            this.label11.Text = "Téléphone:";
            // 
            // comboBoxSexe
            // 
            this.comboBoxSexe.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSexe.Enabled = false;
            this.comboBoxSexe.FormattingEnabled = true;
            this.comboBoxSexe.Items.AddRange(new object[] {
            "FEMININ",
            "MASCULIN"});
            this.comboBoxSexe.Location = new System.Drawing.Point(352, 166);
            this.comboBoxSexe.Name = "comboBoxSexe";
            this.comboBoxSexe.Size = new System.Drawing.Size(89, 21);
            this.comboBoxSexe.TabIndex = 21;
            // 
            // textBoxNewCNI
            // 
            this.textBoxNewCNI.Enabled = false;
            this.textBoxNewCNI.Location = new System.Drawing.Point(146, 140);
            this.textBoxNewCNI.Name = "textBoxNewCNI";
            this.textBoxNewCNI.Size = new System.Drawing.Size(99, 20);
            this.textBoxNewCNI.TabIndex = 18;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(164, 56);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(187, 20);
            this.textBox4.TabIndex = 8;
            this.textBox4.Tag = "NomMère1";
            this.textBox4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxNom1_KeyPress);
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(601, 21);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(187, 20);
            this.textBox3.TabIndex = 7;
            this.textBox3.Tag = "Nom3";
            this.textBox3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxNom1_KeyPress);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(381, 21);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(187, 20);
            this.textBox2.TabIndex = 6;
            this.textBox2.Tag = "Nom2";
            this.textBox2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxNom1_KeyPress);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(164, 21);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(187, 20);
            this.textBox1.TabIndex = 5;
            this.textBox1.Tag = "Nom1";
            this.textBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxNom1_KeyPress);
            // 
            // textBoxNomMère3
            // 
            this.textBoxNomMère3.Enabled = false;
            this.textBoxNomMère3.Location = new System.Drawing.Point(352, 80);
            this.textBoxNomMère3.Name = "textBoxNomMère3";
            this.textBoxNomMère3.Size = new System.Drawing.Size(89, 20);
            this.textBoxNomMère3.TabIndex = 15;
            // 
            // textBoxNomMère2
            // 
            this.textBoxNomMère2.Enabled = false;
            this.textBoxNomMère2.Location = new System.Drawing.Point(252, 80);
            this.textBoxNomMère2.Name = "textBoxNomMère2";
            this.textBoxNomMère2.Size = new System.Drawing.Size(87, 20);
            this.textBoxNomMère2.TabIndex = 14;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(381, 56);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(187, 20);
            this.textBox5.TabIndex = 9;
            this.textBox5.Tag = "NomMère2";
            this.textBox5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxNom1_KeyPress);
            // 
            // textBoxNomMère1
            // 
            this.textBoxNomMère1.Enabled = false;
            this.textBoxNomMère1.Location = new System.Drawing.Point(147, 80);
            this.textBoxNomMère1.Name = "textBoxNomMère1";
            this.textBoxNomMère1.Size = new System.Drawing.Size(98, 20);
            this.textBoxNomMère1.TabIndex = 13;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.button1.Location = new System.Drawing.Point(335, 132);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(176, 27);
            this.button1.TabIndex = 14;
            this.button1.Text = "Rechercher";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(667, 88);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(121, 20);
            this.textBox9.TabIndex = 13;
            this.textBox9.Tag = "NewCNI";
            this.textBox9.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxAncCNI_KeyPress);
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(433, 92);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(112, 20);
            this.textBox7.TabIndex = 12;
            this.textBox7.Tag = "AncienCNI";
            this.textBox7.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxAncCNI_KeyPress);
            // 
            // textBoxAncCNI
            // 
            this.textBoxAncCNI.Enabled = false;
            this.textBoxAncCNI.Location = new System.Drawing.Point(147, 109);
            this.textBoxAncCNI.Name = "textBoxAncCNI";
            this.textBoxAncCNI.Size = new System.Drawing.Size(98, 20);
            this.textBoxAncCNI.TabIndex = 16;
            // 
            // textBoxPOB
            // 
            this.textBoxPOB.Enabled = false;
            this.textBoxPOB.Location = new System.Drawing.Point(352, 50);
            this.textBoxPOB.Name = "textBoxPOB";
            this.textBoxPOB.Size = new System.Drawing.Size(89, 20);
            this.textBoxPOB.TabIndex = 12;
            // 
            // textBoxNom3
            // 
            this.textBoxNom3.Enabled = false;
            this.textBoxNom3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNom3.Location = new System.Drawing.Point(352, 24);
            this.textBoxNom3.Name = "textBoxNom3";
            this.textBoxNom3.Size = new System.Drawing.Size(89, 20);
            this.textBoxNom3.TabIndex = 10;
            // 
            // textBoxNom2
            // 
            this.textBoxNom2.Enabled = false;
            this.textBoxNom2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNom2.Location = new System.Drawing.Point(250, 22);
            this.textBoxNom2.Name = "textBoxNom2";
            this.textBoxNom2.Size = new System.Drawing.Size(89, 20);
            this.textBoxNom2.TabIndex = 9;
            // 
            // textBoxNom1
            // 
            this.textBoxNom1.Enabled = false;
            this.textBoxNom1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNom1.Location = new System.Drawing.Point(147, 22);
            this.textBoxNom1.Name = "textBoxNom1";
            this.textBoxNom1.Size = new System.Drawing.Size(98, 20);
            this.textBoxNom1.TabIndex = 8;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Bell MT", 9F, System.Drawing.FontStyle.Bold);
            this.label21.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label21.Location = new System.Drawing.Point(251, 143);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(97, 14);
            this.label21.TabIndex = 7;
            this.label21.Text = "Date Nouv CNI :";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Bell MT", 9F, System.Drawing.FontStyle.Bold);
            this.label22.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label22.Location = new System.Drawing.Point(17, 143);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(131, 14);
            this.label22.TabIndex = 6;
            this.label22.Text = "Numéro Nouvelle CNI ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.4F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(86, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(220, 25);
            this.label1.TabIndex = 22;
            this.label1.Text = "Critères de recherche";
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Bold);
            this.button3.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.button3.Location = new System.Drawing.Point(103, 671);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(264, 33);
            this.button3.TabIndex = 25;
            this.button3.Text = "Valider";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(601, 56);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(187, 20);
            this.textBox6.TabIndex = 10;
            this.textBox6.Tag = "NomMère3";
            this.textBox6.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxNom1_KeyPress);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Bell MT", 9F, System.Drawing.FontStyle.Bold);
            this.label23.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label23.Location = new System.Drawing.Point(251, 111);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(90, 14);
            this.label23.TabIndex = 5;
            this.label23.Text = "Date Anc CNI :";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Bell MT", 9F, System.Drawing.FontStyle.Bold);
            this.label24.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label24.Location = new System.Drawing.Point(17, 111);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(131, 14);
            this.label24.TabIndex = 4;
            this.label24.Text = "Numéro Ancienne CNI";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Bell MT", 9F, System.Drawing.FontStyle.Bold);
            this.label27.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label27.Location = new System.Drawing.Point(17, 80);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(98, 14);
            this.label27.TabIndex = 3;
            this.label27.Text = "Nom de la mère :";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Bell MT", 9F, System.Drawing.FontStyle.Bold);
            this.label28.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label28.Location = new System.Drawing.Point(267, 52);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(41, 14);
            this.label28.TabIndex = 2;
            this.label28.Text = "Lieu  :";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Bell MT", 9F, System.Drawing.FontStyle.Bold);
            this.label30.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label30.Location = new System.Drawing.Point(17, 27);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(99, 14);
            this.label30.TabIndex = 0;
            this.label30.Text = "Nom                      ";
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.textBox10);
            this.panel3.Controls.Add(this.textBox8);
            this.panel3.Controls.Add(this.dateTimePickerDOB);
            this.panel3.Controls.Add(this.comboBoxSexe);
            this.panel3.Controls.Add(this.label18);
            this.panel3.Controls.Add(this.textBoxCodeElecteur);
            this.panel3.Controls.Add(this.label20);
            this.panel3.Controls.Add(this.textBoxNewCNI);
            this.panel3.Controls.Add(this.textBoxNomMère3);
            this.panel3.Controls.Add(this.textBoxNomMère2);
            this.panel3.Controls.Add(this.textBoxNomMère1);
            this.panel3.Controls.Add(this.textBoxAncCNI);
            this.panel3.Controls.Add(this.textBoxPOB);
            this.panel3.Controls.Add(this.textBoxNom3);
            this.panel3.Controls.Add(this.textBoxNom2);
            this.panel3.Controls.Add(this.textBoxNom1);
            this.panel3.Controls.Add(this.label21);
            this.panel3.Controls.Add(this.label22);
            this.panel3.Controls.Add(this.label23);
            this.panel3.Controls.Add(this.label24);
            this.panel3.Controls.Add(this.label27);
            this.panel3.Controls.Add(this.label28);
            this.panel3.Controls.Add(this.label29);
            this.panel3.Controls.Add(this.label30);
            this.panel3.Location = new System.Drawing.Point(468, 253);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(467, 200);
            this.panel3.TabIndex = 26;
            // 
            // textBox10
            // 
            this.textBox10.Enabled = false;
            this.textBox10.Location = new System.Drawing.Point(352, 139);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(89, 20);
            this.textBox10.TabIndex = 25;
            // 
            // textBox8
            // 
            this.textBox8.Enabled = false;
            this.textBox8.Location = new System.Drawing.Point(352, 109);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(89, 20);
            this.textBox8.TabIndex = 24;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Bell MT", 9F, System.Drawing.FontStyle.Bold);
            this.label29.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label29.Location = new System.Drawing.Point(17, 52);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(108, 14);
            this.label29.TabIndex = 1;
            this.label29.Text = "Date de naissance ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Bell MT", 10F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label6.Location = new System.Drawing.Point(551, 95);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(114, 17);
            this.label6.TabIndex = 4;
            this.label6.Text = "N° Nouvelle CNI";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Bell MT", 10F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Location = new System.Drawing.Point(314, 95);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(117, 17);
            this.label5.TabIndex = 3;
            this.label5.Text = "N° Ancienne CNI";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Bell MT", 10F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Location = new System.Drawing.Point(26, 95);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(124, 17);
            this.label4.TabIndex = 2;
            this.label4.Text = "Date de Naissance";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Bell MT", 10F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(26, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(108, 17);
            this.label3.TabIndex = 1;
            this.label3.Text = "Nom de la mère";
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Bold);
            this.button2.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.button2.Location = new System.Drawing.Point(637, 671);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(264, 33);
            this.button2.TabIndex = 24;
            this.button2.Text = "Modifier et Valider";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.dateTimePicker4);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.textBox9);
            this.panel1.Controls.Add(this.textBox7);
            this.panel1.Controls.Add(this.textBox6);
            this.panel1.Controls.Add(this.textBox5);
            this.panel1.Controls.Add(this.textBox4);
            this.panel1.Controls.Add(this.textBox3);
            this.panel1.Controls.Add(this.textBox2);
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(73, 83);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(862, 164);
            this.panel1.TabIndex = 21;
            // 
            // dateTimePicker4
            // 
            this.dateTimePicker4.Checked = false;
            this.dateTimePicker4.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker4.Location = new System.Drawing.Point(173, 95);
            this.dateTimePicker4.Name = "dateTimePicker4";
            this.dateTimePicker4.ShowCheckBox = true;
            this.dateTimePicker4.Size = new System.Drawing.Size(135, 20);
            this.dateTimePicker4.TabIndex = 18;
            this.dateTimePicker4.Tag = "DOB";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Bell MT", 10F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(26, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "Nom";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label25.Location = new System.Drawing.Point(87, 9);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(261, 23);
            this.label25.TabIndex = 28;
            this.label25.Text = "VALIDATION D\'UN ELECTEUR";
            // 
            // dataGridViewValid
            // 
            this.dataGridViewValid.AllowUserToAddRows = false;
            this.dataGridViewValid.AllowUserToDeleteRows = false;
            this.dataGridViewValid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewValid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewValid.Location = new System.Drawing.Point(73, 254);
            this.dataGridViewValid.Name = "dataGridViewValid";
            this.dataGridViewValid.ReadOnly = true;
            this.dataGridViewValid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewValid.Size = new System.Drawing.Size(389, 394);
            this.dataGridViewValid.TabIndex = 29;
            this.dataGridViewValid.SelectionChanged += new System.EventHandler(this.dataGridViewValid_SelectionChanged);
            // 
            // Valider
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.LightSeaGreen;
            this.ClientSize = new System.Drawing.Size(1008, 742);
            this.Controls.Add(this.dataGridViewValid);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.panel1);
            this.Name = "Valider";
            this.Text = "Valider";
            this.Load += new System.EventHandler(this.Valider_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewValid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dateTimePickerDOB;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBoxCodeElecteur;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox textBoxAdresseCompl;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox comboBoxAdresse;
        private System.Windows.Forms.ComboBox comboBoxCommune;
        private System.Windows.Forms.ComboBox comboBoxArrondis;
        private System.Windows.Forms.ComboBox comboBoxRégion;
        private System.Windows.Forms.TextBox textBoxPhone;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox comboBoxSexe;
        private System.Windows.Forms.TextBox textBoxNewCNI;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBoxNomMère3;
        private System.Windows.Forms.TextBox textBoxNomMère2;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBoxNomMère1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBoxAncCNI;
        private System.Windows.Forms.TextBox textBoxPOB;
        private System.Windows.Forms.TextBox textBoxNom3;
        private System.Windows.Forms.TextBox textBoxNom2;
        private System.Windows.Forms.TextBox textBoxNom1;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DateTimePicker dateTimePicker4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.DataGridView dataGridViewValid;

    }
}