﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class Ajout_PartiPolitique : Form
    {
        public Ajout_PartiPolitique()
        {
            InitializeComponent();
        }

        Connection conn = new Connection();
        private void Ajout_PartiPolitique_Load(object sender, EventArgs e)
        {
            this.FillElection();
         
        }
        private void FillElection()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT ElectionID,ObjetElection from Election where Etat=1";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBoxElec.ValueMember = "ElectionID";
            comboBoxElec.DisplayMember = "ObjetElection";
            comboBoxElec.DataSource = objDs.Tables[0];

        }
        //private void FillParti()
        //{
        //    string sCon = conn.connectdb();
        //    SqlConnection con = new SqlConnection(sCon);
        //    SqlCommand cmd = new SqlCommand();
        //    cmd.Connection = con;
        //    cmd.CommandType = CommandType.Text;
        //    cmd.CommandText = "SELECT ID_parti,NomParti from Parti_Politique";
        //    DataSet objDs = new DataSet();
        //    SqlDataAdapter dAdapter = new SqlDataAdapter();
        //    dAdapter.SelectCommand = cmd;
        //    con.Open();
        //    dAdapter.Fill(objDs);
        //    con.Close();
           

        //}
       
        private void button1_Click(object sender, EventArgs e)
        {

            if (textBoxPartie.Text.Trim().Length == 0)
            {
                MessageBox.Show("veuillez saisir le nom de la partie");
                textBoxPartie.Focus();
            }

            else
            {


                //Connection cn = new Connection();
                string insertCmd = "INSERT INTO Parti_Politique (NomParti,ELECTION_ID,UserCreation,UserModification,DateCreation,DateModification) VALUES (@NomParti,@ELECTION_ID,@UserCreation,@UserModification,@DateCreation,@DateModification)";
                SqlConnection dbConn;
                dbConn = new SqlConnection(conn.connectdb());
                dbConn.Open();
                Login lg = new Login();
                string dat = DateTime.Now.ToString("yyyy-MM-dd");
               
                GetValue value = new GetValue();
                //getAdress add = new getAdress();
                SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);

                if (comboBoxElec.Text != "")
                {
                    int ElectionID = Convert.ToInt32(comboBoxElec.SelectedValue.ToString());
                    myCommand.Parameters.AddWithValue("@ELECTION_ID", ElectionID);
                }
                //if (radioButton1.Checked)
                //{
                //    myCommand.Parameters.AddWithValue("@Etat", 1);
                //}
                //if (radioButton2.Checked)
                //{
                //    myCommand.Parameters.AddWithValue("@Etat", 0);
                //}
                myCommand.Parameters.AddWithValue("@NomParti", textBoxPartie.Text);

                myCommand.Parameters.AddWithValue("@UserCreation", value.getAgentCréation());
                myCommand.Parameters.AddWithValue("@DateCreation", dat);
                myCommand.Parameters.AddWithValue("@UserModification", value.getAgentCréation());
                myCommand.Parameters.AddWithValue("@DateModification", dat);
                myCommand.ExecuteNonQuery();

                MessageBox.Show("Ajoutée avec succès");
                textBoxPartie.Clear();

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
