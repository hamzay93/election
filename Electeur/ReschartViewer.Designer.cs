﻿namespace Electeur
{
    partial class ReschartViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.crystalReportViewer1 = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.comboBoxLoc = new System.Windows.Forms.ComboBox();
            this.comboBoxArrondis = new System.Windows.Forms.ComboBox();
            this.comboBoxCommune = new System.Windows.Forms.ComboBox();
            this.comboBoxRégion = new System.Windows.Forms.ComboBox();
            this.labelArrondis = new System.Windows.Forms.Label();
            this.labelCom = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBoxBV = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.comboBoxCV = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxEtat = new System.Windows.Forms.ComboBox();
            this.CrystalReport21 = new Electeur.CrystalReport2();
            this.Resultatchart11 = new Electeur.Resultatchart1();
            this.Resultatchart12 = new Electeur.Resultatchart1();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxElec = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // crystalReportViewer1
            // 
            this.crystalReportViewer1.ActiveViewIndex = -1;
            this.crystalReportViewer1.AutoScroll = true;
            this.crystalReportViewer1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.crystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crystalReportViewer1.Cursor = System.Windows.Forms.Cursors.Default;
            this.crystalReportViewer1.Location = new System.Drawing.Point(-9, 215);
            this.crystalReportViewer1.Name = "crystalReportViewer1";
            this.crystalReportViewer1.Size = new System.Drawing.Size(1255, 641);
            this.crystalReportViewer1.TabIndex = 0;
            // 
            // comboBoxLoc
            // 
            this.comboBoxLoc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxLoc.FormattingEnabled = true;
            this.comboBoxLoc.Location = new System.Drawing.Point(572, 70);
            this.comboBoxLoc.Name = "comboBoxLoc";
            this.comboBoxLoc.Size = new System.Drawing.Size(234, 21);
            this.comboBoxLoc.TabIndex = 50;
            this.comboBoxLoc.Tag = "LocalitéID";
            this.comboBoxLoc.SelectedIndexChanged += new System.EventHandler(this.comboBoxLoc_SelectedIndexChanged);
            // 
            // comboBoxArrondis
            // 
            this.comboBoxArrondis.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxArrondis.FormattingEnabled = true;
            this.comboBoxArrondis.Location = new System.Drawing.Point(572, 69);
            this.comboBoxArrondis.Name = "comboBoxArrondis";
            this.comboBoxArrondis.Size = new System.Drawing.Size(234, 21);
            this.comboBoxArrondis.TabIndex = 48;
            this.comboBoxArrondis.Tag = "ArrondisID";
            this.comboBoxArrondis.SelectedIndexChanged += new System.EventHandler(this.comboBoxArrondis_SelectedIndexChanged);
            // 
            // comboBoxCommune
            // 
            this.comboBoxCommune.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCommune.FormattingEnabled = true;
            this.comboBoxCommune.Location = new System.Drawing.Point(248, 96);
            this.comboBoxCommune.Name = "comboBoxCommune";
            this.comboBoxCommune.Size = new System.Drawing.Size(188, 21);
            this.comboBoxCommune.TabIndex = 49;
            this.comboBoxCommune.Tag = "CommuneID";
            this.comboBoxCommune.SelectedIndexChanged += new System.EventHandler(this.comboBoxCommune_SelectedIndexChanged);
            // 
            // comboBoxRégion
            // 
            this.comboBoxRégion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxRégion.FormattingEnabled = true;
            this.comboBoxRégion.Location = new System.Drawing.Point(248, 69);
            this.comboBoxRégion.Name = "comboBoxRégion";
            this.comboBoxRégion.Size = new System.Drawing.Size(188, 21);
            this.comboBoxRégion.TabIndex = 47;
            this.comboBoxRégion.Tag = "RégionID";
            this.comboBoxRégion.SelectedIndexChanged += new System.EventHandler(this.comboBoxRégion_SelectedIndexChanged);
            // 
            // labelArrondis
            // 
            this.labelArrondis.AutoSize = true;
            this.labelArrondis.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.labelArrondis.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelArrondis.Location = new System.Drawing.Point(442, 69);
            this.labelArrondis.Name = "labelArrondis";
            this.labelArrondis.Size = new System.Drawing.Size(72, 19);
            this.labelArrondis.TabIndex = 46;
            this.labelArrondis.Text = "Arrondis";
            // 
            // labelCom
            // 
            this.labelCom.AutoSize = true;
            this.labelCom.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.labelCom.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelCom.Location = new System.Drawing.Point(130, 96);
            this.labelCom.Name = "labelCom";
            this.labelCom.Size = new System.Drawing.Size(83, 19);
            this.labelCom.TabIndex = 45;
            this.labelCom.Text = "Commune";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Location = new System.Drawing.Point(130, 69);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 19);
            this.label5.TabIndex = 44;
            this.label5.Text = "Région";
            // 
            // comboBoxBV
            // 
            this.comboBoxBV.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxBV.FormattingEnabled = true;
            this.comboBoxBV.Location = new System.Drawing.Point(572, 123);
            this.comboBoxBV.Name = "comboBoxBV";
            this.comboBoxBV.Size = new System.Drawing.Size(234, 21);
            this.comboBoxBV.TabIndex = 54;
            this.comboBoxBV.Tag = "BureauID";
            this.comboBoxBV.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold);
            this.label9.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label9.Location = new System.Drawing.Point(443, 127);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(113, 16);
            this.label9.TabIndex = 53;
            this.label9.Text = "Bureau de vote";
            this.label9.Visible = false;
            // 
            // comboBoxCV
            // 
            this.comboBoxCV.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCV.FormattingEnabled = true;
            this.comboBoxCV.Location = new System.Drawing.Point(248, 123);
            this.comboBoxCV.Name = "comboBoxCV";
            this.comboBoxCV.Size = new System.Drawing.Size(188, 21);
            this.comboBoxCV.TabIndex = 52;
            this.comboBoxCV.Tag = "CentreID";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label8.Location = new System.Drawing.Point(131, 127);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(111, 16);
            this.label8.TabIndex = 51;
            this.label8.Text = "Centre de Vote";
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Book Antiqua", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(922, 87);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(196, 35);
            this.btnSave.TabIndex = 55;
            this.btnSave.Text = "Rechercher";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.3F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(131, 175);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 15);
            this.label2.TabIndex = 59;
            this.label2.Text = "Etat";
            // 
            // comboBoxEtat
            // 
            this.comboBoxEtat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxEtat.FormattingEnabled = true;
            this.comboBoxEtat.Items.AddRange(new object[] {
            "Editions des résultats détaillés",
            "Editions des résultats détaillés en pourcentage",
            "Editions des résultats en camemberg"});
            this.comboBoxEtat.Location = new System.Drawing.Point(248, 172);
            this.comboBoxEtat.Name = "comboBoxEtat";
            this.comboBoxEtat.Size = new System.Drawing.Size(188, 21);
            this.comboBoxEtat.TabIndex = 58;
            this.comboBoxEtat.Tag = "test";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(130, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 19);
            this.label1.TabIndex = 60;
            this.label1.Text = "Election";
            // 
            // comboBoxElec
            // 
            this.comboBoxElec.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxElec.FormattingEnabled = true;
            this.comboBoxElec.Location = new System.Drawing.Point(248, 31);
            this.comboBoxElec.Name = "comboBoxElec";
            this.comboBoxElec.Size = new System.Drawing.Size(331, 21);
            this.comboBoxElec.TabIndex = 61;
            // 
            // ReschartViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSeaGreen;
            this.ClientSize = new System.Drawing.Size(1258, 741);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBoxElec);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBoxEtat);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.comboBoxBV);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.comboBoxCV);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.comboBoxLoc);
            this.Controls.Add(this.comboBoxArrondis);
            this.Controls.Add(this.comboBoxCommune);
            this.Controls.Add(this.comboBoxRégion);
            this.Controls.Add(this.labelArrondis);
            this.Controls.Add(this.labelCom);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.crystalReportViewer1);
            this.Name = "ReschartViewer";
            this.Text = "ReschartViewer";
            this.Load += new System.EventHandler(this.ReschartViewer_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CrystalDecisions.Windows.Forms.CrystalReportViewer crystalReportViewer1;
        private Resultatchart1 Resultatchart11;
        private Resultatchart1 Resultatchart12;
        private CrystalReport2 CrystalReport21;
        private System.Windows.Forms.ComboBox comboBoxLoc;
        private System.Windows.Forms.ComboBox comboBoxArrondis;
        private System.Windows.Forms.ComboBox comboBoxCommune;
        private System.Windows.Forms.ComboBox comboBoxRégion;
        private System.Windows.Forms.Label labelArrondis;
        private System.Windows.Forms.Label labelCom;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBoxBV;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboBoxCV;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxEtat;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxElec;
    }
}