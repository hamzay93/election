﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class modif_voton_avec_ordonance : Form
    {
        string clause;
        int RégionName, ArrondisName, CommuneName, strID;
        Connection conn = new Connection();
        GetValue get = new GetValue();
        public modif_voton_avec_ordonance()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {


            this.FillRégion();
            this.IsEmptyfield();
            if (String.IsNullOrEmpty(clause))
            {
                MessageBox.Show("veuillez remplir un champ au minimum");
                this.Focus();

            }
            else
            {
                string sCon = conn.connectdb();
                SqlConnection con = new SqlConnection(sCon);
                string str = "SET CONCAT_NULL_YIELDS_NULL OFF; SELECT ElecteurOrdonanceID, Nom1 +'  '+ Nom2 +'  '+Nom3 AS NOM,NomMère1+'  '+NomMère2 AS 'NOM DE LA MERE',DOB AS 'DATE DE NAISSANCE',POB AS LIEU,CodeElecteur AS 'CODE ELECTEUR',AncienCNI AS 'ANCIEN CNI',DateAncienCNI AS 'DATE DELIVRANCE',NewCNI AS 'NOUVEAU CNI',DateNewCNI AS 'DATE DELIVRANCE',Sexe AS 'SEXE',LocalitéID,DateCréation,AdresseID FROM ElecteurOrdonance where " + clause + "";
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(str, con);
                SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sqlAdapter);

                DataTable table = new DataTable();
                table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                sqlAdapter.Fill(table);
                dataGridView1.DataSource = table;
                if (dataGridView1.Rows.Count == 0)
                {
                   // button2.Enabled = false;
                    panel2.Visible = false;
                    panel3.Visible = false;

//                    button4.Enabled = false;
                    //  label10.Enabled = false;
                    MessageBox.Show("Aucun electeur retrouvé pour ces critères");

                }
                else
                {
                    //  label10.Enabled = true;
                   // button2.Enabled = true;
                    panel2.Visible = true;
                    panel3.Visible = true;
                   // button4.Enabled = true;
                }

                this.dataGridView1.Columns["ElecteurOrdonanceID"].Visible = false;
           //     this.dataGridView1.Columns["AdresseID"].Visible = false;
                //this.dataGridView1.Columns["StatutID"].Visible = false;

                con.Open();
                clause = "";
            }
        }
        public void IsEmptyfield()
        {
            clause = null;
            foreach (Control X in this.panel1.Controls)
            {

                if (X is TextBox || X is ComboBox)
                {
                    if (X.Tag.ToString() == "AncienCNI" || X.Tag.ToString() == "NewCNI" || X.Tag.ToString() == "CodeElecteur")
                    {
                        if (X.Text.Trim().Length != 0)
                        {

                            if (string.IsNullOrEmpty(clause))
                            {
                                clause = clause + X.Tag + " = '" + X.Text.Trim() + "'";
                                //return clause;
                            }
                            else
                            {
                                clause = clause + " And " + X.Tag + " = '" + X.Text.Trim() + "'";
                                //return clause;
                            }
                        }
                    }
                    else
                    {
                        if (X.Text.Trim().Length != 0)
                        {
                            if (string.IsNullOrEmpty(clause))
                            {
                                clause = clause + X.Tag + " Like '" + X.Text.Trim() + "%'";
                                //return clause;
                            }
                            else
                            {
                                clause = clause + " And " + X.Tag + " Like '" + X.Text.Trim() + "%'";
                                //return clause;
                            }
                        }
                    }

                }
                else
                {
                    if (X is DateTimePicker)
                    {
                        if (X.Name == "dateTimePicker1")
                        {
                            if (string.IsNullOrEmpty(clause))
                            {
                                if (dateTimePicker1.Checked)
                                {
                                    string dat = dateTimePicker1.Value.ToString("yyyy-MM-dd");
                                    DateTime date4 = DateTime.Parse(dat);
                                    clause = clause + X.Tag + " Like '%" + dat + "%'";
                                }

                            }
                            else
                            {
                                if (dateTimePicker1.Checked)
                                {
                                    string dat = dateTimePicker1.Value.ToString("yyyy-MM-dd");
                                    DateTime date4 = DateTime.Parse(dat);
                                    clause = clause + " AND " + X.Tag + " Like '%" + dat + "%'";
                                }

                            }
                        }
                    }
                }
            }

        }
        private void modif_voton_avec_ordonance_Load(object sender, EventArgs e)
        {
            dataGridView1.SelectionChanged += new EventHandler(dataGridView1_SelectionChanged);
            this.panel3.Visible = false;
            this.panel2.Visible = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ClearClass1 clr = new ClearClass1();
            clr.RecursiveClearTextBoxes(this.panel1.Controls);
        }
        private void FillRégion()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT RégionID, NomRégion FROM Région";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBoxRégion.ValueMember = "RégionID";
            comboBoxRégion.DisplayMember = "NomRégion";
            comboBoxRégion.DataSource = objDs.Tables[0];
        }
       /* private void FillAdresse(int ArrondisID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT AdresseID, NomAdresse FROM Adresse WHERE ArrondisID =@ArrondisID";
            cmd.Parameters.AddWithValue("@ArrondisID", ArrondisID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxAdresse.ValueMember = "AdresseID";
                comboBoxAdresse.DisplayMember = "NomAdresse";
                comboBoxAdresse.DataSource = objDs.Tables[0];

            }
            //comboBoxAdresse.SelectedValue = adresseID1;
            //comboBoxAdresse.SelectedValue = this.getadress();
        }*/
        private void FillCommune(int RégionID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT CommuneID, NomCommune FROM Commune WHERE RégionID =@RégionID";
            cmd.Parameters.AddWithValue("@RégionID", RégionID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxCommune.ValueMember = "CommuneID";
                comboBoxCommune.DisplayMember = "NomCommune";
                comboBoxCommune.DataSource = objDs.Tables[0];
            }
        }
        string clause3;
        private void filldetails()
        {

            this.selection();
            if (String.IsNullOrEmpty(clause3))
            {
                this.panel3.Visible = false;
                this.panel2.Visible = false;
            }
            else
            {
                this.panel3.Visible = true;
                this.panel2.Visible = true;
                string sCon = conn.connectdb();
                SqlConnection con = new SqlConnection(sCon);
                string str = "SELECT * FROM ElecteurOrdonance where ElecteurOrdonanceID= " + clause3;
                SqlCommand cmd = new SqlCommand(str, con);
                con.Open();

                using (SqlDataReader read = cmd.ExecuteReader())

                    while (read.Read())
                    {
                        textBoxNom1.Text = (read["Nom1"].ToString());
                        textBoxNom2.Text = (read["Nom2"].ToString());
                        textBoxNom3.Text = (read["Nom3"].ToString());
                        textBoxNomMère1.Text = (read["NomMère1"].ToString());
                        textBoxNomMère2.Text = (read["NomMère2"].ToString());
                        textBoxNomMère3.Text = (read["NomMère3"].ToString());
                        textBoxProfession.Text = (read["Profession"].ToString());
                        textBoxPortable.Text = (read["Portable"].ToString());
                        int colIndex3 = (read.GetOrdinal("DOB"));
                        if (!read.IsDBNull(colIndex3))
                        {
                            dateTimePickerDOB.Checked = true;
                            dateTimePickerDOB.Value = (read.GetDateTime(colIndex3));
                        }
                        //dateTimePickerDOB.TextDOB.Text = (read["DOB"].ToString());
                        textBoxPOB.Text = (read["POB"].ToString());
                        textBoxCodeElecteur.Text = (read["CodeElecteur"].ToString());
                        textBoxAncCNI.Text = (read["AncienCNI"].ToString());

                        int colIndex = (read.GetOrdinal("DateAncienCNI"));
                        if (!read.IsDBNull(colIndex))
                        {
                            dateTimePickerancni.Checked = true;
                            dateTimePickerancni.Value = (read.GetDateTime(colIndex));
                        }
                        int colIndex2 = (read.GetOrdinal("DateNewCNI"));
                        if (!read.IsDBNull(colIndex2))
                        {
                            dateTimePickernvcni.Checked = true;
                            dateTimePickernvcni.Value = (read.GetDateTime(colIndex2));
                        }
                        textBoxNewCNI.Text = (read["NewCNI"].ToString());
                        textBoxCodeElecteur.Text = (read["CodeElecteur"].ToString());
                        comboBoxSexe.SelectedItem = (read["Sexe"].ToString());
                      /*  textBoxNbureau.Text = (read["CodeBureau"].ToString());
                        string CV = (read["CentreID"].ToString());
                        string bur = (read["BureauID"].ToString());
                        if (!String.IsNullOrEmpty(bur))
                        {
                            int Nbureau = Int32.Parse(bur);
                            textBoxBureau.Text = this.getBureau(Nbureau);
                        }
                        else
                        {
                            textBoxBureau.Text = "";
                        }
                        if (!String.IsNullOrEmpty(CV))
                        {
                            int NCV = Int32.Parse(CV);
                            textBoxCV.Text = this.getCV(NCV);
                        }
                        else
                        {
                            textBoxCV.Text = "";
                        }
                        //statutID = (read["StatutID"].ToString());
                        //LabelStatut.Text = this.getStatutName();
                        //adresseID = this.getAdresseID();
                        //IDRégion = (read["RégionID"].ToString());*/
                        comboBoxRégion.SelectedValue = (read["RégionID"].ToString());
                        if (Int32.Parse(comboBoxRégion.SelectedValue.ToString()) == 1)
                        {
                            comboBoxCommune.SelectedValue = (read["CommuneID"].ToString()); ;
                            comboBoxArrondis.SelectedValue = (read["ArrondisID"].ToString()); ;
                           comboBoxAdresse.SelectedValue = (read["AdresseID"].ToString());
                            labelCom.Text = "Commune";
                            comboBoxLoc.Visible = false;
                            label15.Visible = true;
                            labelArrondis.Visible = true;
                        comboBoxAdresse.Visible = true;
                            comboBoxArrondis.Visible = true;
                            comboBoxCommune.Visible = true;
                        }
                        else
                        {
                            this.Filllocalite(Int32.Parse(comboBoxRégion.SelectedValue.ToString()));
                            comboBoxLoc.SelectedValue = (read["localitéID"].ToString());
                            comboBoxLoc.Visible = true;
                            labelCom.Text = "Localité";
                            comboBoxCommune.Visible = false;
                          label15.Visible = false;
                            labelArrondis.Visible = false;
                           comboBoxAdresse.Visible = false;
                            comboBoxArrondis.Visible = false;
                        }


                    }
            }

        }
        private void Filllocalite(int _reg)
        {
            int reg = _reg;
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT LocalitéID, NomLocalité FROM Localité WHERE RégionID = " + reg;
            //cmd.Parameters.AddWithValue("@RégionID", RégionID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxLoc.ValueMember = "LocalitéID";
                comboBoxLoc.DisplayMember = "NomLocalité";
                comboBoxLoc.DataSource = objDs.Tables[0];
            }
        }
        string adresseID1, StatutID;
        private void selection()
        {
            int counter3;
            clause3 = null;
            StatutID = null;
            adresseID1 = null;
            // Iterate through all the rows and sum up the appropriate columns. 
            for (counter3 = 0; counter3 < (dataGridView1.Rows.Count);
                counter3++)
            {
                //dataGridView1.Rows.
                if (dataGridView1.Rows[counter3].Selected)
                {
                    if (dataGridView1.Rows[counter3].Cells["ElecteurOrdonanceID"].Value
                    != null)
                    {
                        if (dataGridView1.Rows[counter3].
                            Cells["ElecteurOrdonanceID"].Value.ToString().Length != 0)
                        {
                            if (string.IsNullOrEmpty(clause3))
                            {
                                clause3 = clause3 + int.Parse(dataGridView1.Rows[counter3].
                                    Cells["ElecteurOrdonanceID"].Value.ToString());
                            }
                            else
                            {
                                clause3 = clause3 + "," + int.Parse(dataGridView1.Rows[counter3].
                                Cells["ElecteurOrdonanceID"].Value.ToString());

                            }

                        }
                    }
                   if (dataGridView1.Rows[counter3].
                            Cells["AdresseID"].Value.ToString().Length != 0)
                    {
                        if (string.IsNullOrEmpty(adresseID1))
                        {
                            adresseID1 = adresseID1 + int.Parse(dataGridView1.Rows[counter3].
                                Cells["AdresseID"].Value.ToString());
                            ;
                        }
                        else
                        {
                            adresseID1 = adresseID1 + "," + int.Parse(dataGridView1.Rows[counter3].
                            Cells["AdresseID"].Value.ToString());
                            ;
                        }
                    }
                    /*if (dataGridView1.Rows[counter3].
                            Cells["StatutID"].Value.ToString().Length != 0)
                    {
                        if (string.IsNullOrEmpty(StatutID))
                        {
                            StatutID = StatutID + int.Parse(dataGridView1.Rows[counter3].
                                Cells["StatutID"].Value.ToString());
                            strID = Int32.Parse(StatutID);
                            if (strID >= 3)
                            {
                                button2.Text = "Mettre à jour";
                            }
                            else
                            {
                                button2.Text = "Modifier";
                            }

                            break;
                        }
                        
                    }*/
                }

            }
        }
        public string getBureau(int ID)
        {
            //SqlDataReader rd = null;
            int Id = ID;
            string query = "select NomBureau from Bureau where BureauID='" + Id + "'";
            string sCon = conn.connectdb();
            SqlConnection dbConn;
            dbConn = new SqlConnection(sCon);
            dbConn.Open();
            SqlCommand cmd = new SqlCommand(query, dbConn);
            string result = (cmd.ExecuteScalar().ToString());
            //SaisieElecteur se=new SaisieElecteur();
            string Bureau = result;
            return Bureau;

        }
        public string getCV(int ID)
        {
            //SqlDataReader rd = null;
            int Id = ID;
            string query = "select CentreVote from CentreVote where CentreID='" + Id + "'";
            string sCon = conn.connectdb();
            SqlConnection dbConn;
            dbConn = new SqlConnection(sCon);
            dbConn.Open();
            SqlCommand cmd = new SqlCommand(query, dbConn);
            string result = (cmd.ExecuteScalar().ToString());
            //SaisieElecteur se=new SaisieElecteur();
            string Centre = result;
            return Centre;

        }
        public int getRégionName()
        {
            //SqlDataReader rd = null;
            string query = "select RégionID from Région where RégionID = (Select RégionID from Commune where CommuneID= (select CommuneID from Arrondissement where ArrondisID=(select ArrondisID from Adresse where AdresseID ='" + adresseID1 + "')))";
            string sCon = conn.connectdb();
            SqlConnection dbConn;
            dbConn = new SqlConnection(sCon);
            dbConn.Open();
            SqlCommand cmd = new SqlCommand(query, dbConn);
            int result = ((int)cmd.ExecuteScalar());
            //SaisieElecteur se=new SaisieElecteur();
            RégionName = result;
            return RégionName;

        }
        public int getArrondissement()
        {
            //SqlDataReader rd = null;
            string query = "select ArrondisID from Arrondissement where ArrondisID=(select ArrondisID from Adresse where AdresseID ='" + adresseID1 + "')";
            string sCon = conn.connectdb();
            SqlConnection dbConn;
            dbConn = new SqlConnection(sCon);
            dbConn.Open();
            SqlCommand cmd = new SqlCommand(query, dbConn);
            int result = ((int)cmd.ExecuteScalar());
            //SaisieElecteur se=new SaisieElecteur();
            ArrondisName = result;
            return ArrondisName;

        }
        public int getCommune()
        {
            //SqlDataReader rd = null;
            string query = "Select CommuneID from Commune where CommuneID= (select CommuneID from Arrondissement where ArrondisID=(select ArrondisID from Adresse where AdresseID ='" + adresseID1 + "'))";
            string sCon = conn.connectdb();
            SqlConnection dbConn;
            dbConn = new SqlConnection(sCon);
            dbConn.Open();
            SqlCommand cmd = new SqlCommand(query, dbConn);
            int result = ((int)cmd.ExecuteScalar());
            //SaisieElecteur se=new SaisieElecteur();
            CommuneName = result;
            return CommuneName;

        }
        private void FillArrondis(int CommuneID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT ArrondisID, NomArrondis FROM Arrondissement WHERE CommuneID =@CommuneID";
            cmd.Parameters.AddWithValue("@CommuneID", CommuneID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxArrondis.ValueMember = "ArrondisID";
                comboBoxArrondis.DisplayMember = "NomArrondis";
                comboBoxArrondis.DataSource = objDs.Tables[0];
            }
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            ClearClass1 clr = new ClearClass1();
            clr.RecursiveClearTextBoxes(this.panel3.Controls);
            ClearClass1 clr1 = new ClearClass1();
            clr1.RecursiveClearTextBoxes(this.panel2.Controls);
            dateTimePickerDOB.Checked=false;
            dateTimePickerancni.Checked=false;
            dateTimePickernvcni.Checked = false;
            this.filldetails();
        }

        private void comboBoxRégion_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (comboBoxRégion.SelectedValue.ToString() == "1")
            {
                 labelCom.Text = "Commune";
                comboBoxLoc.Visible = false;
                label15.Visible = true;
                labelArrondis.Visible = true;
                comboBoxAdresse.Visible = true;
                comboBoxArrondis.Visible = true;
                comboBoxCommune.Visible = true;
                int RégionID = Convert.ToInt32(comboBoxRégion.SelectedValue.ToString());
                FillCommune(RégionID);
                comboBoxCommune.SelectedIndex = 0;

            }
            else
            {
                comboBoxLoc.Visible = true;
                labelCom.Text = "Localité";
                comboBoxCommune.Visible = false;
                label15.Visible = false;
                labelArrondis.Visible = false;
                comboBoxAdresse.Visible = false;
                comboBoxArrondis.Visible = false;
                int RégionID = Convert.ToInt32(comboBoxRégion.SelectedValue.ToString());
                Filllocalite(RégionID);
                comboBoxLoc.SelectedIndex = 0;
            }
        }

        private void comboBoxArrondis_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxArrondis.SelectedValue.ToString() != "")
            {
                int ArrondisID = Convert.ToInt32(comboBoxArrondis.SelectedValue.ToString());
                this.FillAdresse(ArrondisID);
                //comboBoxAdresse.SelectedValue.ToString() = this.getadress().ToString();
                //comboBoxAdresse.SelectedValue.= this.getadress();
            }
        }
        private void FillAdresse(int ArrondisID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT AdresseID, NomAdresse FROM Adresse WHERE ArrondisID =@ArrondisID";
            cmd.Parameters.AddWithValue("@ArrondisID", ArrondisID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxAdresse.ValueMember = "AdresseID";
                comboBoxAdresse.DisplayMember = "NomAdresse";
                comboBoxAdresse.DataSource = objDs.Tables[0];
            }
        }

        private void comboBoxCommune_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBoxCommune_SelectedIndexChanged_1(object sender, EventArgs e)
        {


            if (comboBoxCommune.SelectedValue.ToString() != "")
            {
                int CommuneID = Convert.ToInt32(comboBoxCommune.SelectedValue.ToString());
                this.FillArrondis(CommuneID);
                comboBoxArrondis.SelectedIndex = 0;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {


            //   this.checkIfExist();
            if (textBoxNom1.Text.Trim().Length == 0 || textBoxNom2.Text.Trim().Length == 0 )
            {
                MessageBox.Show("Veuillez saisir le nom complet du mandateur");
                textBoxNom1.Focus();
            }
            else
            {

                if (textBoxAncCNI.Text.Trim().Length == 0 && textBoxNewCNI.Text.Trim().Length == 0)
                {
                    MessageBox.Show("Veuillez saisir la nouvelle CNI ou l'ancienne CNI");
                    textBoxAncCNI.Focus();
                }
                else
                {
                    if (comboBoxSexe.Text.Length == 0)
                    {
                        MessageBox.Show("Veuillez choisir le sexe de la personne");
                        comboBoxSexe.Focus();
                    }
                    else
                    {

                        if (dateTimePickernvcni.Checked || dateTimePickerancni.Checked)
                        {
                            DateTime Date1 = DateTime.Parse(dateTimePickerancni.Text);
                            DateTime Date2 = DateTime.Parse(dateTimePickernvcni.Text);
                            DateTime Date3 = DateTime.Parse(dateTimePickerDOB.Text);
                            int result = DateTime.Compare(Date1, Date2);

                            if (dateTimePickernvcni.Checked && dateTimePickerancni.Checked)
                            {


                                if (result > 0 || result == 0)
                                {
                                    MessageBox.Show("La date de la nouvelle CNI ne peut être anterieure ou identique à celle de l'ancienne CNI");
                                    dateTimePickerancni.Focus();
                                }
                                else
                                {

                                    try
                                    {
                                        if (Int32.Parse(comboBoxRégion.SelectedValue.ToString()) == 1)
                                        {

                                            string sCon = conn.connectdb();
                                            string insertCmd = "update ElecteurOrdonance set RégionID=@RégionID,CommuneID=@CommuneID,ArrondisID=@ArrondisID,Nom1=@Nom1,Nom2=@Nom2,Nom3=@Nom3,NomMère1=@NomMère1,NomMère2=@NomMère2,NomMère3=@NomMère3,DOB=@DOB,POB=@POB,CodeElecteur=@CodeElecteur,AncienCNI=@AncienCNI,DateAncienCNI=@DateAncienCNI,NewCNI=@NewCNI,DateNewCNI=@DateNewCNI,AgentModif=@AgentModif,Sexe=@Sexe,Région=@Région,Commune=@Commune,Arrondissement=@Arrondissement,DateModification=@DateModification,Profession=@Profession,Portable=@Portable,AdresseID=@AdresseID,Adresse=@Adresse where ElecteurOrdonanceID=" + clause3;
                                            SqlConnection dbConn;
                                            dbConn = new SqlConnection(sCon);
                                            dbConn.Open();
                                            Login lg = new Login();
                                            //string str;
                                            string dat = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                            DateTime date4 = DateTime.Parse(dat);

                                            GetValue value = new GetValue();
                                            //getAdress add = new getAdress();
                                            SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                                            // Create parameters for the SqlCommand object
                                            // initialize with input-form field values
                                            //myCommand.Parameters.AddWithValue("@ElecteurID","");
                                            myCommand.Parameters.AddWithValue("@Nom1", textBoxNom1.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@Nom2", textBoxNom2.Text.ToUpper());

                                            myCommand.Parameters.AddWithValue("@Nom3", textBoxNom3.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@NomMère1", textBoxNomMère1.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@NomMère2", textBoxNomMère2.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@NomMère3", textBoxNomMère3.Text.ToUpper());
                                            if (dateTimePickerDOB.Checked)
                                            {
                                                myCommand.Parameters.AddWithValue("@DOB", Date3);
                                            }
                                            else
                                            {
                                                myCommand.Parameters.AddWithValue("@DOB", DBNull.Value);
                                            }
                                            myCommand.Parameters.AddWithValue("@POB", textBoxPOB.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@CodeElecteur", textBoxCodeElecteur.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@AncienCNI", textBoxAncCNI.Text.ToUpper());
                                            if (dateTimePickerancni.Checked)
                                            {
                                                myCommand.Parameters.AddWithValue("@DateAncienCNI", Date1);
                                            }
                                            else
                                            {
                                                myCommand.Parameters.AddWithValue("@DateAncienCNI", DBNull.Value);
                                            }
                                            myCommand.Parameters.AddWithValue("@NewCNI", textBoxNewCNI.Text.ToUpper());
                                            if (dateTimePickernvcni.Checked)
                                            {
                                                myCommand.Parameters.AddWithValue("@DateNewCNI", Date2);
                                            }
                                            else
                                            {
                                                myCommand.Parameters.AddWithValue("@DateNewCNI", DBNull.Value);
                                            }

                                           // myCommand.Parameters.AddWithValue("@AgentSaisie", value.getAgentSaisie());//recupere agent saisie
                                            myCommand.Parameters.AddWithValue("@AgentModif", value.getAgentModif());//recupere agent modif
                                          //  myCommand.Parameters.AddWithValue("@AgentCréation", value.getAgentCréation());//recupere agent de creation

                                            //myCommand.Parameters.AddWithValue("@Téléphone", textBoxPhone.Text.ToUpper());

                                           // myCommand.Parameters.AddWithValue("@AgentID", value.getAgentID());//recupere l'ID agent
                                            myCommand.Parameters.AddWithValue("@Sexe", comboBoxSexe.Text);

                                            myCommand.Parameters.AddWithValue("@Région", comboBoxRégion.Text);
                                            myCommand.Parameters.AddWithValue("@RégionID", Int32.Parse(comboBoxRégion.SelectedValue.ToString()));


                                            myCommand.Parameters.AddWithValue("@Commune", comboBoxCommune.Text);
                                            myCommand.Parameters.AddWithValue("@CommuneID", Int32.Parse(comboBoxCommune.SelectedValue.ToString()));


                                            myCommand.Parameters.AddWithValue("@Arrondissement", comboBoxArrondis.Text);
                                            myCommand.Parameters.AddWithValue("@ArrondisID", Int32.Parse(comboBoxArrondis.SelectedValue.ToString()));

                                            myCommand.Parameters.AddWithValue("@Adresse", comboBoxAdresse.Text);
                                            myCommand.Parameters.AddWithValue("@AdresseID", Int32.Parse(comboBoxAdresse.SelectedValue.ToString()));

                                         //   myCommand.Parameters.AddWithValue("@DateCréation", dat);
                                            myCommand.Parameters.AddWithValue("@Profession", textBoxProfession.Text);
                                            myCommand.Parameters.AddWithValue("@Portable", textBoxPortable.Text);
                                            myCommand.Parameters.AddWithValue("@DateModification", dat);

                                         //   myCommand.Parameters.AddWithValue("@Complement", textBoxAdresseCompl.Text);



                                            myCommand.ExecuteNonQuery();
                                            MessageBox.Show("Modification effectué avec succès");
                                            ClearClass1 clr1 = new ClearClass1();
                                            clr1.RecursiveClearTextBoxes(this.panel2.Controls);
                                            ClearClass1 clr = new ClearClass1();
                                            clr.RecursiveClearTextBoxes(this.panel3.Controls);
                                            dateTimePickerancni.Checked = false;
                                            dateTimePickernvcni.Checked = false;
                                            dateTimePickerDOB.Checked = false;

                                        }
                                        else
                                        {
                                            string sCon = conn.connectdb();
                                            string insertCmd = "update ElecteurOrdonance set localitéID=@localitéID,RégionID=@RégionID,Nom1=@Nom1,Nom2=@Nom2,Nom3=@Nom3,NomMère1=@NomMère1,NomMère2=@NomMère2,NomMère3=@NomMère3,DOB=@DOB,POB=@POB,CodeElecteur=@CodeElecteur,AncienCNI=@AncienCNI,DateAncienCNI=@DateAncienCNI,NewCNI=@NewCNI,DateNewCNI=@DateNewCNI,AgentModif=@AgentModif,Sexe=@Sexe,Région=@Région,DateModification=@DateModification,Localité=@Localité,Profession=@Profession,Portable=@Portable where ElecteurOrdonanceID="+clause3;
                                            SqlConnection dbConn;
                                            dbConn = new SqlConnection(sCon);
                                            dbConn.Open();
                                            Login lg = new Login();
                                            //string str;
                                            string dat = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                            DateTime date4 = DateTime.Parse(dat);

                                            GetValue value = new GetValue();
                                            //getAdress add = new getAdress();
                                            SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                                            // Create parameters for the SqlCommand object
                                            // initialize with input-form field values
                                            //myCommand.Parameters.AddWithValue("@ElecteurID","");
                                            myCommand.Parameters.AddWithValue("@Nom1", textBoxNom1.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@Nom2", textBoxNom2.Text.ToUpper());

                                            myCommand.Parameters.AddWithValue("@Nom3", textBoxNom3.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@NomMère1", textBoxNomMère1.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@NomMère2", textBoxNomMère2.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@NomMère3", textBoxNomMère3.Text.ToUpper());
                                            if (dateTimePickerDOB.Checked)
                                            {
                                                myCommand.Parameters.AddWithValue("@DOB", Date3);
                                            }
                                            else
                                            {
                                                myCommand.Parameters.AddWithValue("@DOB", DBNull.Value);
                                            }
                                            myCommand.Parameters.AddWithValue("@POB", textBoxPOB.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@CodeElecteur", textBoxCodeElecteur.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@AncienCNI", textBoxAncCNI.Text.ToUpper());
                                            if (dateTimePickerancni.Checked)
                                            {
                                                myCommand.Parameters.AddWithValue("@DateAncienCNI", Date1);
                                            }
                                            else
                                            {
                                                myCommand.Parameters.AddWithValue("@DateAncienCNI", DBNull.Value);
                                            }
                                            myCommand.Parameters.AddWithValue("@NewCNI", textBoxNewCNI.Text.ToUpper());
                                            if (dateTimePickernvcni.Checked)
                                            {
                                                myCommand.Parameters.AddWithValue("@DateNewCNI", Date2);
                                            }
                                            else
                                            {
                                                myCommand.Parameters.AddWithValue("@DateNewCNI", DBNull.Value);
                                            }

                                           // myCommand.Parameters.AddWithValue("@AgentSaisie", value.getAgentSaisie());//recupere agent saisie
                                            myCommand.Parameters.AddWithValue("@AgentModif", value.getAgentModif());//recupere agent modif
                                        //    myCommand.Parameters.AddWithValue("@AgentCréation", value.getAgentCréation());//recupere agent de creation

                                          //  myCommand.Parameters.AddWithValue("@Téléphone", textBoxPhone.Text.ToUpper());

                                            myCommand.Parameters.AddWithValue("@AgentID", value.getAgentID());//recupere l'ID agent
                                            myCommand.Parameters.AddWithValue("@Sexe", comboBoxSexe.Text);

                                            myCommand.Parameters.AddWithValue("@Région", comboBoxRégion.Text);
                                            myCommand.Parameters.AddWithValue("@RégionID", Int32.Parse(comboBoxRégion.SelectedValue.ToString()));


                                            myCommand.Parameters.AddWithValue("@DateCréation", dat);
                                            myCommand.Parameters.AddWithValue("@Profession", textBoxProfession.Text);
                                            myCommand.Parameters.AddWithValue("@Portable", textBoxPortable.Text);
                                            myCommand.Parameters.AddWithValue("@DateModification", dat);

                                            myCommand.Parameters.AddWithValue("@Localité", comboBoxLoc.Text);
                                            myCommand.Parameters.AddWithValue("@localitéID", Int32.Parse(comboBoxLoc.SelectedValue.ToString()));

                                         //   myCommand.Parameters.AddWithValue("@Complement", textBoxAdresseCompl.Text);



                                            myCommand.ExecuteNonQuery();
                                            MessageBox.Show("Modification effectué avec succès");
                                            ClearClass1 clr1 = new ClearClass1();
                                            clr1.RecursiveClearTextBoxes(this.panel2.Controls);
                                            ClearClass1 clr = new ClearClass1();
                                            clr.RecursiveClearTextBoxes(this.panel3.Controls);
                                            dateTimePickerancni.Checked = false;
                                            dateTimePickernvcni.Checked = false;
                                            dateTimePickerDOB.Checked = false;

                                        }

                                    }
                                    catch (SqlException ex)
                                    {
                                        MessageBox.Show("erreur  " + ex.Message);
                                    }
                                }


                            }
                            else
                            {
                                try
                                {
                                    if (Int32.Parse(comboBoxRégion.SelectedValue.ToString()) == 1)
                                    {

                                        string sCon = conn.connectdb();
                                        string insertCmd = "update ElecteurOrdonance set RégionID=@RégionID,CommuneID=@CommuneID,ArrondisID=@ArrondisID,Nom1=@Nom1,Nom2=@Nom2,Nom3=@Nom3,NomMère1=@NomMère1,NomMère2=@NomMère2,NomMère3=@NomMère3,DOB=@DOB,POB=@POB,CodeElecteur=@CodeElecteur,AncienCNI=@AncienCNI,DateAncienCNI=@DateAncienCNI,NewCNI=@NewCNI,DateNewCNI=@DateNewCNI,AgentModif=@AgentModif,Sexe=@Sexe,Région=@Région,Commune=@Commune,Arrondissement=@Arrondissement,DateModification=@DateModification,Profession=@Profession,Portable=@Portable,AdresseID=@AdresseID,Adresse=@Adresse where ElecteurOrdonanceID=" + clause3;
                                        SqlConnection dbConn;
                                        dbConn = new SqlConnection(sCon);
                                        dbConn.Open();
                                        Login lg = new Login();
                                        //string str;
                                        string dat = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                        DateTime date4 = DateTime.Parse(dat);

                                        GetValue value = new GetValue();
                                        //getAdress add = new getAdress();
                                        SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                                        // Create parameters for the SqlCommand object
                                        // initialize with input-form field values
                                        //myCommand.Parameters.AddWithValue("@ElecteurID","");
                                        myCommand.Parameters.AddWithValue("@Nom1", textBoxNom1.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@Nom2", textBoxNom2.Text.ToUpper());

                                        myCommand.Parameters.AddWithValue("@Nom3", textBoxNom3.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@NomMère1", textBoxNomMère1.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@NomMère2", textBoxNomMère2.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@NomMère3", textBoxNomMère3.Text.ToUpper());
                                        if (dateTimePickerDOB.Checked)
                                        {
                                            myCommand.Parameters.AddWithValue("@DOB", Date3);
                                        }
                                        else
                                        {
                                            myCommand.Parameters.AddWithValue("@DOB", DBNull.Value);
                                        }
                                        myCommand.Parameters.AddWithValue("@POB", textBoxPOB.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@CodeElecteur", textBoxCodeElecteur.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@AncienCNI", textBoxAncCNI.Text.ToUpper());
                                        if (dateTimePickerancni.Checked)
                                        {
                                            myCommand.Parameters.AddWithValue("@DateAncienCNI", Date1);
                                        }
                                        else
                                        {
                                            myCommand.Parameters.AddWithValue("@DateAncienCNI", DBNull.Value);
                                        }
                                        myCommand.Parameters.AddWithValue("@NewCNI", textBoxNewCNI.Text.ToUpper());
                                        if (dateTimePickernvcni.Checked)
                                        {
                                            myCommand.Parameters.AddWithValue("@DateNewCNI", Date2);
                                        }
                                        else
                                        {
                                            myCommand.Parameters.AddWithValue("@DateNewCNI", DBNull.Value);
                                        }

                                        // myCommand.Parameters.AddWithValue("@AgentSaisie", value.getAgentSaisie());//recupere agent saisie
                                        myCommand.Parameters.AddWithValue("@AgentModif", value.getAgentModif());//recupere agent modif
                                        //  myCommand.Parameters.AddWithValue("@AgentCréation", value.getAgentCréation());//recupere agent de creation

                                        //myCommand.Parameters.AddWithValue("@Téléphone", textBoxPhone.Text.ToUpper());

                                        // myCommand.Parameters.AddWithValue("@AgentID", value.getAgentID());//recupere l'ID agent
                                        myCommand.Parameters.AddWithValue("@Sexe", comboBoxSexe.Text);

                                        myCommand.Parameters.AddWithValue("@Région", comboBoxRégion.Text);
                                        myCommand.Parameters.AddWithValue("@RégionID", Int32.Parse(comboBoxRégion.SelectedValue.ToString()));


                                        myCommand.Parameters.AddWithValue("@Commune", comboBoxCommune.Text);
                                        myCommand.Parameters.AddWithValue("@CommuneID", Int32.Parse(comboBoxCommune.SelectedValue.ToString()));


                                        myCommand.Parameters.AddWithValue("@Arrondissement", comboBoxArrondis.Text);
                                        myCommand.Parameters.AddWithValue("@ArrondisID", Int32.Parse(comboBoxArrondis.SelectedValue.ToString()));

                                        myCommand.Parameters.AddWithValue("@Adresse", comboBoxAdresse.Text);
                                        myCommand.Parameters.AddWithValue("@AdresseID", Int32.Parse(comboBoxAdresse.SelectedValue.ToString()));

                                        //   myCommand.Parameters.AddWithValue("@DateCréation", dat);
                                        myCommand.Parameters.AddWithValue("@Profession", textBoxProfession.Text);
                                        myCommand.Parameters.AddWithValue("@Portable", textBoxPortable.Text);
                                        myCommand.Parameters.AddWithValue("@DateModification", dat);

                                        //   myCommand.Parameters.AddWithValue("@Complement", textBoxAdresseCompl.Text);



                                        myCommand.ExecuteNonQuery();
                                        MessageBox.Show("Modification effectué avec succès");
                                        ClearClass1 clr1 = new ClearClass1();
                                        clr1.RecursiveClearTextBoxes(this.panel2.Controls);
                                        ClearClass1 clr = new ClearClass1();
                                        clr.RecursiveClearTextBoxes(this.panel3.Controls);
                                        dateTimePickerancni.Checked = false;
                                        dateTimePickernvcni.Checked = false;
                                        dateTimePickerDOB.Checked = false;

                                    }
                                    else
                                    {
                                        string sCon = conn.connectdb();
                                        string insertCmd = "update ElecteurOrdonance set localitéID=@localitéID,RégionID=@RégionID,Nom1=@Nom1,Nom2=@Nom2,Nom3=@Nom3,NomMère1=@NomMère1,NomMère2=@NomMère2,NomMère3=@NomMère3,DOB=@DOB,POB=@POB,CodeElecteur=@CodeElecteur,AncienCNI=@AncienCNI,DateAncienCNI=@DateAncienCNI,NewCNI=@NewCNI,DateNewCNI=@DateNewCNI,AgentModif=@AgentModif,Sexe=@Sexe,Région=@Région,DateModification=@DateModification,Localité=@Localité,Profession=@Profession,Portable=@Portable where ElecteurOrdonanceID=" + clause3;
                                        SqlConnection dbConn;
                                        dbConn = new SqlConnection(sCon);
                                        dbConn.Open();
                                        Login lg = new Login();
                                        //string str;
                                        string dat = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                        DateTime date4 = DateTime.Parse(dat);

                                        GetValue value = new GetValue();
                                        //getAdress add = new getAdress();
                                        SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                                        // Create parameters for the SqlCommand object
                                        // initialize with input-form field values
                                        //myCommand.Parameters.AddWithValue("@ElecteurID","");
                                        myCommand.Parameters.AddWithValue("@Nom1", textBoxNom1.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@Nom2", textBoxNom2.Text.ToUpper());

                                        myCommand.Parameters.AddWithValue("@Nom3", textBoxNom3.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@NomMère1", textBoxNomMère1.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@NomMère2", textBoxNomMère2.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@NomMère3", textBoxNomMère3.Text.ToUpper());
                                        if (dateTimePickerDOB.Checked)
                                        {
                                            myCommand.Parameters.AddWithValue("@DOB", Date3);
                                        }
                                        else
                                        {
                                            myCommand.Parameters.AddWithValue("@DOB", DBNull.Value);
                                        }
                                        myCommand.Parameters.AddWithValue("@POB", textBoxPOB.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@CodeElecteur", textBoxCodeElecteur.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@AncienCNI", textBoxAncCNI.Text.ToUpper());
                                        if (dateTimePickerancni.Checked)
                                        {
                                            myCommand.Parameters.AddWithValue("@DateAncienCNI", Date1);
                                        }
                                        else
                                        {
                                            myCommand.Parameters.AddWithValue("@DateAncienCNI", DBNull.Value);
                                        }
                                        myCommand.Parameters.AddWithValue("@NewCNI", textBoxNewCNI.Text.ToUpper());
                                        if (dateTimePickernvcni.Checked)
                                        {
                                            myCommand.Parameters.AddWithValue("@DateNewCNI", Date2);
                                        }
                                        else
                                        {
                                            myCommand.Parameters.AddWithValue("@DateNewCNI", DBNull.Value);
                                        }

                                     //   myCommand.Parameters.AddWithValue("@AgentSaisie", value.getAgentSaisie());//recupere agent saisie
                                        myCommand.Parameters.AddWithValue("@AgentModif", value.getAgentModif());//recupere agent modif
                                       // myCommand.Parameters.AddWithValue("@AgentCréation", value.getAgentCréation());//recupere agent de creation

                                        //  myCommand.Parameters.AddWithValue("@Téléphone", textBoxPhone.Text.ToUpper());

                                       // myCommand.Parameters.AddWithValue("@AgentID", value.getAgentID());//recupere l'ID agent
                                        myCommand.Parameters.AddWithValue("@Sexe", comboBoxSexe.Text);

                                        myCommand.Parameters.AddWithValue("@Région", comboBoxRégion.Text);
                                        myCommand.Parameters.AddWithValue("@RégionID", Int32.Parse(comboBoxRégion.SelectedValue.ToString()));


                                        myCommand.Parameters.AddWithValue("@DateCréation", dat);
                                        myCommand.Parameters.AddWithValue("@Profession", textBoxProfession.Text);
                                        myCommand.Parameters.AddWithValue("@Portable", textBoxPortable.Text);
                                        myCommand.Parameters.AddWithValue("@DateModification", dat);

                                        myCommand.Parameters.AddWithValue("@Localité", comboBoxLoc.Text);
                                        myCommand.Parameters.AddWithValue("@localitéID", Int32.Parse(comboBoxLoc.SelectedValue.ToString()));

                                        //   myCommand.Parameters.AddWithValue("@Complement", textBoxAdresseCompl.Text);



                                        myCommand.ExecuteNonQuery();
                                        MessageBox.Show("Modification effectué avec succès");
                                        ClearClass1 clr1 = new ClearClass1();
                                        clr1.RecursiveClearTextBoxes(this.panel2.Controls);
                                        ClearClass1 clr = new ClearClass1();
                                        clr.RecursiveClearTextBoxes(this.panel3.Controls);
                                        dateTimePickerancni.Checked = false;
                                        dateTimePickernvcni.Checked = false;
                                        dateTimePickerDOB.Checked = false;


                                    }
                                }
                                catch (SqlException ex)
                                {
                                    MessageBox.Show("erreur  " + ex.Message);
                                }
                            }
                        }
                        else
                        {
                            MessageBox.Show("Veuillez saisir la date de la nouvelle CNI ou la date de l'ancienne CNI");
                        }
                    }
                }
            }

        }
    }
}
