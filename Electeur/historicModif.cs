﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class historicModif : Form
    {
        int electeurID;
        Connection conn = new Connection();
        public historicModif(int ID)
        {
            InitializeComponent();
            electeurID= ID;
        }

        private void historicModif_Load(object sender, EventArgs e)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            string str = "Select TypeAction AS 'ACTION', DateAction AS 'DATE', NomAgent AS 'PAR',CentredeRetrait as'Lieu de Rétrait' from hist where ElecteurID =" + electeurID;
            SqlDataAdapter sqlAdapter = new SqlDataAdapter(str, con);
            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sqlAdapter);

            DataTable table = new DataTable();
            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
            sqlAdapter.Fill(table);
            dataGridViewhist.DataSource = table;
            //this.dataGridView1.Columns[0].Visible = false;

            con.Open();
            //electeurID.ToString() = null;
        }

        
    }
}
