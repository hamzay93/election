﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class LitRetrait : Form
    {
        List<int> bureaus = new List<int>();
        public LitRetrait()
        {
            InitializeComponent();
        }
        string clause;
        string clause1 = null;
        string str = null;
        string param,param2 = null;

        public void IsEmptyfield()
        {

            foreach (Control X in this.panel1.Controls)
            {
                if (X is ComboBox)
                {
                    if (X.Text.Trim().Length != 0 && X.Name != "comboBoxRégion" && X.Name != "AgentSaisie")
                    {
                        if (string.IsNullOrEmpty(clause))
                        {
                            if (X.Name == "comboBoxCommune" && comboBoxArrondis.Text.Length==0)
                            {
                                if (radioButtonCV.Checked)
                                {
                                    clause = "NomCommuneCV ='" + comboBoxCommune.Text.ToString() + "'";
                                }
                                else
                                {
                                    clause = "NomCommune ='" + comboBoxCommune.Text.ToString() + "'";
                                }
                                //clause =  X.Tag.ToString().Trim();
                            }
                            else
                            {
                                if (X.Name == "comboBoxArrondis")
                                {
                                    if (radioButtonCV.Checked)
                                    {
                                        clause = "ArrondisCV ='" + comboBoxArrondis.Text.ToString() + "'";
                                    }
                                    else
                                    {
                                        clause = "NomArrondis ='" + comboBoxArrondis.Text.ToString() + "'";
                                    }
                                    //clause = X.Tag.ToString().Trim();
                                }
                                //clause = "NomCommuneCV =" + comboBoxCommune.Text.ToString();
                            }
                        }
                        else
                        {
                            if (X.Name == "comboBoxArrondis")
                            {
                                if (radioButtonCV.Checked)
                                {
                                    clause = "ArrondisCV ='"+ comboBoxArrondis.Text.ToString()+"'";
                                }
                                else
                                {
                                    clause = "NomArrondis ='" + comboBoxArrondis.Text.ToString() + "'";
                                }
                                //clause = X.Tag.ToString().Trim();
                            }
                            else
                            {
                                //clause = X.Tag.ToString().Trim();
                            }
                        }
                    }

                }
                else
                {
                    if (X is DateTimePicker)
                    {
                        if (X.Name == "dateTimePicker1")
                        {
                            if (string.IsNullOrEmpty(clause1))
                            {
                                if (dateTimePicker1.Checked)
                                {
                                    if (TimePicker1.Checked && dateTimePicker1.Checked)
                                    {
                                        string time = TimePicker1.Value.TimeOfDay.ToString();
                                        string daate = dateTimePicker1.Value.Date.ToString("yyyy-MM-dd") + " " + time;
                                        DateTime datefull = DateTime.Parse(daate);
                                        clause1 = clause1 + " AND " + X.Tag + " >= '" + daate + "'";
                                    }
                                    else
                                    {
                                        //string time = "00:00:00:000";
                                        string dat = dateTimePicker1.Value.ToString("yyyy-MM-dd");
                                        DateTime date4 = DateTime.Parse(dat);
                                        clause1 = clause1 + " AND " + X.Tag + " >= '" + dat + "'";
                                    }

                                }


                            }
                            else
                            {
                                if (dateTimePicker1.Checked)
                                {
                                    if (TimePicker1.Checked && dateTimePicker1.Checked)
                                    {
                                        string time = TimePicker1.Value.TimeOfDay.ToString();
                                        string daate = dateTimePicker1.Value.Date.ToString("yyyy-MM-dd") + " " + time;
                                        DateTime datefull = DateTime.Parse(daate);
                                        clause1 = clause1 + " AND " + X.Tag + " >= '" + daate + "'";
                                    }
                                    else
                                    {
                                        string dat = dateTimePicker1.Value.ToString("yyyy-MM-dd");
                                        clause1 = clause1 + " AND " + X.Tag + " >= '" + dat + "'";
                                    }

                                }

                            }
                        }
                        if (X.Name == "dateTimePicker2")
                        {
                            if (string.IsNullOrEmpty(clause1))
                            {
                                if (dateTimePicker2.Checked)
                                {
                                    if (TimePicker2.Checked && dateTimePicker2.Checked)
                                    {
                                        string time = TimePicker2.Value.TimeOfDay.ToString();
                                        string daate = dateTimePicker2.Value.Date.ToString("yyyy-MM-dd") + " " + time;
                                        DateTime datefull = DateTime.Parse(daate);
                                        clause1 = clause1 + " AND " + X.Tag + " <= '" + daate + "'";
                                    }
                                    else
                                    {
                                        string dat = dateTimePicker2.Value.ToString("yyyy-MM-dd");
                                        clause1 = clause1 + " AND " + X.Tag + " <= '" + dat + "'";
                                    }
                                }

                            }
                            else
                            {
                                if (dateTimePicker2.Checked)
                                {
                                    if (TimePicker2.Checked && dateTimePicker2.Checked)
                                    {
                                        string time = TimePicker2.Value.TimeOfDay.ToString();
                                        string daate = dateTimePicker2.Value.Date.ToString("yyyy-MM-dd") + " " + time;
                                        DateTime datefull = DateTime.Parse(daate);
                                        clause1 = clause1 + " AND " + X.Tag + " <= '" + daate + "'";
                                    }
                                    else
                                    {
                                        string dat = dateTimePicker2.Value.ToString("yyyy-MM-dd");
                                        clause1 = clause1 + " AND " + X.Tag + " <= '" + dat + "'";
                                    }
                                }
                            }
                        }
                      
                    }                 
                }
            }
        }
        Connection conn = new Connection();

        public void ischecked()
        {
            if (checkBoxArr.Checked)
            {
                if (radioButtonCV.Checked)
	            {
		            param = "ArrondisCV";
                    param2 = "ArrondisCV";
	            }
                else
	            {
                    param = "NomArrondis";
                    param2 = "NomArrondis";
	            }
                
            }
            if (checkBoxCV.Checked)
            {
                    if (String.IsNullOrEmpty(param))
                    {
                        param = "CentreVote";
                        param2 = "CentreVote";
                    }
                    else
                    {
                        param = param + ",CentreVote";
                        param2 = param2 + ",CentreVote";
                    }
            }
            if (checkBoxSex.Checked)
            {
                if (String.IsNullOrEmpty(param))
                {
                    param = "Sexe";
                    param2 = "Sexe";
                }
                else
                {
                    param = param + ",Sexe";
                    param2 = param2 + ",Sexe";
                }
            }
            if (checkBoxDR.Checked)
            {
                if (String.IsNullOrEmpty(param))
                {
                    param = "Convert(Varchar(10),DateRetrait,103) as Date";
                    param2 = "Convert(Varchar(10),DateRetrait,103)";
                }
                else
                {
                    param = param + ",Convert(Varchar(10),DateRetrait,103) as Date";
                    param2 = param2 + ",Convert(Varchar(10),DateRetrait,103)";
                }
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            param = null;
            param2 = null;
            this.ischecked();
            this.IsEmptyfield();
            if (String.IsNullOrEmpty(clause))
            {
                clause = "Nom1 like '%'";
            }
            str = "Select "+param + ",Count(*) as Total from View_reg_aff_st where " + clause + clause1 + " Group by " + param2;
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            /*str = "select DateRetrait,  Count(case when ArrondisID = 10006 then (ArrondisID) end) Arrondis5,  Count(case when ArrondisID = 10001 then (ArrondisID) end) Arrondis1,"
  +"Count(case when ArrondisID = 10002 then (ArrondisID) end) Arrondis2,"
  +"Count(case when ArrondisID = 10003 then (ArrondisID) end) Arrondis3,"+
  "Count(case when ArrondisID = 10005 then (ArrondisID) end) Arrondis4,"+
 " Count(case when ArrondisID = 10007 then (ArrondisID) end) Arrondis6  from Retrait  Group by DateRetrait";*/
            con.Open();
            SqlDataAdapter sqlAdapter = new SqlDataAdapter(str, con);
            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sqlAdapter);
            con.Close();
            DataTable table = new DataTable();
            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
            sqlAdapter.Fill(table);
            dataGridView1.DataSource = table;
            //dataGridView1.Columns["ElecteurID"].Visible = false;
            int total = dataGridView1.RowCount;
            label14.Visible = false;
            label14.Text = total.ToString();
            clause = "";
            clause1 = "";
            //clause2 = "";
                
        }
        //private void FillAdresse(int ArrondisID)
        //{
        //    string sCon = conn.connectdb();
        //    SqlConnection con = new SqlConnection(sCon);
        //    SqlCommand cmd = new SqlCommand();
        //    cmd.Connection = con;
        //    cmd.CommandType = CommandType.Text;
        //    cmd.CommandText = "SELECT AdresseID, NomAdresse FROM Adresse WHERE ArrondisID =@ArrondisID";
        //    cmd.Parameters.AddWithValue("@ArrondisID", ArrondisID);
        //    DataSet objDs = new DataSet();
        //    SqlDataAdapter dAdapter = new SqlDataAdapter();
        //    dAdapter.SelectCommand = cmd;
        //    con.Open();
        //    dAdapter.Fill(objDs);
        //    if (ArrondisID == 30)
        //    {
        //        objDs.Tables[0].Rows.Add(30, "");
        //    }
        //    //objDs.Tables[0].Rows.Add(30, "");
        //    con.Close();
        //    if (objDs.Tables[0].Rows.Count > 0)
        //    {
        //        comboBoxAdresse.ValueMember = "AdresseID";
        //        comboBoxAdresse.DisplayMember = "NomAdresse";
        //        comboBoxAdresse.DataSource = objDs.Tables[0];
        //        if (ArrondisID == 30)
        //        {
        //            comboBoxAdresse.SelectedValue = 30;
        //        }
        //        //comboBoxArrondis.SelectedValue = 30;
        //    }
        //}
        //private void FillAgent()
        //{
            
        //    string sCon = conn.connectdb();
        //    SqlConnection con = new SqlConnection(sCon);
        //    SqlCommand cmd = new SqlCommand();
        //    cmd.Connection = con;
        //    cmd.CommandType = CommandType.Text;
        //    cmd.CommandText = "SELECT  AgentID,NomAgent FROM Agent order by NomAgent";
        //    DataSet objDs = new DataSet();
        //    SqlDataAdapter dAdapter = new SqlDataAdapter();
        //    dAdapter.SelectCommand = cmd;
        //    con.Open();
        //    dAdapter.Fill(objDs);
        //    objDs.Tables[0].Rows.Add(30, "");
        //    con.Close();
        //    comboBoxAgent.ValueMember = "AgentID";
        //    comboBoxAgent.DisplayMember = "NomAgent";
        //    comboBoxAgent.DataSource = objDs.Tables[0];
        //    comboBoxAgent.SelectedValue = 30;
           
        //}

        //private void comboBoxAgent_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (comboBoxAgent.SelectedIndex >= 0)
        //    {
        //        //dataGridViewb.Rows.Clear();
        //        //dataGridViewb.Enabled = false;
        //        //bureaus.Clear();
        //        //listBox1.Items.Clear();

        //        //dateTimePicker1.Checked = false;
        //        //dateTimePicker2.Checked = false;
        //    }
        //}

        
        private void FillRégion()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT RégionID, NomRégion FROM Région";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBoxRégion.ValueMember = "RégionID";
            comboBoxRégion.DisplayMember = "NomRégion";
            comboBoxRégion.DataSource = objDs.Tables[0];
        }

        private void FillCommune(int RégionID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT CommuneID, NomCommune FROM Commune WHERE RégionID =@RégionID";
            if (RégionID == 30)
            {
                int reg = 1;
                cmd.Parameters.AddWithValue("@RégionID", reg);


            }
            else
            {
                cmd.Parameters.AddWithValue("@RégionID", RégionID);
            }

            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            objDs.Tables[0].Rows.Add(30, "");
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxCommune.Enabled = true;
                comboBoxCommune.ValueMember = "CommuneID";
                comboBoxCommune.DisplayMember = "NomCommune";
                comboBoxCommune.DataSource = objDs.Tables[0];
                comboBoxCommune.SelectedValue = 30;
            }
            else
            {
                comboBoxCommune.Enabled = false;
            }
        }
        private void FillArrondis(int CommuneID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT ArrondisID, NomArrondis FROM Arrondissement WHERE CommuneID =@CommuneID";
            if (CommuneID == 30)
            {
                int com = 1;
                cmd.Parameters.AddWithValue("@CommuneID", com);
                comboBoxArrondis.Enabled = false;


            }
            else
            {
                cmd.Parameters.AddWithValue("@CommuneID", CommuneID);
                comboBoxArrondis.Enabled = true;
            }
            //cmd.Parameters.AddWithValue("@CommuneID", CommuneID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            objDs.Tables[0].Rows.Add(30, "");
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxArrondis.ValueMember = "ArrondisID";
                comboBoxArrondis.DisplayMember = "NomArrondis";
                comboBoxArrondis.DataSource = objDs.Tables[0];
                comboBoxArrondis.SelectedValue = 30;


            }
        }

     
        //private void FillCenter(int ArrondisID)
        //{
        //    string sCon = conn.connectdb();
        //    SqlConnection con = new SqlConnection(sCon);
        //    SqlCommand cmd = new SqlCommand();
        //    cmd.Connection = con;
        //    cmd.CommandType = CommandType.Text;
        //    cmd.CommandText = "SELECT CentreVote,CentreID FROM  Arrondissement AS a INNER JOIN CentreVote AS c ON a.ArrondisID = c.ArrondisID where a.ArrondisID=@ArrondisID";

        //    cmd.Parameters.AddWithValue("@ArrondisID", ArrondisID);
        //    DataSet objDs = new DataSet();
        //    SqlDataAdapter dAdapter = new SqlDataAdapter();
        //    dAdapter.SelectCommand = cmd;
        //    con.Open();
        //    dAdapter.Fill(objDs);
        //    if (ArrondisID == 30)
        //    {
        //        objDs.Tables[0].Rows.Add("", 30);
        //    }
        //    //
        //    con.Close();
        //    comboBoxCV.ValueMember = "CentreID";
        //    comboBoxCV.DisplayMember = "CentreVote";
        //    comboBoxCV.DataSource = objDs.Tables[0];
        //    //comboBoxCV.SelectedValue = 30;
        //    if (ArrondisID == 30)
        //    {
        //        comboBoxCV.SelectedValue = 30;
        //    }




        //}
   

        private void LitRetrait_Load(object sender, EventArgs e)
        {
            //FillAgent();
            FillRégion();
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //dataGridViewb.Rows.Clear();
            //dataGridView1.Rows.Clear();
            //dataGridViewb.Enabled = false;
            //bureaus.Clear();
            //listBox1.Items.Clear();
           
            // dateTimePicker1.Checked = false;
            //dateTimePicker2.Checked = false;
        }
        private void comboBoxCommune_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxCommune.SelectedValue.ToString() != "")
            {
                int CommuneID = Convert.ToInt32(comboBoxCommune.SelectedValue.ToString());
                this.FillArrondis(CommuneID);
                //comboBoxArrondis.SelectedIndex = 0;
            }
        }
        //private void comboBoxArrondis_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (comboBoxArrondis.SelectedValue.ToString() != "")
        //    {
        //        int ArrondisID = Convert.ToInt32(comboBoxArrondis.SelectedValue.ToString());
        //        if (radioButtonCV.Checked)
        //        {
        //            this.FillCenter(ArrondisID);
        //            comboBoxCV.Visible = true;
        //            comboBoxAdresse.Visible = false;
        //            label2.Text = "Centre de vote";
        //        }
        //        else
        //        {
        //            this.FillAdresse(ArrondisID);
        //            comboBoxCV.Visible = false;
        //            comboBoxAdresse.Visible = true;
        //            label2.Text = "Adresse";
        //        }
        //        //comboBoxAdresse.SelectedIndex = 0;
        //    }
        //}

        private void dataGridViewb_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //int rowindex = dataGridViewb.CurrentRow.Index;
            //if ((bool)dataGridViewb[1, rowindex].Value == true)
            //{
            //    dataGridViewb.Rows[e.RowIndex].Cells[1].Value = false;
            //}
            //else
            //{
            //    dataGridViewb.Rows[e.RowIndex].Cells[1].Value = true;
            //}
            //if ((bool)dataGridViewb[1, rowindex].Value == true)
            //{
            //    listBox1.Items.Add((string)dataGridViewb[2, rowindex].Value);
            //    bureaus.Add(Convert.ToInt32((string)dataGridViewb[0, rowindex].Value));
            //}
            //else if ((bool)dataGridViewb[1, rowindex].Value == false)
            //{
            //    listBox1.Items.Remove((string)dataGridViewb[2, rowindex].Value);
            //    bureaus.Remove(Convert.ToInt32((string)dataGridViewb[0, rowindex].Value));
            //}
        }

        private void comboBoxRégion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxRégion.SelectedValue.ToString() != "")
            {
                int RegionID = Convert.ToInt32(comboBoxRégion.SelectedValue.ToString());
                this.FillCommune(RegionID);
                
                //comboBoxArrondis.SelectedIndex = 0;
            }
        }
    }
}
