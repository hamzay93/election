﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class Rechecher : Form
    {
        public Rechecher()
        {
            InitializeComponent();
        }
        string clause;
        int RégionName, ArrondisName, CommuneName, strID;
        Connection conn = new Connection();
        GetValue get = new GetValue();
        //int profID;
        private void FillRégion()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT RégionID, NomRégion FROM Région";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBoxRégion.ValueMember = "RégionID";
            comboBoxRégion.DisplayMember = "NomRégion";
            comboBoxRégion.DataSource = objDs.Tables[0];
        }

        public void IsEmptyfield()
        {
            clause = null;
            foreach (Control X in this.panel1.Controls)
            {

                if (X is TextBox || X is ComboBox )
                {
                    if (X.Tag.ToString() == "AncienCNI" || X.Tag.ToString() == "NewCNI")
                    {
                        if (X.Text.Trim().Length != 0)
                        {

                            if (string.IsNullOrEmpty(clause))
                            {
                                clause = clause + X.Tag + " = '" + X.Text.Trim() + "'";
                                //return clause;
                            }
                            else
                            {
                                clause = clause + " And " + X.Tag + " = '" + X.Text.Trim() + "'";
                                //return clause;
                            }
                        }
                    }
                    else
                    {
                        if (X.Text.Trim().Length != 0)
                        {
                            if (string.IsNullOrEmpty(clause))
                            {
                                clause = clause + X.Tag + " Like '%" + X.Text.Trim() + "%'";
                                //return clause;
                            }
                            else
                            {
                                clause = clause + " And " + X.Tag + " Like '%" + X.Text.Trim() + "%'";
                                //return clause;
                            }
                        }
                    }

                }
                else
                {
                    if (X is DateTimePicker)
                    {
                        if (X.Name == "dateTimePicker1")
                        {
                            if (string.IsNullOrEmpty(clause))
                            {
                                if (dateTimePicker1.Checked)
                                {
                                    string dat = dateTimePicker1.Value.ToString("yyyy-MM-dd");
                                    DateTime date4 = DateTime.Parse(dat);
                                    clause = clause  + X.Tag + " Like '%" + dat + "%'";
                                }

                            }
                            else
                            {
                                if (dateTimePicker1.Checked)
                                {
                                    string dat = dateTimePicker1.Value.ToString("yyyy-MM-dd");
                                    DateTime date4 = DateTime.Parse(dat);
                                    clause = clause + " AND " + X.Tag + " Like '%" + dat + "%'";
                                }
 
                            }
                        }
                    }
                }
            }
            
        }
        /*public int getAdresseID()
        {
            //SqlDataReader rd = null;
            string query = "select AdresseID from Adresse where NomAdresse='" + comboBox8.Text + "'";
            string sCon = @"Data Source=localhost\SQL2008; Initial Catalog=Electeur; Integrated Security = true";
            SqlConnection dbConn;
            dbConn = new SqlConnection(sCon);
            dbConn.Open();
            SqlCommand cmd = new SqlCommand(query, dbConn);
            int result = ((int)cmd.ExecuteScalar());
            //SaisieElecteur se=new SaisieElecteur();
            int adress = result;
            return adress;

        {
        }*/
        private void button1_Click(object sender, EventArgs e)
        {
            this.FillRégion();
            this.IsEmptyfield();
            if (String.IsNullOrEmpty(clause))
            {
                MessageBox.Show("veuillez remplir un champ au minimum");
                this.Focus();

            }
            else
            {
                string sCon = conn.connectdb();
                SqlConnection con = new SqlConnection(sCon);
                string str = "SET CONCAT_NULL_YIELDS_NULL OFF; SELECT ElecteurID, Nom1 +'  '+ Nom2 +'  '+Nom3 AS NOM,NomMère1+'  '+NomMère2 AS 'NOM DE LA MERE',DOB AS 'DATE DE NAISSANCE',POB AS LIEU,CodeElecteur AS 'CODE ELECTEUR',AncienCNI AS 'ANCIEN CNI',DateAncienCNI AS 'DATE DELIVRANCE',NewCNI AS 'NOUVEAU CNI',DateNewCNI AS 'DATE DELIVRANCE',Sexe AS 'SEXE',AdresseID,StatutID FROM Electeur where " + clause+"and ElecteurID not in (Select ElecteurID from Radiation where Statut='Actif')";
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(str, con);
                SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sqlAdapter);

                DataTable table = new DataTable();
                table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                sqlAdapter.Fill(table);
                dataGridView1.DataSource = table;
                if (dataGridView1.Rows.Count == 0)
                {
                    button2.Enabled = false;
                    panel2.Visible = false;
                    panel3.Visible = false;
                    button4.Enabled = false;
                    label10.Enabled = false;
                    MessageBox.Show("Aucun electeur retrouvé pour ces critères");

                }
                else
                {
                    label10.Enabled = true;
                    button2.Enabled = true;
                    panel2.Visible = true;
                    panel3.Visible = true;
                    button4.Enabled = true;
                }
               
                this.dataGridView1.Columns["ElecteurID"].Visible = false;
                this.dataGridView1.Columns["AdresseID"].Visible = false;
                this.dataGridView1.Columns["StatutID"].Visible = false;

                con.Open();
                clause = "";
            }
        }
        string ID;
        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            // Update the labels to reflect changes to the selection.
            //UpdateLabelText();
            this.filldetails();

            

            
        }
        /*private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            foreach (DataGridViewRow r in dataGridView1.SelectedRows)
            {
                if (e.RowIndex >= 0)
                {
                    DataGridViewRow row = this.dataGridView1.Rows[e.RowIndex];
                    ID = ID + row[0].Cells[0].Value.ToString();
                    row.Selected = false;
                    //add = row.Cells[20].Value.ToString();
                }
            }
        }*/
        string adresseID = null;
        private void button2_Click(object sender, EventArgs e)
        {
            int counter;
            string clause1 = null;
            
            adresseID = null;
            if (dataGridView1.Rows.Count > 0)
            {
                // Iterate through all the rows and sum up the appropriate columns. 
                for (counter = 0; counter < (dataGridView1.Rows.Count);
                    counter++)
                {
                    //dataGridView1.Rows.
                    if (dataGridView1.Rows[counter].Selected)
                    {
                        if (dataGridView1.Rows[counter].Cells["ElecteurID"].Value
                        != null)
                        {
                            if (dataGridView1.Rows[counter].
                                Cells["ElecteurID"].Value.ToString().Length != 0)
                            {
                                if (string.IsNullOrEmpty(clause1))
                                {
                                    clause1 = clause1 + int.Parse(dataGridView1.Rows[counter].
                                        Cells["ElecteurID"].Value.ToString());
                                }
                                else
                                {
                                    clause1 = clause1 + "," + int.Parse(dataGridView1.Rows[counter].
                                    Cells["ElecteurID"].Value.ToString());

                                }

                            }
                        }
                        if (dataGridView1.Rows[counter].
                                Cells["AdresseID"].Value.ToString().Length != 0)
                        {
                            if (string.IsNullOrEmpty(adresseID))
                            {
                                adresseID = adresseID + int.Parse(dataGridView1.Rows[counter].
                                    Cells["AdresseID"].Value.ToString());
                                break;
                            }
                            else
                            {
                                adresseID = adresseID + "," + int.Parse(dataGridView1.Rows[counter].
                                Cells["AdresseID"].Value.ToString());
                                break;
                            }
                        }


                    }

                }
                string btnName =  null;
                if (button2.Text == "Mettre à jour")
                {
                    btnName = "Mettre à jour";
                }
                else
                {
                    btnName = "Modifier";
                }
                SqlDataReader rd = null;
                string query = "select * from View_reg_aff where ElecteurID ="+clause1;
                string sCon = conn.connectdb();
                SqlConnection dbConn;
                dbConn = new SqlConnection(sCon);
                dbConn.Open();
                SqlCommand cmd = new SqlCommand(query, dbConn);
                rd = cmd.ExecuteReader();
                if (!rd.Read())
                {
                    MessageBox.Show("Vous n'êtes pas authorisés à modifier");

                }
                else
                {
                string test = clause1;
                string add1 = adresseID;
                //Form1 fr = new Form1();
                Modif mod = new Modif(test, add1, btnName);
                mod.MdiParent = this.ParentForm;
                mod.Show();
                    }
            }
        }

        private void Rechecher_Load(object sender, EventArgs e)
        {
            dataGridView1.SelectionChanged += new EventHandler(dataGridView1_SelectionChanged);
            this.panel3.Visible = false;
            this.panel2.Visible = false;
            
            int Profil = get.getProfilID();
            if (Profil == 8)
            {
                button2.Enabled = false;                
            }
            if (Profil != 1)
            {
                button4.Visible = false;
            }
            if (Profil == 10 || Profil == 1)
            {
                button4.Visible = false;
                button2.Visible = true;

            }
        }
        string clause3 ;
        private void filldetails()
        {
            
            this.selection();
            if (String.IsNullOrEmpty(clause3))
            {
                this.panel3.Visible = false;
                this.panel2.Visible = false;
            }
            else
            {
                this.panel3.Visible = true;
                this.panel2.Visible = true;
                string sCon = conn.connectdb();
                SqlConnection con = new SqlConnection(sCon);
                string str = "SELECT * FROM View_reg_aff where ElecteurID= " + clause3;
                SqlCommand cmd = new SqlCommand(str, con);
                con.Open();
                
                using (SqlDataReader read = cmd.ExecuteReader())

                    while (read.Read())
                    {
                        textBoxNom1.Text = (read["Nom1"].ToString());
                        textBoxNom2.Text = (read["Nom2"].ToString());
                        textBoxNom3.Text = (read["Nom3"].ToString());
                        textBoxNomMère1.Text = (read["NomMère1"].ToString());
                        textBoxNomMère2.Text = (read["NomMère2"].ToString());
                        textBoxNomMère3.Text = (read["NomMère3"].ToString());
                        int colIndex3 = (read.GetOrdinal("DOB"));
                        if (!read.IsDBNull(colIndex3))
                            dateTimePickerDOB.Value = (read.GetDateTime(colIndex3));
                        //dateTimePickerDOB.TextDOB.Text = (read["DOB"].ToString());
                        textBoxPOB.Text = (read["POB"].ToString());
                        textBoxCodeElecteur.Text = (read["CodeElecteur"].ToString());
                        textBoxAncCNI.Text = (read["AncienCNI"].ToString());

                        int colIndex = (read.GetOrdinal("DateAncienCNI"));
                        if (!read.IsDBNull(colIndex))
                            textBox7.Text = (read.GetDateTime(colIndex)).ToString("yyyy-MM-dd");
                        
                        int colIndex2 = (read.GetOrdinal("DateNewCNI"));
                        if (!read.IsDBNull(colIndex2))

                            textBox11.Text = (read.GetDateTime(colIndex2)).ToString("yyyy-MM-dd");
                        textBoxNewCNI.Text = (read["NewCNI"].ToString());
                        textBoxCodeElecteur.Text = (read["CodeElecteur"].ToString());
                        comboBoxSexe.SelectedItem = (read["Sexe"].ToString());
                        textBoxNbureau.Text = (read["CodeBureau"].ToString());
                        string CV = (read["CentreID"].ToString());
                        string bur = (read["BureauID"].ToString());
                        if (!String.IsNullOrEmpty(bur))
                        {
                            int Nbureau = Int32.Parse(bur);
                            textBoxBureau.Text = this.getBureau(Nbureau);
                        }
                        else
                        {
                            textBoxBureau.Text = "";
                        }
                        if (!String.IsNullOrEmpty(CV))
                        {
                            int NCV = Int32.Parse(CV);
                            textBoxCV.Text = this.getCV(NCV);
                        }
                        else
                        {
                            textBoxCV.Text = "";
                        }
                        //statutID = (read["StatutID"].ToString());
                        //LabelStatut.Text = this.getStatutName();
                        //adresseID = this.getAdresseID();
                        //IDRégion = (read["RégionID"].ToString());
                        comboBoxRégion.SelectedValue = (read["RégionID"].ToString());
                        //comboBoxCommune.SelectedValue = (read["CommuneID"].ToString());
                        comboBoxArrondis.SelectedValue = (read["ArrondisID"].ToString());
                        comboBoxAdresse.SelectedValue = (read["AdresseID"].ToString());

                    }
            }

        }
        string adresseID1, StatutID;
        private void selection()
        {
            int counter3;
            clause3 = null;
            StatutID = null;
            adresseID1 = null;
            // Iterate through all the rows and sum up the appropriate columns. 
            for (counter3 = 0; counter3 < (dataGridView1.Rows.Count);
                counter3++)
            {
                //dataGridView1.Rows.
                if (dataGridView1.Rows[counter3].Selected)
                {
                    if (dataGridView1.Rows[counter3].Cells["ElecteurID"].Value
                    != null)
                    {
                        if (dataGridView1.Rows[counter3].
                            Cells["ElecteurID"].Value.ToString().Length != 0)
                        {
                            if (string.IsNullOrEmpty(clause3))
                            {
                                clause3 = clause3 + int.Parse(dataGridView1.Rows[counter3].
                                    Cells["ElecteurID"].Value.ToString());
                            }
                            else
                            {
                                clause3 = clause3 + "," + int.Parse(dataGridView1.Rows[counter3].
                                Cells["ElecteurID"].Value.ToString());

                            }

                        }
                    }
                    if (dataGridView1.Rows[counter3].
                            Cells["AdresseID"].Value.ToString().Length != 0)
                    {
                        if (string.IsNullOrEmpty(adresseID1))
                        {
                            adresseID1 = adresseID1 + int.Parse(dataGridView1.Rows[counter3].
                                Cells["AdresseID"].Value.ToString());
                            ;
                        }
                        else
                        {
                            adresseID1 = adresseID1 + "," + int.Parse(dataGridView1.Rows[counter3].
                            Cells["AdresseID"].Value.ToString());
                            ;
                        }
                    }
                    if (dataGridView1.Rows[counter3].
                            Cells["StatutID"].Value.ToString().Length != 0)
                    {
                        if (string.IsNullOrEmpty(StatutID))
                        {
                            StatutID = StatutID + int.Parse(dataGridView1.Rows[counter3].
                                Cells["StatutID"].Value.ToString());
                            strID = Int32.Parse(StatutID);
                            if (strID >= 3)
                            {
                                button2.Text = "Mettre à jour";
                            }
                            else
                            {
                                button2.Text = "Modifier";
                            }

                            break;
                        }
                        
                    }
                }

            }
        }
        public string getBureau(int ID)
        {
            //SqlDataReader rd = null;
            int Id = ID;
            string query = "select NomBureau from Bureau where BureauID='" + Id + "'";
            string sCon = conn.connectdb();
            SqlConnection dbConn;
            dbConn = new SqlConnection(sCon);
            dbConn.Open();
            SqlCommand cmd = new SqlCommand(query, dbConn);
            string result = (cmd.ExecuteScalar().ToString());
            //SaisieElecteur se=new SaisieElecteur();
            string Bureau = result;
            return Bureau;

        }
        public string getCV(int ID)
        {
            //SqlDataReader rd = null;
            int Id = ID;
            string query = "select CentreVote from CentreVote where CentreID='" + Id + "'";
            string sCon = conn.connectdb();
            SqlConnection dbConn;
            dbConn = new SqlConnection(sCon);
            dbConn.Open();
            SqlCommand cmd = new SqlCommand(query, dbConn);
            string result = (cmd.ExecuteScalar().ToString());
            //SaisieElecteur se=new SaisieElecteur();
            string Centre = result;
            return Centre;

        }
        public int getRégionName()
        {
            //SqlDataReader rd = null;
            string query = "select RégionID from Région where RégionID = (Select RégionID from Commune where CommuneID= (select CommuneID from Arrondissement where ArrondisID=(select ArrondisID from Adresse where AdresseID ='" + adresseID1 + "')))";
            string sCon = conn.connectdb();
            SqlConnection dbConn;
            dbConn = new SqlConnection(sCon);
            dbConn.Open();
            SqlCommand cmd = new SqlCommand(query, dbConn);
            int result = ((int)cmd.ExecuteScalar());
            //SaisieElecteur se=new SaisieElecteur();
            RégionName = result;
            return RégionName;

        }
        public int getArrondissement()
        {
            //SqlDataReader rd = null;
            string query = "select ArrondisID from Arrondissement where ArrondisID=(select ArrondisID from Adresse where AdresseID ='" + adresseID1 + "')";
            string sCon = conn.connectdb();
            SqlConnection dbConn;
            dbConn = new SqlConnection(sCon);
            dbConn.Open();
            SqlCommand cmd = new SqlCommand(query, dbConn);
            int result = ((int)cmd.ExecuteScalar());
            //SaisieElecteur se=new SaisieElecteur();
            ArrondisName = result;
            return ArrondisName;

        }
        public int getCommune()
        {
            //SqlDataReader rd = null;
            string query = "Select CommuneID from Commune where CommuneID= (select CommuneID from Arrondissement where ArrondisID=(select ArrondisID from Adresse where AdresseID ='" + adresseID1 + "'))";
            string sCon = conn.connectdb();
            SqlConnection dbConn;
            dbConn = new SqlConnection(sCon);
            dbConn.Open();
            SqlCommand cmd = new SqlCommand(query, dbConn);
            int result = ((int)cmd.ExecuteScalar());
            //SaisieElecteur se=new SaisieElecteur();
            CommuneName = result;
            return CommuneName;

        }
        private void FillArrondis(int CommuneID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT ArrondisID, NomArrondis FROM Arrondissement WHERE CommuneID =@CommuneID";
            cmd.Parameters.AddWithValue("@CommuneID", CommuneID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxArrondis.ValueMember = "ArrondisID";
                comboBoxArrondis.DisplayMember = "NomArrondis";
                comboBoxArrondis.DataSource = objDs.Tables[0];
            }
        }
        private void FillAdresse(int ArrondisID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT AdresseID, NomAdresse FROM Adresse WHERE ArrondisID =@ArrondisID";
            cmd.Parameters.AddWithValue("@ArrondisID", ArrondisID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxAdresse.ValueMember = "AdresseID";
                comboBoxAdresse.DisplayMember = "NomAdresse";
                comboBoxAdresse.DataSource = objDs.Tables[0];

            }
            //comboBoxAdresse.SelectedValue = adresseID1;
            //comboBoxAdresse.SelectedValue = this.getadress();
        }
        private void FillCommune(int RégionID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT CommuneID, NomCommune FROM Commune WHERE RégionID =@RégionID";
            cmd.Parameters.AddWithValue("@RégionID", RégionID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxCommune.ValueMember = "CommuneID";
                comboBoxCommune.DisplayMember = "NomCommune";
                comboBoxCommune.DataSource = objDs.Tables[0];
            }
        }
        private void comboBoxCommune_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxCommune.SelectedValue.ToString() != "")
            {
                int CommuneID = Convert.ToInt32(comboBoxCommune.SelectedValue.ToString());
                this.FillArrondis(CommuneID);
                comboBoxArrondis.SelectedIndex = 0;
            }
        }
        private void comboBoxArrondis_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxArrondis.SelectedValue.ToString() != "")
            {
                int ArrondisID = Convert.ToInt32(comboBoxArrondis.SelectedValue.ToString());
                this.FillAdresse(ArrondisID);
                //comboBoxAdresse.SelectedValue.ToString() = this.getadress().ToString();
                //comboBoxAdresse.SelectedValue.= this.getadress();
            }
        }
        private void comboBoxRégion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxRégion.SelectedValue.ToString() != "")
            {
                int RégionID = Convert.ToInt32(comboBoxRégion.SelectedValue.ToString());
                FillCommune(RégionID);
                comboBoxCommune.SelectedIndex = 0;
            }
        }
        private void textBoxNom1_KeyPress(object sender, KeyPressEventArgs e)
        {
            int isNum = 0;
            if (e.KeyChar == ',' || e.KeyChar == (char)Keys.Back)
                e.Handled = false;
            else if (int.TryParse(e.KeyChar.ToString(), out isNum))
                e.Handled = true;
        }
        private void textBoxAncCNI_KeyPress(object sender, KeyPressEventArgs e)
        {
            int isNum = 0;
            if (e.KeyChar == ',' || e.KeyChar == (char)Keys.Back)
                e.Handled = false;
            else if (!int.TryParse(e.KeyChar.ToString(), out isNum))
                e.Handled = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ClearClass1 clr = new ClearClass1();
            clr.RecursiveClearTextBoxes(this.panel1.Controls);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            int counter;
            string clause1 = null;
            string nom = null;
            adresseID = null;

            if (dataGridView1.Rows.Count > -1)
            {
                // Iterate through all the rows and sum up the appropriate columns. 
                for (counter = 0; counter < (dataGridView1.Rows.Count);
                    counter++)
                {
                    //dataGridViewrad.Rows.
                    if (dataGridView1.Rows[counter].Selected)
                    {
                        if (dataGridView1.Rows[counter].Cells["ElecteurID"].Value
                        != null)
                        {
                            if (dataGridView1.Rows[counter].
                                Cells["ElecteurID"].Value.ToString().Length != 0)
                            {
                                if (string.IsNullOrEmpty(clause1))
                                {
                                    clause1 = clause1 + int.Parse(dataGridView1.Rows[counter].
                                        Cells["ElecteurID"].Value.ToString());
                                }
                                else
                                {
                                    clause1 = clause1 + "," + int.Parse(dataGridView1.Rows[counter].
                                    Cells["ElecteurID"].Value.ToString());

                                }

                            }
                        }


                        nom = dataGridView1.Rows[counter].Cells["NOM"].Value.ToString();



                    }

                }
                SqlDataReader rd = null;
                string query = "select * from Radiation where Statut='Actif' and ElecteurID ="+clause1;
                string sCon = conn.connectdb();
                SqlConnection dbConn;
                dbConn = new SqlConnection(sCon);
                dbConn.Open();
                SqlCommand cmd = new SqlCommand(query, dbConn);
                rd = cmd.ExecuteReader();
                if (rd.Read())
                {
                    MessageBox.Show("Cet electeur est déjà radié");

                }
                else
                {
                    string btnName = nom;
                    string test = clause1;
                    //string add1 = adresseID;
                    //Form1 fr = new Form1();
                    Motif mod = new Motif(test, btnName);
                    mod.MdiParent = this.ParentForm;
                    mod.Show();
                    mod.TopMost = true;
                    mod.Activate();
                    

                }
                
            }
        
        }

        private void label10_Click(object sender, EventArgs e)
        {
            this.selection();
            int id = Int32.Parse(clause3);
            Label clickedLabel = sender as Label;
            historicModif histModif = new historicModif(id);
            histModif.Show();
        }
       

    }
}
