﻿namespace Electeur
{
    partial class chartenc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.crystalReportViewer3 = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.DRI = new System.Windows.Forms.Label();
            this.DDV = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.comboBoxhr = new System.Windows.Forms.ComboBox();
            this.comboBoxElec = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.button1.Location = new System.Drawing.Point(523, 22);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(114, 30);
            this.button1.TabIndex = 1;
            this.button1.Text = "AFFICHER";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // crystalReportViewer3
            // 
            this.crystalReportViewer3.ActiveViewIndex = -1;
            this.crystalReportViewer3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crystalReportViewer3.Cursor = System.Windows.Forms.Cursors.Default;
            this.crystalReportViewer3.Location = new System.Drawing.Point(1, 124);
            this.crystalReportViewer3.Name = "crystalReportViewer3";
            this.crystalReportViewer3.Size = new System.Drawing.Size(1566, 757);
            this.crystalReportViewer3.TabIndex = 4;
            this.crystalReportViewer3.Visible = false;
            // 
            // DRI
            // 
            this.DRI.AutoSize = true;
            this.DRI.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold);
            this.DRI.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.DRI.Location = new System.Drawing.Point(519, 80);
            this.DRI.Name = "DRI";
            this.DRI.Size = new System.Drawing.Size(434, 23);
            this.DRI.TabIndex = 22;
            this.DRI.Text = "DETAIL  STATISTIQUE  REGION DE L\'INTERIEUR";
            this.DRI.UseMnemonic = false;
            this.DRI.Visible = false;
            // 
            // DDV
            // 
            this.DDV.AutoSize = true;
            this.DDV.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold);
            this.DDV.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.DDV.Location = new System.Drawing.Point(568, 80);
            this.DDV.Name = "DDV";
            this.DDV.Size = new System.Drawing.Size(350, 23);
            this.DDV.TabIndex = 21;
            this.DDV.Text = "DETAIL STATISTIQUE DJIBOUTI VILLE";
            this.DDV.Visible = false;
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "Statistique de nombre de votant graphique à l\'echelle nationale",
            "Statistique de nombre de votant graphique par commune",
            "Statistique de nombre de votant graphique par arrondissement",
            "Statistique de nombre de votant djibouti ville détailler en tableau",
            "Statistique de nombre de votant région interne détailler en tableau"});
            this.comboBox2.Location = new System.Drawing.Point(173, 76);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(272, 21);
            this.comboBox2.TabIndex = 23;
            // 
            // comboBoxhr
            // 
            this.comboBoxhr.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxhr.FormattingEnabled = true;
            this.comboBoxhr.Items.AddRange(new object[] {
            "Statistique de nombre de votant graphique à l\'echelle nationale",
            "Statistique de nombre de votant graphique par commune",
            "Statistique de nombre de votant graphique par arrondissement",
            "Statistique de nombre de votant djibouti ville détailler en tableau",
            "Statistique de nombre de votant région interne détailler en tableau"});
            this.comboBoxhr.Location = new System.Drawing.Point(173, 44);
            this.comboBoxhr.Name = "comboBoxhr";
            this.comboBoxhr.Size = new System.Drawing.Size(272, 21);
            this.comboBoxhr.TabIndex = 24;
            // 
            // comboBoxElec
            // 
            this.comboBoxElec.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxElec.FormattingEnabled = true;
            this.comboBoxElec.Items.AddRange(new object[] {
            "Statistique de nombre de votant graphique à l\'echelle nationale",
            "Statistique de nombre de votant graphique par commune",
            "Statistique de nombre de votant graphique par arrondissement",
            "Statistique de nombre de votant djibouti ville détailler en tableau",
            "Statistique de nombre de votant région interne détailler en tableau"});
            this.comboBoxElec.Location = new System.Drawing.Point(173, 12);
            this.comboBoxElec.Name = "comboBoxElec";
            this.comboBoxElec.Size = new System.Drawing.Size(272, 21);
            this.comboBoxElec.TabIndex = 25;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Location = new System.Drawing.Point(163, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(307, 100);
            this.panel1.TabIndex = 26;
            // 
            // chartenc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.LightSeaGreen;
            this.ClientSize = new System.Drawing.Size(1387, 766);
            this.Controls.Add(this.comboBoxElec);
            this.Controls.Add(this.comboBoxhr);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.DRI);
            this.Controls.Add(this.DDV);
            this.Controls.Add(this.crystalReportViewer3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panel1);
            this.Name = "chartenc";
            this.Text = "chartenc";
            this.Load += new System.EventHandler(this.chartenc_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer crystalReportViewer3;
        private System.Windows.Forms.Label DRI;
        private System.Windows.Forms.Label DDV;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ComboBox comboBoxhr;
        private System.Windows.Forms.ComboBox comboBoxElec;
        private System.Windows.Forms.Panel panel1;
    }
}