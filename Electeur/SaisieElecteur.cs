﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class SaisieElecteur : Form
    {
        public string adresse, statutID, AgentID;
        Connection conn = new Connection();
        bool Existe = false;
        bool NotEmpty = false;
        bool imp = false;
        int cni = 0;
        string Quest;
        public SaisieElecteur()
        {
            InitializeComponent();
            this.KeyPreview = true;
            textBoxAncCNI.Validating += textBoxAncCNI_Validating;
            textBoxNewCNI.Validating += textBoxNewCNI_Validating;
            //textBoxCodeElecteur.Validating += textBoxCodeElecteur_Validating;
            comboBoxSexe.SelectedIndex = 1;
        }
        public SaisieElecteur(int cin, bool import, string inf)
        {
            Quest = inf;
            cni = cin;
            imp = import;
            InitializeComponent();
            this.KeyPreview = true;
            textBoxAncCNI.Validating += textBoxAncCNI_Validating;
            textBoxNewCNI.Validating += textBoxNewCNI_Validating;
            //textBoxCodeElecteur.Validating += textBoxCodeElecteur_Validating;
            comboBoxSexe.SelectedIndex = 1;
        }

        private void SaisieElecteur_Load(object sender, EventArgs e)
        {

            FillRégion();
            label26.Text = DateTime.Today.ToShortDateString().ToString();
            if (imp)
            {
                if (Quest =="New")
                {
                    string sCon = conn.connectdb();
                    SqlConnection con = new SqlConnection(sCon);
                    string str = "SELECT * FROM NewCIN where CIN= " + cni;
                    SqlCommand cmd = new SqlCommand(str, con);
                    con.Open();
                    //cmd.CommandType = CommandType.Text;
                    GetValue get = new GetValue();

                    using (SqlDataReader read = cmd.ExecuteReader())

                        while (read.Read())
                        {
                            textBoxNom1.Text = (read["NOM1"].ToString());
                            textBoxNom2.Text = (read["NOM2"].ToString());
                            textBoxNom3.Text = (read["NOM3"].ToString());
                            textBoxNomMère1.Text = (read["NOM1_MERE"].ToString());
                            textBoxNomMère2.Text = (read["NOM2_MERE"].ToString());
                            //textBoxNomMère3.Text = (read["NomMère3"].ToString());
                            int colIndex3 = (read.GetOrdinal("DATEDENAISSANCE"));
                            if (!read.IsDBNull(colIndex3))
                                dateTimePickerDOB.Value = (read.GetDateTime(colIndex3));
                            int colIndex2 = (read.GetOrdinal("DATEDEDEPOT"));
                            if (!read.IsDBNull(colIndex2))
                            {
                                dateTimePicker2.Value = (read.GetDateTime(colIndex2));
                                dateTimePicker2.Enabled = true;
                            }

                            textBoxNewCNI.Text = (read["CIN"].ToString());
                            comboBoxSexe.SelectedItem = (read["Sexe"].ToString());
                            /*textBoxNom1.Enabled = false;
                            textBoxNom2.Enabled = false;
                            textBoxNom3.Enabled = false;
                            textBoxNomMère1.Enabled = false;
                            textBoxNomMère2.Enabled = false;
                            textBoxNewCNI.Enabled = false;
                            comboBoxSexe.Enabled = false;
                            dateTimePicker2.Enabled = true;*/
                        }
                }
                else
                {
                    string sCon = conn.connectdb();
                    SqlConnection con = new SqlConnection(sCon);
                    string str = "SELECT * FROM AncienCIN where num_cin= " + cni;
                    SqlCommand cmd = new SqlCommand(str, con);
                    con.Open();
                    //cmd.CommandType = CommandType.Text;
                    GetValue get = new GetValue();

                    using (SqlDataReader read = cmd.ExecuteReader())

                        while (read.Read())
                        {
                            textBoxNom1.Text = (read["nom1_pers"].ToString());
                            textBoxNom2.Text = (read["nom2_pers"].ToString());
                            textBoxNom3.Text = (read["nom3_pers"].ToString());
                            textBoxNomMère1.Text = (read["nom1_mere"].ToString());
                            textBoxNomMère2.Text = (read["nom2_mere"].ToString());
                            //textBoxNomMère3.Text = (read["NomMère3"].ToString());
                            /*int colIndex3 = (read.GetOrdinal("date_na_pers"));
                            if (!read.IsDBNull(colIndex3))
                                dateTimePickerDOB.Value = (read.GetDateTime(colIndex3));*/
                            int colIndex2 = (read.GetOrdinal("date_delivrance"));
                            if (!read.IsDBNull(colIndex2))
                                dateTimePicker2.Value = (read.GetDateTime(colIndex2));
                            textBoxAncCNI.Text = (read["num_cin"].ToString());
                            comboBoxSexe.SelectedItem = (read["sexe"].ToString());
                            /*textBoxNom1.Enabled = false;
                            textBoxNom2.Enabled= false;
                            textBoxNom3.Enabled=false;
                            textBoxNomMère1.Enabled=false;
                            textBoxNomMère2.Enabled = false;
                            textBoxAncCNI.Enabled = false;
                            dateTimePicker1.Enabled = true;
                            comboBoxSexe.Enabled = false;*/
                        }
                }
                
            }
        }

        private void RecursiveClearTextBoxes(Control.ControlCollection cc)
        {
            foreach (Control ctrl in cc)
            {
                TextBox tb = ctrl as TextBox;
                if (tb != null)
                    tb.Clear();
                else
                    RecursiveClearTextBoxes(ctrl.Controls);
            }
        }
        public void checkIfExist()
        {
            if (Quest == "New" || textBoxNewCNI.Text.Trim().Length > 0)
            {


                if (textBoxNom1.Text.Trim().Length > 0)
                {
                    NotEmpty = true;
                }
                SqlDataReader rd = null;
                SqlDataReader rd1 = null;
                string query = "select * from NewCIN where CIN ='" + textBoxNewCNI.Text.Trim() + "'";
                string sCon = conn.connectdb();
                SqlConnection dbConn;
                dbConn = new SqlConnection(sCon);
                dbConn.Open();
                SqlCommand cmd = new SqlCommand(query, dbConn);
                rd = cmd.ExecuteReader();
                if (!rd.Read())
                {
                    MessageBox.Show("Ce numéro de carte n'existe pas", "ATTENTION", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    textBoxNewCNI.Focus();
                    Existe = false;

                }
                else
                {
                    rd.Close();
                    string query1 = "select * from NewCIN where CIN ='" + textBoxNewCNI.Text.Trim() + "'and NOM1 like '%" + textBoxNom1.Text.Trim() + "%' and NOM2 like '%" + textBoxNom2.Text.Trim() + "%' and NOM3 like '%" + textBoxNom3.Text.Trim() + "%'";
                    
                    //dbConn.Open();
                    SqlCommand cmd1 = new SqlCommand(query1, dbConn);
                    rd1 = cmd1.ExecuteReader();
                    if (!rd1.Read())
                    {
                        MessageBox.Show("Ce numéro de carte n'appartient pas à cette personne", "ATTENTION", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        textBoxNewCNI.Focus();
                        Existe = false;

                    }
                    else
                    {
                        Existe = true;
                    }
                    

                }
            }
            else
            {
                Existe = true;
                NotEmpty = true;

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.checkIfExist();
            if (Existe && NotEmpty)
            {

                if (dateTimePicker1.Enabled || dateTimePicker2.Enabled)
                {
                    DateTime Date1 = DateTime.Parse(dateTimePicker1.Text);
                    DateTime Date2 = DateTime.Parse(dateTimePicker2.Text);
                    DateTime Date3 = DateTime.Parse(dateTimePickerDOB.Text);
                    int result = DateTime.Compare(Date1, Date2);
                    if (dateTimePicker1.Enabled && dateTimePicker2.Enabled)
                    {
                        if (result > 0 || result == 0)
                        {
                            MessageBox.Show("La date de la nouvelle CNI ne peut être anterieure ou identique à celle de l'ancienne CNI","ATTENTION",MessageBoxButtons.OK,MessageBoxIcon.Error);
                            dateTimePicker1.Focus();
                        }
                        else
                        {

                            string sCon = conn.connectdb();
                            string insertCmd = "INSERT INTO Electeur(Nom1,Nom2,Nom3,NomMère1,NomMère2,NomMère3,DOB,POB,CodeElecteur,AncienCNI,DateAncienCNI,NewCNI,DateNewCNI,AgentSaisie,AgentCréation,AgentModif,StatutID,Téléphone,AdresseID,AgentID,Sexe,Région,Commune,Arrondissement,DateCréation,Portable,Profession,Complement) VALUES (@Nom1,@Nom2,@Nom3,@NomMère1,@NomMère2,@NomMère3,@DOB,@POB,@CodeElecteur,@AncienCNI,@DateAncienCNI,@NewCNI,@DateNewCNI,@AgentSaisie,@AgentCréation,@AgentModif,@StatutID,@Téléphone,@AdresseID,@AgentID,@Sexe,@Région,@Commune,@Arrondissement,@DateCréation,@Portable,@Profession,@Complement)";
                            SqlConnection dbConn;
                            dbConn = new SqlConnection(sCon);
                            dbConn.Open();
                            Login lg = new Login();
                            //string str;
                            string dat = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                            DateTime date4 = DateTime.Parse(dat);
                            adresse = comboBoxAdresse.Text;
                            statutID = LabelStatut.Text;
                            GetValue value = new GetValue();
                            //getAdress add = new getAdress();
                            SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                            // Create parameters for the SqlCommand object
                            // initialize with input-form field values
                            //myCommand.Parameters.AddWithValue("@ElecteurID","");
                            myCommand.Parameters.AddWithValue("@Nom1", textBoxNom1.Text.ToUpper());
                            myCommand.Parameters.AddWithValue("@Nom2", textBoxNom2.Text.ToUpper());
                            myCommand.Parameters.AddWithValue("@Nom3", textBoxNom3.Text.ToUpper());
                            myCommand.Parameters.AddWithValue("@NomMère1", textBoxNomMère1.Text.ToUpper());
                            myCommand.Parameters.AddWithValue("@NomMère2", textBoxNomMère2.Text.ToUpper());
                            myCommand.Parameters.AddWithValue("@NomMère3", textBoxNomMère3.Text.ToUpper());
                            myCommand.Parameters.AddWithValue("@DOB", Date3);
                            myCommand.Parameters.AddWithValue("@POB", textBoxPOB.Text.ToUpper());
                            myCommand.Parameters.AddWithValue("@CodeElecteur", textBoxCodeElecteur.Text);
                            if (dateTimePicker1.Enabled)
                            {
                                myCommand.Parameters.AddWithValue("@AncienCNI", textBoxAncCNI.Text);
                                myCommand.Parameters.AddWithValue("@DateAncienCNI", Date1);
                            }
                            else
                            {
                                myCommand.Parameters.AddWithValue("@AncienCNI", DBNull.Value);
                                myCommand.Parameters.AddWithValue("@DateAncienCNI", DBNull.Value);
                            }

                            if (dateTimePicker2.Enabled)
                            {
                                myCommand.Parameters.AddWithValue("@NewCNI", textBoxNewCNI.Text);
                                myCommand.Parameters.AddWithValue("@DateNewCNI", Date2);
                            }
                            else
                            {
                                myCommand.Parameters.AddWithValue("@NewCNI", DBNull.Value);
                                myCommand.Parameters.AddWithValue("@DateNewCNI", DBNull.Value);
                            }
                            myCommand.Parameters.AddWithValue("@AgentSaisie", value.getAgentSaisie());//recupere agent saisie
                            myCommand.Parameters.AddWithValue("@AgentModif", value.getAgentModif());//recupere agent modif
                            myCommand.Parameters.AddWithValue("@AgentCréation", value.getAgentCréation());//recupere agent de creation
                            myCommand.Parameters.AddWithValue("@StatutID", this.getStatutID() /*add.getStatutID()*/);//Recupere le statut
                            myCommand.Parameters.AddWithValue("@Téléphone", textBoxPhone.Text);
                            myCommand.Parameters.AddWithValue("@AdresseID", this.getAdresseID());//recupere l'ID
                            myCommand.Parameters.AddWithValue("@AgentID", value.getAgentID());//recupere l'ID agent
                            myCommand.Parameters.AddWithValue("@Sexe", comboBoxSexe.SelectedItem);
                            myCommand.Parameters.AddWithValue("@Région", comboBoxRégion.Text);
                            myCommand.Parameters.AddWithValue("@Commune", comboBoxCommune.Text);
                            myCommand.Parameters.AddWithValue("@Arrondissement", comboBoxArrondis.Text);
                            myCommand.Parameters.AddWithValue("@DateCréation", dat);
                            myCommand.Parameters.AddWithValue("@Portable", textBoxPortable.Text);
                            myCommand.Parameters.AddWithValue("@Profession", textBoxProf.Text);
                            myCommand.Parameters.AddWithValue("@Complement", textBoxAdresseCompl.Text);
                            myCommand.ExecuteNonQuery();
                            MessageBox.Show("Ajout effectué avec succès");
                            RecursiveClearTextBoxes(this.Controls);
                            dateTimePicker1.Enabled = false;
                            dateTimePicker2.Enabled = false;
                        }
                    }
                    else
                    {

                        string sCon = conn.connectdb();
                        string insertCmd = "INSERT INTO Electeur(Nom1,Nom2,Nom3,NomMère1,NomMère2,NomMère3,DOB,POB,CodeElecteur,AncienCNI,DateAncienCNI,NewCNI,DateNewCNI,AgentSaisie,AgentCréation,AgentModif,StatutID,Téléphone,AdresseID,AgentID,Sexe,Région,Commune,Arrondissement,DateCréation) VALUES (@Nom1,@Nom2,@Nom3,@NomMère1,@NomMère2,@NomMère3,@DOB,@POB,@CodeElecteur,@AncienCNI,@DateAncienCNI,@NewCNI,@DateNewCNI,@AgentSaisie,@AgentCréation,@AgentModif,@StatutID,@Téléphone,@AdresseID,@AgentID,@Sexe,@Région,@Commune,@Arrondissement,@DateCréation)";
                        SqlConnection dbConn;
                        dbConn = new SqlConnection(sCon);
                        dbConn.Open();
                        Login lg = new Login();
                        //string str;
                        string dat = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                        DateTime date4 = DateTime.Parse(dat);
                        adresse = comboBoxAdresse.Text;
                        statutID = LabelStatut.Text;
                        GetValue value = new GetValue();
                        //getAdress add = new getAdress();
                        SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                        // Create parameters for the SqlCommand object
                        // initialize with input-form field values
                        //myCommand.Parameters.AddWithValue("@ElecteurID","");
                        myCommand.Parameters.AddWithValue("@Nom1", textBoxNom1.Text.ToUpper());
                        myCommand.Parameters.AddWithValue("@Nom2", textBoxNom2.Text.ToUpper());
                        myCommand.Parameters.AddWithValue("@Nom3", textBoxNom3.Text.ToUpper());
                        myCommand.Parameters.AddWithValue("@NomMère1", textBoxNomMère1.Text.ToUpper());
                        myCommand.Parameters.AddWithValue("@NomMère2", textBoxNomMère2.Text.ToUpper());
                        myCommand.Parameters.AddWithValue("@NomMère3", textBoxNomMère3.Text.ToUpper());
                        myCommand.Parameters.AddWithValue("@DOB", Date3);
                        myCommand.Parameters.AddWithValue("@POB", textBoxPOB.Text.ToUpper());
                        myCommand.Parameters.AddWithValue("@CodeElecteur", textBoxCodeElecteur.Text);

                        if (dateTimePicker1.Enabled)
                        {
                            myCommand.Parameters.AddWithValue("@AncienCNI", textBoxAncCNI.Text);
                            myCommand.Parameters.AddWithValue("@DateAncienCNI", Date1);
                        }
                        else
                        {
                            myCommand.Parameters.AddWithValue("@AncienCNI", DBNull.Value);
                            myCommand.Parameters.AddWithValue("@DateAncienCNI", DBNull.Value);
                        }

                        if (dateTimePicker2.Enabled)
                        {
                            myCommand.Parameters.AddWithValue("@NewCNI", textBoxNewCNI.Text);
                            myCommand.Parameters.AddWithValue("@DateNewCNI", Date2);
                        }
                        else
                        {
                            myCommand.Parameters.AddWithValue("@NewCNI", DBNull.Value);
                            myCommand.Parameters.AddWithValue("@DateNewCNI", DBNull.Value);
                        }
                        myCommand.Parameters.AddWithValue("@AgentSaisie", value.getAgentSaisie());//recupere agent saisie
                        myCommand.Parameters.AddWithValue("@AgentModif", value.getAgentModif());//recupere agent modif
                        myCommand.Parameters.AddWithValue("@AgentCréation", value.getAgentCréation());//recupere agent de creation
                        myCommand.Parameters.AddWithValue("@StatutID", this.getStatutID() /*add.getStatutID()*/);//Recupere le statut
                        myCommand.Parameters.AddWithValue("@Téléphone", textBoxPhone.Text);
                        myCommand.Parameters.AddWithValue("@AdresseID", this.getAdresseID());//recupere l'ID
                        myCommand.Parameters.AddWithValue("@AgentID", value.getAgentID());//recupere l'ID agent
                        myCommand.Parameters.AddWithValue("@Sexe", comboBoxSexe.SelectedItem);
                        myCommand.Parameters.AddWithValue("@Région", comboBoxRégion.Text);
                        myCommand.Parameters.AddWithValue("@Commune", comboBoxCommune.Text);
                        myCommand.Parameters.AddWithValue("@Arrondissement", comboBoxArrondis.Text);
                        myCommand.Parameters.AddWithValue("@DateCréation", dat);
                        myCommand.ExecuteNonQuery();
                        MessageBox.Show("Ajout effectué avec succès");
                        RecursiveClearTextBoxes(this.Controls);
                        dateTimePicker1.Enabled = false;
                        dateTimePicker2.Enabled = false;


                    }
                }
                else
                {
                    MessageBox.Show("Veuillez au moins saisir l'ancienne ou le nouvelle CNI");
                }
            }
            else
            {
                if (!NotEmpty)
                {
                    MessageBox.Show("Veuillez saisir au moins le premier nom");
                    textBoxNom1.Focus();

                }
                else
                {
                    if (!Existe)
                    {
                        MessageBox.Show("Veuillez vous assurer que ce numero existe");
                        textBoxNewCNI.Focus();
                    }
                }
            }
        }
        
        
        private void textBoxNom1_KeyPress(object sender, KeyPressEventArgs e)
        {
            int isNum = 0;
            if (e.KeyChar == ',' || e.KeyChar == (char)Keys.Back)
                e.Handled = false;
            else if (int.TryParse(e.KeyChar.ToString(), out isNum))
                e.Handled = true;
        }
        private void textBoxAncCNI_KeyPress(object sender, KeyPressEventArgs e)
        {
            int isNum = 0;
            if (e.KeyChar == ',' || e.KeyChar == (char)Keys.Back)
                e.Handled = false;
            else if (!int.TryParse(e.KeyChar.ToString(), out isNum))
                e.Handled = true;
        }
        private void textBoxNewCNI_KeyPress(object sender, KeyPressEventArgs e)
        {
            int isNum = 0;
            if (e.KeyChar == ',' || e.KeyChar == (char)Keys.Back)
                e.Handled = false;
            else if (!int.TryParse(e.KeyChar.ToString(), out isNum))
                e.Handled = true;
        }
        private void textBoxPhone_KeyPress(object sender, KeyPressEventArgs e)
        {
            int isNum = 0;
            if (e.KeyChar == ',' || e.KeyChar == (char)Keys.Back)
                e.Handled = false;
            else if (!int.TryParse(e.KeyChar.ToString(), out isNum))
                e.Handled = true;
        }
        private void textBoxCodeElecteur_KeyPress(object sender, KeyPressEventArgs e)
        {
            int isNum = 0;
            if (e.KeyChar == ',' || e.KeyChar == (char)Keys.Back)
                e.Handled = false;
            else if (!int.TryParse(e.KeyChar.ToString(), out isNum))
                e.Handled = true;
        }
        /*private void textBoxCodeElecteur_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Back)
            {
                e.SuppressKeyPress = true;
                e.Handled = true;
            }
        }*/

        public int getStatutID()
        {
            //SqlDataReader rd = null;
            string query = "select StatutID from Statut where Statut='" + statutID + "'";
            string sCon = conn.connectdb();
            SqlConnection dbConn;
            dbConn = new SqlConnection(sCon);
            dbConn.Open();
            SqlCommand cmd = new SqlCommand(query, dbConn);
            int result = (int)cmd.ExecuteScalar();
            //SaisieElecteur se=new SaisieElecteur();
            int statut = result;
            return statut;

        }
        public int getAdresseID()
        {
            //SqlDataReader rd = null;
            string query = "select AdresseID from Adresse where NomAdresse='" + adresse + "'";
            string sCon = conn.connectdb();
            SqlConnection dbConn;
            dbConn = new SqlConnection(sCon);
            dbConn.Open();
            SqlCommand cmd = new SqlCommand(query, dbConn);
            int result = ((int)cmd.ExecuteScalar());
            //SaisieElecteur se=new SaisieElecteur();
            int adress = result;
            return adress;

        }

        private void FillRégion()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT RégionID, NomRégion FROM Région";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBoxRégion.ValueMember = "RégionID";
            comboBoxRégion.DisplayMember = "NomRégion";
            comboBoxRégion.DataSource = objDs.Tables[0];
        }
        //int RégionID;
        private void comboBoxRégion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxRégion.SelectedValue.ToString() != "")
            {
                int RégionID = Convert.ToInt32(comboBoxRégion.SelectedValue.ToString());
                FillCommune(RégionID);
                comboBoxCommune.SelectedIndex = 2;
            }
        }
        private void FillCommune(int RégionID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT CommuneID, NomCommune FROM Commune WHERE RégionID =@RégionID";
            cmd.Parameters.AddWithValue("@RégionID", RégionID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxCommune.ValueMember = "CommuneID";
                comboBoxCommune.DisplayMember = "NomCommune";
                comboBoxCommune.DataSource = objDs.Tables[0];
            }
        }
        private void comboBoxCommune_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxCommune.SelectedValue.ToString() != "")
            {
                int CommuneID = Convert.ToInt32(comboBoxCommune.SelectedValue.ToString());
                this.FillArrondis(CommuneID);
                comboBoxArrondis.SelectedIndex = 0;
            }
        }
        private void comboBoxArrondis_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxArrondis.SelectedValue.ToString() != "")
            {
                int ArrondisID = Convert.ToInt32(comboBoxArrondis.SelectedValue.ToString());
                this.FillAdresse(ArrondisID);
                //comboBoxAdresse.SelectedIndex = 0;
            }
        }
        private void textBoxAncCNI_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //string errorMsg;
            if (textBoxAncCNI.Text.Trim() != "")
            {
                dateTimePicker1.Enabled = true;
                string sCon = conn.connectdb();
                SqlConnection con = new SqlConnection(sCon);
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT AncienCNI FROM Electeur WHERE AncienCNI =@AncienCNI";
                cmd.Parameters.AddWithValue("@AncienCNI", textBoxAncCNI.Text.Trim());
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                con.Open();
                dAdapter.Fill(objDs);
                con.Close();
                if (objDs.Tables[0].Rows.Count > 0)
                {
                    MessageBox.Show("Numéro déjà enregistré");
                    textBoxAncCNI.Focus();
                }
            }
        }

        private void textBoxNewCNI_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //string errorMsg;
            if (textBoxNewCNI.Text.Trim() != "")
            {
                dateTimePicker2.Enabled = true;
                string sCon = conn.connectdb();
                SqlConnection con = new SqlConnection(sCon);
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT NewCNI FROM Electeur WHERE NewCNI =@NewCNI";
                cmd.Parameters.AddWithValue("@NewCNI", textBoxNewCNI.Text.Trim());
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                con.Open();
                dAdapter.Fill(objDs);
                con.Close();
                if (objDs.Tables[0].Rows.Count > 0)
                {
                    MessageBox.Show("Numéro déjà enregistré");
                    textBoxNewCNI.Focus();
                }
            }
        }
        private void FillArrondis(int CommuneID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT ArrondisID, NomArrondis FROM Arrondissement WHERE CommuneID =@CommuneID";
            cmd.Parameters.AddWithValue("@CommuneID", CommuneID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxArrondis.ValueMember = "ArrondisID";
                comboBoxArrondis.DisplayMember = "NomArrondis";
                comboBoxArrondis.DataSource = objDs.Tables[0];
            }
        }
        private void FillAdresse(int ArrondisID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT AdresseID, NomAdresse FROM Adresse WHERE ArrondisID =@ArrondisID";
            cmd.Parameters.AddWithValue("@ArrondisID", ArrondisID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxAdresse.ValueMember = "AdresseID";
                comboBoxAdresse.DisplayMember = "NomAdresse";
                comboBoxAdresse.DataSource = objDs.Tables[0];
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            this.checkIfExist();
            if (Existe && NotEmpty)
            {
                if (dateTimePicker1.Enabled || dateTimePicker2.Enabled)
                {
                    DateTime Date1 = DateTime.Parse(dateTimePicker1.Text);
                    DateTime Date2 = DateTime.Parse(dateTimePicker2.Text);
                    DateTime Date3 = DateTime.Parse(dateTimePickerDOB.Text);
                    int result = DateTime.Compare(Date1, Date2);
                    if (dateTimePicker1.Enabled && dateTimePicker2.Enabled)
                    {
                        if (result > 0 || result == 0)
                        {
                            MessageBox.Show("La date de la nouvelle CNI ne peut être anterieure ou identique à celle de l'ancienne CNI");
                            dateTimePicker1.Focus();
                        }
                        else
                        {





                            string sCon = conn.connectdb();
                            string insertCmd = "INSERT INTO Electeur(Nom1,Nom2,Nom3,NomMère1,NomMère2,NomMère3,DOB,POB,CodeElecteur,AncienCNI,DateAncienCNI,NewCNI,DateNewCNI,AgentSaisie,AgentCréation,AgentModif,StatutID,Téléphone,AdresseID,AgentID,Sexe,Région,Commune,Arrondissement,DateCréation) VALUES (@Nom1,@Nom2,@Nom3,@NomMère1,@NomMère2,@NomMère3,@DOB,@POB,@CodeElecteur,@AncienCNI,@DateAncienCNI,@NewCNI,@DateNewCNI,@AgentSaisie,@AgentCréation,@AgentModif,@StatutID,@Téléphone,@AdresseID,@AgentID,@Sexe,@Région,@Commune,@Arrondissement,@DateCréation)";
                            SqlConnection dbConn;
                            dbConn = new SqlConnection(sCon);
                            dbConn.Open();
                            Login lg = new Login();
                            //string str;
                            string dat = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                            DateTime date4 = DateTime.Parse(dat);
                            adresse = comboBoxAdresse.Text;
                            statutID = LabelStatut.Text;
                            GetValue value = new GetValue();
                            //getAdress add = new getAdress();
                            SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                            // Create parameters for the SqlCommand object
                            // initialize with input-form field values
                            //myCommand.Parameters.AddWithValue("@ElecteurID","");
                            myCommand.Parameters.AddWithValue("@Nom1", textBoxNom1.Text);
                            myCommand.Parameters.AddWithValue("@Nom2", textBoxNom2.Text);
                            myCommand.Parameters.AddWithValue("@Nom3", textBoxNom3.Text);
                            myCommand.Parameters.AddWithValue("@NomMère1", textBoxNomMère1.Text);
                            myCommand.Parameters.AddWithValue("@NomMère2", textBoxNomMère2.Text);
                            myCommand.Parameters.AddWithValue("@NomMère3", textBoxNomMère3.Text);
                            myCommand.Parameters.AddWithValue("@DOB", Date3);
                            myCommand.Parameters.AddWithValue("@POB", textBoxPOB.Text);
                            myCommand.Parameters.AddWithValue("@CodeElecteur", textBoxCodeElecteur.Text);
                            myCommand.Parameters.AddWithValue("@AncienCNI", textBoxAncCNI.Text);
                            myCommand.Parameters.AddWithValue("@DateAncienCNI", Date1);
                            myCommand.Parameters.AddWithValue("@NewCNI", textBoxNewCNI.Text);
                            myCommand.Parameters.AddWithValue("@DateNewCNI", Date2);
                            myCommand.Parameters.AddWithValue("@AgentSaisie", value.getAgentSaisie());//recupere agent saisie
                            myCommand.Parameters.AddWithValue("@AgentModif", value.getAgentModif());//recupere agent modif
                            myCommand.Parameters.AddWithValue("@AgentCréation", value.getAgentCréation());//recupere agent de creation
                            myCommand.Parameters.AddWithValue("@StatutID", this.getStatutID() /*add.getStatutID()*/);//Recupere le statut
                            myCommand.Parameters.AddWithValue("@Téléphone", textBoxPhone.Text);
                            myCommand.Parameters.AddWithValue("@AdresseID", this.getAdresseID());//recupere l'ID
                            myCommand.Parameters.AddWithValue("@AgentID", value.getAgentID());//recupere l'ID agent
                            myCommand.Parameters.AddWithValue("@Sexe", comboBoxSexe.SelectedItem);
                            myCommand.Parameters.AddWithValue("@Région", comboBoxRégion.Text);
                            myCommand.Parameters.AddWithValue("@Commune", comboBoxCommune.Text);
                            myCommand.Parameters.AddWithValue("@Arrondissement", comboBoxArrondis.Text);
                            myCommand.Parameters.AddWithValue("@DateCréation", dat);


                            //myCommand.Parameters.AddWithValue("@Localité", comboBoxLocalité.Text);

                            myCommand.ExecuteNonQuery();
                            MessageBox.Show("Ajout effectué avec succès");
                            RecursiveClearTextBoxes(this.Controls);
                            dateTimePicker1.Enabled = false;
                            dateTimePicker2.Enabled = false;
                            this.Hide();
                        }
                    }
                    else
                    {

                        string sCon = conn.connectdb();
                        string insertCmd = "INSERT INTO Electeur(Nom1,Nom2,Nom3,NomMère1,NomMère2,NomMère3,DOB,POB,CodeElecteur,AncienCNI,DateAncienCNI,NewCNI,DateNewCNI,AgentSaisie,AgentCréation,AgentModif,StatutID,Téléphone,AdresseID,AgentID,Sexe,Région,Commune,Arrondissement,DateCréation) VALUES (@Nom1,@Nom2,@Nom3,@NomMère1,@NomMère2,@NomMère3,@DOB,@POB,@CodeElecteur,@AncienCNI,@DateAncienCNI,@NewCNI,@DateNewCNI,@AgentSaisie,@AgentCréation,@AgentModif,@StatutID,@Téléphone,@AdresseID,@AgentID,@Sexe,@Région,@Commune,@Arrondissement,@DateCréation)";
                        SqlConnection dbConn;
                        dbConn = new SqlConnection(sCon);
                        dbConn.Open();
                        Login lg = new Login();
                        //string str;
                        string dat = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                        DateTime date4 = DateTime.Parse(dat);
                        adresse = comboBoxAdresse.Text;
                        statutID = LabelStatut.Text;
                        GetValue value = new GetValue();
                        //getAdress add = new getAdress();
                        SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                        // Create parameters for the SqlCommand object
                        // initialize with input-form field values
                        //myCommand.Parameters.AddWithValue("@ElecteurID","");
                        myCommand.Parameters.AddWithValue("@Nom1", textBoxNom1.Text.ToUpper());
                        myCommand.Parameters.AddWithValue("@Nom2", textBoxNom2.Text.ToUpper());
                        myCommand.Parameters.AddWithValue("@Nom3", textBoxNom3.Text.ToUpper());
                        myCommand.Parameters.AddWithValue("@NomMère1", textBoxNomMère1.Text.ToUpper());
                        myCommand.Parameters.AddWithValue("@NomMère2", textBoxNomMère2.Text.ToUpper());
                        myCommand.Parameters.AddWithValue("@NomMère3", textBoxNomMère3.Text.ToUpper());
                        myCommand.Parameters.AddWithValue("@DOB", Date3);
                        myCommand.Parameters.AddWithValue("@POB", textBoxPOB.Text.ToUpper());
                        myCommand.Parameters.AddWithValue("@CodeElecteur", textBoxCodeElecteur.Text.ToUpper());
                        myCommand.Parameters.AddWithValue("@AncienCNI", textBoxAncCNI.Text.ToUpper());
                        if (dateTimePicker1.Enabled)
                        {
                            myCommand.Parameters.AddWithValue("@DateAncienCNI", Date1);
                        }
                        else
                        {
                            myCommand.Parameters.AddWithValue("@DateAncienCNI", DBNull.Value);
                        }
                        myCommand.Parameters.AddWithValue("@NewCNI", textBoxNewCNI.Text.ToUpper());
                        if (dateTimePicker2.Enabled)
                        {
                            myCommand.Parameters.AddWithValue("@DateNewCNI", Date2);
                        }
                        else
                        {
                            myCommand.Parameters.AddWithValue("@DateNewCNI", DBNull.Value);
                        }

                        myCommand.Parameters.AddWithValue("@AgentSaisie", value.getAgentSaisie());//recupere agent saisie
                        myCommand.Parameters.AddWithValue("@AgentModif", value.getAgentModif());//recupere agent modif
                        myCommand.Parameters.AddWithValue("@AgentCréation", value.getAgentCréation());//recupere agent de creation
                        myCommand.Parameters.AddWithValue("@StatutID", this.getStatutID() /*add.getStatutID()*/);//Recupere le statut
                        myCommand.Parameters.AddWithValue("@Téléphone", textBoxPhone.Text.ToUpper());
                        myCommand.Parameters.AddWithValue("@AdresseID", this.getAdresseID());//recupere l'ID
                        myCommand.Parameters.AddWithValue("@AgentID", value.getAgentID());//recupere l'ID agent
                        myCommand.Parameters.AddWithValue("@Sexe", comboBoxSexe.SelectedItem);
                        myCommand.Parameters.AddWithValue("@Région", comboBoxRégion.Text);
                        myCommand.Parameters.AddWithValue("@Commune", comboBoxCommune.Text);
                        myCommand.Parameters.AddWithValue("@Arrondissement", comboBoxArrondis.Text);
                        myCommand.Parameters.AddWithValue("@DateCréation", dat);


                        //myCommand.Parameters.AddWithValue("@Localité", comboBoxLocalité.Text);

                        myCommand.ExecuteNonQuery();
                        MessageBox.Show("Ajout effectué avec succès");
                        RecursiveClearTextBoxes(this.Controls);
                        dateTimePicker1.Enabled = false;
                        dateTimePicker2.Enabled = false;
                        this.Hide();
                    }
                }
                else
                {
                    MessageBox.Show("Veuillez au moins saisir l'ancien ou le nouveau CNI");
                }
            }
            else
            {
                if (!NotEmpty)
                {
                    MessageBox.Show("Veuillez saisir au moins le premier nom");
                    textBoxNom1.Focus();

                }
                else
                {
                    if (!Existe)
                    {
                        MessageBox.Show("Veuillez vous assurer que ce numero existe");
                        textBoxNewCNI.Focus();
                    }
                }
            }


        }

        private void label25_Click(object sender, EventArgs e)
        {

        }

        private void label23_Click(object sender, EventArgs e)
        {

        }
    }
}
