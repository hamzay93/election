﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class Edition_Convocation : Form
    {
        
        CrystalReport_Reunion objRpt;
        CrystalReport_Formation objRptf;
        CrystalReport_Convocation objRptc;
        Connection conn = new Connection();
        string clause = null;
        string clause1 = null;
        //public static string strcon = @"Data Source=GUINALEH-PC\SQL2008;Initial Catalog=Djibouti;User ID=sa;Password=651251;";
        //public static string strcon = @"Data Source=SRVDNEVM1\SQL2008;Initial Catalog=DJIBOUTI;User ID=sa;Password=election;";
        //static SqlConnection con = new SqlConnection(strcon);
        public Edition_Convocation()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.IsEmptyfieldbis();
            SqlConnection con = new SqlConnection(conn.connectdb());
            int IDRegion = Convert.ToInt32(comboBox2.SelectedValue.ToString());
            if(comboBox1.SelectedItem.ToString()=="Editions des convocations reunion d\'information")
            {
            objRpt = new CrystalReport_Reunion();
          

            string SQLlist = "SET CONCAT_NULL_YIELDS_NULL OFF; SELECT   * " +
                                 " FROM  View_Convocation_presentation_crutin where " + clause1;

            con.Open();
            SqlDataAdapter sqlAdapter = new SqlDataAdapter(SQLlist, con);
            SqlCommandBuilder commandBuilder1 = new SqlCommandBuilder(sqlAdapter);

            con.Close();
            DataTable dt = new DataTable();
            sqlAdapter.Fill(dt);
            if (dt.Rows.Count == 0)
            {
                MessageBox.Show("Aucun Edition pour ces critères");
            }
            else
            {
                objRpt.SetDataSource(dt);
                crystalReportViewer1.ReportSource = objRpt;
                crystalReportViewer1.Refresh();
            }
            }

            if (comboBox1.SelectedItem.ToString() == "Editions des convocations formations")
            {
                objRptf = new CrystalReport_Formation();


                string SQLlist = "SET CONCAT_NULL_YIELDS_NULL OFF; SELECT   * " +
                                     " FROM  View_Convocation_presentation_crutin where " + clause1;
                con.Open();
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(SQLlist, con);
                SqlCommandBuilder commandBuilder1 = new SqlCommandBuilder(sqlAdapter);

                con.Close();
                DataTable dt = new DataTable();
                sqlAdapter.Fill(dt);
                if (dt.Rows.Count == 0)
                {
                    MessageBox.Show("Aucun Edition pour ces critères");
                }
                else
                {
                    objRptf.SetDataSource(dt);
                    crystalReportViewer1.ReportSource = objRptf;
                    crystalReportViewer1.Refresh();
                }
            }
            if (comboBox1.SelectedItem.ToString() == "Editions des convocations au scrutin")
            {
                objRptc = new CrystalReport_Convocation();


                string SQLlist = "SET CONCAT_NULL_YIELDS_NULL OFF; SELECT   * " +
                                     " FROM  View_Convocation_presentation_crutin where " + clause1;

                con.Open();
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(SQLlist, con);
                SqlCommandBuilder commandBuilder1 = new SqlCommandBuilder(sqlAdapter);

                con.Close();
                DataTable dt = new DataTable();
                sqlAdapter.Fill(dt);
                if (dt.Rows.Count == 0)
                {
                    MessageBox.Show("Aucun Edition pour ces critères");
                }
                else
                {
                    objRptc.SetDataSource(dt);
                    crystalReportViewer1.ReportSource = objRptc;
                    crystalReportViewer1.Refresh();
                }
            }
        }
        private void FillRégion()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT RégionID,NomRégion FROM Région";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBox2.ValueMember = "RégionID";
            comboBox2.DisplayMember = "NomRégion";
            comboBox2.DataSource = objDs.Tables[0];
        }
        private void Edition_Convocation_Load(object sender, EventArgs e)
        {
            FillRégion();
        }
        private void FillBureau(int regionID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT BureauID, ( CONVERT(Nvarchar(50), CodeBureau)) + ' - '+NomBureau as NomBureau FROM View_Lieu_vote WHERE RégionID =@RégionID ORDER BY CodeBureau";
            cmd.Parameters.AddWithValue("@RégionID", regionID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            objDs.Tables[0].Rows.Add(300, "");
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxBV.ValueMember = "BureauID";
                comboBoxBV.DisplayMember = "NomBureau";
                comboBoxBV.DataSource = objDs.Tables[0];
                comboBoxBV.SelectedValue = 300;
            }
        }
        public void IsEmptyfield()
        {
            clause = null;
            foreach (Control X in this.panel1.Controls)
            {

                if (X is ComboBox)
                {
                    if (X.Tag.ToString() != "test" && X.Tag.ToString() != "FonctionID")
                    {
                        if (X.Tag.ToString() == "RégionID")
                        {
                            if (X.Text.Trim().Length != 0)
                            {

                                if (string.IsNullOrEmpty(clause))
                                {
                                    clause = clause + X.Tag + " = '" + Int32.Parse(comboBox2.SelectedValue.ToString()) + "'";
                                    //return clause;
                                }
                                else
                                {
                                    clause = clause + " And " + X.Tag + " = '" + Int32.Parse(comboBox2.SelectedValue.ToString()) + "'";
                                    //return clause;
                                }
                            }
                        }
                        else
                        {
                            if (X.Text.Trim().Length != 0)
                            {
                                if (string.IsNullOrEmpty(clause))
                                {
                                    clause = clause + X.Tag + "='" + Int32.Parse(comboBoxBV.SelectedValue.ToString()) + "'";
                                    //return clause;
                                }
                                else
                                {
                                    clause = clause + " And " + X.Tag + "='" + Int32.Parse(comboBoxBV.SelectedValue.ToString()) + "'";
                                    //return clause;
                                }
                            }
                        }

                    }

                }
            }

        }
        public void IsEmptyfieldbis()
        {
            clause1 = null;
            foreach (Control X in this.panel1.Controls)
            {

                if (X is ComboBox)
                {
                    
                    if (X.Tag.ToString() == "RégionID")
                    {
                        if (X.Text.Trim().Length != 0)
                        {

                            if (string.IsNullOrEmpty(clause1))
                            {
                                clause1 = clause1 + X.Tag + " = '" + Int32.Parse(comboBox2.SelectedValue.ToString()) + "'";
                                //return clause;
                            }
                            else
                            {
                                clause1 = clause1 + " And " + X.Tag + " = '" + Int32.Parse(comboBox2.SelectedValue.ToString()) + "'";
                                //return clause;
                            }
                        }
                    }
                    else
                    {
                        if (X.Tag.ToString() == "BureauID")
                        {
                            if (X.Text.Trim().Length != 0)
                            {
                                if (string.IsNullOrEmpty(clause1))
                                {
                                    clause1 = clause1 + X.Tag + "='" + Int32.Parse(comboBoxBV.SelectedValue.ToString()) + "'";
                                    //return clause;
                                }
                                else
                                {
                                    clause1 = clause1 + " And " + X.Tag + "='" + Int32.Parse(comboBoxBV.SelectedValue.ToString()) + "'";
                                    //return clause;
                                }
                            }
                        }
                        else
                        {
                            if (X.Tag.ToString() == "FonctionID")
                            {
                                if (X.Text.Trim().Length != 0)
                                {
                                    if (string.IsNullOrEmpty(clause1))
                                    {
                                        clause1 = clause1 + X.Tag + "='" + Int32.Parse(comboBoxft.SelectedValue.ToString()) + "'";
                                        //return clause;
                                    }
                                    else
                                    {
                                        clause1 = clause1 + " And " + X.Tag + "='" + Int32.Parse(comboBoxft.SelectedValue.ToString()) + "'";
                                        //return clause;
                                    }
                                }
                            }
                        }
                    }

                }

            }

        }
        private void FillFonction()
        {
            this.IsEmptyfield();
            if (String.IsNullOrEmpty(clause))
            {
                clause = "ObjetFonction like '%'";
            }
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT FonctionID, ObjetFonction from View_Convocation_presentation_crutin where "+clause;
            //cmd.Parameters.AddWithValue("@RégionID", regionID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            objDs.Tables[0].Rows.Add(30, "");
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxft.ValueMember = "FonctionID";
                comboBoxft.DisplayMember = "ObjetFonction";
                comboBoxft.DataSource = objDs.Tables[0];
                comboBoxft.SelectedValue = 30;
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.FillBureau(Int32.Parse( comboBox2.SelectedValue.ToString()));
            this.FillFonction();
        }

        private void comboBoxBV_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.FillFonction();
        }
    }
}
