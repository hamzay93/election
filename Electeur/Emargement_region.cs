﻿using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class Emargement_region : Form
    {
        string SQL;
        CrystalReport_region_emargemnt objRptr;
        public static string strcon = @"Data Source=localhost\SQL2008;Initial Catalog=DJIBOUTI;User ID=sa;Password=Pa$$w0rd;";
        static SqlConnection con = new SqlConnection(strcon);
        public Emargement_region()
        {
            InitializeComponent();
        }

        private void Emargement_region_Load(object sender, EventArgs e)
        {
            SQL = "select RégionID,NomRégion from View_region_district";
            con.Open();
            SqlDataAdapter sqlAdapter = new SqlDataAdapter(SQL, con);
            SqlCommandBuilder commandBuilder1 = new SqlCommandBuilder(sqlAdapter);
            con.Close();
            DataTable dt = new DataTable();
            sqlAdapter.Fill(dt);
            comboBox1.ValueMember = "RégionID";
            comboBox1.DisplayMember = "NomRégion";

            comboBox1.DataSource = dt;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try{
             int idregion = Convert.ToInt32(comboBox1.SelectedValue.ToString());
             objRptr = new CrystalReport_region_emargemnt();
             string SQLlist = "SET CONCAT_NULL_YIELDS_NULL OFF; SELECT   * " +
                                             " FROM  View_carte_reg_region WHERE  RégionID ='" + idregion + "' ORDER BY CodeElecteur";

                        con.Open();
                        SqlDataAdapter sqlAdapter = new SqlDataAdapter(SQLlist, con);
                        SqlCommandBuilder commandBuilder1 = new SqlCommandBuilder(sqlAdapter);

                        con.Close();
                        DataTable dt = new DataTable();
                        sqlAdapter.Fill(dt);
                        objRptr.SetDataSource(dt);
                        crystalReportViewer1.ReportSource = objRptr;
                        crystalReportViewer1.Refresh();
                        button2.Visible = true;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("error" + ex.Message);
                    }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {

                int idregion = Convert.ToInt32(comboBox1.SelectedValue.ToString());
                ExportOptions CrExportOptions;
                DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();
                CrDiskFileDestinationOptions.DiskFileName = "C:\\Users\\Guinaleh\\Desktop\\pdf file\\Région"+idregion+".pdf";
                CrExportOptions = objRptr.ExportOptions;
                {
                    CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                    CrExportOptions.FormatOptions = CrFormatTypeOptions;
                }
                objRptr.Export();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}
