﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class Paramétrage : Form
    {
        public Paramétrage()
        {
            InitializeComponent();
        }
        Connection conn = new Connection();
        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Trim().Length == 0)
            {
                MessageBox.Show("veuillez saisir l'adresse");
                textBox1.Focus();
            }
            else
            {
                Connection cn = new Connection();
                string insertCmd = "INSERT INTO Région (NomRégion,Actif,DateCréation,AgentCréation,AgentModif) VALUES (@NomRégion,@Actif,@DateCréation,@AgentCréation,@AgentModif)";
                SqlConnection dbConn;
                dbConn = new SqlConnection(cn.connectdb());
                dbConn.Open();
                Login lg = new Login();
                string dat = DateTime.Now.ToString("dd/MM/yyyy");
                DateTime date4 = DateTime.Parse(dat);
                GetValue value = new GetValue();
                //getAdress add = new getAdress();
                SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                // Create parameters for the SqlCommand object
                // initialize with input-form field values
                //myCommand.Parameters.AddWithValue("@ElecteurID","");
                myCommand.Parameters.AddWithValue("@NomRégion", textBox1.Text);
                myCommand.Parameters.AddWithValue("@Actif", "Actif");
                myCommand.Parameters.AddWithValue("@DateCréation", date4);
                myCommand.Parameters.AddWithValue("@AgentCréation", value.getAgentSaisie());
                myCommand.Parameters.AddWithValue("@AgentModif", value.getAgentModif());
                myCommand.ExecuteNonQuery();

                MessageBox.Show("Ajoutée avec succès");
                textBox1.Clear();
            }
        }
        private void FillRégion()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT RégionID, NomRégion FROM Région";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBoxRégion.ValueMember = "RégionID";
            comboBoxRégion.DisplayMember = "NomRégion";
            comboBoxRégion.DataSource = objDs.Tables[0];
        }
        //int RégionID;
        private void comboBoxRégion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxRégion.SelectedValue.ToString() != "")
            {
                comboBoxCommune.DataSource = null;
                comboBoxArrondis.DataSource = null;
                int RégionID = Convert.ToInt32(comboBoxRégion.SelectedValue.ToString());
                FillCommune(RégionID);
                comboBoxCommune.SelectedIndex = 0;
            }
        }
        private void FillCommune(int RégionID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT CommuneID, NomCommune FROM Commune WHERE RégionID =@RégionID";
            cmd.Parameters.AddWithValue("@RégionID", RégionID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            if (objDs.Tables[0].Rows.Count >= 0)
            {
                comboBoxCommune.ValueMember = "CommuneID";
                comboBoxCommune.DisplayMember = "NomCommune";
                comboBoxCommune.DataSource = objDs.Tables[0];
            }
            else
            {
                comboBoxCommune.Text = "";
                //comboBoxCommune.DisplayMember = "";
                comboBoxCommune.DataSource = null;
            }
        }
        private void comboBoxCommune_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxCommune.DataSource != null)
            {
                if (comboBoxCommune.SelectedValue.ToString() != "")
                {
                    int CommuneID = Convert.ToInt32(comboBoxCommune.SelectedValue.ToString());
                    this.FillArrondis(CommuneID);
                    comboBoxArrondis.SelectedIndex = 0;
                }
            }
           
        }
       
        private void FillArrondis(int CommuneID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT ArrondisID, NomArrondis FROM Arrondissement WHERE CommuneID =@CommuneID";
            cmd.Parameters.AddWithValue("@CommuneID", CommuneID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            if (objDs.Tables[0].Rows.Count >= 0)
            {
                comboBoxArrondis.ValueMember = "ArrondisID";
                comboBoxArrondis.DisplayMember = "NomArrondis";
                comboBoxArrondis.DataSource = objDs.Tables[0];
            }
        }
        private void FillRégion1()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT RégionID, NomRégion FROM Région";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBoxRégion1.ValueMember = "RégionID";
            comboBoxRégion1.DisplayMember = "NomRégion";
            comboBoxRégion1.DataSource = objDs.Tables[0];
        }
        //int RégionID;
        private void comboBoxRégion1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxRégion1.SelectedValue.ToString() != "")
            {
                comboBoxCommune1.DataSource = null;
                int RégionID = Convert.ToInt32(comboBoxRégion1.SelectedValue.ToString());
                FillCommune1(RégionID);
                comboBoxCommune1.SelectedIndex = 0;
            }
        }
        private void FillCommune1(int RégionID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT CommuneID, NomCommune FROM Commune WHERE RégionID =@RégionID";
            cmd.Parameters.AddWithValue("@RégionID", RégionID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxCommune1.ValueMember = "CommuneID";
                comboBoxCommune1.DisplayMember = "NomCommune";
                comboBoxCommune1.DataSource = objDs.Tables[0];
            }
        }
        private void FillRégion2()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT RégionID, NomRégion FROM Région";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBoxRégion2.ValueMember = "RégionID";
            comboBoxRégion2.DisplayMember = "NomRégion";
            comboBoxRégion2.DataSource = objDs.Tables[0];
        }

        private void Paramétrage_Load(object sender, EventArgs e)
        {
            FillRégion();
            FillRégion1();
            FillRégion2();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (textBoxAdd.Text.Trim().Length == 0)
            {
                MessageBox.Show("veuillez saisir l'adresse");
                textBoxAdd.Focus();
            }
            else
            {
                Connection cn = new Connection();
                string insertCmd = "INSERT INTO Adresse (NomAdresse,Statut,DateCréation,AgentCréation,AgentModif,ArrondisID) VALUES (@NomAdresse,@Actif,@DateCréation,@AgentCréation,@AgentModif,@ArrondisID)";
                SqlConnection dbConn;
                dbConn = new SqlConnection(cn.connectdb());
                dbConn.Open();
                Login lg = new Login();
                string dat = DateTime.Now.ToString("dd/MM/yyyy");
                DateTime date4 = DateTime.Parse(dat);
                GetValue value = new GetValue();
                //getAdress add = new getAdress();
                SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                // Create parameters for the SqlCommand object
                // initialize with input-form field values
                //myCommand.Parameters.AddWithValue("@ElecteurID","");
                myCommand.Parameters.AddWithValue("@NomAdresse", textBoxAdd.Text);
                myCommand.Parameters.AddWithValue("@Actif", "Actif");
                myCommand.Parameters.AddWithValue("@DateCréation", date4);
                myCommand.Parameters.AddWithValue("@AgentCréation", value.getAgentSaisie());
                myCommand.Parameters.AddWithValue("@AgentModif", value.getAgentModif());                
                int ArrondisID = Convert.ToInt32(comboBoxArrondis.SelectedValue.ToString());
                myCommand.Parameters.AddWithValue("@ArrondisID", ArrondisID);
                myCommand.ExecuteNonQuery();

                MessageBox.Show("Ajoutée avec succès");
                textBoxAdd.Clear();
 
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (textBox3.Text.Trim().Length == 0)
            {
                MessageBox.Show("veuillez saisir l'arrondissement");
                textBox3.Focus();
            }
            else
            {
                Connection cn = new Connection();
                string insertCmd = "INSERT INTO Arrondissement (NomArrondis,Statut,DateCréation,AgentCréation,AgentModif,CommuneID) VALUES (@NomArrondis,@Actif,@DateCréation,@AgentCréation,@AgentModif,@CommuneID)";
                SqlConnection dbConn;
                dbConn = new SqlConnection(cn.connectdb());
                dbConn.Open();
                Login lg = new Login();
                string dat = DateTime.Now.ToString("dd/MM/yyyy");
                DateTime date4 = DateTime.Parse(dat);
                GetValue value = new GetValue();
                //getAdress add = new getAdress();
                SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                // Create parameters for the SqlCommand object
                // initialize with input-form field values
                //myCommand.Parameters.AddWithValue("@ElecteurID","");
                myCommand.Parameters.AddWithValue("@NomArrondis", textBox3.Text);
                myCommand.Parameters.AddWithValue("@Actif", "Actif");
                myCommand.Parameters.AddWithValue("@DateCréation", date4);
                myCommand.Parameters.AddWithValue("@AgentCréation", value.getAgentSaisie());
                myCommand.Parameters.AddWithValue("@AgentModif", value.getAgentModif());
                int CommuneID = Convert.ToInt32(comboBoxCommune1.SelectedValue.ToString());
                myCommand.Parameters.AddWithValue("@CommuneID", CommuneID);
                myCommand.ExecuteNonQuery();

                MessageBox.Show("Ajoutée avec succès");
                textBox3.Clear();

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox2.Text.Trim().Length == 0)
            {
                MessageBox.Show("veuillez saisir la commune");
                textBox2.Focus();
            }
            else
            {
                Connection cn = new Connection();
                string insertCmd = "INSERT INTO Commune (NomCommune,Statut,DateCréation,AgentCréation,AgentModif,RégionID) VALUES (@NomCommune,@Actif,@DateCréation,@AgentCréation,@AgentModif,@RégionID)";
                SqlConnection dbConn;
                dbConn = new SqlConnection(cn.connectdb());
                dbConn.Open();
                Login lg = new Login();
                string dat = DateTime.Now.ToString("dd/MM/yyyy");
                DateTime date4 = DateTime.Parse(dat);
                GetValue value = new GetValue();
                //getAdress add = new getAdress();
                SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                // Create parameters for the SqlCommand object
                // initialize with input-form field values
                //myCommand.Parameters.AddWithValue("@ElecteurID","");
                myCommand.Parameters.AddWithValue("@NomCommune", textBox2.Text);
                myCommand.Parameters.AddWithValue("@Actif", "Actif");
                myCommand.Parameters.AddWithValue("@DateCréation", date4);
                myCommand.Parameters.AddWithValue("@AgentCréation", value.getAgentSaisie());
                myCommand.Parameters.AddWithValue("@AgentModif", value.getAgentModif());
                int RégionID = Convert.ToInt32(comboBoxRégion2.SelectedValue.ToString());
                myCommand.Parameters.AddWithValue("@RégionID", RégionID);
                myCommand.ExecuteNonQuery();

                MessageBox.Show("Ajoutée avec succès");
                textBox2.Clear();

            }
        }
       
    }
}
