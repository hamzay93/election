﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Data.SqlClient;

namespace Electeur
{
    public partial class printcart : Form
    {
        List<int> bureaus = new List<int>();
        string SQL;
        string chk = null;
        public static string strcon = @"Data Source=SRV-PREF-VM\SQL2008;Initial Catalog=DJIBOUTI;User ID=sa;Password=Pa$$w0rd;";
        //public static string strcon = @"Data Source=localhost\SQL2008;Initial Catalog=DJIBOUTI;User ID=sa;Password=Pa$$w0rd;";
        //public static string strcon = @"Data Source=SRVDNEVM1\SQL2008;Initial Catalog=Delect;User ID=sa;Password=root@db.2016;";
        static SqlConnection con = new SqlConnection(strcon);
        CrystalReportcard objRpt; CrystalReportcarte_region crcart;
        public printcart()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int idregion = Convert.ToInt32(comboBox7.SelectedValue.ToString());
            if (idregion == 1)
            {
                try
                {
                    if (checkBox1.Checked)
                    {
                        string SQLlist = "SET CONCAT_NULL_YIELDS_NULL OFF; select * from View_electeur_null where ElecteurID in(Select ElecteurID from Réimp) ORDER BY CodeElecteur";
                        objRpt = new CrystalReportcard();
                        con.Open();
                        SqlDataAdapter sqlAdapter = new SqlDataAdapter(SQLlist, con);
                        SqlCommandBuilder commandBuilder1 = new SqlCommandBuilder(sqlAdapter);

                        con.Close();
                        DataTable dt = new DataTable();
                        sqlAdapter.Fill(dt);
                        objRpt.SetDataSource(dt);
                        crystalReportViewer2.ReportSource = objRpt;
                        crystalReportViewer2.Refresh();

                    }
                    else
                    {
                        if (bureaus.Count > 0)
                        {


                            string str = null;

                            for (int i = 0; i < bureaus.Count; i++)
                            {


                                if (String.IsNullOrEmpty(str))
                                {
                                    str = "" + bureaus[i].ToString() + "";
                                }
                                else
                                {
                                    str = str + "," + bureaus[i].ToString() + "";
                                }


                            }

                            string SQLlist = "SET CONCAT_NULL_YIELDS_NULL OFF; select * from View_electeur_null where BureauID IN (" + str + ") ORDER BY CodeElecteur";
                            objRpt = new CrystalReportcard();
                            con.Open();
                            SqlDataAdapter sqlAdapter = new SqlDataAdapter(SQLlist, con);
                            SqlCommandBuilder commandBuilder1 = new SqlCommandBuilder(sqlAdapter);

                            con.Close();
                            DataTable dt = new DataTable();
                            sqlAdapter.Fill(dt);
                            objRpt.SetDataSource(dt);
                            crystalReportViewer2.ReportSource = objRpt;
                            crystalReportViewer2.Refresh();

                        }
                    }
                   
                }
                catch (Exception ex)
                {
                    MessageBox.Show("error" + ex.Message);
                }
            }
           else
            {
                try
                {
                    if (bureaus.Count > 0)
                    {


                        string str = null;

                        for (int i = 0; i < bureaus.Count; i++)
                        {


                            if (String.IsNullOrEmpty(str))
                            {
                                str = "" + bureaus[i].ToString() + "";
                            }
                            else
                            {
                                str = str + "," + bureaus[i].ToString() + "";
                            }


                        }
                   
                    string SQLlist = "SET CONCAT_NULL_YIELDS_NULL OFF; SELECT   * " +
                                             " FROM  View_carte_reg WHERE  BureauID IN (" + str + ") ORDER BY CodeElecteur";
                        crcart = new CrystalReportcarte_region();
                        con.Open();
                        SqlDataAdapter sqlAdapter = new SqlDataAdapter(SQLlist, con);
                        SqlCommandBuilder commandBuilder1 = new SqlCommandBuilder(sqlAdapter);

                        con.Close();
                        DataTable dt = new DataTable();
                        sqlAdapter.Fill(dt);
                        crcart.SetDataSource(dt);
                        crystalReportViewer2.ReportSource = crcart;
                        crystalReportViewer2.Refresh();


                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("error" + ex.Message);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

            try
            {
                if (Int32.Parse(comboBox7.SelectedValue.ToString()) == 1)
                {
                    ExportOptions CrExportOptions;
                    DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                    PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();
                    CrDiskFileDestinationOptions.DiskFileName = "C:\\Users\\Guinaleh\\Desktop\\emargement\\cart.pdf";
                    CrExportOptions = objRpt.ExportOptions;
                    {
                        CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                        CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                        CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                        CrExportOptions.FormatOptions = CrFormatTypeOptions;
                    }
                    objRpt.Export();
                }
                else
                {
                    ExportOptions CrExportOptions;
                    DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                    PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();
                    CrDiskFileDestinationOptions.DiskFileName = "C:\\Users\\Guinaleh\\Desktop\\emargement\\cartregion.pdf";
                    CrExportOptions = crcart.ExportOptions;
                    {
                        CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                        CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                        CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                        CrExportOptions.FormatOptions = CrFormatTypeOptions;
                    }
                    crcart.Export();
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void printcart_Load(object sender, EventArgs e)
        {
            this.FillRégion();
        }

        private void comboBox7_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox7.SelectedIndex >= 0)
            {
                
                comboBox3.Enabled = false;
                comboBox4.Enabled = false;
                comboBox5.Enabled = false;
                comboBox6.Enabled = false;
                dataGridViewb.Enabled = false;
                bureaus.Clear();
                listBox1.Items.Clear();
                if (Int32.Parse(comboBox7.SelectedValue.ToString()) == 1)
                {

                    comboBox2.Enabled = true;

                    //comboBox3.Items.Clear(); 
                    comboBox3.Text = "";
                    // comboBox4.Items.Clear(); 
                    comboBox4.Text = "";
                    //  comboBox2.Items.Clear(); 
                    comboBox2.Text = "";
                    comboBox5.Text = "";
                    comboBox6.Text = "";
                    int id = Int32.Parse(comboBox7.SelectedValue.ToString());
                    FillCommune(id);
                    dataGridViewb.Rows.Clear();
                    bureaus.Clear();
                    listBox1.Items.Clear();
                }
                else
                {
                    comboBox2.Enabled = false;

                    comboBox2.Text = "";
                    comboBox3.Text = "";
                    comboBox5.Text = "";
                    comboBox6.Text = "";
                    comboBox4.Text = "";
                    if (Int32.Parse(comboBox7.SelectedValue.ToString()) == 30)
                    {
                        int RégionID = Convert.ToInt32(comboBox7.SelectedValue.ToString());
                        FillCommune(RégionID);
                        this.comboBox5.Enabled = false;
                        dataGridViewb.Rows.Clear();
                        bureaus.Clear();
                        listBox1.Items.Clear();
                    }
                    else
                    {
                        this.Filllocalite(Int32.Parse(comboBox7.SelectedValue.ToString()));
                        this.comboBox5.Enabled = true;
                        dataGridViewb.Rows.Clear();
                        bureaus.Clear();
                        listBox1.Items.Clear();
                    }
                    /*int id = Int32.Parse(comboBox1.SelectedValue.ToString());
                    SQL = "SELECT NomLocalité,LocalitéID FROM Région AS A INNER JOIN Localité AS B on A.RégionID = B.RégionID where  A.RégionID=" + id + "";
                    con.Open();
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter(SQL, con);
                    SqlCommandBuilder commandBuilder1 = new SqlCommandBuilder(sqlAdapter);
                    con.Close();
                    DataTable dt = new DataTable();
                    sqlAdapter.Fill(dt);

                    comboBox5.ValueMember = "LocalitéID";
                    comboBox5.DisplayMember = "NomLocalité";

                    comboBox5.DataSource = dt;*/
                }

            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            if (Int32.Parse(comboBox2.SelectedValue.ToString()) == 30)
            {
                int CommuneID = Convert.ToInt32(comboBox2.SelectedValue.ToString());
                this.FillArrondis(CommuneID);
                this.comboBox3.Enabled = false;
                dataGridViewb.Rows.Clear();
                bureaus.Clear();
                listBox1.Items.Clear();
            }
            else
            {
                int CommuneID = Convert.ToInt32(comboBox2.SelectedValue.ToString());
                this.FillArrondis(CommuneID);
                this.comboBox3.Enabled = true;
                dataGridViewb.Rows.Clear();
                bureaus.Clear();
                listBox1.Items.Clear();
            }
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            comboBox4.Text = "";
            if (Int32.Parse(comboBox3.SelectedValue.ToString()) == 30)
            {
                int ArrondisID = Convert.ToInt32(comboBox3.SelectedValue.ToString());
                this.FillCenter(ArrondisID);
                this.comboBox4.Enabled = false;
                dataGridViewb.Rows.Clear();
                bureaus.Clear();
                listBox1.Items.Clear();
            }
            else
            {
                int ArrondisID = Convert.ToInt32(comboBox3.SelectedValue.ToString());
                this.FillCenter(ArrondisID);
                this.comboBox4.Enabled = true;
                dataGridViewb.Rows.Clear();
                bureaus.Clear();
                listBox1.Items.Clear();
            }
        }

        private void comboBox5_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox5.SelectedIndex >= 0)
            {
                
                // comboBox4..Clear(); 
                comboBox6.Text = "";
                
                if (Int32.Parse(comboBox5.SelectedValue.ToString()) == 30)
                {
                    int ArrondisID = Convert.ToInt32(comboBox5.SelectedValue.ToString());
                    this.FillCenterlocalite(ArrondisID);
                    this.comboBox6.Enabled = false;
                    dataGridViewb.Rows.Clear();
                    bureaus.Clear();
                    listBox1.Items.Clear();
                }
                else
                {
                    int ArrondisID = Convert.ToInt32(comboBox5.SelectedValue.ToString());
                    this.FillCenterlocalite(ArrondisID);
                    this.comboBox6.Enabled = true;
                    dataGridViewb.Rows.Clear();
                    bureaus.Clear();
                    listBox1.Items.Clear();
                }
            }
        }
        private void FillRégion()
        {

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT RégionID, NomRégion FROM Région";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            objDs.Tables[0].Rows.Add(30, "");
            con.Close();
            comboBox7.ValueMember = "RégionID";
            comboBox7.DisplayMember = "NomRégion";
            comboBox7.DataSource = objDs.Tables[0];
            comboBox7.SelectedValue = 1;
        }

        private void FillCommune(int RégionID)
        {

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT CommuneID, NomCommune FROM Commune WHERE RégionID =@RégionID";
            if (RégionID == 30)
            {
               int reg = 1;
               cmd.Parameters.AddWithValue("@RégionID", reg);


            }
            else
            {
                cmd.Parameters.AddWithValue("@RégionID", RégionID);
            }

            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            objDs.Tables[0].Rows.Add(30, "");
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBox2.ValueMember = "CommuneID";
                comboBox2.DisplayMember = "NomCommune";
                comboBox2.DataSource = objDs.Tables[0];
                comboBox2.SelectedValue = 30;
            }
        }
        private void FillArrondis(int CommuneID)
        {

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT ArrondisID, NomArrondis FROM Arrondissement WHERE CommuneID =@CommuneID";
            if (CommuneID == 30)
            {
                int com = 1;
                cmd.Parameters.AddWithValue("@CommuneID", com);


            }
            else
            {
                cmd.Parameters.AddWithValue("@CommuneID", CommuneID);
            }
            //cmd.Parameters.AddWithValue("@CommuneID", CommuneID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            objDs.Tables[0].Rows.Add(30, "");
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBox3.ValueMember = "ArrondisID";
                comboBox3.DisplayMember = "NomArrondis";
                comboBox3.DataSource = objDs.Tables[0];
                comboBox3.SelectedValue = 30;


            }
        }

        private void Filllocalite(int _reg)
        {
            int reg = _reg;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT LocalitéID, NomLocalité FROM Localité WHERE RégionID = @RégionID";
            if (_reg == 30)
            {
                int com = 1;
                cmd.Parameters.AddWithValue("@RégionID", com);
            }
            else
            {
                cmd.Parameters.AddWithValue("@RégionID", _reg);
            }
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            objDs.Tables[0].Rows.Add(30, "");
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBox5.ValueMember = "LocalitéID";
                comboBox5.DisplayMember = "NomLocalité";
                comboBox5.DataSource = objDs.Tables[0];
                comboBox5.SelectedValue = 30;
            }
        }
        private void FillCenter(int ArrondisID)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT CodeCentre,( CONVERT(Nvarchar(50), CodeCentre)) + ' - '+CentreVote as CentreVote,CentreID FROM  Arrondissement AS a INNER JOIN CentreVote AS c ON a.ArrondisID = c.ArrondisID where a.ArrondisID=@ArrondisID and c.CodeCentre is not null order by c.CodeCentre";
            if (ArrondisID == 30)
            {
                int reg = 1;
                cmd.Parameters.AddWithValue("@ArrondisID", reg);
            }
            else
            {
                cmd.Parameters.AddWithValue("@ArrondisID", ArrondisID);
            }

            DataTable dt = new DataTable();

            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            // con.Open();
            dAdapter.Fill(dt);

            comboBox4.ValueMember = "CentreID";
            comboBox4.DisplayMember = "CentreVote";
            comboBox4.DataSource = dt;

                
           
        }
        private void FillCenterlocalite(int ArrondisID)
        {

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT CentreVote,CentreID FROM  Localité AS a INNER JOIN CentreVote AS c ON a.LocalitéID = c.LocalitéID where a.LocalitéID=@LocaliteID";

            
            if (ArrondisID == 30)
            {
                int reg = 1;
                cmd.Parameters.AddWithValue("@LocaliteID", reg);
            }
            else
            {
                cmd.Parameters.AddWithValue("@LocaliteID", ArrondisID);
            }

            DataTable dt = new DataTable();

            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            // con.Open();
            dAdapter.Fill(dt);

            comboBox6.ValueMember = "CentreID";
            comboBox6.DisplayMember = "CentreVote";
            comboBox6.DataSource = dt;

        }


        private void comboBox6_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataGridViewb.Rows.Clear();
            dataGridViewb.Enabled = true;
            bureaus.Clear();
            listBox1.Items.Clear();
            int tstv = 0;
            int id = Int32.Parse(comboBox6.SelectedValue.ToString());
            SQL = "SELECT c.CentreID,BureauID,( CONVERT(Nvarchar(50), CodeBureau)) + ' - '+NomBureau as NomBureau FROM  Bureau  AS b INNER JOIN CentreVote AS c ON b.CentreID = c.CentreID where c.CentreID=" + id + " order by CodeBureau";
            con.Open();
            SqlDataAdapter sqlAdapter = new SqlDataAdapter(SQL, con);
            SqlCommandBuilder commandBuilder1 = new SqlCommandBuilder(sqlAdapter);
            con.Close();
            DataTable dt = new DataTable();
            sqlAdapter.Fill(dt);

            foreach (DataRow row in dt.Rows)
            {

                string[] row1 = null;

                {
                    row1 = new string[] {
                            row["BureauID"].ToString()
                             };

                }

                dataGridViewb.Rows.Add(row1);
                dataGridViewb[1, tstv].Value = false;
                dataGridViewb[2, tstv].Value = row["NomBureau"].ToString();
                dataGridViewb[3, tstv].Value = row["CentreID"].ToString();
                tstv++;

            } 
        }
         
        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {

           
                dataGridViewb.Rows.Clear();
                bureaus.Clear();
                listBox1.Items.Clear();
                dataGridViewb.Enabled = true;
                int tstv = 0;
                int id = Int32.Parse(comboBox4.SelectedValue.ToString());
                SQL = "SELECT c.CentreID,BureauID, ( CONVERT(Nvarchar(50), CodeBureau)) + ' - '+NomBureau as NomBureau FROM  Bureau  AS b INNER JOIN CentreVote AS c ON b.CentreID = c.CentreID where c.CentreID=" + id + "";
                con.Open();
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(SQL, con);
                SqlCommandBuilder commandBuilder1 = new SqlCommandBuilder(sqlAdapter);
                con.Close();
                DataTable dt = new DataTable();
                sqlAdapter.Fill(dt);

                foreach (DataRow row in dt.Rows)
                {

                    string[] row1 = null;

                    {
                        row1 = new string[] {
                            row["BureauID"].ToString()
                             };

                    }
                    dataGridViewb.Rows.Add(row1);
                    dataGridViewb[1, tstv].Value = false;
                    dataGridViewb[2, tstv].Value = row["NomBureau"].ToString();
                    dataGridViewb[3, tstv].Value = row["CentreID"].ToString();
                    tstv++;

                }
            
            }

        private void dataGridViewb_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowindex = dataGridViewb.CurrentRow.Index;
            if ((bool)dataGridViewb[1, rowindex].Value == true)
            {
                dataGridViewb.Rows[e.RowIndex].Cells[1].Value = false;
            }
            else
            {
                dataGridViewb.Rows[e.RowIndex].Cells[1].Value = true;
            }
            if ((bool)dataGridViewb[1, rowindex].Value == true)
            {
                listBox1.Items.Add((string)dataGridViewb[2, rowindex].Value);
                bureaus.Add(Convert.ToInt32((string)dataGridViewb[0, rowindex].Value));
            }
            else if ((bool)dataGridViewb[1, rowindex].Value == false)
            {
                listBox1.Items.Remove((string)dataGridViewb[2, rowindex].Value);
                bureaus.Remove(Convert.ToInt32((string)dataGridViewb[0, rowindex].Value));
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                comboBox7.Enabled = false;

            }
            else
            {
                comboBox7.Enabled = true;
            }
        }
    }
}
