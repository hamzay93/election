﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class réactivation : Form
    {
        public réactivation()
        {
            InitializeComponent();
        }
        Connection conn = new Connection();
        GetValue get = new GetValue();
        private void réactivation_Load(object sender, EventArgs e)
        {
            this.FillAgent();
        }
        private void FillAgent()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT  NomAgent FROM Agent where Statut = 'Inactif' ";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBoxAgent.ValueMember = "AgentID";
            comboBoxAgent.DisplayMember = "NomAgent";
            comboBoxAgent.DataSource = objDs.Tables[0];
        }
        public int getAgentID(string AgentID)
        {
            //SqlDataReader rd = null;
            string Id = AgentID;
            string query = "select AgentID from Agent where NomAgent='" + Id + "'";
            string sCon = conn.connectdb();
            SqlConnection dbConn;
            dbConn = new SqlConnection(sCon);
            dbConn.Open();
            SqlCommand cmd = new SqlCommand(query, dbConn);
            int result = ((int)cmd.ExecuteScalar());
            //SaisieElecteur se=new SaisieElecteur();
            int agentID = result;
            return agentID;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                string sCon = conn.connectdb();
                int ID = this.getAgentID(comboBoxAgent.Text);
                string updateCmd = "UPDATE Agent SET Statut =@Statut,AgentModif=@AgentModif,DateModif=@DateModif where AgentID =(" + ID + ")";
                SqlConnection dbConn;
                dbConn = new SqlConnection(sCon);
                dbConn.Open();
                //GetValue value = new GetValue();
                //DateTime dt = new DateTime();
                //dt.ToShortDateString();
                string dat = DateTime.Now.ToString("dd/MM/yyyy");
                DateTime date4 = DateTime.Parse(dat);
                SqlCommand myCommand = new SqlCommand(updateCmd, dbConn);
                // Create parameters for the SqlCommand object
                // initialize with input-form field values
                myCommand.Parameters.AddWithValue("@Statut", "Actif");
                myCommand.Parameters.AddWithValue("@AgentModif", get.getAgentModif());
                myCommand.Parameters.AddWithValue("@DateModif", date4);
                myCommand.ExecuteNonQuery();
                MessageBox.Show("Agent réactivé avec succès");
                //this.Hide();
                this.FillAgent();
            }
            catch (Exception ex)
            {

            }
        }
    }
}
