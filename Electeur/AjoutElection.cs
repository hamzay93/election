﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class AjoutElection : Form
    {
        public AjoutElection()
        {
            InitializeComponent();
        }
        Connection conn = new Connection();
        private void button7_Click(object sender, EventArgs e)
        {
            if (textBox10.Text.Trim().Length == 0)
            {
                MessageBox.Show("veuillez saisir l'inttitulé");
                textBox10.Focus();
            }
           
                else
                {

                    if (comboBox5.Text == "")
                    {
                        MessageBox.Show("veuillez choisir on est dans quel tour d'election");
                    }
                    else
                    {

                        //Connection cn = new Connection();
                        string insertCmd = "INSERT INTO Election (ObjetElection,Annee,DateElection,Etat,TourElection,UserCreation,UserModification,DateCreation,DateModification) VALUES (@Objet,@Annee,@DateElection,@Etat,@TourElection,@UserCreation,@UserModification,@DateCreation,@DateModification)";
                        SqlConnection dbConn;
                        dbConn = new SqlConnection(conn.connectdb());
                        dbConn.Open();
                        Login lg = new Login();
                        string dat = DateTime.Now.ToString("yyyy-MM-dd");
                        DateTime datea = DateTime.Parse(dateTimePicker1.Value.ToString("yyyy-MM-dd"));
                        DateTime datee = DateTime.Parse(dateTimePicker3.Value.ToString("yyyy-MM-dd"));
                        GetValue value = new GetValue();
                        //getAdress add = new getAdress();
                        SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                       
                        if (comboBox5.Text != "")
                        {
                            int TourElectionID = Convert.ToInt32(comboBox5.SelectedValue.ToString());
                            myCommand.Parameters.AddWithValue("@TourElection", TourElectionID);
                        }
                        if (radioButton1.Checked)
                        {
                            myCommand.Parameters.AddWithValue("@Etat", 1);
                        }
                        if (radioButton2.Checked)
                        {
                            myCommand.Parameters.AddWithValue("@Etat", 0);
                        }
                        myCommand.Parameters.AddWithValue("@Objet", textBox10.Text);

                        myCommand.Parameters.AddWithValue("@Annee", datea);
                        myCommand.Parameters.AddWithValue("@DateElection", datee);
                        myCommand.Parameters.AddWithValue("@UserCreation", value.getAgentCréation());
                        myCommand.Parameters.AddWithValue("@DateCreation", dat);
                        myCommand.Parameters.AddWithValue("@UserModification", value.getAgentCréation());
                        myCommand.Parameters.AddWithValue("@DateModification", dat);
                        myCommand.ExecuteNonQuery();

                        MessageBox.Show("Ajoutée avec succès");
                        textBox10.Clear();
                    }

                
            }
        }
        private void FillCv()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT TourElectionID,ObjetTourElection FROM TourElection";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBox5.ValueMember = "TourElectionID";
            comboBox5.DisplayMember = "ObjetTourElection";
            comboBox5.DataSource = objDs.Tables[0];
        }
        //private void FillCv2()H:\sdsi\Election Communale\Electeur\AjoutTourElection.cs
        //{
        //    string sCon = conn.connectdb();
        //    SqlConnection con = new SqlConnection(sCon);
        //    SqlCommand cmd = new SqlCommand();
        //    cmd.Connection = con;
        //    cmd.CommandType = CommandType.Text;
        //    cmd.CommandText = "SELECT TypeElectionID,ObjetTypeElection FROM TypeElection";
        //    DataSet objDs = new DataSet();
        //    SqlDataAdapter dAdapter = new SqlDataAdapter();
        //    dAdapter.SelectCommand = cmd;
        //    con.Open();
        //    dAdapter.Fill(objDs);
        //    con.Close();
        //    comboBox3.ValueMember = "TypeElectionID";
        //    comboBox3.DisplayMember = "ObjetTypeElection";
        //    comboBox3.DataSource = objDs.Tables[0];
        //}
        private void AjoutElection_Load(object sender, EventArgs e)
        {
            this.FillCv();
            //this.FillCv2();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}
