﻿namespace Electeur
{
    partial class AjoutMembrelieuvote
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label36 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.comboBox10 = new System.Windows.Forms.ComboBox();
            this.comboBox9 = new System.Windows.Forms.ComboBox();
            this.label35 = new System.Windows.Forms.Label();
            this.comboBox8 = new System.Windows.Forms.ComboBox();
            this.label34 = new System.Windows.Forms.Label();
            this.comboBox7 = new System.Windows.Forms.ComboBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.dateTimePicker4 = new System.Windows.Forms.DateTimePicker();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.comboBox6 = new System.Windows.Forms.ComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.button8 = new System.Windows.Forms.Button();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel8.SuspendLayout();
            this.SuspendLayout();
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.BackColor = System.Drawing.Color.LightSeaGreen;
            this.label36.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold);
            this.label36.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label36.Location = new System.Drawing.Point(46, 21);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(268, 23);
            this.label36.TabIndex = 13;
            this.label36.Text = "AJOUT Membre de Lieu de Vote";
            // 
            // panel8
            // 
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.comboBox1);
            this.panel8.Controls.Add(this.label1);
            this.panel8.Controls.Add(this.comboBox10);
            this.panel8.Controls.Add(this.comboBox9);
            this.panel8.Controls.Add(this.label35);
            this.panel8.Controls.Add(this.comboBox8);
            this.panel8.Controls.Add(this.label34);
            this.panel8.Controls.Add(this.comboBox7);
            this.panel8.Controls.Add(this.label33);
            this.panel8.Controls.Add(this.label32);
            this.panel8.Controls.Add(this.dateTimePicker4);
            this.panel8.Controls.Add(this.label31);
            this.panel8.Controls.Add(this.label30);
            this.panel8.Controls.Add(this.textBox14);
            this.panel8.Controls.Add(this.label29);
            this.panel8.Controls.Add(this.textBox13);
            this.panel8.Controls.Add(this.label26);
            this.panel8.Controls.Add(this.textBox11);
            this.panel8.Controls.Add(this.comboBox6);
            this.panel8.Controls.Add(this.label27);
            this.panel8.Controls.Add(this.label28);
            this.panel8.Controls.Add(this.button8);
            this.panel8.Controls.Add(this.textBox12);
            this.panel8.Location = new System.Drawing.Point(30, 37);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(751, 403);
            this.panel8.TabIndex = 12;
            // 
            // comboBox10
            // 
            this.comboBox10.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox10.FormattingEnabled = true;
            this.comboBox10.Items.AddRange(new object[] {
            "MASCULIN",
            "FEMININ"});
            this.comboBox10.Location = new System.Drawing.Point(125, 94);
            this.comboBox10.Name = "comboBox10";
            this.comboBox10.Size = new System.Drawing.Size(238, 21);
            this.comboBox10.TabIndex = 21;
            // 
            // comboBox9
            // 
            this.comboBox9.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox9.FormattingEnabled = true;
            this.comboBox9.Location = new System.Drawing.Point(125, 169);
            this.comboBox9.Name = "comboBox9";
            this.comboBox9.Size = new System.Drawing.Size(238, 21);
            this.comboBox9.TabIndex = 23;
            this.comboBox9.SelectedIndexChanged += new System.EventHandler(this.comboBox9_SelectedIndexChanged);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.3F, System.Drawing.FontStyle.Bold);
            this.label35.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label35.Location = new System.Drawing.Point(22, 169);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(99, 15);
            this.label35.TabIndex = 40;
            this.label35.Text = "Centre de vote";
            // 
            // comboBox8
            // 
            this.comboBox8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox8.FormattingEnabled = true;
            this.comboBox8.Location = new System.Drawing.Point(125, 130);
            this.comboBox8.Name = "comboBox8";
            this.comboBox8.Size = new System.Drawing.Size(238, 21);
            this.comboBox8.TabIndex = 22;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.3F, System.Drawing.FontStyle.Bold);
            this.label34.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label34.Location = new System.Drawing.Point(60, 136);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(59, 15);
            this.label34.TabIndex = 38;
            this.label34.Text = "Election";
            // 
            // comboBox7
            // 
            this.comboBox7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox7.FormattingEnabled = true;
            this.comboBox7.Location = new System.Drawing.Point(125, 276);
            this.comboBox7.Name = "comboBox7";
            this.comboBox7.Size = new System.Drawing.Size(238, 21);
            this.comboBox7.TabIndex = 26;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.3F, System.Drawing.FontStyle.Bold);
            this.label33.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label33.Location = new System.Drawing.Point(16, 282);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(105, 15);
            this.label33.TabIndex = 36;
            this.label33.Text = "Bureau de Vote";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.3F, System.Drawing.FontStyle.Bold);
            this.label32.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label32.Location = new System.Drawing.Point(80, 100);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(39, 15);
            this.label32.TabIndex = 35;
            this.label32.Text = "Sexe";
            // 
            // dateTimePicker4
            // 
            this.dateTimePicker4.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker4.Location = new System.Drawing.Point(125, 205);
            this.dateTimePicker4.Name = "dateTimePicker4";
            this.dateTimePicker4.Size = new System.Drawing.Size(238, 20);
            this.dateTimePicker4.TabIndex = 24;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.3F, System.Drawing.FontStyle.Bold);
            this.label31.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label31.Location = new System.Drawing.Point(11, 211);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(108, 15);
            this.label31.TabIndex = 32;
            this.label31.Text = "Date Naissance";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.3F, System.Drawing.FontStyle.Bold);
            this.label30.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label30.Location = new System.Drawing.Point(275, 20);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(45, 15);
            this.label30.TabIndex = 31;
            this.label30.Text = "Nom2";
            // 
            // textBox14
            // 
            this.textBox14.Location = new System.Drawing.Point(327, 20);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(155, 20);
            this.textBox14.TabIndex = 18;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.3F, System.Drawing.FontStyle.Bold);
            this.label29.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label29.Location = new System.Drawing.Point(486, 20);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(45, 15);
            this.label29.TabIndex = 29;
            this.label29.Text = "Nom3";
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(537, 20);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(157, 20);
            this.textBox13.TabIndex = 19;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.3F, System.Drawing.FontStyle.Bold);
            this.label26.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label26.Location = new System.Drawing.Point(13, 65);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(106, 15);
            this.label26.TabIndex = 27;
            this.label26.Text = "Lieu Naissance";
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(125, 62);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(238, 20);
            this.textBox11.TabIndex = 20;
            // 
            // comboBox6
            // 
            this.comboBox6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox6.FormattingEnabled = true;
            this.comboBox6.Location = new System.Drawing.Point(125, 240);
            this.comboBox6.Name = "comboBox6";
            this.comboBox6.Size = new System.Drawing.Size(238, 21);
            this.comboBox6.TabIndex = 25;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.3F, System.Drawing.FontStyle.Bold);
            this.label27.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label27.Location = new System.Drawing.Point(57, 240);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(62, 15);
            this.label27.TabIndex = 22;
            this.label27.Text = "Fonction";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.3F, System.Drawing.FontStyle.Bold);
            this.label28.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label28.Location = new System.Drawing.Point(74, 23);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(45, 15);
            this.label28.TabIndex = 19;
            this.label28.Text = "Nom1";
            // 
            // button8
            // 
            this.button8.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold);
            this.button8.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.button8.Location = new System.Drawing.Point(580, 360);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(114, 38);
            this.button8.TabIndex = 27;
            this.button8.Text = "Enregistrer";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(125, 20);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(139, 20);
            this.textBox12.TabIndex = 17;
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(125, 312);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(238, 21);
            this.comboBox1.TabIndex = 41;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.3F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(66, 318);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 15);
            this.label1.TabIndex = 42;
            this.label1.Text = "Région";
            // 
            // AjoutMembrelieuvote
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSeaGreen;
            this.ClientSize = new System.Drawing.Size(811, 478);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.panel8);
            this.Name = "AjoutMembrelieuvote";
            this.Text = "AjoutMembrelieuvote";
            this.Load += new System.EventHandler(this.AjoutMembrelieuvote_Load);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.ComboBox comboBox10;
        private System.Windows.Forms.ComboBox comboBox9;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.ComboBox comboBox8;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.ComboBox comboBox7;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.DateTimePicker dateTimePicker4;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.ComboBox comboBox6;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label1;
    }
}