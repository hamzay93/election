﻿namespace Electeur
{
    partial class SaisieElecteur
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.textBoxProf = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxPortable = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxAdresseCompl = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.comboBoxAdresse = new System.Windows.Forms.ComboBox();
            this.comboBoxCommune = new System.Windows.Forms.ComboBox();
            this.comboBoxArrondis = new System.Windows.Forms.ComboBox();
            this.comboBoxRégion = new System.Windows.Forms.ComboBox();
            this.textBoxPhone = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.LabelStatut = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dateTimePickerDOB = new System.Windows.Forms.DateTimePicker();
            this.comboBoxSexe = new System.Windows.Forms.ComboBox();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label18 = new System.Windows.Forms.Label();
            this.textBoxCodeElecteur = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.textBoxNewCNI = new System.Windows.Forms.TextBox();
            this.textBoxNomMère3 = new System.Windows.Forms.TextBox();
            this.textBoxNomMère2 = new System.Windows.Forms.TextBox();
            this.textBoxNomMère1 = new System.Windows.Forms.TextBox();
            this.textBoxAncCNI = new System.Windows.Forms.TextBox();
            this.textBoxPOB = new System.Windows.Forms.TextBox();
            this.textBoxNom3 = new System.Windows.Forms.TextBox();
            this.textBoxNom2 = new System.Windows.Forms.TextBox();
            this.textBoxNom1 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Berlin Sans FB Demi", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(122, 74);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 23);
            this.label1.TabIndex = 1;
            this.label1.Text = "Identification";
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.BackColor = System.Drawing.Color.LightSeaGreen;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.textBoxProf);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.textBoxPortable);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.textBoxAdresseCompl);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.comboBoxAdresse);
            this.panel2.Controls.Add(this.comboBoxCommune);
            this.panel2.Controls.Add(this.comboBoxArrondis);
            this.panel2.Controls.Add(this.comboBoxRégion);
            this.panel2.Controls.Add(this.textBoxPhone);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Location = new System.Drawing.Point(109, 365);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(791, 189);
            this.panel2.TabIndex = 2;
            // 
            // textBoxProf
            // 
            this.textBoxProf.Location = new System.Drawing.Point(591, 25);
            this.textBoxProf.Name = "textBoxProf";
            this.textBoxProf.Size = new System.Drawing.Size(148, 20);
            this.textBoxProf.TabIndex = 31;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(482, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 19);
            this.label3.TabIndex = 30;
            this.label3.Text = "Profession ";
            // 
            // textBoxPortable
            // 
            this.textBoxPortable.Location = new System.Drawing.Point(376, 24);
            this.textBoxPortable.Name = "textBoxPortable";
            this.textBoxPortable.Size = new System.Drawing.Size(100, 20);
            this.textBoxPortable.TabIndex = 29;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(296, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 19);
            this.label2.TabIndex = 28;
            this.label2.Text = "Portable ";
            // 
            // textBoxAdresseCompl
            // 
            this.textBoxAdresseCompl.Location = new System.Drawing.Point(486, 125);
            this.textBoxAdresseCompl.Multiline = true;
            this.textBoxAdresseCompl.Name = "textBoxAdresseCompl";
            this.textBoxAdresseCompl.Size = new System.Drawing.Size(253, 37);
            this.textBoxAdresseCompl.TabIndex = 11;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(482, 103);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(193, 19);
            this.label17.TabIndex = 10;
            this.label17.Text = "Adresse Complémentaire ";
            // 
            // comboBoxAdresse
            // 
            this.comboBoxAdresse.DisplayMember = "NomAdresse";
            this.comboBoxAdresse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxAdresse.FormattingEnabled = true;
            this.comboBoxAdresse.Location = new System.Drawing.Point(591, 79);
            this.comboBoxAdresse.Name = "comboBoxAdresse";
            this.comboBoxAdresse.Size = new System.Drawing.Size(148, 21);
            this.comboBoxAdresse.TabIndex = 27;
            this.comboBoxAdresse.ValueMember = "NomAdresse";
            // 
            // comboBoxCommune
            // 
            this.comboBoxCommune.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCommune.FormattingEnabled = true;
            this.comboBoxCommune.Location = new System.Drawing.Point(591, 50);
            this.comboBoxCommune.Name = "comboBoxCommune";
            this.comboBoxCommune.Size = new System.Drawing.Size(148, 21);
            this.comboBoxCommune.TabIndex = 25;
            this.comboBoxCommune.SelectedIndexChanged += new System.EventHandler(this.comboBoxCommune_SelectedIndexChanged);
            // 
            // comboBoxArrondis
            // 
            this.comboBoxArrondis.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxArrondis.FormattingEnabled = true;
            this.comboBoxArrondis.Location = new System.Drawing.Point(192, 81);
            this.comboBoxArrondis.Name = "comboBoxArrondis";
            this.comboBoxArrondis.Size = new System.Drawing.Size(166, 21);
            this.comboBoxArrondis.TabIndex = 26;
            this.comboBoxArrondis.SelectedIndexChanged += new System.EventHandler(this.comboBoxArrondis_SelectedIndexChanged);
            // 
            // comboBoxRégion
            // 
            this.comboBoxRégion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxRégion.Enabled = false;
            this.comboBoxRégion.FormattingEnabled = true;
            this.comboBoxRégion.Location = new System.Drawing.Point(192, 53);
            this.comboBoxRégion.Name = "comboBoxRégion";
            this.comboBoxRégion.Size = new System.Drawing.Size(166, 21);
            this.comboBoxRégion.TabIndex = 24;
            this.comboBoxRégion.SelectedIndexChanged += new System.EventHandler(this.comboBoxRégion_SelectedIndexChanged);
            // 
            // textBoxPhone
            // 
            this.textBoxPhone.Location = new System.Drawing.Point(192, 22);
            this.textBoxPhone.Name = "textBoxPhone";
            this.textBoxPhone.Size = new System.Drawing.Size(98, 20);
            this.textBoxPhone.TabIndex = 22;
            this.textBoxPhone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxPhone_KeyPress);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(482, 79);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(70, 19);
            this.label15.TabIndex = 4;
            this.label15.Text = "Adresse ";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(20, 80);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(128, 19);
            this.label14.TabIndex = 3;
            this.label14.Text = "Arrondissement ";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(482, 53);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(87, 19);
            this.label13.TabIndex = 2;
            this.label13.Text = "Commune ";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(23, 50);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(79, 19);
            this.label12.TabIndex = 1;
            this.label12.Text = "Région     ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(20, 24);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(115, 19);
            this.label11.TabIndex = 0;
            this.label11.Text = "Téléphone fixe";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Berlin Sans FB Demi", 14.25F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(122, 348);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(124, 23);
            this.label10.TabIndex = 3;
            this.label10.Text = "Coordonnées";
            // 
            // LabelStatut
            // 
            this.LabelStatut.AutoSize = true;
            this.LabelStatut.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelStatut.Location = new System.Drawing.Point(757, 81);
            this.LabelStatut.Name = "LabelStatut";
            this.LabelStatut.Size = new System.Drawing.Size(41, 16);
            this.LabelStatut.TabIndex = 4;
            this.LabelStatut.Text = "Créé";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.button1.Location = new System.Drawing.Point(297, 594);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(189, 33);
            this.button1.TabIndex = 7;
            this.button1.Text = "Enregistrer - Créer";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Transparent;
            this.button2.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold);
            this.button2.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.button2.Location = new System.Drawing.Point(509, 594);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(189, 33);
            this.button2.TabIndex = 8;
            this.button2.Text = "Enregistrer - Fermer";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(122, 22);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(303, 23);
            this.label25.TabIndex = 9;
            this.label25.Text = "ENREGISTREMENT D\'UN ELECTEUR";
            this.label25.Click += new System.EventHandler(this.label25_Click);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(643, 24);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(113, 20);
            this.label26.TabIndex = 10;
            this.label26.Text = "Date du Jour";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.LightSeaGreen;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.dateTimePickerDOB);
            this.panel3.Controls.Add(this.comboBoxSexe);
            this.panel3.Controls.Add(this.dateTimePicker2);
            this.panel3.Controls.Add(this.dateTimePicker1);
            this.panel3.Controls.Add(this.label18);
            this.panel3.Controls.Add(this.textBoxCodeElecteur);
            this.panel3.Controls.Add(this.label20);
            this.panel3.Controls.Add(this.textBoxNewCNI);
            this.panel3.Controls.Add(this.textBoxNomMère3);
            this.panel3.Controls.Add(this.textBoxNomMère2);
            this.panel3.Controls.Add(this.textBoxNomMère1);
            this.panel3.Controls.Add(this.textBoxAncCNI);
            this.panel3.Controls.Add(this.textBoxPOB);
            this.panel3.Controls.Add(this.textBoxNom3);
            this.panel3.Controls.Add(this.textBoxNom2);
            this.panel3.Controls.Add(this.textBoxNom1);
            this.panel3.Controls.Add(this.label21);
            this.panel3.Controls.Add(this.label22);
            this.panel3.Controls.Add(this.label23);
            this.panel3.Controls.Add(this.label24);
            this.panel3.Controls.Add(this.label27);
            this.panel3.Controls.Add(this.label28);
            this.panel3.Controls.Add(this.label29);
            this.panel3.Controls.Add(this.label30);
            this.panel3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel3.Location = new System.Drawing.Point(109, 90);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(791, 200);
            this.panel3.TabIndex = 0;
            // 
            // dateTimePickerDOB
            // 
            this.dateTimePickerDOB.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerDOB.Location = new System.Drawing.Point(187, 55);
            this.dateTimePickerDOB.MaxDate = new System.DateTime(2005, 7, 28, 17, 22, 29, 166);
            this.dateTimePickerDOB.Name = "dateTimePickerDOB";
            this.dateTimePickerDOB.Size = new System.Drawing.Size(184, 20);
            this.dateTimePickerDOB.TabIndex = 11;
            this.dateTimePickerDOB.Value = new System.DateTime(2005, 7, 28, 17, 22, 29, 166);
            // 
            // comboBoxSexe
            // 
            this.comboBoxSexe.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSexe.FormattingEnabled = true;
            this.comboBoxSexe.Items.AddRange(new object[] {
            "FEMININ",
            "MASCULIN"});
            this.comboBoxSexe.Location = new System.Drawing.Point(591, 167);
            this.comboBoxSexe.Name = "comboBoxSexe";
            this.comboBoxSexe.Size = new System.Drawing.Size(148, 21);
            this.comboBoxSexe.TabIndex = 21;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Enabled = false;
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker2.Location = new System.Drawing.Point(591, 139);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(148, 20);
            this.dateTimePicker2.TabIndex = 19;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Enabled = false;
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(591, 111);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(148, 20);
            this.dateTimePicker1.TabIndex = 17;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(395, 167);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(47, 19);
            this.label18.TabIndex = 22;
            this.label18.Text = "Sexe ";
            // 
            // textBoxCodeElecteur
            // 
            this.textBoxCodeElecteur.Location = new System.Drawing.Point(187, 167);
            this.textBoxCodeElecteur.Name = "textBoxCodeElecteur";
            this.textBoxCodeElecteur.Size = new System.Drawing.Size(184, 20);
            this.textBoxCodeElecteur.TabIndex = 20;
            this.textBoxCodeElecteur.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxCodeElecteur_KeyPress);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label20.Location = new System.Drawing.Point(17, 167);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(115, 19);
            this.label20.TabIndex = 23;
            this.label20.Text = "Code Electeur ";
            // 
            // textBoxNewCNI
            // 
            this.textBoxNewCNI.Location = new System.Drawing.Point(187, 139);
            this.textBoxNewCNI.Name = "textBoxNewCNI";
            this.textBoxNewCNI.Size = new System.Drawing.Size(184, 20);
            this.textBoxNewCNI.TabIndex = 18;
            this.textBoxNewCNI.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxNewCNI_KeyPress);
            // 
            // textBoxNomMère3
            // 
            this.textBoxNomMère3.Location = new System.Drawing.Point(590, 83);
            this.textBoxNomMère3.Name = "textBoxNomMère3";
            this.textBoxNomMère3.Size = new System.Drawing.Size(148, 20);
            this.textBoxNomMère3.TabIndex = 15;
            this.textBoxNomMère3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxNom1_KeyPress);
            // 
            // textBoxNomMère2
            // 
            this.textBoxNomMère2.Location = new System.Drawing.Point(399, 83);
            this.textBoxNomMère2.Name = "textBoxNomMère2";
            this.textBoxNomMère2.Size = new System.Drawing.Size(163, 20);
            this.textBoxNomMère2.TabIndex = 14;
            this.textBoxNomMère2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxNom1_KeyPress);
            // 
            // textBoxNomMère1
            // 
            this.textBoxNomMère1.Location = new System.Drawing.Point(187, 83);
            this.textBoxNomMère1.Name = "textBoxNomMère1";
            this.textBoxNomMère1.Size = new System.Drawing.Size(184, 20);
            this.textBoxNomMère1.TabIndex = 13;
            this.textBoxNomMère1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxNom1_KeyPress);
            // 
            // textBoxAncCNI
            // 
            this.textBoxAncCNI.Location = new System.Drawing.Point(187, 111);
            this.textBoxAncCNI.Name = "textBoxAncCNI";
            this.textBoxAncCNI.Size = new System.Drawing.Size(184, 20);
            this.textBoxAncCNI.TabIndex = 16;
            this.textBoxAncCNI.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxAncCNI_KeyPress);
            // 
            // textBoxPOB
            // 
            this.textBoxPOB.Location = new System.Drawing.Point(591, 55);
            this.textBoxPOB.Name = "textBoxPOB";
            this.textBoxPOB.Size = new System.Drawing.Size(148, 20);
            this.textBoxPOB.TabIndex = 12;
            // 
            // textBoxNom3
            // 
            this.textBoxNom3.Location = new System.Drawing.Point(590, 27);
            this.textBoxNom3.Name = "textBoxNom3";
            this.textBoxNom3.Size = new System.Drawing.Size(148, 20);
            this.textBoxNom3.TabIndex = 10;
            this.textBoxNom3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxNom1_KeyPress);
            // 
            // textBoxNom2
            // 
            this.textBoxNom2.Location = new System.Drawing.Point(399, 27);
            this.textBoxNom2.Name = "textBoxNom2";
            this.textBoxNom2.Size = new System.Drawing.Size(163, 20);
            this.textBoxNom2.TabIndex = 9;
            this.textBoxNom2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxNom1_KeyPress);
            // 
            // textBoxNom1
            // 
            this.textBoxNom1.Location = new System.Drawing.Point(187, 27);
            this.textBoxNom1.Name = "textBoxNom1";
            this.textBoxNom1.Size = new System.Drawing.Size(184, 20);
            this.textBoxNom1.TabIndex = 8;
            this.textBoxNom1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxNom1_KeyPress);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label21.Location = new System.Drawing.Point(395, 139);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(147, 19);
            this.label21.TabIndex = 7;
            this.label21.Text = "Date Nouvelle CNI ";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label22.Location = new System.Drawing.Point(17, 139);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(171, 19);
            this.label22.TabIndex = 6;
            this.label22.Text = "Numéro Nouvelle CNI ";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label23.Location = new System.Drawing.Point(395, 111);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(151, 19);
            this.label23.TabIndex = 5;
            this.label23.Text = "Date Ancienne CNI ";
            this.label23.Click += new System.EventHandler(this.label23_Click);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label24.Location = new System.Drawing.Point(17, 111);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(171, 19);
            this.label24.TabIndex = 4;
            this.label24.Text = "Numéro Ancienne CNI";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label27.Location = new System.Drawing.Point(17, 83);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(126, 19);
            this.label27.TabIndex = 3;
            this.label27.Text = "Nom de la mère ";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label28.Location = new System.Drawing.Point(395, 55);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(137, 19);
            this.label28.TabIndex = 2;
            this.label28.Text = "Lieu de naissance ";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(17, 55);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(141, 19);
            this.label29.TabIndex = 1;
            this.label29.Text = "Date de naissance ";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(17, 27);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(132, 19);
            this.label30.TabIndex = 0;
            this.label30.Text = "Nom                      ";
            // 
            // SaisieElecteur
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.LightSeaGreen;
            this.ClientSize = new System.Drawing.Size(1008, 730);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.LabelStatut);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Name = "SaisieElecteur";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Saisi Electeur";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.SaisieElecteur_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox comboBoxAdresse;
        private System.Windows.Forms.ComboBox comboBoxCommune;
        private System.Windows.Forms.ComboBox comboBoxArrondis;
        private System.Windows.Forms.ComboBox comboBoxRégion;
        private System.Windows.Forms.TextBox textBoxPhone;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxAdresseCompl;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label LabelStatut;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ComboBox comboBoxSexe;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBoxCodeElecteur;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBoxNewCNI;
        private System.Windows.Forms.TextBox textBoxNomMère3;
        private System.Windows.Forms.TextBox textBoxNomMère2;
        private System.Windows.Forms.TextBox textBoxNomMère1;
        private System.Windows.Forms.TextBox textBoxAncCNI;
        private System.Windows.Forms.TextBox textBoxPOB;
        private System.Windows.Forms.TextBox textBoxNom3;
        private System.Windows.Forms.TextBox textBoxNom2;
        private System.Windows.Forms.TextBox textBoxNom1;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.DateTimePicker dateTimePickerDOB;
        private System.Windows.Forms.TextBox textBoxProf;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxPortable;
        private System.Windows.Forms.Label label2;
    }
}