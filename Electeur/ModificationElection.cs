﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class ModificationElection : Form
    {
        string clause;

        Connection conn = new Connection();
        GetValue get = new GetValue();

        public ModificationElection()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (textBox1.Text.Trim().Length == 0)
            {
                MessageBox.Show("veuillez saisir l'inttitulé");
                textBox1.Focus();
            }
           
                else
                {

                    if (comboBox2.Text.Length == 0)
                    {
                        MessageBox.Show("veuillez choisir on est dans quel tour d'election");
                    }
                    else
                    {

                        //Connection cn = new Connection();
                        string insertCmd = "update Election set ObjetElection=@ObjetElection,Annee=@Annee,DateElection=@DateElection,Etat=@Etat,TourElection=@TourElection,UserModification=@UserModification,DateModification=@DateModification  where ElectionID=" + clause3 + "";
                        SqlConnection dbConn;
                        dbConn = new SqlConnection(conn.connectdb());
                        dbConn.Open();
                        Login lg = new Login();
                        string dat = DateTime.Now.ToString("yyyy-MM-dd");
                        DateTime datea = DateTime.Parse(dateTimePicker4.Value.ToString("yyyy-MM-dd"));
                        DateTime datee = DateTime.Parse(dateTimePicker1.Value.ToString("yyyy-MM-dd"));
                        GetValue value = new GetValue();
                        //getAdress add = new getAdress();
                        SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                       
                         
                            int TourElectionID = Convert.ToInt32(comboBox5.SelectedValue.ToString());
                            myCommand.Parameters.AddWithValue("@TourElection", TourElectionID);
                        
                        if (radioButton4.Checked)
                        {
                            myCommand.Parameters.AddWithValue("@Etat", 1);
                        }
                        if (radioButton3.Checked)
                        {
                            myCommand.Parameters.AddWithValue("@Etat", 0);
                        }
                        myCommand.Parameters.AddWithValue("@ObjetElection", textBox1.Text.ToString());

                        myCommand.Parameters.AddWithValue("@Annee", datea);
                        myCommand.Parameters.AddWithValue("@DateElection", datee);
                       
                        myCommand.Parameters.AddWithValue("@UserModification", value.getAgentCréation());
                        myCommand.Parameters.AddWithValue("@DateModification", dat);
                        myCommand.ExecuteNonQuery();

                        MessageBox.Show("Modification avec succès");
                        textBox1.Clear();
                    }

                }
            
        }

        private void button7_Click(object sender, EventArgs e)
        {

            this.IsEmptyfield();
            if (String.IsNullOrEmpty(clause))
            {
                MessageBox.Show("veuillez remplir un champ au minimum");
                this.Focus();

            }
            else
            {
                string sCon = conn.connectdb();
                SqlConnection con = new SqlConnection(sCon);
                string str = "SET CONCAT_NULL_YIELDS_NULL OFF; SELECT ElectionID, ObjetElection AS 'Intitulé Election',ObjetTourElection as 'Tour Election' FROM View_tourelection_election where " + clause + "";
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(str, con);
                SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sqlAdapter);

                DataTable table = new DataTable();
                table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                con.Open();
                sqlAdapter.Fill(table);
                con.Close();
                dataGridView1.DataSource = table;
                if (dataGridView1.Rows.Count == 0)
                {

                    panel1.Visible = false;
                    label7.Visible = false;
                    MessageBox.Show("Aucun election retrouvé pour ces critères");

                }
                else
                {
                    panel1.Visible = true;
                    label7.Visible = true;
                }

                this.dataGridView1.Columns["ElectionID"].Visible = false;



                clause = "";
            }
        }
        public void IsEmptyfield()
        {
            clause = null;
            foreach (Control X in this.panel4.Controls)
            {

                if (X is TextBox || X is ComboBox)
                {

                    if (X.Text.Trim().Length != 0)
                    {
                        if (string.IsNullOrEmpty(clause))
                        {
                            clause = clause + X.Tag + " Like '%" + X.Text.Trim() + "%'";
                            //return clause;
                        }
                        else
                        {
                            clause = clause + " And " + X.Tag + " Like '%" + X.Text.Trim() + "%'";
                            //return clause;
                        }
                    }


                }
                else
                {
                    if (X is DateTimePicker)
                    {
                        if (X.Name == "dateTimePicker2")
                        {
                            if (string.IsNullOrEmpty(clause))
                            {
                                if (dateTimePicker2.Checked)
                                {
                                    string dat = dateTimePicker2.Value.ToString("yyyy");
                                  
                                    clause = clause + X.Tag + " Like '%" + dat + "%'";
                                }

                            }
                            else
                            {
                                if (dateTimePicker2.Checked)
                                {
                                    string dat = dateTimePicker2.Value.ToString("yyyy");
                                 
                                    clause = clause + " AND " + X.Tag + " Like '%" + dat + "%'";
                                }

                            }
                        }
                        if (X.Name == "dateTimePicker3")
                        {
                            if (string.IsNullOrEmpty(clause))
                            {
                                if (dateTimePicker3.Checked)
                                {
                                    string dat = dateTimePicker3.Value.ToString("yyyy-MM-dd");
                                    DateTime date4 = DateTime.Parse(dat);
                                    clause = clause + X.Tag + " Like '%" + dat + "%'";
                                }

                            }
                            else
                            {
                                if (dateTimePicker3.Checked)
                                {
                                    string dat = dateTimePicker3.Value.ToString("yyyy-MM-dd");
                                    DateTime date4 = DateTime.Parse(dat);
                                    clause = clause + " AND " + X.Tag + " Like '%" + dat + "%'";
                                }

                            }
                        }
                    }
                }


            }

        }
        //private void Filltypeelection()
        //{
        //    string sCon = conn.connectdb();
        //    SqlConnection con = new SqlConnection(sCon);
        //    SqlCommand cmd = new SqlCommand();
        //    cmd.Connection = con;
        //    cmd.CommandType = CommandType.Text;
        //    cmd.CommandText = "SELECT TypeElectionID,ObjetTypeElection FROM TypeElection";
        //    DataSet objDs = new DataSet();
        //    SqlDataAdapter dAdapter = new SqlDataAdapter();
        //    dAdapter.SelectCommand = cmd;
        //    con.Open();
        //    dAdapter.Fill(objDs);
        //    con.Close();
        //    comboBox3.ValueMember = "TypeElectionID";
        //    comboBox3.DisplayMember = "ObjetTypeElection";
        //    comboBox3.DataSource = objDs.Tables[0];
        //}
        //private void Filltypeelection2()
        //{
        //    string sCon = conn.connectdb();
        //    SqlConnection con = new SqlConnection(sCon);
        //    SqlCommand cmd = new SqlCommand();
        //    cmd.Connection = con;
        //    cmd.CommandType = CommandType.Text;
        //    cmd.CommandText = "SELECT TypeElectionID,ObjetTypeElection FROM TypeElection";
        //    DataSet objDs = new DataSet();
        //    SqlDataAdapter dAdapter = new SqlDataAdapter();
        //    dAdapter.SelectCommand = cmd;
        //    con.Open();
        //    dAdapter.Fill(objDs);
        //    con.Close();
        //    comboBox1.ValueMember = "TypeElectionID";
        //    comboBox1.DisplayMember = "ObjetTypeElection";
        //    comboBox1.DataSource = objDs.Tables[0];
        //}
        private void Filltourelection()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT TourElectionID,ObjetTourElection FROM TourElection";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBox5.ValueMember = "TourElectionID";
            comboBox5.DisplayMember = "ObjetTourElection";
            comboBox5.DataSource = objDs.Tables[0];
        }
        private void Filltourelection2()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT TourElectionID,ObjetTourElection FROM TourElection";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBox2.ValueMember = "TourElectionID";
            comboBox2.DisplayMember = "ObjetTourElection";
            comboBox2.DataSource = objDs.Tables[0];
        }

        private void ModificationElection_Load(object sender, EventArgs e)
        {
            //Filltypeelection();
            //Filltypeelection2();
            Filltourelection2();
            Filltourelection();
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            this.filldetails();
        }
        string clause3;
        private void filldetails()
        {

            this.selection();
            if (String.IsNullOrEmpty(clause3))
            {
                panel1.Visible = false;
                label7.Visible = false;

            }
            else
            {
                panel1.Visible = true;
                label7.Visible = true;
                string sCon = conn.connectdb();
                SqlConnection con = new SqlConnection(sCon);
                string str = "SELECT * FROM View_tourelection_election where ElectionID= " + clause3;
                SqlCommand cmd = new SqlCommand(str, con);
                con.Open();

                using (SqlDataReader read = cmd.ExecuteReader())

                    while (read.Read())
                    {
                        textBox1.Text = (read["ObjetElection"].ToString());

                        int colIndex3 = (read.GetOrdinal("Annee"));
                        if (!read.IsDBNull(colIndex3))
                        {
                            dateTimePicker4.Value = (read.GetDateTime(colIndex3));
                        }
                        int colIndex1 = (read.GetOrdinal("DateElection"));
                        if (!read.IsDBNull(colIndex1))
                        {
                            dateTimePicker1.Value = (read.GetDateTime(colIndex1));
                        }
                        if ((read["Etat"].ToString()) == "1")
                        {
                            radioButton4.Checked = true;
                        }
                        if ((read["Etat"].ToString()) == "0")
                        {
                            radioButton3.Checked = true;
                        }

                        //comboBox1.SelectedValue = (read["TypeElectionID"].ToString());
                        comboBox2.SelectedValue = (read["TourElectionID"].ToString());



                    }
            }

        }
        private void selection()
        {
            int counter3;
            clause3 = null;

            // Iterate through all the rows and sum up the appropriate columns. 
            for (counter3 = 0; counter3 < (dataGridView1.Rows.Count);
                counter3++)
            {
                //dataGridView1.Rows.
                if (dataGridView1.Rows[counter3].Selected)
                {
                    if (dataGridView1.Rows[counter3].Cells["ElectionID"].Value
                    != null)
                    {
                        if (dataGridView1.Rows[counter3].
                            Cells["ElectionID"].Value.ToString().Length != 0)
                        {
                            if (string.IsNullOrEmpty(clause3))
                            {
                                clause3 = clause3 + int.Parse(dataGridView1.Rows[counter3].
                                    Cells["ElectionID"].Value.ToString());
                            }
                            else
                            {
                                clause3 = clause3 + "," + int.Parse(dataGridView1.Rows[counter3].
                                Cells["ElectionID"].Value.ToString());

                            }

                        }
                    }


                }

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ClearClass1 clr = new ClearClass1();
            clr.RecursiveClearTextBoxes(this.panel4.Controls);
        }
    }
}
