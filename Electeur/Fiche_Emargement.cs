﻿using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class Fiche_Emargement : Form
    {
        int tstc = 0;
        int tsta = 0;
        int tstv = 0;
        List<int> bureaus = new List<int>();
        string SQL;
        public static string strcon = @"Data Source=Localhost\SQL2008;Initial Catalog=DirElection;User ID=sa;Password=Pa$$w0rd;";
        static SqlConnection con = new SqlConnection(strcon);
        CrystalReport_emargement objRpt; CrystalReport_Ambassade_Emargement objRptr; CrystalReportb objRptb;
        string text1, text2, text3;
        public Fiche_Emargement()
        {
            InitializeComponent();
        }

        private void Fiche_Emargement_Load(object sender, EventArgs e)
        {

            comboBox1.Items.Clear();
            SQL = "select RégionID,NomRégion from Région";
            con.Open();
            SqlDataAdapter sqlAdapter = new SqlDataAdapter(SQL, con);
            SqlCommandBuilder commandBuilder1 = new SqlCommandBuilder(sqlAdapter);
            con.Close();
            DataTable dt = new DataTable();
            sqlAdapter.Fill(dt);
            comboBox1.ValueMember = "RégionID";
            comboBox1.DisplayMember = "NomRégion";
           
            comboBox1.DataSource = dt;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int idregion = Convert.ToInt32(comboBox1.SelectedValue.ToString());
            if (idregion == 1)
            {
                if (bureaus.Count > 0)
                {

                    try
                    {
                        objRpt = new CrystalReport_emargement();
                        string str = null;

                        DataSet_emargementTableAdapters.viewemargement_DTableAdapter _adapter = new DataSet_emargementTableAdapters.viewemargement_DTableAdapter();

                        DataTable dtcr = new DataTable();

                        for (int i = 0; i < bureaus.Count; i++)
                        {


                            if (String.IsNullOrEmpty(str))
                            {
                                str = "" + bureaus[i].ToString() + "";
                            }
                            else
                            {
                                str = str + "," + bureaus[i].ToString() + "";
                            }


                        }

                        string SQLlist = "SET CONCAT_NULL_YIELDS_NULL OFF; SELECT   * " +
                                             " FROM  viewemargement_D WHERE  BureauID IN (" + str + ") ORDER BY NewOrdre";

                        con.Open();
                        SqlDataAdapter sqlAdapter = new SqlDataAdapter(SQLlist, con);
                        SqlCommandBuilder commandBuilder1 = new SqlCommandBuilder(sqlAdapter);

                        con.Close();
                        DataTable dt = new DataTable();
                        sqlAdapter.Fill(dt);
                        objRpt.SetDataSource(dt);
                        crystalReportViewer1.ReportSource = objRpt;
                        crystalReportViewer1.Refresh();
                        button2.Visible = true;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("error" + ex.Message);
                    }
                }
                else
                {
                    MessageBox.Show("Veuillez selectionnée au moins un bureau de vote!", "Emargement", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                if (bureaus.Count > 0)
                {

                    try
                    {
                        objRptr = new CrystalReport_Ambassade_Emargement();
                        string str = null;

                        DataSet_AmbassadeTableAdapters.View_carte_regTableAdapter _adapter = new DataSet_AmbassadeTableAdapters.View_carte_regTableAdapter();

                        DataTable dtcr = new DataTable();

                        for (int i = 0; i < bureaus.Count; i++)
                        {


                            if (String.IsNullOrEmpty(str))
                            {
                                str = "" + bureaus[i].ToString() + "";
                            }
                            else
                            {
                                str = str + "," + bureaus[i].ToString() + "";
                            }


                        }

                        string SQLlist = "SET CONCAT_NULL_YIELDS_NULL OFF; SELECT   * " +
                                             " FROM  View_carte_reg WHERE  BureauID IN (" + str + ") ORDER BY CodeElecteur";

                        con.Open();
                        SqlDataAdapter sqlAdapter = new SqlDataAdapter(SQLlist, con);
                        SqlCommandBuilder commandBuilder1 = new SqlCommandBuilder(sqlAdapter);

                        con.Close();
                        DataTable dt = new DataTable();
                        sqlAdapter.Fill(dt);
                        objRptr.SetDataSource(dt);
                        crystalReportViewer1.ReportSource = objRptr;
                        crystalReportViewer1.Refresh();
                        button2.Visible = true;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("error" + ex.Message);
                    }
                }
                else
                {
                    MessageBox.Show("Veuillez selectionnée au moins un bureau de vote!", "Emargement", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex >= 0)
            {
                if (Int32.Parse(comboBox1.SelectedValue.ToString()) == 1)
                {
                    int id = Int32.Parse(comboBox1.SelectedValue.ToString());
                    SQL = "SELECT NomCommune,CommuneID FROM Région AS A INNER JOIN Commune AS B on A.RégionID = B.RégionID where  A.RégionID=" + id + "";
                    con.Open();
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter(SQL, con);
                    SqlCommandBuilder commandBuilder1 = new SqlCommandBuilder(sqlAdapter);
                    con.Close();
                    DataTable dt = new DataTable();
                    sqlAdapter.Fill(dt);
                    dataGridViewc.Enabled = true;
                    dataGridViewc.Rows.Clear();
                    dataGridViewa.Rows.Clear();
                    dataGridViewv.Rows.Clear();
                    dataGridViewb.Rows.Clear();
                    listBox1.Items.Clear();
                    bureaus.Clear();
                    int tst = 0;
                    tstc = 0;
                    tsta = 0;
                    tstv = 0;
                    foreach (DataRow row in dt.Rows)
                    {

                        string[] row1 = null;

                        DataGridViewRow rowt = new DataGridViewRow();

                        {
                            row1 = new string[] {
                                row["CommuneID"].ToString()
                                };

                        }
                        dataGridViewc.Rows.Add(row1);
                        dataGridViewc[1, tst].Value = false;
                        dataGridViewc[2, tst].Value = row["NomCommune"].ToString();
                        tst++;
                    }
                }
                else
                {

                    int id = Int32.Parse(comboBox1.SelectedValue.ToString());
                    SQL = "SELECT NomLocalité,LocalitéID FROM Région AS A INNER JOIN Localité AS B on A.RégionID = B.RégionID where  A.RégionID=" + id + "";
                    con.Open();
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter(SQL, con);
                    SqlCommandBuilder commandBuilder1 = new SqlCommandBuilder(sqlAdapter);
                    con.Close();
                    DataTable dt = new DataTable();
                    sqlAdapter.Fill(dt);
                    dataGridViewc.Enabled = false;
                    dataGridViewc.Rows.Clear();
                    dataGridViewa.Rows.Clear();
                    dataGridViewv.Rows.Clear();
                    dataGridViewb.Rows.Clear();
                    listBox1.Items.Clear();
                    bureaus.Clear();
                    tsta = 0;
                    tstv = 0;
                    int tst = 0;
                    foreach (DataRow row in dt.Rows)
                    {

                        string[] row1 = null;

                        DataGridViewRow rowt = new DataGridViewRow();

                        {
                            row1 = new string[] {
                                row["LocalitéID"].ToString()
                                };

                        }
                        dataGridViewa.Rows.Add(row1);
                        dataGridViewa[1, tst].Value = false;
                        dataGridViewa[2, tst].Value = row["NomLocalité"].ToString();
                        tst++;
                    }

                }



            }
        }

        private void dataGridViewc_CellClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void dataGridViewa_CellClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void dataGridViewv_CellClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void dataGridViewb_CellClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void comboBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {

            if (comboBox1.SelectedIndex >= 0)
            {
                if (Int32.Parse(comboBox1.SelectedValue.ToString()) == 1)
                {
                    int id = Int32.Parse(comboBox1.SelectedValue.ToString());
                    SQL = "SELECT NomCommune,CommuneID FROM Région AS A INNER JOIN Commune AS B on A.RégionID = B.RégionID where  A.RégionID=" + id + "";
                    con.Open();
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter(SQL, con);
                    SqlCommandBuilder commandBuilder1 = new SqlCommandBuilder(sqlAdapter);
                    con.Close();
                    DataTable dt = new DataTable();
                    sqlAdapter.Fill(dt);
                    dataGridViewc.Enabled = true;
                    dataGridViewc.Rows.Clear();
                    dataGridViewa.Rows.Clear();
                    dataGridViewv.Rows.Clear();
                    dataGridViewb.Rows.Clear();
                    listBox1.Items.Clear();
                    bureaus.Clear();
                    int tst = 0;
                    tstc = 0;
                    tsta = 0;
                    tstv = 0;
                    foreach (DataRow row in dt.Rows)
                    {

                        string[] row1 = null;

                        DataGridViewRow rowt = new DataGridViewRow();

                        {
                            row1 = new string[] {
                                row["CommuneID"].ToString()
                                };

                        }
                        dataGridViewc.Rows.Add(row1);
                        dataGridViewc[1, tst].Value = false;
                        dataGridViewc[2, tst].Value = row["NomCommune"].ToString();
                        tst++;
                    }
                }
                else
                {

                    int id = Int32.Parse(comboBox1.SelectedValue.ToString());
                    SQL = "SELECT NomLocalité,LocalitéID FROM Région AS A INNER JOIN Localité AS B on A.RégionID = B.RégionID where  A.RégionID=" + id + "";
                    con.Open();
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter(SQL, con);
                    SqlCommandBuilder commandBuilder1 = new SqlCommandBuilder(sqlAdapter);
                    con.Close();
                    DataTable dt = new DataTable();
                    sqlAdapter.Fill(dt);
                    dataGridViewc.Enabled = false;
                    dataGridViewc.Rows.Clear();
                    dataGridViewa.Rows.Clear();
                    dataGridViewv.Rows.Clear();
                    dataGridViewb.Rows.Clear();
                    listBox1.Items.Clear();
                    bureaus.Clear();
                    tsta = 0;
                    tstv = 0;
                    int tst = 0;
                    foreach (DataRow row in dt.Rows)
                    {

                        string[] row1 = null;

                        DataGridViewRow rowt = new DataGridViewRow();

                        {
                            row1 = new string[] {
                                row["LocalitéID"].ToString()
                                };

                        }
                        dataGridViewa.Rows.Add(row1);
                        dataGridViewa[1, tst].Value = false;
                        dataGridViewa[2, tst].Value = row["NomLocalité"].ToString();
                        tst++;
                    }

                }



            }
        }

        private void dataGridViewc_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {


            if (dataGridViewc.Rows.Count > 0)
            {
                if (Int32.Parse(comboBox1.SelectedValue.ToString()) == 1)
                {

                    int rowindex = dataGridViewc.CurrentRow.Index;
                    if ((bool)dataGridViewc[1, rowindex].Value == true)
                    {
                        dataGridViewc.Rows[e.RowIndex].Cells[1].Value = false;
                    }
                    else
                    {
                        dataGridViewc.Rows[e.RowIndex].Cells[1].Value = true;
                    }

                    if ((bool)dataGridViewc[1, rowindex].Value == true)
                    {

                        int id = Convert.ToInt32((string)dataGridViewc[0, rowindex].Value);
                        SQL = "SELECT  A.CommuneID,NomArrondis,ArrondisID FROM Commune AS A join Arrondissement AS B on A.CommuneID=B.CommuneID where  A.CommuneID=" + id + "";

                        con.Open();
                        SqlDataAdapter sqlAdapter = new SqlDataAdapter(SQL, con);
                        SqlCommandBuilder commandBuilder1 = new SqlCommandBuilder(sqlAdapter);
                        con.Close();
                        DataTable dt = new DataTable();
                        sqlAdapter.Fill(dt);

                        foreach (DataRow row in dt.Rows)
                        {

                            string[] row1 = null;

                            {
                                row1 = new string[] {
                                 row["ArrondisID"].ToString()
                                  };

                            }
                            dataGridViewa.Rows.Add(row1);
                            dataGridViewa[1, tstc].Value = false;
                            dataGridViewa[2, tstc].Value = row["NomArrondis"].ToString();
                            dataGridViewa[3, tstc].Value = row["CommuneID"].ToString();
                            tstc++;

                        }
                    }
                    else if ((bool)dataGridViewc[1, rowindex].Value == false && dataGridViewc.Rows.Count > 0)
                    {
                        int id = Convert.ToInt32((string)dataGridViewc[0, rowindex].Value);
                        if (dataGridViewa.Rows.Count > 0)
                        {
                            for (int a = dataGridViewa.Rows.Count - 1; a >= 0; a--)
                            {
                                if (Convert.ToInt32(dataGridViewa.Rows[a].Cells[3].Value.ToString()) == id)
                                {


                                    if (dataGridViewv.Rows.Count > 0)
                                    {
                                        for (int v = dataGridViewv.Rows.Count - 1; v >= 0; v--)
                                        {
                                            if (dataGridViewv.Rows[v].Cells[3].Value.ToString() == dataGridViewa.Rows[a].Cells[0].Value.ToString())
                                            {

                                                if (dataGridViewb.Rows.Count > 0)
                                                {
                                                    for (int b = dataGridViewb.Rows.Count - 1; b >= 0; b--)
                                                    {
                                                        if (dataGridViewb.Rows[b].Cells[3].Value.ToString() == dataGridViewv.Rows[v].Cells[0].Value.ToString())
                                                        {

                                                            listBox1.Items.Remove(dataGridViewb.Rows[b].Cells[2].Value.ToString());
                                                            bureaus.Remove(Convert.ToInt32(dataGridViewb.Rows[b].Cells[0].Value.ToString()));
                                                            dataGridViewb.Rows.Remove(dataGridViewb.Rows[b]);
                                                        }
                                                    }
                                                    tstv = dataGridViewb.Rows.Count;
                                                }
                                                dataGridViewv.Rows.Remove(dataGridViewv.Rows[v]);
                                            }
                                        }
                                        tsta = dataGridViewv.Rows.Count;
                                    }
                                    dataGridViewa.Rows.Remove(dataGridViewa.Rows[a]);
                                }
                            }
                            tstc = dataGridViewa.Rows.Count;
                        }
                    }
                }

            }
        }

        private void dataGridViewa_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {


            if (dataGridViewa.Rows.Count > 0)
            {

                if (Int32.Parse(comboBox1.SelectedValue.ToString()) == 1)
                {
                    int rowindex = dataGridViewa.CurrentRow.Index;
                    if ((bool)dataGridViewa[1, rowindex].Value == true)
                    {
                        dataGridViewa.Rows[e.RowIndex].Cells[1].Value = false;
                    }
                    else
                    {
                        dataGridViewa.Rows[e.RowIndex].Cells[1].Value = true;
                    }
                    if ((bool)dataGridViewa[1, rowindex].Value == true)
                    {

                        int id = Convert.ToInt32((string)dataGridViewa[0, rowindex].Value);
                        SQL = "SELECT a.ArrondisID,CentreVote,CentreID FROM  Arrondissement AS a INNER JOIN CentreVote AS c ON a.ArrondisID = c.ArrondisID where a.ArrondisID=" + id + "";
                        con.Open();
                        SqlDataAdapter sqlAdapter = new SqlDataAdapter(SQL, con);
                        SqlCommandBuilder commandBuilder1 = new SqlCommandBuilder(sqlAdapter);
                        con.Close();
                        DataTable dt = new DataTable();
                        sqlAdapter.Fill(dt);

                        foreach (DataRow row in dt.Rows)
                        {

                            string[] row1 = null;
                            {
                                row1 = new string[] {
                                row["CentreID"].ToString()
                             };

                            }
                            dataGridViewv.Rows.Add(row1);
                            dataGridViewv[1, tsta].Value = false;
                            dataGridViewv[2, tsta].Value = row["CentreVote"].ToString();
                            dataGridViewv[3, tsta].Value = row["ArrondisID"].ToString();
                            tsta++;

                        }
                    }
                    else if ((bool)dataGridViewa[1, rowindex].Value == false)
                    {
                        int id = Convert.ToInt32((string)dataGridViewa[0, rowindex].Value);
                        if (dataGridViewv.Rows.Count > 0)
                        {
                            for (int i = dataGridViewv.Rows.Count - 1; i >= 0; i--)
                            {
                                if (Convert.ToInt32(dataGridViewv.Rows[i].Cells[3].Value.ToString()) == id)
                                {


                                    if (dataGridViewb.Rows.Count > 0)
                                    {
                                        for (int j = dataGridViewb.Rows.Count - 1; j >= 0; j--)
                                        {
                                            if (dataGridViewb.Rows[j].Cells[3].Value.ToString() == dataGridViewv.Rows[i].Cells[0].Value.ToString())
                                            {
                                                listBox1.Items.Remove(dataGridViewb.Rows[j].Cells[2].Value.ToString());
                                                bureaus.Remove(Convert.ToInt32(dataGridViewb.Rows[j].Cells[0].Value.ToString()));
                                                dataGridViewb.Rows.Remove(dataGridViewb.Rows[j]);

                                            }
                                        }
                                        tstv = dataGridViewb.Rows.Count;

                                    }
                                    dataGridViewv.Rows.Remove(dataGridViewv.Rows[i]);
                                }
                            }
                            tsta = dataGridViewv.Rows.Count;
                        }

                    }
                }
                else
                {

                    int rowindex = dataGridViewa.CurrentRow.Index;
                    if ((bool)dataGridViewa[1, rowindex].Value == true)
                    {
                        dataGridViewa.Rows[e.RowIndex].Cells[1].Value = false;
                    }
                    else
                    {
                        dataGridViewa.Rows[e.RowIndex].Cells[1].Value = true;
                    }
                    if ((bool)dataGridViewa[1, rowindex].Value == true)
                    {

                        int id = Convert.ToInt32((string)dataGridViewa[0, rowindex].Value);
                        SQL = "SELECT a.LocalitéID,CentreVote,CentreID FROM  Localité AS a INNER JOIN CentreVote AS c ON a.LocalitéID = c.LocalitéID where a.LocalitéID=" + id + "";
                        con.Open();
                        SqlDataAdapter sqlAdapter = new SqlDataAdapter(SQL, con);
                        SqlCommandBuilder commandBuilder1 = new SqlCommandBuilder(sqlAdapter);
                        con.Close();
                        DataTable dt = new DataTable();
                        sqlAdapter.Fill(dt);

                        foreach (DataRow row in dt.Rows)
                        {

                            string[] row1 = null;


                            {
                                row1 = new string[] {
                                row["CentreID"].ToString()
                                };

                            }
                            dataGridViewv.Rows.Add(row1);
                            dataGridViewv[1, tsta].Value = false;
                            dataGridViewv[2, tsta].Value = row["CentreVote"].ToString();
                            dataGridViewv[3, tsta].Value = row["LocalitéID"].ToString();
                            tsta++;

                        }
                    }
                    else if ((bool)dataGridViewa[1, rowindex].Value == false)
                    {
                        int id = Convert.ToInt32((string)dataGridViewa[0, rowindex].Value);
                        if (dataGridViewv.Rows.Count > 0)
                        {
                            for (int i = dataGridViewv.Rows.Count - 1; i >= 0; i--)
                            {
                                if (Convert.ToInt32(dataGridViewv.Rows[i].Cells[3].Value.ToString()) == id)
                                {


                                    if (dataGridViewb.Rows.Count > 0)
                                    {
                                        for (int j = dataGridViewb.Rows.Count - 1; j >= 0; j--)
                                        {
                                            if (dataGridViewb.Rows[j].Cells[3].Value.ToString() == dataGridViewv.Rows[i].Cells[0].Value.ToString())
                                            {
                                                listBox1.Items.Remove(dataGridViewb.Rows[j].Cells[2].Value.ToString());
                                                bureaus.Remove(Convert.ToInt32(dataGridViewb.Rows[j].Cells[0].Value.ToString()));
                                                dataGridViewb.Rows.Remove(dataGridViewb.Rows[j]);

                                            }
                                        }
                                        tstv = dataGridViewb.Rows.Count;

                                    }
                                    dataGridViewv.Rows.Remove(dataGridViewv.Rows[i]);
                                }
                            }
                            tsta = dataGridViewv.Rows.Count;
                        }

                    }
                }
            }
        }

        private void dataGridViewv_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {


            if (dataGridViewv.Rows.Count > 0)
            {
                int rowindex = dataGridViewv.CurrentRow.Index;
                if ((bool)dataGridViewv[1, rowindex].Value == true)
                {
                    dataGridViewv.Rows[e.RowIndex].Cells[1].Value = false;
                }
                else
                {
                    dataGridViewv.Rows[e.RowIndex].Cells[1].Value = true;
                }
                if ((bool)dataGridViewv[1, rowindex].Value == true)
                {

                    int id = Convert.ToInt32((string)dataGridViewv[0, rowindex].Value);
                    SQL = "SELECT c.CentreID,BureauID,NomBureau FROM  Bureau  AS b INNER JOIN CentreVote AS c ON b.CentreID = c.CentreID where c.CentreID=" + id + "";
                    con.Open();
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter(SQL, con);
                    SqlCommandBuilder commandBuilder1 = new SqlCommandBuilder(sqlAdapter);
                    con.Close();
                    DataTable dt = new DataTable();
                    sqlAdapter.Fill(dt);

                    foreach (DataRow row in dt.Rows)
                    {

                        string[] row1 = null;

                        {
                            row1 = new string[] {
                            row["BureauID"].ToString()
                             };

                        }
                        dataGridViewb.Rows.Add(row1);
                        dataGridViewb[1, tstv].Value = false;
                        dataGridViewb[2, tstv].Value = row["NomBureau"].ToString();
                        dataGridViewb[3, tstv].Value = row["CentreID"].ToString();
                        tstv++;

                    }
                }
                else if ((bool)dataGridViewv[1, rowindex].Value == false)
                {
                    int id = Convert.ToInt32((string)dataGridViewv[0, rowindex].Value);
                    if (dataGridViewb.Rows.Count > 0)
                    {
                        for (int i = dataGridViewb.Rows.Count - 1; i >= 0; i--)
                        {

                            if (Convert.ToInt32(dataGridViewb.Rows[i].Cells[3].Value.ToString()) == id)
                            {

                                listBox1.Items.Remove(dataGridViewb.Rows[i].Cells[2].Value.ToString());
                                bureaus.Remove(Convert.ToInt32(dataGridViewb.Rows[i].Cells[0].Value.ToString()));
                                dataGridViewb.Rows.Remove(dataGridViewb.Rows[i]);
                            }
                        }
                        tstv = dataGridViewb.Rows.Count;
                    }
                }
            }
        }

        private void dataGridViewb_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {


            int rowindex = dataGridViewb.CurrentRow.Index;
            if ((bool)dataGridViewb[1, rowindex].Value == true)
            {
                dataGridViewb.Rows[e.RowIndex].Cells[1].Value = false;
            }
            else
            {
                dataGridViewb.Rows[e.RowIndex].Cells[1].Value = true;
            }
            if ((bool)dataGridViewb[1, rowindex].Value == true)
            {
                listBox1.Items.Add((string)dataGridViewb[2, rowindex].Value);
                bureaus.Add(Convert.ToInt32((string)dataGridViewb[0, rowindex].Value));
            }
            else if ((bool)dataGridViewb[1, rowindex].Value == false)
            {
                listBox1.Items.Remove((string)dataGridViewb[2, rowindex].Value);
                bureaus.Remove(Convert.ToInt32((string)dataGridViewb[0, rowindex].Value));
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
             int idregion = Convert.ToInt32(comboBox1.SelectedValue.ToString());
            if (idregion == 1)
                {
            
                     try
                {

                   
                    ExportOptions CrExportOptions;
                    DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                    PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();
                    CrDiskFileDestinationOptions.DiskFileName = "C:\\Users\\Guinaleh\\Desktop\\pdf file\\Région"+idregion+".pdf";
                    CrExportOptions = objRpt.ExportOptions;
                    {
                        CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                        CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                        CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                        CrExportOptions.FormatOptions = CrFormatTypeOptions;
                    }
                    objRpt.Export();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
            else
	            {
                    string str = null;
                    for (int i = 0; i < bureaus.Count; i++)
                    {


                        if (String.IsNullOrEmpty(str))
                        {
                            str = "" + bureaus[i].ToString() + "";
                        }
                        else
                        {
                            str = str + "," + bureaus[i].ToString() + "";
                        }


                    }
                    try
                    {


                        ExportOptions CrExportOptions;
                        DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                        PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();
                        CrDiskFileDestinationOptions.DiskFileName = "C:\\Users\\Guinaleh\\Desktop\\pdf file\\Région" + str + ".pdf";
                        CrExportOptions = objRptr.ExportOptions;
                        {
                            CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                            CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                            CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                            CrExportOptions.FormatOptions = CrFormatTypeOptions;
                        }
                        objRptr.Export();

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
	            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
