﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class AncienCNI : Form
    {
        public AncienCNI()
        {
            InitializeComponent();
        }
         string clause,clause1;
         int CNI;
         bool verif= true;
        Connection conn = new Connection();
        private void button1_Click(object sender, EventArgs e)
        {
            this.IsEmptyfield();
            if (String.IsNullOrEmpty(clause))
            {
                MessageBox.Show("veuillez remplir un champ au minimum");
                this.Focus();

            }
            else
            {
                string sCon = conn.connectdb();
                SqlConnection con = new SqlConnection(sCon);
                string str = "SET CONCAT_NULL_YIELDS_NULL OFF; SELECT Nom1_pers +'  '+ Nom2_pers +'  '+Nom3_pers AS NOM,nom1_mere+'  '+nom2_mere AS 'NOM DE LA MERE', SEXE ,date_na_pers AS 'DATE DE NAISSANCE',num_cin FROM AncienCIN where " + clause;
                con.Open();
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(str, con);
                SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sqlAdapter);
                con.Close();
                DataTable table = new DataTable();
                table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                sqlAdapter.Fill(table);
                if (table.Rows.Count > 0)
                {
                    dataGridView1.DataSource = table;
                }
                else
                {
                    dataGridView1.DataSource = null;
                }
            }
        }
        public void IsEmptyfield()
        {
        clause = null;
            foreach (Control X in this.panel1.Controls)
            {

                if (X is TextBox || X is ComboBox)
                {
                    if (X.Tag.ToString() == "num_cin")
                    {
                        if (X.Text.Trim().Length != 0)
                        {
                            if (string.IsNullOrEmpty(clause))
                            {
                                clause = clause + X.Tag + " = '" + X.Text.Trim() + "'";
                                //return clause;
                            }
                            else
                            {
                                clause = clause + " And " + X.Tag + " = '" + X.Text.Trim() + "'";
                                //return clause;
                            }
                        }
                    }
                    else
                    {
                        if (X.Text.Trim().Length != 0)
                        {
                            if (string.IsNullOrEmpty(clause))
                            {
                                clause = clause + X.Tag + " Like '%" + X.Text.Trim() + "%'";
                                //return clause;
                            }
                            else
                            {
                                clause = clause + " And " + X.Tag + " Like '%" + X.Text.Trim() + "%'";
                                //return clause;
                            }
                        }
                    }

                }
                else
                {
                    if (X is DateTimePicker)
                    {
                        if (X.Name == "dateTimePicker1")
                        {
                            if (string.IsNullOrEmpty(clause))
                            {
                                if (dateTimePicker1.Checked)
                                {
                                    string dat = dateTimePicker1.Value.ToString("yyyy-MM-dd");
                                    DateTime date4 = DateTime.Parse(dat);
                                    clause = clause + X.Tag + " Like '%" + dat + "%'";
                                }

                            }
                            else
                            {
                                if (dateTimePicker1.Checked)
                                {
                                    string dat = dateTimePicker1.Value.ToString("yyyy-MM-dd");
                                    DateTime date4 = DateTime.Parse(dat);
                                    clause = clause + " AND " + X.Tag + " Like '%" + dat + "%'";
                                }

                            }
                        }
                    }
                }
            }

        }
        public void selectcin()
        {
            int counter;
            clause1 = null;



            if (dataGridView1.Rows.Count > 0)
            {
                // Iterate through all the rows and sum up the appropriate columns. 
                for (counter = 0; counter < (dataGridView1.Rows.Count);
                    counter++)
                {
                    //dataGridView1.Rows.
                    if (dataGridView1.Rows[counter].Selected)
                    {
                        if (dataGridView1.Rows[counter].Cells["num_cin"].Value
                        != null)
                        {
                            if (dataGridView1.Rows[counter].
                                Cells["num_cin"].Value.ToString().Length != 0)
                            {
                                if (string.IsNullOrEmpty(clause1))
                                {
                                    clause1 = clause1 + int.Parse(dataGridView1.Rows[counter].
                                        Cells["num_cin"].Value.ToString());
                                }
                                else
                                {
                                    clause1 = clause1 + "," + int.Parse(dataGridView1.Rows[counter].
                                    Cells["num_cin"].Value.ToString());

                                }

                            }
                        }
                    }
                }
            }
            SqlDataReader rd = null;
            string query = "select AncienCNI from Electeur where AncienCNI =" + CNI;
            string sCon = conn.connectdb();
            SqlConnection dbConn;
            dbConn = new SqlConnection(sCon);
            dbConn.Open();
            SqlCommand cmd = new SqlCommand(query, dbConn);
            rd = cmd.ExecuteReader();
            if (rd.Read())
            {
                button2.Enabled = true;


            }
            else
            {
                button2.Enabled = false;

            }
        }       
            

        private void button2_Click(object sender, EventArgs e)
        {
            this.selectcin();
            CNI = Int32.Parse(clause1);
            string info = "ANC";
            SqlDataReader rd = null;
            string query = "select * from Electeur where AncienCNI =" + CNI;
            string sCon = conn.connectdb();
            SqlConnection dbConn;
            dbConn = new SqlConnection(sCon);
            dbConn.Open();
            SqlCommand cmd = new SqlCommand(query, dbConn);
            rd = cmd.ExecuteReader();
            if (rd.Read())
            {
                MessageBox.Show("Cet electeur est déjà enregistré");

            }
            else
            {
                SaisieElecteur se = new SaisieElecteur(CNI, verif, info);
                se.MdiParent = this.ParentForm;
                se.Show();
                this.Hide();

            }
        }

        private void AncienCNI_Load(object sender, EventArgs e)
        {
            GetValue get = new GetValue();
            int Profil = get.getProfilID();
            if (Profil == 8)
            {
                button2.Enabled = false;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ClearClass1 clr = new ClearClass1();
            clr.RecursiveClearTextBoxes(this.panel1.Controls);
        }
    }
}
