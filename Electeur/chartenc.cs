﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class chartenc : Form
    {
        CrystalReport_participation_centreville objRptcC;
       
        CrystalReport_participant_regionale objRptcr;
       
        Connection conn = new Connection();
        CrystalReport_chart objRptc; CrystalReport_commune objRpt; CrystalReport_statistic_nationale objRptr;
       
      
        public chartenc()
        {
            InitializeComponent();
        }
        string elec = null;

        private void FillElection()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT ElectionID, ObjetElection +' - '+ObjetTourElection as NomElection from View_Elect";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBoxElec.ValueMember = "ElectionID";
            comboBoxElec.DisplayMember = "NomElection";
            comboBoxElec.DataSource = objDs.Tables[0];
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox2.SelectedIndex >= 0)
            {
                elec = " and ElectionId = '" + comboBoxElec.SelectedValue.ToString() + " ' ";

                string sCon = conn.connectdb();
                SqlConnection con = new SqlConnection(sCon);

                if (comboBox2.SelectedItem.ToString() == "Statistique de nombre de votant graphique à l\'echelle nationale")
                {
                    DDV.Visible = false; DRI.Visible = false;
                    crystalReportViewer3.Visible = true;
                    objRptr = new CrystalReport_statistic_nationale();


                    string SQLlistr = "SET CONCAT_NULL_YIELDS_NULL OFF; SELECT   * " +
                                         " FROM  View_nationale where horaire_id=" + Int32.Parse(comboBoxhr.SelectedValue.ToString()) + elec;

                    con.Open();
                    SqlDataAdapter sqlAdapterr = new SqlDataAdapter(SQLlistr, con);
                    SqlCommandBuilder commandBuilderr = new SqlCommandBuilder(sqlAdapterr);

                    con.Close();
                    DataTable dtr = new DataTable();
                    sqlAdapterr.Fill(dtr);
                    objRptr.SetDataSource(dtr);
                    crystalReportViewer3.ReportSource = objRptr;
                    crystalReportViewer3.Refresh();
                }
                if (comboBox2.SelectedItem.ToString() == "Statistique de nombre de votant graphique par commune")
                {
                    crystalReportViewer3.Visible = true;
                    DDV.Visible = false; DRI.Visible = false;
                    objRpt = new CrystalReport_commune();


                    string SQLlist1 = "SET CONCAT_NULL_YIELDS_NULL OFF; SELECT   * " +
                                         " FROM  View_participation_centre_ville where horaire_id=" + Int32.Parse(comboBoxhr.SelectedValue.ToString()) + elec;
                    con.Open();
                    SqlDataAdapter sqlAdapter1 = new SqlDataAdapter(SQLlist1, con);
                    SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sqlAdapter1);

                    con.Close();
                    DataTable dt1 = new DataTable();
                    sqlAdapter1.Fill(dt1);
                    objRpt.SetDataSource(dt1);
                    crystalReportViewer3.ReportSource = objRpt;
                    crystalReportViewer3.Refresh();
                }
                if (comboBox2.SelectedItem.ToString() == "Statistique de nombre de votant graphique par arrondissement")
                {
                    crystalReportViewer3.Visible = true;
                    DDV.Visible = false; DRI.Visible = false;
                    objRptc = new CrystalReport_chart();


                    string SQLlist = "SET CONCAT_NULL_YIELDS_NULL OFF; SELECT   * " +
                                         " FROM  View_participation_centre_ville  where horaire_id=" + Int32.Parse(comboBoxhr.SelectedValue.ToString()) + elec;

                    con.Open();
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter(SQLlist, con);
                    SqlCommandBuilder commandBuilder1 = new SqlCommandBuilder(sqlAdapter);

                    con.Close();
                    DataTable dt = new DataTable();
                    sqlAdapter.Fill(dt);
                    objRptc.SetDataSource(dt);
                    crystalReportViewer3.ReportSource = objRptc;
                    crystalReportViewer3.Refresh();

                }
                if (comboBox2.SelectedItem.ToString() == "Statistique de nombre de votant djibouti ville détailler en tableau")
                {
                    crystalReportViewer3.Visible = true;
                    DDV.Visible = true; DRI.Visible = false;
                    objRptcC = new CrystalReport_participation_centreville();


                    string SQLlist = "SET CONCAT_NULL_YIELDS_NULL OFF; SELECT   * " +
                                         " FROM  View_participation_centre_ville where horaire_id=" + Int32.Parse(comboBoxhr.SelectedValue.ToString()) + elec;

                    con.Open();
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter(SQLlist, con);
                    SqlCommandBuilder commandBuilder1 = new SqlCommandBuilder(sqlAdapter);

                    con.Close();
                    DataTable dt = new DataTable();
                    sqlAdapter.Fill(dt);
                    objRptcC.SetDataSource(dt);
                    crystalReportViewer3.ReportSource = objRptcC;
                    crystalReportViewer3.Refresh();
                }
                if (comboBox2.SelectedItem.ToString() == "Statistique de nombre de votant région interne détailler en tableau")
                {
                    crystalReportViewer3.Visible = true;
                    DDV.Visible = false; DRI.Visible = true;
                    objRptcr = new CrystalReport_participant_regionale();


                    string SQLlist1 = "SET CONCAT_NULL_YIELDS_NULL OFF; SELECT   * " +
                                         " FROM  View_participation_regionale where horaire_id=" + Int32.Parse(comboBoxhr.SelectedValue.ToString()) + elec;

                    con.Open();
                    SqlDataAdapter sqlAdapter1 = new SqlDataAdapter(SQLlist1, con);
                    SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sqlAdapter1);

                    con.Close();
                    DataTable dt1 = new DataTable();
                    sqlAdapter1.Fill(dt1);
                    objRptcr.SetDataSource(dt1);
                    crystalReportViewer3.ReportSource = objRptcr;
                    crystalReportViewer3.Refresh();
                }
            }
            else
            {
                MessageBox.Show("veuillez selectionner le statistique souhaité");
            }

        }

        private void chartenc_Load(object sender, EventArgs e)
        {
            this.Fillheure();
            this.FillElection();
        }
        private void Fillheure()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT horaire_id, heure FROM horaire ";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBoxhr.ValueMember = "horaire_id";
            comboBoxhr.DisplayMember = "heure";
            comboBoxhr.DataSource = objDs.Tables[0];
        }
    }
}
