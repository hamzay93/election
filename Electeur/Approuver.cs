﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class Approuver : Form
    {
        public Approuver()
        {
            InitializeComponent();
        }

        string adresseID = null;
        int RégionName, ArrondisName, CommuneName;
        Connection conn = new Connection();
        private void button2_Click(object sender, EventArgs e)
        {
            int counter = 0;
            //int counter2 = 0;
            string clause1 = null;
            adresseID = null;

            //for(counter2);
            // Iterate through all the rows and sum up the appropriate columns. 
            //if (counter2 < 1)
            // {
            for (counter = 0; counter < (dataGridViewApp.Rows.Count);
                counter++)
            {
                //dataGridView1.Rows.
                if (dataGridViewApp.Rows[counter].Selected)
                {
                    if (dataGridViewApp.Rows[counter].Cells["ElecteurID"].Value
                    != null)
                    {
                        if (dataGridViewApp.Rows[counter].
                            Cells["ElecteurID"].Value.ToString().Length != 0)
                        {
                            if (string.IsNullOrEmpty(clause1))
                            {
                                clause1 = clause1 + int.Parse(dataGridViewApp.Rows[counter].
                                    Cells["ElecteurID"].Value.ToString());
                                counter1++;

                            }
                            else
                            {
                                clause1 = clause1 + "," + int.Parse(dataGridViewApp.Rows[counter].
                                Cells["ElecteurID"].Value.ToString());
                                counter1++;

                            }

                        }
                        if (dataGridViewApp.Rows[counter].
                           Cells["AdresseID"].Value.ToString().Length != 0)
                        {
                            if (string.IsNullOrEmpty(adresseID))
                            {
                                adresseID = adresseID + int.Parse(dataGridViewApp.Rows[counter].
                                    Cells["AdresseID"].Value.ToString());
                                break;
                            }
                            else
                            {
                                adresseID = adresseID + "," + int.Parse(dataGridViewApp.Rows[counter].
                                Cells["AdresseID"].Value.ToString());
                                break;
                            }
                        }
                    }

                }

            }
            string test = clause1;
            string add1 = adresseID;
            string btn = null;
            Modif mod = new Modif(test, add1, btn);
            mod.Show();
            mod.MdiParent = this.ParentForm;
            this.Hide();
        }
        private void dataGridViewApp_SelectionChanged(object sender, EventArgs e)
        {
            // Update the labels to reflect changes to the selection.
            //UpdateLabelText();
            for (int counter5 = 0; counter5 < (dataGridViewApp.Rows.Count);
                counter5++)
            {
                if (dataGridViewApp.Rows[counter5].Selected)
                {
                    if (dataGridViewApp.Rows[counter5].
                                   Cells["AdresseID"].Value.ToString().Length != 0)
                    {
                        if (string.IsNullOrEmpty(adresseID))
                        {
                            adresseID = adresseID + int.Parse(dataGridViewApp.Rows[counter5].
                                Cells["AdresseID"].Value.ToString());
                            break;
                        }
                        else
                        {
                            adresseID = adresseID + "," + int.Parse(dataGridViewApp.Rows[counter5].
                            Cells["AdresseID"].Value.ToString());
                            break;
                        }
                    }
                }
            }
            this.filldetails();




        }

        private void radGridViewApp_Click(object sender, EventArgs e)
        {

        }
        public string ID, add;
        private void dataGridViewApp_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.dataGridViewApp.Rows[e.RowIndex];
                ID = row.Cells[0].Value.ToString();
                //add = row.Cells[20].Value.ToString();


            }
        }
        private void FillRégion()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT RégionID, NomRégion FROM Région";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBoxRégion.ValueMember = "RégionID";
            comboBoxRégion.DisplayMember = "NomRégion";
            comboBoxRégion.DataSource = objDs.Tables[0];
        }

        private void button1_Click(object sender, EventArgs e)
        {

            this.IsEmptyfield();
            if (String.IsNullOrEmpty(clause))
            {
                MessageBox.Show("veuillez remplir un champ au minimum");
                this.Focus();

            }
            else
            {
                string sCon = conn.connectdb();
                SqlConnection con = new SqlConnection(sCon);
                string str = "SET CONCAT_NULL_YIELDS_NULL OFF; SELECT ElecteurID, Nom1 +'  '+ Nom2 +'  '+Nom3 AS NOM,NomMère1+'  '+NomMère2+'  '+NomMère3 AS 'NOM DE LA MERE',DOB AS 'DATE DE NAISSANCE',POB AS LIEU,CodeElecteur AS 'CODE ELECTEUR',AncienCNI AS 'ANCIEN CNI',DateAncienCNI AS 'DATE DELIVRANCE',NewCNI AS 'NOUVEAU CNI',DateNewCNI AS 'DATE DELIVRANCE',Sexe AS 'SEXE',AdresseID FROM Electeur where StatutID <3 " + clause;
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(str, con);
                SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sqlAdapter);

                DataTable table = new DataTable();
                table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                sqlAdapter.Fill(table);
                dataGridViewApp.DataSource = table;
                if (dataGridViewApp.Rows.Count == 0)
                {
                    button2.Enabled = false;
                    button3.Enabled = false;
                }
                this.dataGridViewApp.Columns["ElecteurID"].Visible = false;
                this.dataGridViewApp.Columns["AdresseID"].Visible = false;
                con.Open();
                clause = "";
            }
        }
        string clause;
        public void IsEmptyfield()
        {

            foreach (Control X in this.panel1.Controls)
            {

                if (X is TextBox || X is ComboBox)
                {
                    if (X.Tag.ToString() == "AncienCNI" || X.Tag.ToString() == "NewCNI")
                    {
                        if (X.Text.Trim().Length != 0)
                        {

                            if (string.IsNullOrEmpty(clause))
                            {
                                clause = clause + " And " + X.Tag + " = " + X.Text.Trim() + "";
                                //return clause;
                            }
                            else
                            {
                                clause = clause + " And " + X.Tag + " = " + X.Text.Trim() + "";
                                //return clause;
                            }
                        }
                    }
                    else
                    {
                        if (X.Text.Trim().Length != 0)
                        {
                            if (string.IsNullOrEmpty(clause))
                            {
                                clause = clause + " And " + X.Tag + " Like '%" + X.Text.Trim() + "%'";
                                //return clause;
                            }
                            else
                            {
                                clause = clause + " And " + X.Tag + " Like '" + X.Text.Trim() + "'";
                                //return clause;
                            }
                        }
                    }

                }
                else
                {
                    if (X is DateTimePicker)
                    {
                        if (X.Name == "dateTimePicker11")
                        {
                            if (string.IsNullOrEmpty(clause))
                            {
                                if (dateTimePicker11.Checked)
                                {
                                    string dat = dateTimePicker11.Value.ToString("yyyy-MM-dd");
                                    DateTime date4 = DateTime.Parse(dat);
                                    clause = clause + " And " + X.Tag + " Like '%" + dat + "%'";
                                }

                            }
                            else
                            {
                                if (dateTimePicker11.Checked)
                                {
                                    string dat = dateTimePicker11.Value.ToString("yyyy-MM-dd");
                                    DateTime date4 = DateTime.Parse(dat);
                                    clause = clause + " AND " + X.Tag + " Like '%" + dat + "%'";
                                }

                            }
                        }
                    }
                }
            }
        }
        string clause3;
        string adresseID1, adresseID2;
        private void selection()
        {
            int counter3;
            clause3 = null;
            adresseID1 = null;
            // Iterate through all the rows and sum up the appropriate columns. 
            for (counter3 = 0; counter3 < (dataGridViewApp.Rows.Count);
                counter3++)
            {
                //dataGridView1.Rows.
                if (dataGridViewApp.Rows[counter3].Selected)
                {
                    if (dataGridViewApp.Rows[counter3].Cells["ElecteurID"].Value
                    != null)
                    {
                        if (dataGridViewApp.Rows[counter3].
                            Cells["ElecteurID"].Value.ToString().Length != 0)
                        {
                            if (string.IsNullOrEmpty(clause3))
                            {
                                clause3 = clause3 + int.Parse(dataGridViewApp.Rows[counter3].
                                    Cells["ElecteurID"].Value.ToString());
                            }
                            else
                            {
                                clause3 = clause3 + "," + int.Parse(dataGridViewApp.Rows[counter3].
                                Cells["ElecteurID"].Value.ToString());

                            }

                        }
                    }
                    if (dataGridViewApp.Rows[counter3].
                            Cells["AdresseID"].Value.ToString().Length != 0)
                    {
                        if (string.IsNullOrEmpty(adresseID1))
                        {
                            adresseID1 = adresseID1 + int.Parse(dataGridViewApp.Rows[counter3].
                                Cells["AdresseID"].Value.ToString());
                            break;
                        }
                        else
                        {
                            adresseID1 = adresseID1 + "," + int.Parse(dataGridViewApp.Rows[counter3].
                            Cells["AdresseID"].Value.ToString());
                            break;
                        }
                    }
                }

            }
        }
        private void filldetails()
        {
            this.selection();
            if (String.IsNullOrEmpty(clause3))
            {
                this.panel3.Visible = false;
                this.panel2.Visible = false;
            }
            else
            {
                this.panel3.Visible = true;
                this.panel2.Visible = true;
                string sCon = conn.connectdb();
                SqlConnection con = new SqlConnection(sCon);
                string str = "SELECT * FROM Electeur where ElecteurID= " + clause3;
                SqlCommand cmd = new SqlCommand(str, con);
                con.Open();
                GetValue get = new GetValue();
                using (SqlDataReader read = cmd.ExecuteReader())



                    while (read.Read())
                    {
                        textBoxNom1.Text = (read["Nom1"].ToString());
                        textBoxNom2.Text = (read["Nom2"].ToString());
                        textBoxNom3.Text = (read["Nom3"].ToString());
                        textBoxNomMère1.Text = (read["NomMère1"].ToString());
                        textBoxNomMère2.Text = (read["NomMère2"].ToString());
                        textBoxNomMère3.Text = (read["NomMère3"].ToString());
                        int colIndex3 = (read.GetOrdinal("DOB"));
                        if (!read.IsDBNull(colIndex3))
                            dateTimePickerDOB.Value = (read.GetDateTime(colIndex3));
                        //dateTimePickerDOB.TextDOB.Text = (read["DOB"].ToString());
                        textBoxPOB.Text = (read["POB"].ToString());
                        textBoxCodeElecteur.Text = (read["CodeElecteur"].ToString());
                        textBoxAncCNI.Text = (read["AncienCNI"].ToString());

                        int colIndex = (read.GetOrdinal("DateAncienCNI"));
                        if (!read.IsDBNull(colIndex))
                            textBox8.Text = (read.GetDateTime(colIndex)).ToString("dd/MM/yyyy");
                        int colIndex2 = (read.GetOrdinal("DateNewCNI"));
                        if (!read.IsDBNull(colIndex2))
                            textBox10.Text = (read.GetDateTime(colIndex2)).ToString("dd/MM/yyyy");
                        textBoxNewCNI.Text = (read["NewCNI"].ToString());
                        textBoxCodeElecteur.Text = (read["CodeElecteur"].ToString());
                        comboBoxSexe.SelectedItem = (read["Sexe"].ToString());
                        textBoxPhone.Text = (read["Téléphone"].ToString());
                        adresseID2 = (read["AdresseID"].ToString());
                        //statutID = (read["StatutID"].ToString());
                        //LabelStatut.Text = this.getStatutName();
                        //adresseID = this.getAdresseID();
                        //IDRégion = (read["RégionID"].ToString());
                        comboBoxRégion.SelectedValue = this.getRégionName().ToString();
                        comboBoxCommune.SelectedValue = this.getCommune().ToString();
                        comboBoxArrondis.SelectedValue = this.getArrondissement().ToString();
                        comboBoxAdresse.SelectedValue = (read["AdresseID"].ToString());

                    }
            }

        }

        private void Approuver_Load(object sender, EventArgs e)
        {
            this.FillRégion();
            dataGridViewApp.SelectionChanged += new EventHandler(dataGridViewApp_SelectionChanged);
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            string str = "SELECT ElecteurID, Nom1 +'  '+ Nom2 +'  '+Nom3 AS NOM,NomMère1+'  '+NomMère2+'  '+NomMère3 AS 'NOM DE LA MERE',DOB AS 'DATE DE NAISSANCE',POB AS LIEU,CodeElecteur AS 'CODE ELECTEUR',AncienCNI AS 'ANCIEN CNI',DateAncienCNI AS 'DATE DELIVRANCE',NewCNI AS 'NOUVEAU CNI',DateNewCNI AS 'DATE DELIVRANCE',Sexe AS 'SEXE',AdresseID FROM Electeur where StatutID < 3 and Nom1 like '%'";
            SqlDataAdapter sqlAdapter = new SqlDataAdapter(str, con);
            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sqlAdapter);

            DataTable table = new DataTable();
            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
            sqlAdapter.Fill(table);
            dataGridViewApp.DataSource = table;
            if (dataGridViewApp.Rows.Count == 0)
            {
                button2.Enabled = false;
                button3.Enabled = false;
            }
            dataGridViewApp.Columns["ElecteurID"].Visible = false;
            this.dataGridViewApp.Columns["AdresseID"].Visible = false;
            con.Open();
        }
        int counter1;
        private void button3_Click(object sender, EventArgs e)
        {
            int counter;
            string clause1 = null;


            // Iterate through all the rows and sum up the appropriate columns. 
            for (counter = 0; counter < (dataGridViewApp.Rows.Count);
                counter++)
            {
                //dataGridView1.Rows.
                if (dataGridViewApp.Rows[counter].Selected)
                {
                    if (dataGridViewApp.Rows[counter].Cells["ElecteurID"].Value
                    != null)
                    {
                        if (dataGridViewApp.Rows[counter].
                            Cells["ElecteurID"].Value.ToString().Length != 0)
                        {
                            if (string.IsNullOrEmpty(clause1))
                            {
                                clause1 = clause1 + int.Parse(dataGridViewApp.Rows[counter].
                                    Cells["ElecteurID"].Value.ToString());
                                counter1++;
                            }
                            else
                            {
                                clause1 = clause1 + "," + int.Parse(dataGridViewApp.Rows[counter].
                                Cells["ElecteurID"].Value.ToString());
                                counter1++;

                            }

                        }
                    }

                }

            }
            Connection cn = new Connection();
            string sCon = conn.connectdb();
            string updateCmd = "UPDATE Electeur SET DateModification =@DateModification,DateApprobation =@DateApprobation, StatutID =@StatutID, AgentApprobation =@AgentApprobation where ElecteurID in(" + clause1 + ")";
            SqlConnection dbConn;
            dbConn = new SqlConnection(sCon);
            dbConn.Open();
            GetValue get = new GetValue();
            //DateTime dt = new DateTime();
            //dt.ToShortDateString();
            string dat = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            //DateTime date4 = DateTime.Parse(dat);
            SqlCommand myCommand = new SqlCommand(updateCmd, dbConn);
            // Create parameters for the SqlCommand object
            // initialize with input-form field values
            //myCommand.Parameters.AddWithValue("@ElecteurID","");
            myCommand.Parameters.AddWithValue("@StatutID", 3);
            myCommand.Parameters.AddWithValue("@DateApprobation", dat);
            myCommand.Parameters.AddWithValue("@AgentApprobation", get.getAgentModif());
            myCommand.Parameters.AddWithValue("@DateModification", dat);
            myCommand.ExecuteNonQuery();
            if (counter1 == 1)
            {
                MessageBox.Show("Electeur Approuvé");
            }
            else
            {
                MessageBox.Show("Liste approuvée");
            }
            // insertion de l'historique
            GetValue value = new GetValue();
            historique hist = new historique();
            for (int compteur = 0; compteur < (dataGridViewApp.Rows.Count);
                compteur++)
            {
                //dataGridView1.Rows.
                if (dataGridViewApp.Rows[compteur].Selected)
                {
                    if (dataGridViewApp.Rows[compteur].Cells["ElecteurID"].Value
                    != null)
                    {
                        if (dataGridViewApp.Rows[compteur].
                            Cells["ElecteurID"].Value.ToString().Length != 0)
                        {
                            string action = "Approbation";
                            int ID = int.Parse(dataGridViewApp.Rows[compteur].Cells["ElecteurID"].Value.ToString());
                            int AgentID = value.getAgentID();
                            int none = 0;
                            hist.addhistoric(AgentID, ID, action, none);


                        }
                    }

                }

            }
            Approuver app = new Approuver();
            app.MdiParent = this.ParentForm;
            app.Show();
            this.Hide();
            //MessageBox.Show("Liste Approuvée");
            //RecursiveClearTextBoxes(this.Controls);
            // Appr.Show();
        }
        public int getRégionName()
        {
            //SqlDataReader rd = null;
            string query = "select RégionID from Région where RégionID = (Select RégionID from Commune where CommuneID= (select CommuneID from Arrondissement where ArrondisID=(select ArrondisID from Adresse where AdresseID ='" + adresseID2 + "')))";
            string sCon = conn.connectdb();
            SqlConnection dbConn;
            dbConn = new SqlConnection(sCon);
            dbConn.Open();
            SqlCommand cmd = new SqlCommand(query, dbConn);
            int result = ((int)cmd.ExecuteScalar());
            //SaisieElecteur se=new SaisieElecteur();
            RégionName = result;
            return RégionName;

        }
        public int getArrondissement()
        {
            //SqlDataReader rd = null;
            string query = "select ArrondisID from Arrondissement where ArrondisID=(select ArrondisID from Adresse where AdresseID ='" + adresseID1 + "')";
            string sCon = conn.connectdb();
            SqlConnection dbConn;
            dbConn = new SqlConnection(sCon);
            dbConn.Open();
            SqlCommand cmd = new SqlCommand(query, dbConn);
            int result = ((int)cmd.ExecuteScalar());
            //SaisieElecteur se=new SaisieElecteur();
            ArrondisName = result;
            return ArrondisName;

        }
        public int getCommune()
        {
            //SqlDataReader rd = null;
            string query = "Select CommuneID from Commune where CommuneID= (select CommuneID from Arrondissement where ArrondisID=(select ArrondisID from Adresse where AdresseID ='" + adresseID1 + "'))";
            string sCon = conn.connectdb();
            SqlConnection dbConn;
            dbConn = new SqlConnection(sCon);
            dbConn.Open();
            SqlCommand cmd = new SqlCommand(query, dbConn);
            int result = ((int)cmd.ExecuteScalar());
            //SaisieElecteur se=new SaisieElecteur();
            CommuneName = result;
            return CommuneName;

        }
        private void FillArrondis(int CommuneID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT ArrondisID, NomArrondis FROM Arrondissement WHERE CommuneID =@CommuneID";
            cmd.Parameters.AddWithValue("@CommuneID", CommuneID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxArrondis.ValueMember = "ArrondisID";
                comboBoxArrondis.DisplayMember = "NomArrondis";
                comboBoxArrondis.DataSource = objDs.Tables[0];
            }
        }
        private void FillAdresse(int ArrondisID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT AdresseID, NomAdresse FROM Adresse WHERE ArrondisID =@ArrondisID";
            cmd.Parameters.AddWithValue("@ArrondisID", ArrondisID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxAdresse.ValueMember = "AdresseID";
                comboBoxAdresse.DisplayMember = "NomAdresse";
                comboBoxAdresse.DataSource = objDs.Tables[0];

            }
            //comboBoxAdresse.SelectedValue = adresseID1;
            //comboBoxAdresse.SelectedValue = this.getadress();
        }
        private void FillCommune(int RégionID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT CommuneID, NomCommune FROM Commune WHERE RégionID =@RégionID";
            cmd.Parameters.AddWithValue("@RégionID", RégionID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxCommune.ValueMember = "CommuneID";
                comboBoxCommune.DisplayMember = "NomCommune";
                comboBoxCommune.DataSource = objDs.Tables[0];
            }
        }
        private void comboBoxCommune_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxCommune.SelectedValue.ToString() != "")
            {
                int CommuneID = Convert.ToInt32(comboBoxCommune.SelectedValue.ToString());
                this.FillArrondis(CommuneID);
                comboBoxArrondis.SelectedIndex = 0;
            }
        }
        private void comboBoxArrondis_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxArrondis.SelectedValue.ToString() != "")
            {
                int ArrondisID = Convert.ToInt32(comboBoxArrondis.SelectedValue.ToString());
                this.FillAdresse(ArrondisID);
                //comboBoxAdresse.SelectedValue.ToString() = this.getadress().ToString();
                //comboBoxAdresse.SelectedValue.= this.getadress();
            }
        }
        private void comboBoxRégion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxRégion.SelectedValue.ToString() != "")
            {
                int RégionID = Convert.ToInt32(comboBoxRégion.SelectedValue.ToString());
                FillCommune(RégionID);
                comboBoxCommune.SelectedIndex = 0;
            }
        }
        private void textBoxAncCNI_KeyPress(object sender, KeyPressEventArgs e)
        {
            int isNum = 0;
            if (e.KeyChar == ',' || e.KeyChar == (char)Keys.Back)
                e.Handled = false;
            else if (!int.TryParse(e.KeyChar.ToString(), out isNum))
                e.Handled = true;
        }
        private void textBoxNom1_KeyPress(object sender, KeyPressEventArgs e)
        {
            int isNum = 0;
            if (e.KeyChar == ',' || e.KeyChar == (char)Keys.Back)
                e.Handled = false;
            else if (int.TryParse(e.KeyChar.ToString(), out isNum))
                e.Handled = true;
        }

        


    }
}
