﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class Ajout_MembrePartiPolitique : Form
    {
        Connection conn = new Connection();
        public Ajout_MembrePartiPolitique()
        {
            InitializeComponent();
        }

        private void Ajout_MembrePartiPolitique_Load(object sender, EventArgs e)
        {
            this.FillElection();
            this.FillPArtie();
        }
       
        private void FillElection()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT ElectionID,ObjetElection FROM Election";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBox_Election.ValueMember = "ElectionID";
            comboBox_Election.DisplayMember = "ObjetElection";
            comboBox_Election.DataSource = objDs.Tables[0];
        }
        private void FillPArtie()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT ID_parti,NomParti FROM Parti_Politique";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBox_parti.ValueMember = "ID_parti";
            comboBox_parti.DisplayMember = "NomParti";
            comboBox_parti.DataSource = objDs.Tables[0];
        }

        private void button2_Click(object sender, EventArgs e)
        {

            if (n1.Text.Trim().Length == 0 && n2.Text.Trim().Length == 0)
            {
                MessageBox.Show("veuillez saisir le nom complet");
                n1.Focus(); n2.Focus();
            }

            else
            {


                //Connection cn = new Connection();
                string insertCmd = "INSERT INTO List_de_Parti (Prenom,nom1,nom2,ID_ELECTION,ID_PARTI,UserCreation,UserModification,DateCreation,DateModification) VALUES (@Prenom,@nom1,@nom2,@ID_ELECTION,@ID_PARTI,@UserCreation,@UserModification,@DateCreation,@DateModification)";
                SqlConnection dbConn;
                dbConn = new SqlConnection(conn.connectdb());
                dbConn.Open();

                string dat = DateTime.Now.ToString("yyyy-MM-dd");

                GetValue value = new GetValue();
                //getAdress add = new getAdress();
                SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);

                if (comboBox_Election.Text != "")
                {
                    int ElectionID = Convert.ToInt32(comboBox_Election.SelectedValue.ToString());
                    myCommand.Parameters.AddWithValue("@ID_ELECTION", ElectionID);
                }
                if (comboBox_parti.Text != "")
                {
                    int partiID = Convert.ToInt32(comboBox_parti.SelectedValue.ToString());
                    myCommand.Parameters.AddWithValue("@ID_PARTI", partiID);
                }
                //if (radioButton1.Checked)
                //{
                //    myCommand.Parameters.AddWithValue("@Etat", 1);
                //}
                //if (radioButton2.Checked)
                //{
                //    myCommand.Parameters.AddWithValue("@Etat", 0);
                //}
                myCommand.Parameters.AddWithValue("@Prenom", n1.Text);
                myCommand.Parameters.AddWithValue("@nom1", n2.Text);
                if (n3.Text != "")
                {
                    myCommand.Parameters.AddWithValue("@nom2", n3.Text);
                }
                myCommand.Parameters.AddWithValue("@UserCreation", value.getAgentCréation());
                myCommand.Parameters.AddWithValue("@DateCreation", dat);
                myCommand.Parameters.AddWithValue("@UserModification", value.getAgentCréation());
                myCommand.Parameters.AddWithValue("@DateModification", dat);
                myCommand.ExecuteNonQuery();

                MessageBox.Show("Ajoutée avec succès");
                n1.Clear();
                n2.Clear();
                n3.Clear();

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
