﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class saisielecteur_votonordonance : Form
    {
        public string adresse, statutID, AgentID;
        Connection conn = new Connection();
        public saisielecteur_votonordonance()
        {
            InitializeComponent();
        }

        private void saisielecteur_votonordonance_Load(object sender, EventArgs e)
        {
            FillRégion();
            label26.Text = DateTime.Today.ToShortDateString().ToString();
        }
        public int getStatutID()
        {
            //SqlDataReader rd = null;
            string query = "select StatutID from Statut where Statut='" + statutID + "'";
            string sCon = conn.connectdb();
            SqlConnection dbConn;
            dbConn = new SqlConnection(sCon);
            dbConn.Open();
            SqlCommand cmd = new SqlCommand(query, dbConn);
            int result = (int)cmd.ExecuteScalar();
            //SaisieElecteur se=new SaisieElecteur();
            int statut = result;
            return statut;

        }
        public int getAdresseID()
        {
            //SqlDataReader rd = null;
            string query = "select AdresseID from Adresse where NomAdresse='" + adresse + "'";
            string sCon = conn.connectdb();
            SqlConnection dbConn;
            dbConn = new SqlConnection(sCon);
            dbConn.Open();
            SqlCommand cmd = new SqlCommand(query, dbConn);
            int result = ((int)cmd.ExecuteScalar());
            //SaisieElecteur se=new SaisieElecteur();
            int adress = result;
            return adress;

        }

        private void FillRégion()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT RégionID, NomRégion FROM Région";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBoxRégion.ValueMember = "RégionID";
            comboBoxRégion.DisplayMember = "NomRégion";
            comboBoxRégion.DataSource = objDs.Tables[0];
        }
        private void FillArrondis(int CommuneID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT ArrondisID, NomArrondis FROM Arrondissement WHERE CommuneID =@CommuneID";
            cmd.Parameters.AddWithValue("@CommuneID", CommuneID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxArrondis.ValueMember = "ArrondisID";
                comboBoxArrondis.DisplayMember = "NomArrondis";
                comboBoxArrondis.DataSource = objDs.Tables[0];
            }
        }
        /* private void FillAdresse(int ArrondisID)
         {
             string sCon = conn.connectdb();
             SqlConnection con = new SqlConnection(sCon);
             SqlCommand cmd = new SqlCommand();
             cmd.Connection = con;
             cmd.CommandType = CommandType.Text;
             cmd.CommandText = "SELECT AdresseID, NomAdresse FROM Adresse WHERE ArrondisID =@ArrondisID";
             cmd.Parameters.AddWithValue("@ArrondisID", ArrondisID);
             DataSet objDs = new DataSet();
             SqlDataAdapter dAdapter = new SqlDataAdapter();
             dAdapter.SelectCommand = cmd;
             con.Open();
             dAdapter.Fill(objDs);
             con.Close();
             if (objDs.Tables[0].Rows.Count > 0)
             {
                 comboBoxAdresse.ValueMember = "AdresseID";
                 comboBoxAdresse.DisplayMember = "NomAdresse";
                 comboBoxAdresse.DataSource = objDs.Tables[0];
             }
         }*/
        private void FillCommune(int RégionID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT CommuneID, NomCommune FROM Commune WHERE RégionID =@RégionID";
            cmd.Parameters.AddWithValue("@RégionID", RégionID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxCommune.ValueMember = "CommuneID";
                comboBoxCommune.DisplayMember = "NomCommune";
                comboBoxCommune.DataSource = objDs.Tables[0];
            }
        }
        private void Filllocalite(int _reg)
        {
            int reg = _reg;
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT LocalitéID, NomLocalité FROM Localité WHERE RégionID = " + reg;
            //cmd.Parameters.AddWithValue("@RégionID", RégionID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxLoc.ValueMember = "LocalitéID";
                comboBoxLoc.DisplayMember = "NomLocalité";
                comboBoxLoc.DataSource = objDs.Tables[0];
            }
        }

        private void comboBoxRégion_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (comboBoxRégion.SelectedValue.ToString() == "1")
            {
                label13.Text = "Commune";
                comboBoxLoc.Visible = false;
                label15.Visible = true;
                label14.Visible = true;
                comboBoxAdresse.Visible = true;
                comboBoxArrondis.Visible = true;
                comboBoxCommune.Visible = true;
                int RégionID = Convert.ToInt32(comboBoxRégion.SelectedValue.ToString());
                FillCommune(RégionID);
                comboBoxCommune.SelectedIndex = 0;

            }
            else
            {
                comboBoxLoc.Visible = true;
                label13.Text = "Localité";
                comboBoxCommune.Visible = false;
                label15.Visible = false;
                label14.Visible = false;
                comboBoxAdresse.Visible = false;
                comboBoxArrondis.Visible = false;
                int RégionID = Convert.ToInt32(comboBoxRégion.SelectedValue.ToString());
                Filllocalite(RégionID);
                comboBoxCommune.SelectedIndex = 0;
            }
        }

        private void comboBoxCommune_SelectedIndexChanged(object sender, EventArgs e)
        {


            if (comboBoxCommune.SelectedValue.ToString() != "")
            {
                int CommuneID = Convert.ToInt32(comboBoxCommune.SelectedValue.ToString());
                this.FillArrondis(CommuneID);
                comboBoxArrondis.SelectedIndex = 0;
            }
        }

        private void comboBoxLoc_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBoxCodeElecteur_KeyPress(object sender, KeyPressEventArgs e)
        {

            int isNum = 0;
            if (e.KeyChar == ',' || e.KeyChar == (char)Keys.Back)
                e.Handled = false;
            else if (!int.TryParse(e.KeyChar.ToString(), out isNum))
                e.Handled = true;
        }
     

        private void textBoxPortable_KeyPress_1(object sender, KeyPressEventArgs e)
        {


            int isNum = 0;
            if (e.KeyChar == ',' || e.KeyChar == (char)Keys.Back)
                e.Handled = false;
            else if (!int.TryParse(e.KeyChar.ToString(), out isNum))
                e.Handled = true;
        }

        private void textBoxNewCNI_KeyPress_1(object sender, KeyPressEventArgs e)
        {

            int isNum = 0;
            if (e.KeyChar == ',' || e.KeyChar == (char)Keys.Back)
                e.Handled = false;
            else if (!int.TryParse(e.KeyChar.ToString(), out isNum))
                e.Handled = true;
        }

        private void textBoxNom1_KeyPress_1(object sender, KeyPressEventArgs e)
        {

            int isNum = 0;
            if (e.KeyChar == ',' || e.KeyChar == (char)Keys.Back)
                e.Handled = false;
            else if (int.TryParse(e.KeyChar.ToString(), out isNum))
                e.Handled = true;
        }

        private void textBoxNom2_KeyPress_1(object sender, KeyPressEventArgs e)
        {

            int isNum = 0;
            if (e.KeyChar == ',' || e.KeyChar == (char)Keys.Back)
                e.Handled = false;
            else if (int.TryParse(e.KeyChar.ToString(), out isNum))
                e.Handled = true;
        }

        private void textBoxNom3_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            int isNum = 0;
            if (e.KeyChar == ',' || e.KeyChar == (char)Keys.Back)
                e.Handled = false;
            else if (int.TryParse(e.KeyChar.ToString(), out isNum))
                e.Handled = true;
        }

        private void textBoxAncCNI_KeyPress_1(object sender, KeyPressEventArgs e)
        {

            int isNum = 0;
            if (e.KeyChar == ',' || e.KeyChar == (char)Keys.Back)
                e.Handled = false;
            else if (!int.TryParse(e.KeyChar.ToString(), out isNum))
                e.Handled = true;
        }

        private void textBoxPhone_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            int isNum = 0;
            if (e.KeyChar == ',' || e.KeyChar == (char)Keys.Back)
                e.Handled = false;
            else if (!int.TryParse(e.KeyChar.ToString(), out isNum))
                e.Handled = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {

            //   this.checkIfExist();
            if (textBoxNom1.Text.Trim().Length == 0 || textBoxNom2.Text.Trim().Length == 0)
            {
                MessageBox.Show("Veuillez saisir le nom complet du mandateur");
                textBoxNom1.Focus();
            }
            else
            {

                if (textBoxAncCNI.Text.Trim().Length == 0 && textBoxNewCNI.Text.Trim().Length == 0)
                {
                    MessageBox.Show("Veuillez saisir la nouvelle CNI ou l'ancienne CNI");
                    textBoxAncCNI.Focus();
                }
                else
                {
                    if (comboBoxSexe.Text.Length == 0)
                    {
                        MessageBox.Show("Veuillez choisir le sexe de la personne");
                        comboBoxSexe.Focus();
                    }
                    else
                    {

                        if (dateTimePicker1.Checked || dateTimePicker2.Checked)
                        {
                            DateTime Date1 = DateTime.Parse(dateTimePicker1.Text);
                            DateTime Date2 = DateTime.Parse(dateTimePicker2.Text);
                            DateTime Date3 = DateTime.Parse(dateTimePickerDOB.Text);
                            int result = DateTime.Compare(Date1, Date2);

                            if (dateTimePicker1.Checked && dateTimePicker2.Checked)
                            {


                                if (result > 0 || result == 0)
                                {
                                    MessageBox.Show("La date de la nouvelle CNI ne peut être anterieure ou identique à celle de l'ancienne CNI");
                                    dateTimePicker1.Focus();
                                }
                                else
                                {

                                    try
                                    {
                                        if (Int32.Parse(comboBoxRégion.SelectedValue.ToString()) == 1)
                                        {

                                            string sCon = conn.connectdb();
                                            string insertCmd = "INSERT INTO ElecteurOrdonance (RégionID,CommuneID,ArrondisID,Nom1,Nom2,Nom3,NomMère1,NomMère2,NomMère3,DOB,POB,CodeElecteur,AncienCNI,DateAncienCNI,NewCNI,DateNewCNI,AgentSaisie,AgentCréation,AgentModif,Téléphone,Sexe,Région,Commune,Arrondissement,DateCréation,DateModification,Profession,Portable,Complement,AgentID,AdresseID,Adresse) VALUES (@RégionID,@CommuneID,@ArrondisID,@Nom1,@Nom2,@Nom3,@NomMère1,@NomMère2,@NomMère3,@DOB,@POB,@CodeElecteur,@AncienCNI,@DateAncienCNI,@NewCNI,@DateNewCNI,@AgentSaisie,@AgentCréation,@AgentModif,@Téléphone,@Sexe,@Région,@Commune,@Arrondissement,@DateCréation,@DateModification,@Profession,@Portable,@Complement,@AgentID,@AdresseID,@Adresse)";
                                            SqlConnection dbConn;
                                            dbConn = new SqlConnection(sCon);
                                            dbConn.Open();
                                            Login lg = new Login();
                                            //string str;
                                            string dat = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                            DateTime date4 = DateTime.Parse(dat);

                                            GetValue value = new GetValue();
                                            //getAdress add = new getAdress();
                                            SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                                            // Create parameters for the SqlCommand object
                                            // initialize with input-form field values
                                            //myCommand.Parameters.AddWithValue("@ElecteurID","");
                                            myCommand.Parameters.AddWithValue("@Nom1", textBoxNom1.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@Nom2", textBoxNom2.Text.ToUpper());

                                            myCommand.Parameters.AddWithValue("@Nom3", textBoxNom3.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@NomMère1", textBoxNomMère1.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@NomMère2", textBoxNomMère2.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@NomMère3", textBoxNomMère3.Text.ToUpper());
                                            if (dateTimePickerDOB.Checked)
                                            {
                                                myCommand.Parameters.AddWithValue("@DOB", Date3);
                                            }
                                            else
                                            {
                                                myCommand.Parameters.AddWithValue("@DOB", DBNull.Value);
                                            }
                                            myCommand.Parameters.AddWithValue("@POB", textBoxPOB.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@CodeElecteur", textBoxCodeElecteur.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@AncienCNI", textBoxAncCNI.Text.ToUpper());
                                            if (dateTimePicker1.Checked)
                                            {
                                                myCommand.Parameters.AddWithValue("@DateAncienCNI", Date1);
                                            }
                                            else
                                            {
                                                myCommand.Parameters.AddWithValue("@DateAncienCNI", DBNull.Value);
                                            }
                                            myCommand.Parameters.AddWithValue("@NewCNI", textBoxNewCNI.Text.ToUpper());
                                            if (dateTimePicker2.Checked)
                                            {
                                                myCommand.Parameters.AddWithValue("@DateNewCNI", Date2);
                                            }
                                            else
                                            {
                                                myCommand.Parameters.AddWithValue("@DateNewCNI", DBNull.Value);
                                            }

                                            myCommand.Parameters.AddWithValue("@AgentSaisie", value.getAgentSaisie());//recupere agent saisie
                                            myCommand.Parameters.AddWithValue("@AgentModif", value.getAgentModif());//recupere agent modif
                                            myCommand.Parameters.AddWithValue("@AgentCréation", value.getAgentCréation());//recupere agent de creation

                                            myCommand.Parameters.AddWithValue("@Téléphone", textBoxPhone.Text.ToUpper());

                                            myCommand.Parameters.AddWithValue("@AgentID", value.getAgentID());//recupere l'ID agent
                                            myCommand.Parameters.AddWithValue("@Sexe", comboBoxSexe.Text);

                                            myCommand.Parameters.AddWithValue("@Région", comboBoxRégion.Text);
                                            myCommand.Parameters.AddWithValue("@RégionID", Int32.Parse(comboBoxRégion.SelectedValue.ToString()));


                                            myCommand.Parameters.AddWithValue("@Commune", comboBoxCommune.Text);
                                            myCommand.Parameters.AddWithValue("@CommuneID", Int32.Parse(comboBoxCommune.SelectedValue.ToString()));


                                            myCommand.Parameters.AddWithValue("@Arrondissement", comboBoxArrondis.Text);
                                            myCommand.Parameters.AddWithValue("@ArrondisID", Int32.Parse(comboBoxArrondis.SelectedValue.ToString()));

                                            myCommand.Parameters.AddWithValue("@Adresse", comboBoxAdresse.Text);
                                            myCommand.Parameters.AddWithValue("@AdresseID", Int32.Parse(comboBoxAdresse.SelectedValue.ToString()));

                                            myCommand.Parameters.AddWithValue("@DateCréation", dat);
                                            myCommand.Parameters.AddWithValue("@Profession", textBoxProf.Text);
                                            myCommand.Parameters.AddWithValue("@Portable", textBoxPortable.Text);
                                            myCommand.Parameters.AddWithValue("@DateModification", dat);

                                            myCommand.Parameters.AddWithValue("@Complement", textBoxAdresseCompl.Text);



                                            myCommand.ExecuteNonQuery();
                                            MessageBox.Show("Ajout effectué avec succès");
                                            RecursiveClearTextBoxes(this.Controls);
                                            dateTimePicker1.Checked = false;
                                            dateTimePicker2.Checked = false;
                                            dateTimePickerDOB.Checked = false;
                                           
                                        }
                                        else
                                        {
                                            string sCon = conn.connectdb();
                                            string insertCmd = "INSERT INTO ElecteurOrdonance (localitéID,RégionID,Nom1,Nom2,Nom3,NomMère1,NomMère2,NomMère3,DOB,POB,CodeElecteur,AncienCNI,DateAncienCNI,NewCNI,DateNewCNI,AgentSaisie,AgentCréation,AgentModif,Téléphone,Sexe,Région,DateCréation,DateModification,Localité,Profession,Portable,Complement,AgentID) VALUES (@localitéID,@RégionID,@Nom1,@Nom2,@Nom3,@NomMère1,@NomMère2,@NomMère3,@DOB,@POB,@CodeElecteur,@AncienCNI,@DateAncienCNI,@NewCNI,@DateNewCNI,@AgentSaisie,@AgentCréation,@AgentModif,@Téléphone,@Sexe,@Région,@DateCréation,@DateModification,@Localité,@Profession,@Portable,@Complement,@AgentID)";
                                            SqlConnection dbConn;
                                            dbConn = new SqlConnection(sCon);
                                            dbConn.Open();
                                            Login lg = new Login();
                                            //string str;
                                            string dat = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                            DateTime date4 = DateTime.Parse(dat);

                                            GetValue value = new GetValue();
                                            //getAdress add = new getAdress();
                                            SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                                            // Create parameters for the SqlCommand object
                                            // initialize with input-form field values
                                            //myCommand.Parameters.AddWithValue("@ElecteurID","");
                                            myCommand.Parameters.AddWithValue("@Nom1", textBoxNom1.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@Nom2", textBoxNom2.Text.ToUpper());

                                            myCommand.Parameters.AddWithValue("@Nom3", textBoxNom3.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@NomMère1", textBoxNomMère1.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@NomMère2", textBoxNomMère2.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@NomMère3", textBoxNomMère3.Text.ToUpper());
                                            if (dateTimePickerDOB.Checked)
                                            {
                                                myCommand.Parameters.AddWithValue("@DOB", Date3);
                                            }
                                            else
                                            {
                                                myCommand.Parameters.AddWithValue("@DOB", DBNull.Value);
                                            }
                                            myCommand.Parameters.AddWithValue("@POB", textBoxPOB.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@CodeElecteur", textBoxCodeElecteur.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@AncienCNI", textBoxAncCNI.Text.ToUpper());
                                            if (dateTimePicker1.Checked)
                                            {
                                                myCommand.Parameters.AddWithValue("@DateAncienCNI", Date1);
                                            }
                                            else
                                            {
                                                myCommand.Parameters.AddWithValue("@DateAncienCNI", DBNull.Value);
                                            }
                                            myCommand.Parameters.AddWithValue("@NewCNI", textBoxNewCNI.Text.ToUpper());
                                            if (dateTimePicker2.Checked)
                                            {
                                                myCommand.Parameters.AddWithValue("@DateNewCNI", Date2);
                                            }
                                            else
                                            {
                                                myCommand.Parameters.AddWithValue("@DateNewCNI", DBNull.Value);
                                            }

                                            myCommand.Parameters.AddWithValue("@AgentSaisie", value.getAgentSaisie());//recupere agent saisie
                                            myCommand.Parameters.AddWithValue("@AgentModif", value.getAgentModif());//recupere agent modif
                                            myCommand.Parameters.AddWithValue("@AgentCréation", value.getAgentCréation());//recupere agent de creation

                                            myCommand.Parameters.AddWithValue("@Téléphone", textBoxPhone.Text.ToUpper());

                                            myCommand.Parameters.AddWithValue("@AgentID", value.getAgentID());//recupere l'ID agent
                                            myCommand.Parameters.AddWithValue("@Sexe", comboBoxSexe.Text);

                                            myCommand.Parameters.AddWithValue("@Région", comboBoxRégion.Text);
                                            myCommand.Parameters.AddWithValue("@RégionID", Int32.Parse(comboBoxRégion.SelectedValue.ToString()));


                                            myCommand.Parameters.AddWithValue("@DateCréation", dat);
                                            myCommand.Parameters.AddWithValue("@Profession", textBoxProf.Text);
                                            myCommand.Parameters.AddWithValue("@Portable", textBoxPortable.Text);
                                            myCommand.Parameters.AddWithValue("@DateModification", dat);

                                            myCommand.Parameters.AddWithValue("@Localité", comboBoxLoc.Text);
                                            myCommand.Parameters.AddWithValue("@localitéID", Int32.Parse(comboBoxLoc.SelectedValue.ToString()));

                                            myCommand.Parameters.AddWithValue("@Complement", textBoxAdresseCompl.Text);



                                            myCommand.ExecuteNonQuery();
                                            MessageBox.Show("Ajout effectué avec succès");
                                            RecursiveClearTextBoxes(this.Controls);
                                            dateTimePicker1.Checked = false;
                                            dateTimePicker2.Checked = false;
                                            dateTimePickerDOB.Checked = false;
                                           
                                        }

                                    }
                                    catch (SqlException ex)
                                    {
                                        MessageBox.Show("erreur  " + ex.Message);
                                    }
                                }


                            }
                            else
                            {
                                try
                                {
                                    if (Int32.Parse(comboBoxRégion.SelectedValue.ToString()) == 1)
                                    {

                                        string sCon = conn.connectdb();
                                        string insertCmd = "INSERT INTO ElecteurOrdonance (RégionID,CommuneID,ArrondisID,Nom1,Nom2,Nom3,NomMère1,NomMère2,NomMère3,DOB,POB,CodeElecteur,AncienCNI,DateAncienCNI,NewCNI,DateNewCNI,AgentSaisie,AgentCréation,AgentModif,Téléphone,Sexe,Région,Commune,Arrondissement,DateCréation,DateModification,Profession,Portable,Complement,AgentID,AdresseID,Adresse) VALUES (@RégionID,@CommuneID,@ArrondisID,@Nom1,@Nom2,@Nom3,@NomMère1,@NomMère2,@NomMère3,@DOB,@POB,@CodeElecteur,@AncienCNI,@DateAncienCNI,@NewCNI,@DateNewCNI,@AgentSaisie,@AgentCréation,@AgentModif,@Téléphone,@Sexe,@Région,@Commune,@Arrondissement,@DateCréation,@DateModification,@Profession,@Portable,@Complement,@AgentID,@AdresseID,@Adresse)";
                                        SqlConnection dbConn;
                                        dbConn = new SqlConnection(sCon);
                                        dbConn.Open();
                                        Login lg = new Login();
                                        //string str;
                                        string dat = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                        DateTime date4 = DateTime.Parse(dat);

                                        GetValue value = new GetValue();
                                        //getAdress add = new getAdress();
                                        SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                                        // Create parameters for the SqlCommand object
                                        // initialize with input-form field values
                                        //myCommand.Parameters.AddWithValue("@ElecteurID","");
                                        myCommand.Parameters.AddWithValue("@Nom1", textBoxNom1.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@Nom2", textBoxNom2.Text.ToUpper());

                                        myCommand.Parameters.AddWithValue("@Nom3", textBoxNom3.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@NomMère1", textBoxNomMère1.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@NomMère2", textBoxNomMère2.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@NomMère3", textBoxNomMère3.Text.ToUpper());
                                        if (dateTimePickerDOB.Checked)
                                        {
                                            myCommand.Parameters.AddWithValue("@DOB", Date3);
                                        }
                                        else
                                        {
                                            myCommand.Parameters.AddWithValue("@DOB", DBNull.Value);
                                        }
                                        myCommand.Parameters.AddWithValue("@POB", textBoxPOB.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@CodeElecteur", textBoxCodeElecteur.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@AncienCNI", textBoxAncCNI.Text.ToUpper());
                                        if (dateTimePicker1.Checked)
                                        {
                                            myCommand.Parameters.AddWithValue("@DateAncienCNI", Date1);
                                        }
                                        else
                                        {
                                            myCommand.Parameters.AddWithValue("@DateAncienCNI", DBNull.Value);
                                        }
                                        myCommand.Parameters.AddWithValue("@NewCNI", textBoxNewCNI.Text.ToUpper());
                                        if (dateTimePicker2.Checked)
                                        {
                                            myCommand.Parameters.AddWithValue("@DateNewCNI", Date2);
                                        }
                                        else
                                        {
                                            myCommand.Parameters.AddWithValue("@DateNewCNI", DBNull.Value);
                                        }

                                        myCommand.Parameters.AddWithValue("@AgentSaisie", value.getAgentSaisie());//recupere agent saisie
                                        myCommand.Parameters.AddWithValue("@AgentModif", value.getAgentModif());//recupere agent modif
                                        myCommand.Parameters.AddWithValue("@AgentCréation", value.getAgentCréation());//recupere agent de creation

                                        myCommand.Parameters.AddWithValue("@Téléphone", textBoxPhone.Text.ToUpper());

                                        myCommand.Parameters.AddWithValue("@AgentID", value.getAgentID());//recupere l'ID agent
                                        myCommand.Parameters.AddWithValue("@Sexe", comboBoxSexe.Text);

                                        myCommand.Parameters.AddWithValue("@Région", comboBoxRégion.Text);
                                        myCommand.Parameters.AddWithValue("@RégionID", Int32.Parse(comboBoxRégion.SelectedValue.ToString()));


                                        myCommand.Parameters.AddWithValue("@Commune", comboBoxCommune.Text);
                                        myCommand.Parameters.AddWithValue("@CommuneID", Int32.Parse(comboBoxCommune.SelectedValue.ToString()));


                                        myCommand.Parameters.AddWithValue("@Arrondissement", comboBoxArrondis.Text);
                                        myCommand.Parameters.AddWithValue("@ArrondisID", Int32.Parse(comboBoxArrondis.SelectedValue.ToString()));

                                        myCommand.Parameters.AddWithValue("@Adresse", comboBoxAdresse.Text);
                                        myCommand.Parameters.AddWithValue("@AdresseID", Int32.Parse(comboBoxAdresse.SelectedValue.ToString()));

                                        myCommand.Parameters.AddWithValue("@DateCréation", dat);
                                        myCommand.Parameters.AddWithValue("@Profession", textBoxProf.Text);
                                        myCommand.Parameters.AddWithValue("@Portable", textBoxPortable.Text);
                                        myCommand.Parameters.AddWithValue("@DateModification", dat);

                                        myCommand.Parameters.AddWithValue("@Complement", textBoxAdresseCompl.Text);



                                        myCommand.ExecuteNonQuery();
                                        MessageBox.Show("Ajout effectué avec succès");
                                        RecursiveClearTextBoxes(this.Controls);
                                        dateTimePicker1.Checked = false;
                                        dateTimePicker2.Checked = false;
                                        dateTimePickerDOB.Checked = false;
                                       
                                    }
                                    else
                                    {
                                        string sCon = conn.connectdb();
                                        string insertCmd = "INSERT INTO ElecteurOrdonance (localitéID,RégionID,Nom1,Nom2,Nom3,NomMère1,NomMère2,NomMère3,DOB,POB,CodeElecteur,AncienCNI,DateAncienCNI,NewCNI,DateNewCNI,AgentSaisie,AgentCréation,AgentModif,Téléphone,Sexe,Région,DateCréation,DateModification,Localité,Profession,Portable,Complement,AgentID) VALUES (@localitéID,@RégionID,@Nom1,@Nom2,@Nom3,@NomMère1,@NomMère2,@NomMère3,@DOB,@POB,@CodeElecteur,@AncienCNI,@DateAncienCNI,@NewCNI,@DateNewCNI,@AgentSaisie,@AgentCréation,@AgentModif,@Téléphone,@Sexe,@Région,@DateCréation,@DateModification,@Localité,@Profession,@Portable,@Complement,@AgentID)";
                                        SqlConnection dbConn;
                                        dbConn = new SqlConnection(sCon);
                                        dbConn.Open();
                                        Login lg = new Login();
                                        //string str;
                                        string dat = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                        DateTime date4 = DateTime.Parse(dat);

                                        GetValue value = new GetValue();
                                        //getAdress add = new getAdress();
                                        SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                                        // Create parameters for the SqlCommand object
                                        // initialize with input-form field values
                                        //myCommand.Parameters.AddWithValue("@ElecteurID","");
                                        myCommand.Parameters.AddWithValue("@Nom1", textBoxNom1.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@Nom2", textBoxNom2.Text.ToUpper());

                                        myCommand.Parameters.AddWithValue("@Nom3", textBoxNom3.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@NomMère1", textBoxNomMère1.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@NomMère2", textBoxNomMère2.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@NomMère3", textBoxNomMère3.Text.ToUpper());
                                        if (dateTimePickerDOB.Checked)
                                        {
                                            myCommand.Parameters.AddWithValue("@DOB", Date3);
                                        }
                                        else
                                        {
                                            myCommand.Parameters.AddWithValue("@DOB", DBNull.Value);
                                        }
                                        myCommand.Parameters.AddWithValue("@POB", textBoxPOB.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@CodeElecteur", textBoxCodeElecteur.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@AncienCNI", textBoxAncCNI.Text.ToUpper());
                                        if (dateTimePicker1.Checked)
                                        {
                                            myCommand.Parameters.AddWithValue("@DateAncienCNI", Date1);
                                        }
                                        else
                                        {
                                            myCommand.Parameters.AddWithValue("@DateAncienCNI", DBNull.Value);
                                        }
                                        myCommand.Parameters.AddWithValue("@NewCNI", textBoxNewCNI.Text.ToUpper());
                                        if (dateTimePicker2.Checked)
                                        {
                                            myCommand.Parameters.AddWithValue("@DateNewCNI", Date2);
                                        }
                                        else
                                        {
                                            myCommand.Parameters.AddWithValue("@DateNewCNI", DBNull.Value);
                                        }

                                        myCommand.Parameters.AddWithValue("@AgentSaisie", value.getAgentSaisie());//recupere agent saisie
                                        myCommand.Parameters.AddWithValue("@AgentModif", value.getAgentModif());//recupere agent modif
                                        myCommand.Parameters.AddWithValue("@AgentCréation", value.getAgentCréation());//recupere agent de creation

                                        myCommand.Parameters.AddWithValue("@Téléphone", textBoxPhone.Text.ToUpper());

                                        myCommand.Parameters.AddWithValue("@AgentID", value.getAgentID());//recupere l'ID agent
                                        myCommand.Parameters.AddWithValue("@Sexe", comboBoxSexe.Text);

                                        myCommand.Parameters.AddWithValue("@Région", comboBoxRégion.Text);
                                        myCommand.Parameters.AddWithValue("@RégionID", Int32.Parse(comboBoxRégion.SelectedValue.ToString()));


                                        myCommand.Parameters.AddWithValue("@DateCréation", dat);
                                        myCommand.Parameters.AddWithValue("@Profession", textBoxProf.Text);
                                        myCommand.Parameters.AddWithValue("@Portable", textBoxPortable.Text);
                                        myCommand.Parameters.AddWithValue("@DateModification", dat);

                                        myCommand.Parameters.AddWithValue("@Localité", comboBoxLoc.Text);
                                        myCommand.Parameters.AddWithValue("@localitéID", Int32.Parse(comboBoxLoc.SelectedValue.ToString()));

                                        myCommand.Parameters.AddWithValue("@Complement", textBoxAdresseCompl.Text);



                                        myCommand.ExecuteNonQuery();
                                        MessageBox.Show("Ajout effectué avec succès");
                                        RecursiveClearTextBoxes(this.Controls);
                                        dateTimePicker1.Checked = false;
                                        dateTimePicker2.Checked = false;
                                        dateTimePickerDOB.Checked = false;
                                       
                                    }
                                }
                                catch (SqlException ex)
                                {
                                    MessageBox.Show("erreur  " + ex.Message);
                                }
                            }
                        }
                        else
                        {
                            MessageBox.Show("Veuillez saisir la date de la nouvelle CNI ou la date de l'ancienne CNI");
                        }
                    }
                }
            }

        }
        private void RecursiveClearTextBoxes(Control.ControlCollection cc)
        {
            foreach (Control ctrl in cc)
            {
                TextBox tb = ctrl as TextBox;
                if (tb != null)
                    tb.Clear();
                else
                    RecursiveClearTextBoxes(ctrl.Controls);
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {


            //   this.checkIfExist();
            if (textBoxNom1.Text.Trim().Length == 0 || textBoxNom2.Text.Trim().Length == 0 )
            {
                MessageBox.Show("Veuillez saisir le nom complet du mandateur");
                textBoxNom1.Focus();
            }
            else
            {

                if (textBoxAncCNI.Text.Trim().Length == 0 && textBoxNewCNI.Text.Trim().Length == 0)
                {
                    MessageBox.Show("Veuillez saisir la nouvelle CNI ou l'ancienne CNI");
                    textBoxAncCNI.Focus();
                }
                else
                {
                    if (comboBoxSexe.Text.Length == 0)
                    {
                        MessageBox.Show("Veuillez choisir le sexe de la personne");
                        comboBoxSexe.Focus();
                    }
                    else
                    {

                        if (dateTimePicker1.Checked || dateTimePicker2.Checked)
                        {
                            DateTime Date1 = DateTime.Parse(dateTimePicker1.Text);
                            DateTime Date2 = DateTime.Parse(dateTimePicker2.Text);
                            DateTime Date3 = DateTime.Parse(dateTimePickerDOB.Text);
                            int result = DateTime.Compare(Date1, Date2);

                            if (dateTimePicker1.Checked && dateTimePicker2.Checked)
                            {


                                if (result > 0 || result == 0)
                                {
                                    MessageBox.Show("La date de la nouvelle CNI ne peut être anterieure ou identique à celle de l'ancienne CNI");
                                    dateTimePicker1.Focus();
                                }
                                else
                                {

                                    try
                                    {
                                        if (Int32.Parse(comboBoxRégion.SelectedValue.ToString()) == 1)
                                        {

                                            string sCon = conn.connectdb();
                                            string insertCmd = "INSERT INTO ElecteurOrdonance (RégionID,CommuneID,ArrondisID,Nom1,Nom2,Nom3,NomMère1,NomMère2,NomMère3,DOB,POB,CodeElecteur,AncienCNI,DateAncienCNI,NewCNI,DateNewCNI,AgentSaisie,AgentCréation,AgentModif,Téléphone,Sexe,Région,Commune,Arrondissement,DateCréation,DateModification,Profession,Portable,Complement,AgentID,AdresseID,Adresse) VALUES (@RégionID,@CommuneID,@ArrondisID,@Nom1,@Nom2,@Nom3,@NomMère1,@NomMère2,@NomMère3,@DOB,@POB,@CodeElecteur,@AncienCNI,@DateAncienCNI,@NewCNI,@DateNewCNI,@AgentSaisie,@AgentCréation,@AgentModif,@Téléphone,@Sexe,@Région,@Commune,@Arrondissement,@DateCréation,@DateModification,@Profession,@Portable,@Complement,@AgentID,@AdresseID,@Adresse)";
                                            SqlConnection dbConn;
                                            dbConn = new SqlConnection(sCon);
                                            dbConn.Open();
                                            Login lg = new Login();
                                            //string str;
                                            string dat = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                            DateTime date4 = DateTime.Parse(dat);

                                            GetValue value = new GetValue();
                                            //getAdress add = new getAdress();
                                            SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                                            // Create parameters for the SqlCommand object
                                            // initialize with input-form field values
                                            //myCommand.Parameters.AddWithValue("@ElecteurID","");
                                            myCommand.Parameters.AddWithValue("@Nom1", textBoxNom1.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@Nom2", textBoxNom2.Text.ToUpper());

                                            myCommand.Parameters.AddWithValue("@Nom3", textBoxNom3.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@NomMère1", textBoxNomMère1.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@NomMère2", textBoxNomMère2.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@NomMère3", textBoxNomMère3.Text.ToUpper());
                                            if (dateTimePickerDOB.Checked)
                                            {
                                                myCommand.Parameters.AddWithValue("@DOB", Date3);
                                            }
                                            else
                                            {
                                                myCommand.Parameters.AddWithValue("@DOB", DBNull.Value);
                                            }
                                            myCommand.Parameters.AddWithValue("@POB", textBoxPOB.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@CodeElecteur", textBoxCodeElecteur.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@AncienCNI", textBoxAncCNI.Text.ToUpper());
                                            if (dateTimePicker1.Checked)
                                            {
                                                myCommand.Parameters.AddWithValue("@DateAncienCNI", Date1);
                                            }
                                            else
                                            {
                                                myCommand.Parameters.AddWithValue("@DateAncienCNI", DBNull.Value);
                                            }
                                            myCommand.Parameters.AddWithValue("@NewCNI", textBoxNewCNI.Text.ToUpper());
                                            if (dateTimePicker2.Checked)
                                            {
                                                myCommand.Parameters.AddWithValue("@DateNewCNI", Date2);
                                            }
                                            else
                                            {
                                                myCommand.Parameters.AddWithValue("@DateNewCNI", DBNull.Value);
                                            }

                                            myCommand.Parameters.AddWithValue("@AgentSaisie", value.getAgentSaisie());//recupere agent saisie
                                            myCommand.Parameters.AddWithValue("@AgentModif", value.getAgentModif());//recupere agent modif
                                            myCommand.Parameters.AddWithValue("@AgentCréation", value.getAgentCréation());//recupere agent de creation

                                            myCommand.Parameters.AddWithValue("@Téléphone", textBoxPhone.Text.ToUpper());

                                            myCommand.Parameters.AddWithValue("@AgentID", value.getAgentID());//recupere l'ID agent
                                            myCommand.Parameters.AddWithValue("@Sexe", comboBoxSexe.Text);

                                            myCommand.Parameters.AddWithValue("@Région", comboBoxRégion.Text);
                                            myCommand.Parameters.AddWithValue("@RégionID", Int32.Parse(comboBoxRégion.SelectedValue.ToString()));


                                            myCommand.Parameters.AddWithValue("@Commune", comboBoxCommune.Text);
                                            myCommand.Parameters.AddWithValue("@CommuneID", Int32.Parse(comboBoxCommune.SelectedValue.ToString()));


                                            myCommand.Parameters.AddWithValue("@Arrondissement", comboBoxArrondis.Text);
                                            myCommand.Parameters.AddWithValue("@ArrondisID", Int32.Parse(comboBoxArrondis.SelectedValue.ToString()));

                                            myCommand.Parameters.AddWithValue("@Adresse", comboBoxAdresse.Text);
                                            myCommand.Parameters.AddWithValue("@AdresseID", Int32.Parse(comboBoxAdresse.SelectedValue.ToString()));

                                            myCommand.Parameters.AddWithValue("@DateCréation", dat);
                                            myCommand.Parameters.AddWithValue("@Profession", textBoxProf.Text);
                                            myCommand.Parameters.AddWithValue("@Portable", textBoxPortable.Text);
                                            myCommand.Parameters.AddWithValue("@DateModification", dat);

                                            myCommand.Parameters.AddWithValue("@Complement", textBoxAdresseCompl.Text);



                                            myCommand.ExecuteNonQuery();
                                            MessageBox.Show("Ajout effectué avec succès");
                                            RecursiveClearTextBoxes(this.Controls);
                                            dateTimePicker1.Checked = false;
                                            dateTimePicker2.Checked = false;
                                            dateTimePickerDOB.Checked = false;
                                            this.Hide();
                                        }
                                        else
                                        {
                                            string sCon = conn.connectdb();
                                            string insertCmd = "INSERT INTO ElecteurOrdonance (localitéID,RégionID,Nom1,Nom2,Nom3,NomMère1,NomMère2,NomMère3,DOB,POB,CodeElecteur,AncienCNI,DateAncienCNI,NewCNI,DateNewCNI,AgentSaisie,AgentCréation,AgentModif,Téléphone,Sexe,Région,DateCréation,DateModification,Localité,Profession,Portable,Complement,AgentID) VALUES (@localitéID,@RégionID,@Nom1,@Nom2,@Nom3,@NomMère1,@NomMère2,@NomMère3,@DOB,@POB,@CodeElecteur,@AncienCNI,@DateAncienCNI,@NewCNI,@DateNewCNI,@AgentSaisie,@AgentCréation,@AgentModif,@Téléphone,@Sexe,@Région,@DateCréation,@DateModification,@Localité,@Profession,@Portable,@Complement,@AgentID)";
                                            SqlConnection dbConn;
                                            dbConn = new SqlConnection(sCon);
                                            dbConn.Open();
                                            Login lg = new Login();
                                            //string str;
                                            string dat = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                            DateTime date4 = DateTime.Parse(dat);

                                            GetValue value = new GetValue();
                                            //getAdress add = new getAdress();
                                            SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                                            // Create parameters for the SqlCommand object
                                            // initialize with input-form field values
                                            //myCommand.Parameters.AddWithValue("@ElecteurID","");
                                            myCommand.Parameters.AddWithValue("@Nom1", textBoxNom1.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@Nom2", textBoxNom2.Text.ToUpper());

                                            myCommand.Parameters.AddWithValue("@Nom3", textBoxNom3.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@NomMère1", textBoxNomMère1.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@NomMère2", textBoxNomMère2.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@NomMère3", textBoxNomMère3.Text.ToUpper());
                                            if (dateTimePickerDOB.Checked)
                                            {
                                                myCommand.Parameters.AddWithValue("@DOB", Date3);
                                            }
                                            else
                                            {
                                                myCommand.Parameters.AddWithValue("@DOB", DBNull.Value);
                                            }
                                            myCommand.Parameters.AddWithValue("@POB", textBoxPOB.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@CodeElecteur", textBoxCodeElecteur.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@AncienCNI", textBoxAncCNI.Text.ToUpper());
                                            if (dateTimePicker1.Checked)
                                            {
                                                myCommand.Parameters.AddWithValue("@DateAncienCNI", Date1);
                                            }
                                            else
                                            {
                                                myCommand.Parameters.AddWithValue("@DateAncienCNI", DBNull.Value);
                                            }
                                            myCommand.Parameters.AddWithValue("@NewCNI", textBoxNewCNI.Text.ToUpper());
                                            if (dateTimePicker2.Checked)
                                            {
                                                myCommand.Parameters.AddWithValue("@DateNewCNI", Date2);
                                            }
                                            else
                                            {
                                                myCommand.Parameters.AddWithValue("@DateNewCNI", DBNull.Value);
                                            }

                                            myCommand.Parameters.AddWithValue("@AgentSaisie", value.getAgentSaisie());//recupere agent saisie
                                            myCommand.Parameters.AddWithValue("@AgentModif", value.getAgentModif());//recupere agent modif
                                            myCommand.Parameters.AddWithValue("@AgentCréation", value.getAgentCréation());//recupere agent de creation

                                            myCommand.Parameters.AddWithValue("@Téléphone", textBoxPhone.Text.ToUpper());

                                            myCommand.Parameters.AddWithValue("@AgentID", value.getAgentID());//recupere l'ID agent
                                            myCommand.Parameters.AddWithValue("@Sexe", comboBoxSexe.Text);

                                            myCommand.Parameters.AddWithValue("@Région", comboBoxRégion.Text);
                                            myCommand.Parameters.AddWithValue("@RégionID", Int32.Parse(comboBoxRégion.SelectedValue.ToString()));


                                            myCommand.Parameters.AddWithValue("@DateCréation", dat);
                                            myCommand.Parameters.AddWithValue("@Profession", textBoxProf.Text);
                                            myCommand.Parameters.AddWithValue("@Portable", textBoxPortable.Text);
                                            myCommand.Parameters.AddWithValue("@DateModification", dat);

                                            myCommand.Parameters.AddWithValue("@Localité", comboBoxLoc.Text);
                                            myCommand.Parameters.AddWithValue("@localitéID", Int32.Parse(comboBoxLoc.SelectedValue.ToString()));

                                            myCommand.Parameters.AddWithValue("@Complement", textBoxAdresseCompl.Text);



                                            myCommand.ExecuteNonQuery();
                                            MessageBox.Show("Ajout effectué avec succès");
                                            RecursiveClearTextBoxes(this.Controls);
                                            dateTimePicker1.Checked = false;
                                            dateTimePicker2.Checked = false;
                                            dateTimePickerDOB.Checked = false;
                                            this.Hide();
                                        }

                                    }
                                    catch (SqlException ex)
                                    {
                                        MessageBox.Show("erreur  " + ex.Message);
                                    }
                                }


                            }
                            else
                            {
                                try
                                {
                                    if (Int32.Parse(comboBoxRégion.SelectedValue.ToString()) == 1)
                                    {

                                        string sCon = conn.connectdb();
                                        string insertCmd = "INSERT INTO ElecteurOrdonance (RégionID,CommuneID,ArrondisID,Nom1,Nom2,Nom3,NomMère1,NomMère2,NomMère3,DOB,POB,CodeElecteur,AncienCNI,DateAncienCNI,NewCNI,DateNewCNI,AgentSaisie,AgentCréation,AgentModif,Téléphone,Sexe,Région,Commune,Arrondissement,DateCréation,DateModification,Profession,Portable,Complement,AgentID,AdresseID,Adresse) VALUES (@RégionID,@CommuneID,@ArrondisID,@Nom1,@Nom2,@Nom3,@NomMère1,@NomMère2,@NomMère3,@DOB,@POB,@CodeElecteur,@AncienCNI,@DateAncienCNI,@NewCNI,@DateNewCNI,@AgentSaisie,@AgentCréation,@AgentModif,@Téléphone,@Sexe,@Région,@Commune,@Arrondissement,@DateCréation,@DateModification,@Profession,@Portable,@Complement,@AgentID,@AdresseID,@Adresse)";
                                        SqlConnection dbConn;
                                        dbConn = new SqlConnection(sCon);
                                        dbConn.Open();
                                        Login lg = new Login();
                                        //string str;
                                        string dat = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                        DateTime date4 = DateTime.Parse(dat);

                                        GetValue value = new GetValue();
                                        //getAdress add = new getAdress();
                                        SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                                        // Create parameters for the SqlCommand object
                                        // initialize with input-form field values
                                        //myCommand.Parameters.AddWithValue("@ElecteurID","");
                                        myCommand.Parameters.AddWithValue("@Nom1", textBoxNom1.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@Nom2", textBoxNom2.Text.ToUpper());

                                        myCommand.Parameters.AddWithValue("@Nom3", textBoxNom3.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@NomMère1", textBoxNomMère1.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@NomMère2", textBoxNomMère2.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@NomMère3", textBoxNomMère3.Text.ToUpper());
                                        if (dateTimePickerDOB.Checked)
                                        {
                                            myCommand.Parameters.AddWithValue("@DOB", Date3);
                                        }
                                        else
                                        {
                                            myCommand.Parameters.AddWithValue("@DOB", DBNull.Value);
                                        }
                                        myCommand.Parameters.AddWithValue("@POB", textBoxPOB.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@CodeElecteur", textBoxCodeElecteur.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@AncienCNI", textBoxAncCNI.Text.ToUpper());
                                        if (dateTimePicker1.Checked)
                                        {
                                            myCommand.Parameters.AddWithValue("@DateAncienCNI", Date1);
                                        }
                                        else
                                        {
                                            myCommand.Parameters.AddWithValue("@DateAncienCNI", DBNull.Value);
                                        }
                                        myCommand.Parameters.AddWithValue("@NewCNI", textBoxNewCNI.Text.ToUpper());
                                        if (dateTimePicker2.Checked)
                                        {
                                            myCommand.Parameters.AddWithValue("@DateNewCNI", Date2);
                                        }
                                        else
                                        {
                                            myCommand.Parameters.AddWithValue("@DateNewCNI", DBNull.Value);
                                        }

                                        myCommand.Parameters.AddWithValue("@AgentSaisie", value.getAgentSaisie());//recupere agent saisie
                                        myCommand.Parameters.AddWithValue("@AgentModif", value.getAgentModif());//recupere agent modif
                                        myCommand.Parameters.AddWithValue("@AgentCréation", value.getAgentCréation());//recupere agent de creation

                                        myCommand.Parameters.AddWithValue("@Téléphone", textBoxPhone.Text.ToUpper());

                                        myCommand.Parameters.AddWithValue("@AgentID", value.getAgentID());//recupere l'ID agent
                                        myCommand.Parameters.AddWithValue("@Sexe", comboBoxSexe.Text);

                                        myCommand.Parameters.AddWithValue("@Région", comboBoxRégion.Text);
                                        myCommand.Parameters.AddWithValue("@RégionID", Int32.Parse(comboBoxRégion.SelectedValue.ToString()));


                                        myCommand.Parameters.AddWithValue("@Commune", comboBoxCommune.Text);
                                        myCommand.Parameters.AddWithValue("@CommuneID", Int32.Parse(comboBoxCommune.SelectedValue.ToString()));


                                        myCommand.Parameters.AddWithValue("@Arrondissement", comboBoxArrondis.Text);
                                        myCommand.Parameters.AddWithValue("@ArrondisID", Int32.Parse(comboBoxArrondis.SelectedValue.ToString()));

                                        myCommand.Parameters.AddWithValue("@Adresse", comboBoxAdresse.Text);
                                        myCommand.Parameters.AddWithValue("@AdresseID", Int32.Parse(comboBoxAdresse.SelectedValue.ToString()));

                                        myCommand.Parameters.AddWithValue("@DateCréation", dat);
                                        myCommand.Parameters.AddWithValue("@Profession", textBoxProf.Text);
                                        myCommand.Parameters.AddWithValue("@Portable", textBoxPortable.Text);
                                        myCommand.Parameters.AddWithValue("@DateModification", dat);

                                        myCommand.Parameters.AddWithValue("@Complement", textBoxAdresseCompl.Text);



                                        myCommand.ExecuteNonQuery();
                                        MessageBox.Show("Ajout effectué avec succès");
                                        RecursiveClearTextBoxes(this.Controls);
                                        dateTimePicker1.Checked = false;
                                        dateTimePicker2.Checked = false;
                                        dateTimePickerDOB.Checked = false;
                                        this.Hide();
                                    }
                                    else
                                    {
                                        string sCon = conn.connectdb();
                                        string insertCmd = "INSERT INTO ElecteurOrdonance (localitéID,RégionID,Nom1,Nom2,Nom3,NomMère1,NomMère2,NomMère3,DOB,POB,CodeElecteur,AncienCNI,DateAncienCNI,NewCNI,DateNewCNI,AgentSaisie,AgentCréation,AgentModif,Téléphone,Sexe,Région,DateCréation,DateModification,Localité,Profession,Portable,Complement,AgentID) VALUES (@localitéID,@RégionID,@Nom1,@Nom2,@Nom3,@NomMère1,@NomMère2,@NomMère3,@DOB,@POB,@CodeElecteur,@AncienCNI,@DateAncienCNI,@NewCNI,@DateNewCNI,@AgentSaisie,@AgentCréation,@AgentModif,@Téléphone,@Sexe,@Région,@DateCréation,@DateModification,@Localité,@Profession,@Portable,@Complement,@AgentID)";
                                        SqlConnection dbConn;
                                        dbConn = new SqlConnection(sCon);
                                        dbConn.Open();
                                        Login lg = new Login();
                                        //string str;
                                        string dat = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                        DateTime date4 = DateTime.Parse(dat);

                                        GetValue value = new GetValue();
                                        //getAdress add = new getAdress();
                                        SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                                        // Create parameters for the SqlCommand object
                                        // initialize with input-form field values
                                        //myCommand.Parameters.AddWithValue("@ElecteurID","");
                                        myCommand.Parameters.AddWithValue("@Nom1", textBoxNom1.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@Nom2", textBoxNom2.Text.ToUpper());

                                        myCommand.Parameters.AddWithValue("@Nom3", textBoxNom3.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@NomMère1", textBoxNomMère1.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@NomMère2", textBoxNomMère2.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@NomMère3", textBoxNomMère3.Text.ToUpper());
                                        if (dateTimePickerDOB.Checked)
                                        {
                                            myCommand.Parameters.AddWithValue("@DOB", Date3);
                                        }
                                        else
                                        {
                                            myCommand.Parameters.AddWithValue("@DOB", DBNull.Value);
                                        }
                                        myCommand.Parameters.AddWithValue("@POB", textBoxPOB.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@CodeElecteur", textBoxCodeElecteur.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@AncienCNI", textBoxAncCNI.Text.ToUpper());
                                        if (dateTimePicker1.Checked)
                                        {
                                            myCommand.Parameters.AddWithValue("@DateAncienCNI", Date1);
                                        }
                                        else
                                        {
                                            myCommand.Parameters.AddWithValue("@DateAncienCNI", DBNull.Value);
                                        }
                                        myCommand.Parameters.AddWithValue("@NewCNI", textBoxNewCNI.Text.ToUpper());
                                        if (dateTimePicker2.Checked)
                                        {
                                            myCommand.Parameters.AddWithValue("@DateNewCNI", Date2);
                                        }
                                        else
                                        {
                                            myCommand.Parameters.AddWithValue("@DateNewCNI", DBNull.Value);
                                        }

                                        myCommand.Parameters.AddWithValue("@AgentSaisie", value.getAgentSaisie());//recupere agent saisie
                                        myCommand.Parameters.AddWithValue("@AgentModif", value.getAgentModif());//recupere agent modif
                                        myCommand.Parameters.AddWithValue("@AgentCréation", value.getAgentCréation());//recupere agent de creation

                                        myCommand.Parameters.AddWithValue("@Téléphone", textBoxPhone.Text.ToUpper());

                                        myCommand.Parameters.AddWithValue("@AgentID", value.getAgentID());//recupere l'ID agent
                                        myCommand.Parameters.AddWithValue("@Sexe", comboBoxSexe.Text);

                                        myCommand.Parameters.AddWithValue("@Région", comboBoxRégion.Text);
                                        myCommand.Parameters.AddWithValue("@RégionID", Int32.Parse(comboBoxRégion.SelectedValue.ToString()));


                                        myCommand.Parameters.AddWithValue("@DateCréation", dat);
                                        myCommand.Parameters.AddWithValue("@Profession", textBoxProf.Text);
                                        myCommand.Parameters.AddWithValue("@Portable", textBoxPortable.Text);
                                        myCommand.Parameters.AddWithValue("@DateModification", dat);

                                        myCommand.Parameters.AddWithValue("@Localité", comboBoxLoc.Text);
                                        myCommand.Parameters.AddWithValue("@localitéID", Int32.Parse(comboBoxLoc.SelectedValue.ToString()));

                                        myCommand.Parameters.AddWithValue("@Complement", textBoxAdresseCompl.Text);



                                        myCommand.ExecuteNonQuery();
                                        MessageBox.Show("Ajout effectué avec succès");
                                        RecursiveClearTextBoxes(this.Controls);
                                        dateTimePicker1.Checked = false;
                                        dateTimePicker2.Checked = false;
                                        dateTimePickerDOB.Checked = false;
                                        this.Hide();
                                    }
                                }
                                catch (SqlException ex)
                                {
                                    MessageBox.Show("erreur  " + ex.Message);
                                }
                            }
                        }
                        else
                        {
                            MessageBox.Show("Veuillez saisir la date de la nouvelle CNI ou la date de l'ancienne CNI");
                        }
                    }
                }
            }
        }

        private void comboBoxArrondis_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxArrondis.SelectedValue.ToString() != "")
            {
                int ArrondisID = Convert.ToInt32(comboBoxArrondis.SelectedValue.ToString());
                this.FillAdresse(ArrondisID);
                //comboBoxAdresse.SelectedIndex = 0;
            }
        }
        private void FillAdresse(int ArrondisID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT AdresseID, NomAdresse FROM Adresse WHERE ArrondisID =@ArrondisID";
            cmd.Parameters.AddWithValue("@ArrondisID", ArrondisID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxAdresse.ValueMember = "AdresseID";
                comboBoxAdresse.DisplayMember = "NomAdresse";
                comboBoxAdresse.DataSource = objDs.Tables[0];
            }
        }

    }
}
