﻿namespace Electeur
{
    partial class Validation_Resultat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxBV = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.comboBoxCV = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.Label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbRegion = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBoxElect = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.Liste1 = new System.Windows.Forms.Label();
            this.txtListe1 = new System.Windows.Forms.TextBox();
            this.Liste2 = new System.Windows.Forms.Label();
            this.txtListe2 = new System.Windows.Forms.TextBox();
            this.Liste3 = new System.Windows.Forms.Label();
            this.txtListe3 = new System.Windows.Forms.TextBox();
            this.Liste4 = new System.Windows.Forms.Label();
            this.txtListe4 = new System.Windows.Forms.TextBox();
            this.Liste5 = new System.Windows.Forms.Label();
            this.txtListe5 = new System.Windows.Forms.TextBox();
            this.Liste6 = new System.Windows.Forms.Label();
            this.txtListe6 = new System.Windows.Forms.TextBox();
            this.lblSuffrage = new System.Windows.Forms.Label();
            this.txtNul = new System.Windows.Forms.TextBox();
            this.txtVotant = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblTTinscrit = new System.Windows.Forms.Label();
            this.lblInscrit = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtOROM = new System.Windows.Forms.TextBox();
            this.groupBox2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // comboBoxBV
            // 
            this.comboBoxBV.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxBV.FormattingEnabled = true;
            this.comboBoxBV.Location = new System.Drawing.Point(714, 112);
            this.comboBoxBV.Name = "comboBoxBV";
            this.comboBoxBV.Size = new System.Drawing.Size(184, 21);
            this.comboBoxBV.TabIndex = 118;
            this.comboBoxBV.SelectedIndexChanged += new System.EventHandler(this.comboBoxBV_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold);
            this.label9.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label9.Location = new System.Drawing.Point(584, 116);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(113, 16);
            this.label9.TabIndex = 117;
            this.label9.Text = "Bureau de vote";
            // 
            // comboBoxCV
            // 
            this.comboBoxCV.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCV.FormattingEnabled = true;
            this.comboBoxCV.Location = new System.Drawing.Point(390, 112);
            this.comboBoxCV.Name = "comboBoxCV";
            this.comboBoxCV.Size = new System.Drawing.Size(188, 21);
            this.comboBoxCV.TabIndex = 116;
            this.comboBoxCV.SelectedIndexChanged += new System.EventHandler(this.comboBoxCV_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(273, 116);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 16);
            this.label3.TabIndex = 115;
            this.label3.Text = "Centre de Vote";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.btnClear);
            this.groupBox2.Controls.Add(this.btnSave);
            this.groupBox2.Location = new System.Drawing.Point(654, 486);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(244, 119);
            this.groupBox2.TabIndex = 114;
            this.groupBox2.TabStop = false;
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.Font = new System.Drawing.Font("Book Antiqua", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(38, 115);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(196, 35);
            this.button1.TabIndex = 11;
            this.button1.Text = "REJETER";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnClear
            // 
            this.btnClear.Font = new System.Drawing.Font("Book Antiqua", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.Location = new System.Drawing.Point(38, 71);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(196, 32);
            this.btnClear.TabIndex = 10;
            this.btnClear.Text = "EFFACER";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Book Antiqua", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(38, 23);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(196, 35);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "VALIDER";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Label1.Font = new System.Drawing.Font("Book Antiqua", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(64, 21);
            this.Label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(741, 28);
            this.Label1.TabIndex = 95;
            this.Label1.Text = "Validation des resultats de Scrutin des elections communales 2017";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(16, 116);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 16);
            this.label2.TabIndex = 96;
            this.label2.Text = "Region :";
            // 
            // cbRegion
            // 
            this.cbRegion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbRegion.Font = new System.Drawing.Font("Book Antiqua", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbRegion.FormattingEnabled = true;
            this.cbRegion.Location = new System.Drawing.Point(88, 110);
            this.cbRegion.Name = "cbRegion";
            this.cbRegion.Size = new System.Drawing.Size(155, 25);
            this.cbRegion.TabIndex = 97;
            this.cbRegion.SelectedIndexChanged += new System.EventHandler(this.cbRegion_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Location = new System.Drawing.Point(15, 68);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 19);
            this.label4.TabIndex = 127;
            this.label4.Text = "Election";
            // 
            // comboBoxElect
            // 
            this.comboBoxElect.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxElect.FormattingEnabled = true;
            this.comboBoxElect.Location = new System.Drawing.Point(88, 68);
            this.comboBoxElect.Name = "comboBoxElect";
            this.comboBoxElect.Size = new System.Drawing.Size(331, 21);
            this.comboBoxElect.TabIndex = 128;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Location = new System.Drawing.Point(12, 52);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(919, 110);
            this.panel1.TabIndex = 129;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.Liste1);
            this.panel2.Controls.Add(this.txtListe1);
            this.panel2.Controls.Add(this.Liste2);
            this.panel2.Controls.Add(this.txtListe2);
            this.panel2.Controls.Add(this.Liste3);
            this.panel2.Controls.Add(this.txtListe3);
            this.panel2.Controls.Add(this.Liste4);
            this.panel2.Controls.Add(this.txtListe4);
            this.panel2.Controls.Add(this.Liste5);
            this.panel2.Controls.Add(this.txtListe5);
            this.panel2.Controls.Add(this.Liste6);
            this.panel2.Controls.Add(this.txtListe6);
            this.panel2.Controls.Add(this.lblSuffrage);
            this.panel2.Controls.Add(this.txtNul);
            this.panel2.Controls.Add(this.txtVotant);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.lblTTinscrit);
            this.panel2.Controls.Add(this.lblInscrit);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.txtOROM);
            this.panel2.Location = new System.Drawing.Point(12, 168);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(583, 437);
            this.panel2.TabIndex = 130;
            // 
            // Liste1
            // 
            this.Liste1.AutoSize = true;
            this.Liste1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold);
            this.Liste1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Liste1.Location = new System.Drawing.Point(75, 248);
            this.Liste1.Name = "Liste1";
            this.Liste1.Size = new System.Drawing.Size(80, 16);
            this.Liste1.TabIndex = 133;
            this.Liste1.Text = "candidat 1";
            this.Liste1.Visible = false;
            // 
            // txtListe1
            // 
            this.txtListe1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtListe1.Location = new System.Drawing.Point(395, 245);
            this.txtListe1.Name = "txtListe1";
            this.txtListe1.Size = new System.Drawing.Size(145, 22);
            this.txtListe1.TabIndex = 141;
            this.txtListe1.Text = "0";
            this.txtListe1.Visible = false;
            // 
            // Liste2
            // 
            this.Liste2.AutoSize = true;
            this.Liste2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold);
            this.Liste2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Liste2.Location = new System.Drawing.Point(75, 279);
            this.Liste2.Name = "Liste2";
            this.Liste2.Size = new System.Drawing.Size(80, 16);
            this.Liste2.TabIndex = 134;
            this.Liste2.Text = "candidat 2";
            this.Liste2.Visible = false;
            // 
            // txtListe2
            // 
            this.txtListe2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtListe2.Location = new System.Drawing.Point(395, 276);
            this.txtListe2.Name = "txtListe2";
            this.txtListe2.Size = new System.Drawing.Size(145, 22);
            this.txtListe2.TabIndex = 142;
            this.txtListe2.Text = "0";
            this.txtListe2.Visible = false;
            // 
            // Liste3
            // 
            this.Liste3.AutoSize = true;
            this.Liste3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold);
            this.Liste3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Liste3.Location = new System.Drawing.Point(75, 310);
            this.Liste3.Name = "Liste3";
            this.Liste3.Size = new System.Drawing.Size(80, 16);
            this.Liste3.TabIndex = 143;
            this.Liste3.Text = "candidat 3";
            this.Liste3.Visible = false;
            // 
            // txtListe3
            // 
            this.txtListe3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtListe3.Location = new System.Drawing.Point(395, 307);
            this.txtListe3.Name = "txtListe3";
            this.txtListe3.Size = new System.Drawing.Size(145, 22);
            this.txtListe3.TabIndex = 144;
            this.txtListe3.Text = "0";
            this.txtListe3.Visible = false;
            // 
            // Liste4
            // 
            this.Liste4.AutoSize = true;
            this.Liste4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold);
            this.Liste4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Liste4.Location = new System.Drawing.Point(75, 341);
            this.Liste4.Name = "Liste4";
            this.Liste4.Size = new System.Drawing.Size(80, 16);
            this.Liste4.TabIndex = 145;
            this.Liste4.Text = "candidat 4";
            this.Liste4.Visible = false;
            // 
            // txtListe4
            // 
            this.txtListe4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtListe4.Location = new System.Drawing.Point(395, 338);
            this.txtListe4.Name = "txtListe4";
            this.txtListe4.Size = new System.Drawing.Size(145, 22);
            this.txtListe4.TabIndex = 146;
            this.txtListe4.Text = "0";
            this.txtListe4.Visible = false;
            // 
            // Liste5
            // 
            this.Liste5.AutoSize = true;
            this.Liste5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold);
            this.Liste5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Liste5.Location = new System.Drawing.Point(75, 372);
            this.Liste5.Name = "Liste5";
            this.Liste5.Size = new System.Drawing.Size(80, 16);
            this.Liste5.TabIndex = 147;
            this.Liste5.Text = "candidat 5";
            this.Liste5.Visible = false;
            // 
            // txtListe5
            // 
            this.txtListe5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtListe5.Location = new System.Drawing.Point(395, 369);
            this.txtListe5.Name = "txtListe5";
            this.txtListe5.Size = new System.Drawing.Size(145, 22);
            this.txtListe5.TabIndex = 148;
            this.txtListe5.Text = "0";
            this.txtListe5.Visible = false;
            // 
            // Liste6
            // 
            this.Liste6.AutoSize = true;
            this.Liste6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold);
            this.Liste6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Liste6.Location = new System.Drawing.Point(75, 403);
            this.Liste6.Name = "Liste6";
            this.Liste6.Size = new System.Drawing.Size(80, 16);
            this.Liste6.TabIndex = 149;
            this.Liste6.Text = "candidat 6";
            this.Liste6.Visible = false;
            // 
            // txtListe6
            // 
            this.txtListe6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtListe6.Location = new System.Drawing.Point(395, 400);
            this.txtListe6.Name = "txtListe6";
            this.txtListe6.Size = new System.Drawing.Size(145, 22);
            this.txtListe6.TabIndex = 150;
            this.txtListe6.Text = "0";
            this.txtListe6.Visible = false;
            // 
            // lblSuffrage
            // 
            this.lblSuffrage.AutoSize = true;
            this.lblSuffrage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSuffrage.Location = new System.Drawing.Point(392, 217);
            this.lblSuffrage.Name = "lblSuffrage";
            this.lblSuffrage.Size = new System.Drawing.Size(16, 16);
            this.lblSuffrage.TabIndex = 140;
            this.lblSuffrage.Text = "0";
            // 
            // txtNul
            // 
            this.txtNul.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNul.Location = new System.Drawing.Point(395, 179);
            this.txtNul.Name = "txtNul";
            this.txtNul.Size = new System.Drawing.Size(145, 22);
            this.txtNul.TabIndex = 139;
            this.txtNul.Tag = "Bulletin Null";
            this.txtNul.Text = "0";
            this.txtNul.TextChanged += new System.EventHandler(this.txtNul_TextChanged);
            // 
            // txtVotant
            // 
            this.txtVotant.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVotant.Location = new System.Drawing.Point(395, 149);
            this.txtVotant.Name = "txtVotant";
            this.txtVotant.Size = new System.Drawing.Size(145, 22);
            this.txtVotant.TabIndex = 138;
            this.txtVotant.Tag = "Votant";
            this.txtVotant.Text = "0";
            this.txtVotant.TextChanged += new System.EventHandler(this.txtVotant_TextChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold);
            this.label13.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label13.Location = new System.Drawing.Point(75, 215);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(210, 16);
            this.label13.TabIndex = 132;
            this.label13.Text = "Suffrages exprimés (e = c-d) :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Location = new System.Drawing.Point(75, 54);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(164, 16);
            this.label5.TabIndex = 127;
            this.label5.Text = "Nombre d\'inscrits (a) : ";
            // 
            // lblTTinscrit
            // 
            this.lblTTinscrit.AutoSize = true;
            this.lblTTinscrit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTTinscrit.Location = new System.Drawing.Point(392, 123);
            this.lblTTinscrit.Name = "lblTTinscrit";
            this.lblTTinscrit.Size = new System.Drawing.Size(16, 16);
            this.lblTTinscrit.TabIndex = 137;
            this.lblTTinscrit.Text = "0";
            // 
            // lblInscrit
            // 
            this.lblInscrit.AutoSize = true;
            this.lblInscrit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInscrit.Location = new System.Drawing.Point(392, 56);
            this.lblInscrit.Name = "lblInscrit";
            this.lblInscrit.Size = new System.Drawing.Size(16, 16);
            this.lblInscrit.TabIndex = 135;
            this.lblInscrit.Text = "0";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold);
            this.label11.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label11.Location = new System.Drawing.Point(75, 181);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(210, 16);
            this.label11.TabIndex = 131;
            this.label11.Text = "Nombre de bulletins nuls (d) :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label7.Location = new System.Drawing.Point(75, 89);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(224, 16);
            this.label7.TabIndex = 128;
            this.label7.Text = "Nombre de votants OR/OM (b) :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label8.Location = new System.Drawing.Point(75, 121);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(145, 16);
            this.label8.TabIndex = 129;
            this.label8.Text = "Total inscrits (a+b) :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold);
            this.label10.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label10.Location = new System.Drawing.Point(75, 149);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(169, 16);
            this.label10.TabIndex = 130;
            this.label10.Text = "Nombre de votants (c) :";
            // 
            // txtOROM
            // 
            this.txtOROM.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOROM.Location = new System.Drawing.Point(395, 87);
            this.txtOROM.Name = "txtOROM";
            this.txtOROM.Size = new System.Drawing.Size(145, 22);
            this.txtOROM.TabIndex = 136;
            this.txtOROM.Tag = "Ordonnance";
            this.txtOROM.Text = "0";
            this.txtOROM.TextChanged += new System.EventHandler(this.txtOROM_TextChanged);
            // 
            // Validation_Resultat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSeaGreen;
            this.ClientSize = new System.Drawing.Size(943, 674);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comboBoxElect);
            this.Controls.Add(this.comboBoxBV);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.comboBoxCV);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbRegion);
            this.Controls.Add(this.panel1);
            this.Name = "Validation_Resultat";
            this.Text = "Validation_Resultat";
            this.Load += new System.EventHandler(this.Validation_Resultat_Load);
            this.groupBox2.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxBV;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboBoxCV;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.Label Label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbRegion;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBoxElect;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label Liste6;
        private System.Windows.Forms.TextBox txtListe6;
        private System.Windows.Forms.Label Liste5;
        private System.Windows.Forms.TextBox txtListe5;
        private System.Windows.Forms.Label Liste4;
        private System.Windows.Forms.TextBox txtListe4;
        private System.Windows.Forms.Label Liste3;
        private System.Windows.Forms.TextBox txtListe3;
        private System.Windows.Forms.Label lblSuffrage;
        private System.Windows.Forms.Label Liste2;
        private System.Windows.Forms.TextBox txtListe2;
        private System.Windows.Forms.TextBox txtListe1;
        private System.Windows.Forms.Label Liste1;
        private System.Windows.Forms.TextBox txtNul;
        private System.Windows.Forms.TextBox txtVotant;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblTTinscrit;
        private System.Windows.Forms.Label lblInscrit;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtOROM;
    }
}