﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

        }

        
        private void electeurToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Rechecher src = new Rechecher();
            src.MdiParent = this;
            src.Show();
            src.WindowState = FormWindowState.Maximized;
        }

        private void parListeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RechercheListe srcList = new RechercheListe();
            srcList.MdiParent = this;
            srcList.Show();
            srcList.WindowState = FormWindowState.Maximized;
        }

        private void créerUnAgentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CréationAgent crAgent = new CréationAgent();
            crAgent.MdiParent = this;
            crAgent.Show();
            crAgent.WindowState = FormWindowState.Maximized;
        }

        private void auditToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Audit audit = new Audit();
            audit.MdiParent = this;
            audit.Show();
            audit.WindowState = FormWindowState.Maximized;
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox1 about = new AboutBox1();
            about.MdiParent = this;
            about.Show();
            about.WindowState = FormWindowState.Maximized;
        }

        private void saisieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaisieElecteur saisieElecteur = new SaisieElecteur();
            saisieElecteur.MdiParent = this;
            saisieElecteur.Show();
            saisieElecteur.WindowState = FormWindowState.Maximized;
            //saisieElecteur.BringToFront();
        }

        private void ApprouverToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            Approuver app = new Approuver();
            app.MdiParent = this;
            app.Show();
            app.WindowState = FormWindowState.Maximized;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            foreach(Control ctr in this.Controls)
            {
                if (ctr is MdiClient)
                {
                    //pictureBox1.Visible = true;
                }
            }
            GetValue get = new GetValue();
            int Profil = get.getProfilID();
            
            if (Profil == 2 )
            {
                rechercheToolStripMenuItem.Visible = true;
                parListeToolStripMenuItem.Visible = true;
                
            }
            if (Profil == 4 )
            {
                saisiToolStripMenuItem.Visible = true;
                ApprouverToolStripMenuItem.Visible = true;
                radiationToolStripMenuItem.Visible = true;
            }
            if (Profil == 5 )
            {
                saisiToolStripMenuItem.Visible = true;
                validerToolStripMenuItem.Visible = true;
                radiationToolStripMenuItem.Visible = true;
            }
            if (Profil == 3 )
            {
                saisiToolStripMenuItem.Visible = false;
                electeurToolStripMenuItem.Visible = true;
                saisieToolStripMenuItem.Visible = true;
                rechercheToolStripMenuItem.Visible = true;
                radiationToolStripMenuItem.Visible = true;
                radierToolStripMenuItem.Visible = false;
                affectationToolStripMenuItem.Visible = true;
            }
            if (Profil == 12)
            {
                impressionToolStripMenuItem.Visible = false;
                listeDeRetraitToolStripMenuItem.Visible = false;
            }
            if (Profil == 13)
            {
                impressionToolStripMenuItem.Visible = false;
                //listeDeRetraitToolStripMenuItem.Visible = false;
            }
            if (Profil == 7)
            {
                affectationToolStripMenuItem.Visible = true;
            }
            if (Profil == 1)
            {
                radiationToolStripMenuItem.Visible = true;
            }
            if (Profil == 8)
            {
                rechercheToolStripMenuItem.Visible = true;
                parListeToolStripMenuItem.Enabled = false;
            }
            if (Profil == 9)
            {
                rechercheToolStripMenuItem.Visible = true;
                affectationToolStripMenuItem.Visible = true;
                consultationToolStripMenuItem1.Visible = true;
                electeurToolStripMenuItem.Visible = true;
            }
            if (Profil == 1)
            {
                administrationToolStripMenuItem.Visible = true;
                affectationToolStripMenuItem.Visible = true;
                affectationIndividuelleToolStripMenuItem.Visible = true;
                affectationParListeToolStripMenuItem.Visible = true;
                réaffectationToolStripMenuItem.Visible = true;
                réaffectationEnMasseToolStripMenuItem.Visible = true;
                rechercheToolStripMenuItem.Visible = true;
                electeurToolStripMenuItem.Visible = true;
                parListeToolStripMenuItem.Visible = true;
                ApprouverToolStripMenuItem.Visible = true;
                validerToolStripMenuItem.Visible = true;
                electeurToolStripMenuItem.Visible = true;
                scrutinToolStripMenuItem.Visible = true;
                lieuxDeVoteToolStripMenuItem.Visible = true;
                gestionDesToolStripMenuItem.Visible = true;
                saisiToolStripMenuItem.Visible = true;
                saisieToolStripMenuItem.Visible = true;
            }
            if (Profil == 10)
            {
                administrationToolStripMenuItem.Visible = true;
                affectationToolStripMenuItem.Visible = true;
                affectationIndividuelleToolStripMenuItem.Visible = false;
                affectationParListeToolStripMenuItem.Visible = false;
                réaffectationToolStripMenuItem.Visible = false;
                réaffectationEnMasseToolStripMenuItem.Visible = false;
                radiationToolStripMenuItem.Visible = true;
                radierToolStripMenuItem.Visible = false;
                rechercheToolStripMenuItem.Visible = true;
                electeurToolStripMenuItem.Visible = true;
                parListeToolStripMenuItem.Visible = true;
                ApprouverToolStripMenuItem.Visible = true;
                validerToolStripMenuItem.Visible = true;
                electeurToolStripMenuItem.Visible = true;
                consultationToolStripMenuItem1.Visible = true;
            }
            if (Profil == 15)
            {
                statistiqueToolStripMenuItem.Visible = false;
                validationRésultatToolStripMenuItem.Visible = false;
            }
           
        }
        private void Form1_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
               
                if (MessageBox.Show("Voulez vous vraiment fermer", "Electeur",
                   MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    // Cancel the Closing event from closing the form.
                    e.Cancel = false;
                    // Call method to save file...
                }
            
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void validerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Valider valid = new Valider();
            valid.MdiParent = this;
            valid.Show();
            valid.WindowState = FormWindowState.Maximized;
        }

        private void affectationParListeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Affectation affect = new Affectation();
            affect.MdiParent = this;
            affect.Show();
            affect.WindowState = FormWindowState.Maximized;
        }

        private void réaffectationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Réaffectation réaffect = new Réaffectation();
            réaffect.MdiParent = this;
            réaffect.Show();
            réaffect.WindowState = FormWindowState.Maximized;
        }

        private void réaffectationEnMasseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RéaffectationMasse réaffectMasse = new RéaffectationMasse();
            réaffectMasse.MdiParent = this;
            réaffectMasse.Show();
            réaffectMasse.WindowState = FormWindowState.Maximized;
        }

        private void consultationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Consultation cons = new Consultation();
            cons.MdiParent = this;
            cons.Show();
            cons.WindowState = FormWindowState.Maximized;
        }

        private void paramétrageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Paramétrage param = new Paramétrage();
            param.MdiParent = this;
            param.Show();
            param.WindowState = FormWindowState.Maximized;
        }

        private void radiationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void radierToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Radiation rad = new Radiation();
            rad.MdiParent = this;
            rad.Show();
            rad.WindowState = FormWindowState.Maximized;
        }

        private void listesDesRadiésToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listeRadiés lsRad = new listeRadiés();
            lsRad.MdiParent = this;
            lsRad.Show();
            lsRad.WindowState = FormWindowState.Maximized;
        }

        private void historiqueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            historicPerAgent lsRad = new historicPerAgent();
            lsRad.MdiParent = this;
            lsRad.Show();
            lsRad.WindowState = FormWindowState.Maximized;
        }

        private void consultationToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void suspendreUnAgentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 fr2 = new Form2();
            fr2.MdiParent = this;
            fr2.Show();
            //fr2.WindowState = FormWindowState.Maximized;
        }

        private void importNouveauCNIToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Import import = new Import();
            import.MdiParent = this;
            import.Show();
        }

        private void nouveauCINToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewCIN NCIN = new NewCIN();
            NCIN.MdiParent = this;
            NCIN.Show();
            NCIN.WindowState = FormWindowState.Maximized;
            //pictureBox1.Visible = false;
        }

        private void changementMotDePasseToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
            //changeMP.WindowState = FormWindowState.Maximized;
        }

        private void réactivationDunAgentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            réactivation fr3 = new réactivation();
            fr3.MdiParent = this;
            fr3.Show();
        }

        private void changerMonMotDePasseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            changementMP changeMP = new changementMP();
            changeMP.MdiParent = this;
            changeMP.Show();
        }

        private void réinitialiserLeMotDePasseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Réinitialiser rein = new Réinitialiser();
            rein.MdiParent = this;
            rein.Show();
        }

        private void rechercheAncienneCINToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AncienCNI NCIN = new AncienCNI();
            NCIN.MdiParent = this;
            NCIN.Show();
            NCIN.WindowState = FormWindowState.Maximized;
        }

        private void quitterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Voulez vous vraiment fermer", "Electeur",
                   MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this.Dispose();
                this.Close();
            }
        }

        private void affectationIndividuelleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AffectationIndividuelle AffInd = new AffectationIndividuelle();
            AffInd.MdiParent = this;
            AffInd.Show();
            AffInd.WindowState = FormWindowState.Maximized;
        }

        private void paramétrageCentreEtBureauToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ParamCentre cv = new ParamCentre();
            cv.MdiParent = this;
            cv.Show();
            cv.WindowState = FormWindowState.Maximized;
        }

        private void rechercheDesRadiésToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RechercheRad rechRad = new RechercheRad();
            rechRad.MdiParent = this;
            rechRad.Show();
            rechRad.WindowState = FormWindowState.Maximized;
        }

        private void impressionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void carteDélecteurToolStripMenuItem_Click(object sender, EventArgs e)
        {
            printcart pcard = new printcart();
            pcard.MdiParent = this;
            pcard.Show();
            pcard.WindowState = FormWindowState.Maximized;
        }

        private void listeDaffichageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Impression imp = new Impression();
            imp.MdiParent = this;
            imp.Show();
            imp.WindowState = FormWindowState.Maximized;
        }

        private void listeDemargementToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
        }

        private void emargementparrégionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Emargement_region em = new Emargement_region();
            em.MdiParent = this;
            em.Show();
            em.WindowState = FormWindowState.Maximized;
        }

        private void emargementparBureauToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Fiche_Emargement em = new Fiche_Emargement();
            em.MdiParent = this;
            em.Show();
            em.WindowState = FormWindowState.Maximized;
        }

        private void retraitCarteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            retirer rt = new retirer();
            rt.MdiParent = this;
            rt.Show();
            rt.WindowState = FormWindowState.Maximized;
        }

        private void listeDeRetraitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LitRetrait rt = new LitRetrait();
            rt.MdiParent = this;
            rt.Show();
            rt.WindowState = FormWindowState.Maximized;
        }

        private void radiéInterregionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SrcInterregion rt = new SrcInterregion();
            rt.MdiParent = this;
            rt.Show();
            rt.WindowState = FormWindowState.Maximized;
        }

        private void membreLieuToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void ajoutElectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AjoutElection ajelect = new AjoutElection();
            ajelect.MdiParent = this;
            ajelect.Show();
            ajelect.WindowState = FormWindowState.Maximized;
        }

        private void modificationElectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ModificationElection mdelect = new ModificationElection();
            mdelect.MdiParent = this;
            mdelect.Show();
            mdelect.WindowState = FormWindowState.Maximized;
        }

        private void ajoutTypeElectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Ajouttypeelection ajtelect = new Ajouttypeelection();
            ajtelect.MdiParent = this;
            ajtelect.Show();
            ajtelect.WindowState = FormWindowState.Maximized;
        
        }

        private void modificationTypeElectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ModificationTypeElection mdtelect = new ModificationTypeElection();
            mdtelect.MdiParent = this;
            mdtelect.Show();
            mdtelect.WindowState = FormWindowState.Maximized;
        }

        private void ajoutReunionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AjoutReunion ajre = new AjoutReunion();
            ajre.MdiParent = this;
            ajre.Show();
            ajre.WindowState = FormWindowState.Maximized;
        }

        private void modificationReunionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ModifReunion mdre = new ModifReunion();
            mdre.MdiParent = this;
            mdre.Show();
            mdre.WindowState = FormWindowState.Maximized;
        }

        private void ajoutTypeReunionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AjoutTypeReunion ajtre = new AjoutTypeReunion();
            ajtre.MdiParent = this;
            ajtre.Show();
            ajtre.WindowState = FormWindowState.Maximized;
        }

        private void modificationTypeReunionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Modifitypereunion mtre = new Modifitypereunion();
            mtre.MdiParent = this;
            mtre.Show();
            mtre.WindowState = FormWindowState.Maximized;
        }

        private void ajoutMembreLieuVoteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AjoutMembrelieuvote ajmlv = new AjoutMembrelieuvote();
            ajmlv.MdiParent = this;
            ajmlv.Show();
            ajmlv.WindowState = FormWindowState.Maximized;
        }

        private void modificationMembreLieuVoteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ModificationMembrelieudevote mmlv = new ModificationMembrelieudevote();
            mmlv.MdiParent = this;
            mmlv.Show();
            mmlv.WindowState = FormWindowState.Maximized;
        }

        private void ajoutPrefetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AjoutPrefet ajpf = new AjoutPrefet();
            ajpf.MdiParent = this;
            ajpf.Show();
            ajpf.WindowState = FormWindowState.Maximized;
        }

        private void modificationPrefetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ModificationPrefet mdpf = new ModificationPrefet();
            mdpf.MdiParent = this;
            mdpf.Show();
            mdpf.WindowState = FormWindowState.Maximized;
        }

        private void ajoutConvocationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AjoutConvocation ajcv = new AjoutConvocation();
            ajcv.MdiParent = this;
            ajcv.Show();
            ajcv.WindowState = FormWindowState.Maximized;
        }

        private void modificationConvocationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ModificationConvocation mdcv = new ModificationConvocation();
            mdcv.MdiParent = this;
            mdcv.Show();
            mdcv.WindowState = FormWindowState.Maximized;
        }

        private void ajoutFonctionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AjoutFonction ajft = new AjoutFonction();
            ajft.MdiParent = this;
            ajft.Show();
            ajft.WindowState = FormWindowState.Maximized;
        }

        private void modificationFonctionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ModificationFonction mdfc = new ModificationFonction();
            mdfc.MdiParent = this;
            mdfc.Show();
            mdfc.WindowState = FormWindowState.Maximized;
        }

        private void ajoutTourElectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AjoutTourElection ajte = new AjoutTourElection();
            ajte.MdiParent = this;
            ajte.Show();
            ajte.WindowState = FormWindowState.Maximized;
        }

        private void modificationTourElectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ModificationTourElection mdte = new ModificationTourElection();
            mdte.MdiParent = this;
            mdte.Show();
            mdte.WindowState = FormWindowState.Maximized;
        }

        private void editionDeConvocationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Edition_Convocation em = new Edition_Convocation();
            em.MdiParent = this;
            em.Show();
            em.WindowState = FormWindowState.Maximized;
        }

        private void retraitToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void paramétrageToolStripMenuItem1_Click(object sender, EventArgs e)
        {
        //    ParamScrutin pScrut = new ParamScrutin();
        //    pScrut.MdiParent = this;
        //    pScrut.Show();
        //    pScrut.WindowState = FormWindowState.Maximized;
        }

        private void résultatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaisieResultat sr = new SaisieResultat();
            sr.MdiParent = this;
            sr.Show();
            sr.WindowState = FormWindowState.Maximized;
        }

        private void mandaterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Mandaté mand = new Mandaté();
            mand.MdiParent = this;
            mand.Show();
            mand.WindowState = FormWindowState.Maximized;
        }

        private void statistiqueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            testreport rpt = new testreport();
            rpt.MdiParent = this;
            rpt.Show();
            rpt.WindowState = FormWindowState.Maximized;
        }

        private void saisieDeNombreDeVotantToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saisie_nombrevotant mdte = new saisie_nombrevotant();
            mdte.MdiParent = this;
            mdte.Show();
            mdte.WindowState = FormWindowState.Maximized;
        }

        private void modificationNombreDeVotantDansBureauDeVoteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            modification_saisinombre_votant mdte = new modification_saisinombre_votant();
            mdte.MdiParent = this;
            mdte.Show();
            mdte.WindowState = FormWindowState.Maximized;
        }

        private void saisieLheureDeSaisieDeNombreDeVotantToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saisie_horaire mdte = new saisie_horaire();

            mdte.Show();

        }

        private void affichageTauxDeParticipationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            chartenc mdte = new chartenc();
            mdte.MdiParent = this;
            mdte.Show();
            mdte.WindowState = FormWindowState.Maximized;
        }

        private void validationRésultatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Validation_Resultat mdte = new Validation_Resultat();
            mdte.MdiParent = this;
            mdte.Show();
            mdte.WindowState = FormWindowState.Maximized;
        }

        private void scrutinToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

      

        private void ajoutPartiePolitiqueToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Ajout_PartiPolitique mdte = new Ajout_PartiPolitique();
            mdte.MdiParent = this;
            mdte.Show();
            mdte.WindowState = FormWindowState.Maximized;
        }

        private void modificationPartiePolitiqueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Modification_PartiPolitique mdte = new Modification_PartiPolitique();
            mdte.MdiParent = this;
            mdte.Show();
            mdte.WindowState = FormWindowState.Maximized;
        }

        private void ajoutMembreDunePartiePolituqeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Ajout_MembrePartiPolitique mdte = new Ajout_MembrePartiPolitique();
            mdte.MdiParent = this;
            mdte.Show();
            mdte.WindowState = FormWindowState.Maximized;
        }

        private void modificationMembreDunePartiePolituqeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Modification_Membre_parti_politique mdte = new Modification_Membre_parti_politique();
            mdte.MdiParent = this;
            mdte.Show();
            mdte.WindowState = FormWindowState.Maximized;
        }
      
        
    }
}
