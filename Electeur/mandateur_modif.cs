﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class mandateur_modif : Form
    {
        Connection conn = new Connection();
        int mandatéID;
        public mandateur_modif(int ID)
        {
            InitializeComponent();
            mandatéID = ID;
           
        }

        private void modificationmandateur()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            string str = "SELECT * FROM mandaté where mandatéID= " + mandatéID;
            SqlCommand cmd = new SqlCommand(str, con);
            con.Open();
            //cmd.CommandType = CommandType.Text;
            GetValue get = new GetValue();

            using (SqlDataReader read = cmd.ExecuteReader())

                while (read.Read())
                {
                    textBoxNom1.Text = (read["Nom1"].ToString());
                    textBoxNom2.Text = (read["Nom2"].ToString());
                    textBoxNom3.Text = (read["Nom3"].ToString());
                    textBoxNomMère1.Text = (read["NomMère1"].ToString());
                    textBoxNomMère2.Text = (read["NomMère2"].ToString());
                    textBoxNomMère3.Text = (read["NomMère3"].ToString());
                    int colIndex3 = (read.GetOrdinal("DOB"));
                    if (!read.IsDBNull(colIndex3))
                    {
                        dateTimePickerDOB.Checked = true;
                        dateTimePickerDOB.Value = (read.GetDateTime(colIndex3));
                    }
                    //dateTimePickerDOB.TextDOB.Text = (read["DOB"].ToString());
                    textBoxPOB.Text = (read["POB"].ToString());
                    textBoxCodeElecteur.Text = (read["CodeElecteur"].ToString());
                    textBoxAncCNI.Text = (read["AncienCNI"].ToString());

                    int colIndex = (read.GetOrdinal("DateAncienCNI"));
                    if (!read.IsDBNull(colIndex))
                    {
                        dateTimePicker1.Checked = true;
                        dateTimePicker1.Value = (read.GetDateTime(colIndex));
                    }

                    int colIndex2 = (read.GetOrdinal("DateNewCNI"));
                    if (!read.IsDBNull(colIndex2))
                    {
                        dateTimePicker2.Checked = true;
                        dateTimePicker2.Value = (read.GetDateTime(colIndex2));
                    }

                    textBoxNewCNI.Text = (read["NewCNI"].ToString());
                    comboBoxSexe.SelectedItem = (read["Sexe"].ToString());
                   textBoxProf.Text= (read["Profession"].ToString());
                   textBoxPortable.Text= (read["Portable"].ToString());
                 textBoxPhone.Text= (read["Téléphone"].ToString());
                 textBoxAdresseCompl.Text= (read["Complement"].ToString());

                    comboBoxRégion.SelectedValue = (read["RégionID"].ToString());
                    if (Int32.Parse(comboBoxRégion.SelectedValue.ToString()) == 1)
                    {
                        comboBoxCommune.SelectedValue = (read["CommuneID"].ToString()); ;
                        comboBoxArrondis.SelectedValue = (read["ArrondisID"].ToString()); ;
                        label13.Text = "Commune";
                        comboBoxLoc.Visible = false;

                        label14.Visible = true;
                       
                        comboBoxArrondis.Visible = true;
                        comboBoxCommune.Visible = true;
                    }
                    else
                    {
                        this.Filllocalite(Int32.Parse(comboBoxRégion.SelectedValue.ToString()));
                        comboBoxLoc.SelectedValue = (read["localitéID"].ToString());
                        comboBoxLoc.Visible = true;
                        label13.Text = "Localité";
                        comboBoxCommune.Visible = false;
                       
                        label14.Visible = false;
                       
                        comboBoxArrondis.Visible = false;
                    }

                }
        }
        private void mandateur_modif_Load(object sender, EventArgs e)
        {
            FillRégion();
            label26.Text = DateTime.Today.ToShortDateString().ToString();
            modificationmandateur(); 
        }
       
       
        private void FillRégion()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT RégionID, NomRégion FROM Région";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBoxRégion.ValueMember = "RégionID";
            comboBoxRégion.DisplayMember = "NomRégion";
            comboBoxRégion.DataSource = objDs.Tables[0];
        }
        private void FillArrondis(int CommuneID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT ArrondisID, NomArrondis FROM Arrondissement WHERE CommuneID =@CommuneID";
            cmd.Parameters.AddWithValue("@CommuneID", CommuneID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxArrondis.ValueMember = "ArrondisID";
                comboBoxArrondis.DisplayMember = "NomArrondis";
                comboBoxArrondis.DataSource = objDs.Tables[0];
            }
        }
        
        private void FillCommune(int RégionID)
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT CommuneID, NomCommune FROM Commune WHERE RégionID =@RégionID";
            cmd.Parameters.AddWithValue("@RégionID", RégionID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxCommune.ValueMember = "CommuneID";
                comboBoxCommune.DisplayMember = "NomCommune";
                comboBoxCommune.DataSource = objDs.Tables[0];
            }
        }
        private void Filllocalite(int _reg)
        {
            int reg = _reg;
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT LocalitéID, NomLocalité FROM Localité WHERE RégionID = " + reg;
            //cmd.Parameters.AddWithValue("@RégionID", RégionID);
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            if (objDs.Tables[0].Rows.Count > 0)
            {
                comboBoxLoc.ValueMember = "LocalitéID";
                comboBoxLoc.DisplayMember = "NomLocalité";
                comboBoxLoc.DataSource = objDs.Tables[0];
            }
        }

        private void comboBoxRégion_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (comboBoxRégion.SelectedValue.ToString() == "1")
            {
                label13.Text = "Commune";
                comboBoxLoc.Visible = false;
                //label15.Visible = true;
                label14.Visible = true;
                //   comboBoxAdresse.Visible = true;
                comboBoxArrondis.Visible = true;
                comboBoxCommune.Visible = true;
                int RégionID = Convert.ToInt32(comboBoxRégion.SelectedValue.ToString());
                FillCommune(RégionID);
                comboBoxCommune.SelectedIndex = 0;

            }
            else
            {
                comboBoxLoc.Visible = true;
                label13.Text = "Localité";
                comboBoxCommune.Visible = false;
                // label15.Visible = false;
                label14.Visible = false;
                //   comboBoxAdresse.Visible = false;
                comboBoxArrondis.Visible = false;
                int RégionID = Convert.ToInt32(comboBoxRégion.SelectedValue.ToString());
                Filllocalite(RégionID);
                comboBoxCommune.SelectedIndex = 0;
            }
        }

        private void comboBoxCommune_SelectedIndexChanged(object sender, EventArgs e)
        {


            if (comboBoxCommune.SelectedValue.ToString() != "")
            {
                int CommuneID = Convert.ToInt32(comboBoxCommune.SelectedValue.ToString());
                this.FillArrondis(CommuneID);
                comboBoxArrondis.SelectedIndex = 0;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {


            //   this.checkIfExist();
            if (textBoxNom1.Text.Trim().Length == 0 || textBoxNom2.Text.Trim().Length == 0 )
            {
                MessageBox.Show("Veuillez saisir le nom complet du mandateur");
                textBoxNom1.Focus();
            }
            else
            {

                if (textBoxAncCNI.Text.Trim().Length == 0 && textBoxNewCNI.Text.Trim().Length == 0)
                {
                    MessageBox.Show("Veuillez saisir la nouvelle CNI ou l'ancienne CNI");
                    textBoxAncCNI.Focus();
                }
                else
                {
                    if (comboBoxSexe.Text.Length == 0)
                    {
                        MessageBox.Show("Veuillez choisir le sexe de la personne");
                        comboBoxSexe.Focus();
                    }
                    else
                    {

                        if (dateTimePicker1.Checked || dateTimePicker2.Checked)
                        {
                            DateTime Date1 = DateTime.Parse(dateTimePicker1.Text);
                            DateTime Date2 = DateTime.Parse(dateTimePicker2.Text);
                            DateTime Date3 = DateTime.Parse(dateTimePickerDOB.Text);
                            int result = DateTime.Compare(Date1, Date2);

                            if (dateTimePicker1.Checked && dateTimePicker2.Checked)
                            {


                                if (result > 0 || result == 0)
                                {
                                    MessageBox.Show("La date de la nouvelle CNI ne peut être anterieure ou identique à celle de l'ancienne CNI");
                                    dateTimePicker1.Focus();
                                }
                                else
                                {

                                    try
                                    {
                                        if (Int32.Parse(comboBoxRégion.SelectedValue.ToString()) == 1)
                                        {
                                            string sCon = conn.connectdb();
                                            string insertCmd = "update  mandaté set RégionID=@RégionID,CommuneID=@CommuneID,ArrondisID=@ArrondisID,Nom1=@Nom1,Nom2=@Nom2,Nom3=@Nom3,NomMère1=@NomMère1,NomMère2=@NomMère2,NomMère3=@NomMère3,DOB=@DOB,POB=@POB,CodeElecteur=@CodeElecteur,AncienCNI=@AncienCNI,DateAncienCNI=@DateAncienCNI,NewCNI=@NewCNI,DateNewCNI=@DateNewCNI,AgentSaisie=@AgentSaisie,AgentCréation=@AgentCréation,AgentModif=@AgentModif,Téléphone=@Téléphone,Sexe=@Sexe,Région=@Région,Commune=@Commune,Arrondissement=@Arrondissement,DateModification=@DateModification,Profession=@Profession,Portable=@Portable,Complement=@Complement,AgentID=@AgentID where mandatéID=" + mandatéID + "";
                                            SqlConnection dbConn;
                                            dbConn = new SqlConnection(sCon);
                                            dbConn.Open();
                                            Login lg = new Login();
                                            //string str;
                                            string dat = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                            DateTime date4 = DateTime.Parse(dat);

                                            GetValue value = new GetValue();
                                            //getAdress add = new getAdress();
                                            SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                                            // Create parameters for the SqlCommand object
                                            // initialize with input-form field values
                                            //myCommand.Parameters.AddWithValue("@ElecteurID","");
                                            myCommand.Parameters.AddWithValue("@Nom1", textBoxNom1.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@Nom2", textBoxNom2.Text.ToUpper());

                                            myCommand.Parameters.AddWithValue("@Nom3", textBoxNom3.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@NomMère1", textBoxNomMère1.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@NomMère2", textBoxNomMère2.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@NomMère3", textBoxNomMère3.Text.ToUpper());
                                            if (dateTimePickerDOB.Checked)
                                            {
                                                myCommand.Parameters.AddWithValue("@DOB", Date3);
                                            }
                                            else
                                            {
                                                myCommand.Parameters.AddWithValue("@DOB", DBNull.Value);
                                            }
                                            myCommand.Parameters.AddWithValue("@POB", textBoxPOB.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@CodeElecteur", textBoxCodeElecteur.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@AncienCNI", textBoxAncCNI.Text.ToUpper());
                                            if (dateTimePicker1.Checked)
                                            {
                                                myCommand.Parameters.AddWithValue("@DateAncienCNI", Date1);
                                            }
                                            else
                                            {
                                                myCommand.Parameters.AddWithValue("@DateAncienCNI", DBNull.Value);
                                            }
                                            myCommand.Parameters.AddWithValue("@NewCNI", textBoxNewCNI.Text.ToUpper());
                                            if (dateTimePicker2.Checked)
                                            {
                                                myCommand.Parameters.AddWithValue("@DateNewCNI", Date2);
                                            }
                                            else
                                            {
                                                myCommand.Parameters.AddWithValue("@DateNewCNI", DBNull.Value);
                                            }

                                            myCommand.Parameters.AddWithValue("@AgentSaisie", value.getAgentSaisie());//recupere agent saisie
                                            myCommand.Parameters.AddWithValue("@AgentModif", value.getAgentModif());//recupere agent modif
                                            myCommand.Parameters.AddWithValue("@AgentCréation", value.getAgentCréation());//recupere agent de creation

                                            myCommand.Parameters.AddWithValue("@Téléphone", textBoxPhone.Text.ToUpper());

                                            myCommand.Parameters.AddWithValue("@AgentID", value.getAgentID());//recupere l'ID agent
                                            myCommand.Parameters.AddWithValue("@Sexe", comboBoxSexe.Text);

                                            myCommand.Parameters.AddWithValue("@Région", comboBoxRégion.Text);
                                            myCommand.Parameters.AddWithValue("@RégionID", Int32.Parse(comboBoxRégion.SelectedValue.ToString()));

                                            myCommand.Parameters.AddWithValue("@Commune", comboBoxCommune.Text);
                                            myCommand.Parameters.AddWithValue("@CommuneID", Int32.Parse(comboBoxCommune.SelectedValue.ToString()));

                                            myCommand.Parameters.AddWithValue("@Arrondissement", comboBoxArrondis.Text);
                                            myCommand.Parameters.AddWithValue("@ArrondisID", Int32.Parse(comboBoxArrondis.SelectedValue.ToString()));

                                            // myCommand.Parameters.AddWithValue("@DateCréation", dat);
                                            myCommand.Parameters.AddWithValue("@Profession", textBoxProf.Text);
                                            myCommand.Parameters.AddWithValue("@Portable", textBoxPortable.Text);
                                            myCommand.Parameters.AddWithValue("@DateModification", dat);

                                            myCommand.Parameters.AddWithValue("@Complement", textBoxAdresseCompl.Text);



                                            myCommand.ExecuteNonQuery();
                                            MessageBox.Show("Modification effectué avec succès");
                                            RecursiveClearTextBoxes(this.Controls);
                                            dateTimePicker1.Checked = false;
                                            dateTimePicker2.Checked = false;
                                            dateTimePickerDOB.Checked = false;
                                            this.Hide();
                                        }
                                        else
                                        {
                                            string sCon = conn.connectdb();
                                            string insertCmd = "update  mandaté set localitéID=@localitéID,RégionID=@RégionID,Nom1=@Nom1,Nom2=@Nom2,Nom3=@Nom3,NomMère1=@NomMère1,NomMère2=@NomMère2,NomMère3=@NomMère3,DOB=@DOB,POB=@POB,CodeElecteur=@CodeElecteur,AncienCNI=@AncienCNI,DateAncienCNI=@DateAncienCNI,NewCNI=@NewCNI,DateNewCNI=@DateNewCNI,AgentSaisie=@AgentSaisie,AgentCréation=@AgentCréation,AgentModif=@AgentModif,Téléphone=@Téléphone,Sexe=@Sexe,Région=@Région,DateModification=@DateModification,Localité=@Localité,Profession=@Profession,Portable=@Portable,Complement=@Complement,AgentID=@AgentID where mandatéID=" + mandatéID + "";
                                            SqlConnection dbConn;
                                            dbConn = new SqlConnection(sCon);
                                            dbConn.Open();
                                            Login lg = new Login();
                                            //string str;
                                            string dat = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                            DateTime date4 = DateTime.Parse(dat);

                                            GetValue value = new GetValue();
                                            //getAdress add = new getAdress();
                                            SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                                            // Create parameters for the SqlCommand object
                                            // initialize with input-form field values
                                            //myCommand.Parameters.AddWithValue("@ElecteurID","");
                                            myCommand.Parameters.AddWithValue("@Nom1", textBoxNom1.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@Nom2", textBoxNom2.Text.ToUpper());

                                            myCommand.Parameters.AddWithValue("@Nom3", textBoxNom3.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@NomMère1", textBoxNomMère1.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@NomMère2", textBoxNomMère2.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@NomMère3", textBoxNomMère3.Text.ToUpper());
                                            if (dateTimePickerDOB.Checked)
                                            {
                                                myCommand.Parameters.AddWithValue("@DOB", Date3);
                                            }
                                            else
                                            {
                                                myCommand.Parameters.AddWithValue("@DOB", DBNull.Value);
                                            }
                                            myCommand.Parameters.AddWithValue("@POB", textBoxPOB.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@CodeElecteur", textBoxCodeElecteur.Text.ToUpper());
                                            myCommand.Parameters.AddWithValue("@AncienCNI", textBoxAncCNI.Text.ToUpper());
                                            if (dateTimePicker1.Checked)
                                            {
                                                myCommand.Parameters.AddWithValue("@DateAncienCNI", Date1);
                                            }
                                            else
                                            {
                                                myCommand.Parameters.AddWithValue("@DateAncienCNI", DBNull.Value);
                                            }
                                            myCommand.Parameters.AddWithValue("@NewCNI", textBoxNewCNI.Text.ToUpper());
                                            if (dateTimePicker2.Checked)
                                            {
                                                myCommand.Parameters.AddWithValue("@DateNewCNI", Date2);
                                            }
                                            else
                                            {
                                                myCommand.Parameters.AddWithValue("@DateNewCNI", DBNull.Value);
                                            }

                                            myCommand.Parameters.AddWithValue("@AgentSaisie", value.getAgentSaisie());//recupere agent saisie
                                            myCommand.Parameters.AddWithValue("@AgentModif", value.getAgentModif());//recupere agent modif
                                            myCommand.Parameters.AddWithValue("@AgentCréation", value.getAgentCréation());//recupere agent de creation

                                            myCommand.Parameters.AddWithValue("@Téléphone", textBoxPhone.Text.ToUpper());

                                            myCommand.Parameters.AddWithValue("@AgentID", value.getAgentID());//recupere l'ID agent
                                            myCommand.Parameters.AddWithValue("@Sexe", comboBoxSexe.Text);
                                           
                                                myCommand.Parameters.AddWithValue("@Région", comboBoxRégion.Text);
                                                myCommand.Parameters.AddWithValue("@RégionID", Int32.Parse(comboBoxRégion.SelectedValue.ToString()));
                                            
                                           
                                            // myCommand.Parameters.AddWithValue("@DateCréation", dat);
                                            myCommand.Parameters.AddWithValue("@Profession", textBoxProf.Text);
                                            myCommand.Parameters.AddWithValue("@Portable", textBoxPortable.Text);
                                            myCommand.Parameters.AddWithValue("@DateModification", dat);
                                           
                                                myCommand.Parameters.AddWithValue("@Localité", comboBoxLoc.Text);
                                                myCommand.Parameters.AddWithValue("@localitéID", Int32.Parse(comboBoxLoc.SelectedValue.ToString()));
                                            
                                            myCommand.Parameters.AddWithValue("@Complement", textBoxAdresseCompl.Text);



                                            myCommand.ExecuteNonQuery();
                                            MessageBox.Show("Modification effectué avec succès");
                                            RecursiveClearTextBoxes(this.Controls);
                                            dateTimePicker1.Checked = false;
                                            dateTimePicker2.Checked = false;
                                            dateTimePickerDOB.Checked = false;
                                            this.Hide();
                                        }

                                    }
                                    catch (SqlException ex)
                                    {
                                        MessageBox.Show("erreur  " + ex.Message);
                                    }
                                }


                            }
                            else
                            {
                                try
                                {
                                    if (Int32.Parse(comboBoxRégion.SelectedValue.ToString()) == 1)
                                    {
                                        string sCon = conn.connectdb();
                                        string insertCmd = "update  mandaté set RégionID=@RégionID,CommuneID=@CommuneID,ArrondisID=@ArrondisID,Nom1=@Nom1,Nom2=@Nom2,Nom3=@Nom3,NomMère1=@NomMère1,NomMère2=@NomMère2,NomMère3=@NomMère3,DOB=@DOB,POB=@POB,CodeElecteur=@CodeElecteur,AncienCNI=@AncienCNI,DateAncienCNI=@DateAncienCNI,NewCNI=@NewCNI,DateNewCNI=@DateNewCNI,AgentSaisie=@AgentSaisie,AgentCréation=@AgentCréation,AgentModif=@AgentModif,Téléphone=@Téléphone,Sexe=@Sexe,Région=@Région,Commune=@Commune,Arrondissement=@Arrondissement,DateModification=@DateModification,Profession=@Profession,Portable=@Portable,Complement=@Complement,AgentID=@AgentID where mandatéID=" + mandatéID + "";
                                        SqlConnection dbConn;
                                        dbConn = new SqlConnection(sCon);
                                        dbConn.Open();
                                        Login lg = new Login();
                                        //string str;
                                        string dat = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                        DateTime date4 = DateTime.Parse(dat);

                                        GetValue value = new GetValue();
                                        //getAdress add = new getAdress();
                                        SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                                        // Create parameters for the SqlCommand object
                                        // initialize with input-form field values
                                        //myCommand.Parameters.AddWithValue("@ElecteurID","");
                                        myCommand.Parameters.AddWithValue("@Nom1", textBoxNom1.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@Nom2", textBoxNom2.Text.ToUpper());

                                        myCommand.Parameters.AddWithValue("@Nom3", textBoxNom3.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@NomMère1", textBoxNomMère1.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@NomMère2", textBoxNomMère2.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@NomMère3", textBoxNomMère3.Text.ToUpper());
                                        if (dateTimePickerDOB.Checked)
                                        {
                                            myCommand.Parameters.AddWithValue("@DOB", Date3);
                                        }
                                        else
                                        {
                                            myCommand.Parameters.AddWithValue("@DOB", DBNull.Value);
                                        }
                                        myCommand.Parameters.AddWithValue("@POB", textBoxPOB.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@CodeElecteur", textBoxCodeElecteur.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@AncienCNI", textBoxAncCNI.Text.ToUpper());
                                        if (dateTimePicker1.Checked)
                                        {
                                            myCommand.Parameters.AddWithValue("@DateAncienCNI", Date1);
                                        }
                                        else
                                        {
                                            myCommand.Parameters.AddWithValue("@DateAncienCNI", DBNull.Value);
                                        }
                                        myCommand.Parameters.AddWithValue("@NewCNI", textBoxNewCNI.Text.ToUpper());
                                        if (dateTimePicker2.Checked)
                                        {
                                            myCommand.Parameters.AddWithValue("@DateNewCNI", Date2);
                                        }
                                        else
                                        {
                                            myCommand.Parameters.AddWithValue("@DateNewCNI", DBNull.Value);
                                        }

                                        myCommand.Parameters.AddWithValue("@AgentSaisie", value.getAgentSaisie());//recupere agent saisie
                                        myCommand.Parameters.AddWithValue("@AgentModif", value.getAgentModif());//recupere agent modif
                                        myCommand.Parameters.AddWithValue("@AgentCréation", value.getAgentCréation());//recupere agent de creation

                                        myCommand.Parameters.AddWithValue("@Téléphone", textBoxPhone.Text.ToUpper());

                                        myCommand.Parameters.AddWithValue("@AgentID", value.getAgentID());//recupere l'ID agent
                                        myCommand.Parameters.AddWithValue("@Sexe", comboBoxSexe.Text);

                                        myCommand.Parameters.AddWithValue("@Région", comboBoxRégion.Text);
                                        myCommand.Parameters.AddWithValue("@RégionID", Int32.Parse(comboBoxRégion.SelectedValue.ToString()));

                                        myCommand.Parameters.AddWithValue("@Commune", comboBoxCommune.Text);
                                        myCommand.Parameters.AddWithValue("@CommuneID", Int32.Parse(comboBoxCommune.SelectedValue.ToString()));

                                        myCommand.Parameters.AddWithValue("@Arrondissement", comboBoxArrondis.Text);
                                        myCommand.Parameters.AddWithValue("@ArrondisID", Int32.Parse(comboBoxArrondis.SelectedValue.ToString()));

                                        // myCommand.Parameters.AddWithValue("@DateCréation", dat);
                                        myCommand.Parameters.AddWithValue("@Profession", textBoxProf.Text);
                                        myCommand.Parameters.AddWithValue("@Portable", textBoxPortable.Text);
                                        myCommand.Parameters.AddWithValue("@DateModification", dat);

                                        myCommand.Parameters.AddWithValue("@Complement", textBoxAdresseCompl.Text);



                                        myCommand.ExecuteNonQuery();
                                        MessageBox.Show("Modification effectué avec succès");
                                        RecursiveClearTextBoxes(this.Controls);
                                        dateTimePicker1.Checked = false;
                                        dateTimePicker2.Checked = false;
                                        dateTimePickerDOB.Checked = false;
                                        this.Hide();
                                    }
                                    else
                                    {
                                        string sCon = conn.connectdb();
                                        string insertCmd = "update  mandaté set localitéID=@localitéID,RégionID=@RégionID,Nom1=@Nom1,Nom2=@Nom2,Nom3=@Nom3,NomMère1=@NomMère1,NomMère2=@NomMère2,NomMère3=@NomMère3,DOB=@DOB,POB=@POB,CodeElecteur=@CodeElecteur,AncienCNI=@AncienCNI,DateAncienCNI=@DateAncienCNI,NewCNI=@NewCNI,DateNewCNI=@DateNewCNI,AgentSaisie=@AgentSaisie,AgentCréation=@AgentCréation,AgentModif=@AgentModif,Téléphone=@Téléphone,Sexe=@Sexe,Région=@Région,DateModification=@DateModification,Localité=@Localité,Profession=@Profession,Portable=@Portable,Complement=@Complement,AgentID=@AgentID where mandatéID=" + mandatéID + "";
                                        SqlConnection dbConn;
                                        dbConn = new SqlConnection(sCon);
                                        dbConn.Open();
                                        Login lg = new Login();
                                        //string str;
                                        string dat = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                        DateTime date4 = DateTime.Parse(dat);

                                        GetValue value = new GetValue();
                                        //getAdress add = new getAdress();
                                        SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                                        // Create parameters for the SqlCommand object
                                        // initialize with input-form field values
                                        //myCommand.Parameters.AddWithValue("@ElecteurID","");
                                        myCommand.Parameters.AddWithValue("@Nom1", textBoxNom1.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@Nom2", textBoxNom2.Text.ToUpper());

                                        myCommand.Parameters.AddWithValue("@Nom3", textBoxNom3.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@NomMère1", textBoxNomMère1.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@NomMère2", textBoxNomMère2.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@NomMère3", textBoxNomMère3.Text.ToUpper());
                                        if (dateTimePickerDOB.Checked)
                                        {
                                            myCommand.Parameters.AddWithValue("@DOB", Date3);
                                        }
                                        else
                                        {
                                            myCommand.Parameters.AddWithValue("@DOB", DBNull.Value);
                                        }
                                        myCommand.Parameters.AddWithValue("@POB", textBoxPOB.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@CodeElecteur", textBoxCodeElecteur.Text.ToUpper());
                                        myCommand.Parameters.AddWithValue("@AncienCNI", textBoxAncCNI.Text.ToUpper());
                                        if (dateTimePicker1.Checked)
                                        {
                                            myCommand.Parameters.AddWithValue("@DateAncienCNI", Date1);
                                        }
                                        else
                                        {
                                            myCommand.Parameters.AddWithValue("@DateAncienCNI", DBNull.Value);
                                        }
                                        myCommand.Parameters.AddWithValue("@NewCNI", textBoxNewCNI.Text.ToUpper());
                                        if (dateTimePicker2.Checked)
                                        {
                                            myCommand.Parameters.AddWithValue("@DateNewCNI", Date2);
                                        }
                                        else
                                        {
                                            myCommand.Parameters.AddWithValue("@DateNewCNI", DBNull.Value);
                                        }

                                        myCommand.Parameters.AddWithValue("@AgentSaisie", value.getAgentSaisie());//recupere agent saisie
                                        myCommand.Parameters.AddWithValue("@AgentModif", value.getAgentModif());//recupere agent modif
                                        myCommand.Parameters.AddWithValue("@AgentCréation", value.getAgentCréation());//recupere agent de creation

                                        myCommand.Parameters.AddWithValue("@Téléphone", textBoxPhone.Text.ToUpper());

                                        myCommand.Parameters.AddWithValue("@AgentID", value.getAgentID());//recupere l'ID agent
                                        myCommand.Parameters.AddWithValue("@Sexe", comboBoxSexe.Text);

                                        myCommand.Parameters.AddWithValue("@Région", comboBoxRégion.Text);
                                        myCommand.Parameters.AddWithValue("@RégionID", Int32.Parse(comboBoxRégion.SelectedValue.ToString()));


                                        // myCommand.Parameters.AddWithValue("@DateCréation", dat);
                                        myCommand.Parameters.AddWithValue("@Profession", textBoxProf.Text);
                                        myCommand.Parameters.AddWithValue("@Portable", textBoxPortable.Text);
                                        myCommand.Parameters.AddWithValue("@DateModification", dat);

                                        myCommand.Parameters.AddWithValue("@Localité", comboBoxLoc.Text);
                                        myCommand.Parameters.AddWithValue("@localitéID", Int32.Parse(comboBoxLoc.SelectedValue.ToString()));

                                        myCommand.Parameters.AddWithValue("@Complement", textBoxAdresseCompl.Text);



                                        myCommand.ExecuteNonQuery();
                                        MessageBox.Show("Modification effectué avec succès");
                                        RecursiveClearTextBoxes(this.Controls);
                                        dateTimePicker1.Checked = false;
                                        dateTimePicker2.Checked = false;
                                        dateTimePickerDOB.Checked = false;
                                        this.Hide();
                                    }
                                }
                                catch (SqlException ex)
                                {
                                    MessageBox.Show("erreur  " + ex.Message);
                                }
                            }
                        }
                    }
                }
            }

        }
        private void RecursiveClearTextBoxes(Control.ControlCollection cc)
        {
            foreach (Control ctrl in cc)
            {
                TextBox tb = ctrl as TextBox;
                if (tb != null)
                    tb.Clear();
                else
                    RecursiveClearTextBoxes(ctrl.Controls);
            }
        }
        private void textBoxPhone_KeyPress(object sender, KeyPressEventArgs e)
        {

            int isNum = 0;
            if (e.KeyChar == ',' || e.KeyChar == (char)Keys.Back)
                e.Handled = false;
            else if (!int.TryParse(e.KeyChar.ToString(), out isNum))
                e.Handled = true;
        }

        private void textBoxPortable_KeyPress(object sender, KeyPressEventArgs e)
        {

            int isNum = 0;
            if (e.KeyChar == ',' || e.KeyChar == (char)Keys.Back)
                e.Handled = false;
            else if (!int.TryParse(e.KeyChar.ToString(), out isNum))
                e.Handled = true;
        }

        private void textBoxNom1_KeyPress(object sender, KeyPressEventArgs e)
        {

            int isNum = 0;
            if (e.KeyChar == ',' || e.KeyChar == (char)Keys.Back)
                e.Handled = false;
            else if (int.TryParse(e.KeyChar.ToString(), out isNum))
                e.Handled = true;
        }

        private void textBoxNom2_KeyPress(object sender, KeyPressEventArgs e)
        {

            int isNum = 0;
            if (e.KeyChar == ',' || e.KeyChar == (char)Keys.Back)
                e.Handled = false;
            else if (int.TryParse(e.KeyChar.ToString(), out isNum))
                e.Handled = true;
        }

        private void textBoxNom3_KeyPress(object sender, KeyPressEventArgs e)
        {

            int isNum = 0;
            if (e.KeyChar == ',' || e.KeyChar == (char)Keys.Back)
                e.Handled = false;
            else if (int.TryParse(e.KeyChar.ToString(), out isNum))
                e.Handled = true;
        }

        private void textBoxAncCNI_KeyPress(object sender, KeyPressEventArgs e)
        {

            int isNum = 0;
            if (e.KeyChar == ',' || e.KeyChar == (char)Keys.Back)
                e.Handled = false;
            else if (!int.TryParse(e.KeyChar.ToString(), out isNum))
                e.Handled = true;
        }

        private void textBoxNewCNI_KeyPress(object sender, KeyPressEventArgs e)
        {

            int isNum = 0;
            if (e.KeyChar == ',' || e.KeyChar == (char)Keys.Back)
                e.Handled = false;
            else if (!int.TryParse(e.KeyChar.ToString(), out isNum))
                e.Handled = true;
        }
        private void textBoxCodeElecteur_KeyPress(object sender, KeyPressEventArgs e)
        {
            int isNum = 0;
            if (e.KeyChar == ',' || e.KeyChar == (char)Keys.Back)
                e.Handled = false;
            else if (!int.TryParse(e.KeyChar.ToString(), out isNum))
                e.Handled = true;
        }


    }
}
