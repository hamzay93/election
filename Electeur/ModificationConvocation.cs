﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electeur
{
    public partial class ModificationConvocation : Form
    {
        string clause;

        Connection conn = new Connection();
        GetValue get = new GetValue();
        public ModificationConvocation()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Trim().Length == 0)
            {
                MessageBox.Show("veuillez saisir l'inttitulé");
                textBox1.Focus();
            }
            else
            {
                if (comboBox1.Text.Length ==0)
                {
                    MessageBox.Show("veuillez choisir un election");

                }
                else
                {
                    //Connection cn = new Connection();
                    string insertCmd = "update Convocation set ObjetConvocation=@Objet,ElectionID=@ElectionID,Date_Heure=@Date_Heure,UserModification=@UserModification,DateModification=@DateModification where ConvocationID=" + clause3 + "";
                    SqlConnection dbConn;
                    dbConn = new SqlConnection(conn.connectdb());
                    dbConn.Open();
                    Login lg = new Login();
                    string dat = DateTime.Now.ToString("yyyy-MM-dd");
                    DateTime date4 = DateTime.Parse(dateTimePicker1.Value.ToString("yyyy-MM-dd HH:mm:ss"));
                    GetValue value = new GetValue();
                    //getAdress add = new getAdress();
                    SqlCommand myCommand = new SqlCommand(insertCmd, dbConn);
                   
                        int electionID = Convert.ToInt32(comboBox1.SelectedValue.ToString());
                        myCommand.Parameters.AddWithValue("@ElectionID", electionID);
                  
                    myCommand.Parameters.AddWithValue("@Objet", textBox1.Text);

                    myCommand.Parameters.AddWithValue("@Date_Heure", date4);
                   
                    myCommand.Parameters.AddWithValue("@UserModification", value.getAgentCréation());
                    myCommand.Parameters.AddWithValue("@DateModification", dat);
                    myCommand.ExecuteNonQuery();

                    MessageBox.Show("Modification avec succès");
                    textBox1.Clear();
                }

            }
        }
        public void IsEmptyfield()
        {
            clause = null;
            foreach (Control X in this.panel9.Controls)
            {

                if (X is TextBox || X is ComboBox)
                {

                    if (X.Text.Trim().Length != 0)
                    {
                        if (string.IsNullOrEmpty(clause))
                        {
                            clause = clause + X.Tag + " Like '%" + X.Text.Trim() + "%'";
                            //return clause;
                        }
                        else
                        {
                            clause = clause + " And " + X.Tag + " Like '%" + X.Text.Trim() + "%'";
                            //return clause;
                        }
                    }


                }
                else
                {
                    if (X is DateTimePicker)
                    {
                        if (X.Name == "dateTimePicker5")
                        {
                            if (string.IsNullOrEmpty(clause))
                            {
                                if (dateTimePicker5.Checked)
                                {
                                    string dat = dateTimePicker5.Value.ToString("yyyy-MM-dd HH:mm:ss");
                                    DateTime date4 = DateTime.Parse(dat);
                                    clause = clause + X.Tag + " Like '%" + dat + "%'";
                                }

                            }
                            else
                            {
                                if (dateTimePicker5.Checked)
                                {
                                    string dat = dateTimePicker5.Value.ToString("yyyy-MM-dd HH:mm:ss");
                                    DateTime date4 = DateTime.Parse(dat);
                                    clause = clause + " AND " + X.Tag + " Like '%" + dat + "%'";
                                }

                            }
                        }
                    }
                }


            }

        }
        private void Fillelection()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT ElectionID,ObjetElection FROM Election";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBox12.ValueMember = "ElectionID";
            comboBox12.DisplayMember = "ObjetElection";
            comboBox12.DataSource = objDs.Tables[0];
        }
        private void Fillelection2()
        {
            string sCon = conn.connectdb();
            SqlConnection con = new SqlConnection(sCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT ElectionID,ObjetElection FROM Election";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            comboBox1.ValueMember = "ElectionID";
            comboBox1.DisplayMember = "ObjetElection";
            comboBox1.DataSource = objDs.Tables[0];
        }
        private void button10_Click(object sender, EventArgs e)
        {
             
            this.IsEmptyfield();
            if (String.IsNullOrEmpty(clause))
            {
                MessageBox.Show("veuillez remplir un champ au minimum");
                this.Focus();

            }
            else
            {
                string sCon = conn.connectdb();
                SqlConnection con = new SqlConnection(sCon);
                string str = "SET CONCAT_NULL_YIELDS_NULL OFF; SELECT ConvocationID, ObjetConvocation AS 'Intitulé du convocation',ObjetElection AS 'Election',Date_Heure,Annee as 'Année Election' FROM View_convocation_election where " + clause + "";
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(str, con);
                SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sqlAdapter);

                DataTable table = new DataTable();
                table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                con.Open();
                sqlAdapter.Fill(table);
                con.Close();
                dataGridView1.DataSource = table;
                if (dataGridView1.Rows.Count == 0)
                {

                    panel1.Visible = false;
                    label1.Visible = false;
                    MessageBox.Show("Aucun Convocation retrouvé pour ces critères");

                }
                else
                {
                    panel1.Visible = true;
                    label1.Visible = true;
                }

                this.dataGridView1.Columns["ConvocationID"].Visible = false;


              
                clause = "";
            }
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            this.filldetails();
        }
        string clause3;
        private void filldetails()
        {

            this.selection();
            if (String.IsNullOrEmpty(clause3))
            {
                panel1.Visible = false;
                label1.Visible = false;

            }
            else
            {
                panel1.Visible = true;
                label1.Visible = true;
                string sCon = conn.connectdb();
                SqlConnection con = new SqlConnection(sCon);
                string str = "SELECT * FROM View_convocation_election where ConvocationID= " + clause3;
                SqlCommand cmd = new SqlCommand(str, con);
                con.Open();

                using (SqlDataReader read = cmd.ExecuteReader())

                    while (read.Read())
                    {
                        textBox1.Text = (read["ObjetConvocation"].ToString());

                        int colIndex3 = (read.GetOrdinal("Date_Heure"));
                        if (!read.IsDBNull(colIndex3))
                        {
                            dateTimePicker1.Value = (read.GetDateTime(colIndex3));
                        }
                        comboBox1.SelectedValue = (read["ElectionID"].ToString());



                    }
            }

        }
        private void selection()
        {
            int counter3;
            clause3 = null;

            // Iterate through all the rows and sum up the appropriate columns. 
            for (counter3 = 0; counter3 < (dataGridView1.Rows.Count);
                counter3++)
            {
                //dataGridView1.Rows.
                if (dataGridView1.Rows[counter3].Selected)
                {
                    if (dataGridView1.Rows[counter3].Cells["ConvocationID"].Value
                    != null)
                    {
                        if (dataGridView1.Rows[counter3].
                            Cells["ConvocationID"].Value.ToString().Length != 0)
                        {
                            if (string.IsNullOrEmpty(clause3))
                            {
                                clause3 = clause3 + int.Parse(dataGridView1.Rows[counter3].
                                    Cells["ConvocationID"].Value.ToString());
                            }
                            else
                            {
                                clause3 = clause3 + "," + int.Parse(dataGridView1.Rows[counter3].
                                Cells["ConvocationID"].Value.ToString());

                            }

                        }
                    }


                }

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ClearClass1 clr = new ClearClass1();
            clr.RecursiveClearTextBoxes(this.panel9.Controls);
        }

        private void ModificationConvocation_Load(object sender, EventArgs e)
        {
            Fillelection2();
            this.Fillelection();

        }
    }
}
